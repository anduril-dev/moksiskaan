# README #

Moksiskaan is a generic database and a toolkit that can be used to integrate information about the connections between genes, proteins, pathways, drugs, and other biological entities. The database is used to combine various existing databases to find biological relationships between the genes of interest and to predict their interactions.

For more information, visit http://csbi.ltdk.helsinki.fi/moksiskaan/

Component docs: http://www.anduril.org/anduril/bundles/all/categories/Moksiskaan/index.html

Database docs:

* Userguide (HTML) http://csbi.ltdk.helsinki.fi/moksiskaan/userguide/userguide.html
* Userguide (PDF) http://csbi.ltdk.helsinki.fi/moksiskaan/userguide/userguide.pdf
* Database schema http://csbi.ltdk.helsinki.fi/moksiskaan/hibernate/tables/index.html
* ER diagram http://csbi.ltdk.helsinki.fi/moksiskaan/er/moksiskaan.png
* SQL definition http://csbi.ltdk.helsinki.fi/moksiskaan/schema-export.txt
* Physical model http://csbi.ltdk.helsinki.fi/moksiskaan/er/moksiskaan.html
* Link annotations in use http://csbi.ltdk.helsinki.fi/moksiskaan/LinkAnnotationsUsed.txt


API docs:

* Java API documentation for the engine http://csbi.ltdk.helsinki.fi/moksiskaan/javadoc/index.html
* PMD report (source code inspection for Java) http://csbi.ltdk.helsinki.fi/moksiskaan/pmd_report.html
* Codetools report (source code inspection for R components) http://csbi.ltdk.helsinki.fi/moksiskaan/RComponentTest.txt
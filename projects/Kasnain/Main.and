// Kasnain - Cancer data illustrations in Moksiskaan
// author: Marko Laakso

// --------------
// INITIALIZATION
// --------------

metadata = std.metadata()

moksiskaanInit = MoksiskaanInit()
include moksiskaanPipelines+"/Studies.and"

authors = "Marko Laakso,Riku Louhimo,Sampsa Hautaniemi"

studiesU = std.concat(sep=",",
   study.TCGA.Ovarian_cghA,
   study.TCGA.Glioma_cghA,
   study.TCGA.Breast_cghA,
   study.TCGA.Colon_cghA,
   study.TCGA.Kidney_cghA,
   study.TCGA.Breast_ge,
   study.TCGA.Colon_ge,
   study.TCGA.Glioma_ge,
   study.TCGA.Ovarian_ge,
   study.TCGA.Kidney_ge,
   study.Tumorscape.BC_a,
   study.Tumorscape.CRC_a,
   study.Tumorscape.Glioma_a,
   study.Tumorscape.HCC_a,
   study.Tumorscape.Melanoma_a,
   study.Tumorscape.NSCLC_a,
   study.Tumorscape.Ovarian_a,
   study.Tumorscape.Prostate_a,
   study.Tumorscape.RCC_a,
   study.Tumorscape.SCLC_a,
   study.CG_Census.act,
   study.file.AmpOver,
   study.file.BC2brain
)
studiesN = std.concat(sep=",",
   studyList.COSMIC,
   //study.SNPs3D.BC,
   //study.SNPs3D.CRC,
   //study.SNPs3D.Glioma,
   //study.SNPs3D.LungC,
   //study.SNPs3D.Metastasis,
   //study.SNPs3D.Prostate,
   //study.SNPs3D.ThyroidC,
   study.TCGA.Breast_methyl,
   study.TCGA.Colon_methyl,
   study.TCGA.Kidney_methyl,
   study.TCGA.Ovarian_methyl
)
studiesD = std.concat(sep=",",
   study.TCGA.Ovarian_cghD,
   study.TCGA.Glioma_cghD,
   study.TCGA.Breast_cghD,
   study.TCGA.Colon_cghD,
   study.TCGA.Kidney_cghD,
   study.TCGA.Breast_ge,
   study.TCGA.Colon_ge,
   study.TCGA.Glioma_ge,
   study.TCGA.Ovarian_ge,
   study.TCGA.Kidney_ge,
   study.Tumorscape.BC_d,
   study.Tumorscape.CRC_d,
   study.Tumorscape.Glioma_d,
   study.Tumorscape.HCC_d,
   study.Tumorscape.Melanoma_d,
   study.Tumorscape.NSCLC_d,
   study.Tumorscape.Ovarian_d,
   study.Tumorscape.Prostate_d,
   study.Tumorscape.RCC_d,
   study.Tumorscape.SCLC_d,
   study.CG_Census.inact
)
studiesS = std.concat(sep=",",
   study.TCGA.Breast_geSurv,
   study.TCGA.Breast_methylSurv,
   study.TCGA.Breast_cghSurv,
   study.TCGA.Colon_geSurv,
   study.TCGA.Colon_methylSurv,
   //study.TCGA.Colon_cghSurv,
   study.TCGA.Glioma_geSurv,
   study.TCGA.Glioma_methylSurv,
   study.TCGA.Glioma_cghSurv,
   study.TCGA.Kidney_geSurv,
   study.TCGA.Kidney_methylSurv,
   study.TCGA.Kidney_cghSurv,
   study.TCGA.Ovarian_geSurv,
   study.TCGA.Ovarian_methylSurv,
   study.TCGA.Ovarian_cghSurv
)

figFolders    = record()
figFoldersGO  = record()
figFoldersSim = record()
figDescs      = record()

// ----------
// INPUT DATA
// ----------

/** AndurilScript source code for this analysis workflow */
sourceCode = INPUT(path=metadata.file)

/** Database configurations for Ensembl~\cite{Flicek2008} database */
ensembl = INPUT(path=moksiskaanPipelines+"/annotationdb.properties")

/** Moksiskaan specific a priori probabilities for gene ontology terms */
enrichmentTable = INPUT(path=moksiskaanInitOut+"/GOEnrichment_"+Organism_Homo_sapiens+".csv")

/** Gene Ontology */
go = INPUT(path=moksiskaanInitOut+"/goDB-in.obo")

/** Gene Ontology annotations of all genes */
geneGO = INPUT(path=moksiskaanInitOut+"/geneGO_"+Organism_Homo_sapiens+".csv")

/** Moksiskaan related references */
bibtexMoksiskaan = INPUT(path=moksiskaanBundle+"/components/report-BibTeX/moksiskaan.bib")

/** Visualization configuration for the gene interactions */
linkStyles = INPUT(path=moksiskaanBundle+"/functions/CandidateReport/LinkTypeProperties.csv")

/** Regulatory functions associated to the link types */
linkFunctions = INPUT(path=moksiskaanBundle+"/functions/CandidateReport/LinkTypeFunctions.csv")

/** Cascading Style Sheet for the Kasnain web site. */
wwwStyle = INPUT(path="wwwStyle.css")

// ---------
// FUNCTIONS
// ---------

function PrepareFigure(IDList          genes,
                       ActivityTable   status,
                       optional IDList scope,
                       string          linkTypes,
                       string          eTypes,
                       string          expand,
                       int             gap,
                       string          description,
                       string          statusFilter = "",
                       string          xrefType     = XrefType_Ensembl_gene,
                       string          layout       = "spring2") ->
                      (Latex           report) {
  metadata = std.metadata()
  prePathway = CandidatePathway(hits           = genes,
                                linkStyles     = linkStyles,
                                within         = scope,
                                maxGap         = gap,
                                organism       = Organism_Homo_sapiens,
                                xrefType       = xrefType,
                                gapProperties  = "fillcolor=#AAAAAA,fontsize=8,isHit=false",
                                hitProperties  = "fillcolor=#FFFFFF,isHit=true",
                                linkTypes      = linkTypes,
                                expand         = expand,
                                bioentityTypes = eTypes)
  pathway = ExpressionGraph(graph        = prePathway.graph,
                            status       = status,
                            statusFilter = statusFilter,
                            linkTypes    = linkFunctions)
  nodeJoin = VertexJoin(graph    = pathway.graph,
                        idPrefix = "group")
  pathwayPlot = GraphVisualizer(graph          = nodeJoin.graph,
                                layout         = layout,
                                titleAttribute = "GeneName",
                                reportHeight   = 20,
                                reportWidth    = 23,
                                minSize        = 1)
  figDescs[metadata.instanceName+"-pathwayPlot-graph.pdf"] = description
  return pathwayPlot.figure
}

// ---------------
// EXECUTION LOGIC
// ---------------

limitU = "=1/,"
limitD = "=/1,"

topHitsAnnot = record(
 method       = 'mean',
 scoreType    = 'nrank',
 testScores   = 10,
 testEntities = 1000,
 maxScoreP    = 0.01,
 maxEntityP   = 0.50,
 roundD       = 6,
 organism     = Organism_Homo_sapiens
)

topsU = TopHits(useStudies = studiesU,
                moksiskaan = moksiskaanInit.connection,
                limits     = study.TCGA.Breast_ge  +limitU+
                             study.TCGA.Colon_ge   +limitU+
                             study.TCGA.Glioma_ge  +limitU+
                             study.TCGA.Kidney_ge  +limitU+
                             study.TCGA.Ovarian_ge +limitU,
                @par       = topHitsAnnot)
topsN = TopHits(useStudies = studiesN,
                moksiskaan = moksiskaanInit.connection,
                @par       = topHitsAnnot)
topsD = TopHits(useStudies = studiesD,
                moksiskaan = moksiskaanInit.connection,
                limits     = study.TCGA.Breast_ge  +limitD+
                             study.TCGA.Colon_ge   +limitD+
                             study.TCGA.Glioma_ge  +limitD+
                             study.TCGA.Kidney_ge  +limitD+
                             study.TCGA.Ovarian_ge +limitD,
                @par       = topHitsAnnot)
topsS = TopHits(useStudies = studiesS,
                moksiskaan = moksiskaanInit.connection,
                @par       = topHitsAnnot)

allFenn = FennPathway(force green  = topsU.scores,
                      force red    = topsN.scores,
                      force blue   = topsD.scores,
                      /* No scores
                      colGreen     = "scores",
                      colRed       = "scores",
                      colBlue      = "scores",
                      */
                      lG           = "Activated",
                      lR           = "Important",
                      lB           = "Inactivated",
                      maxGap       = 0,
                      relativeVenn = true,
                      linkTypes    = std.concat(sep=",",
                                     LinkType_protein_activation,
                                     LinkType_protein_inhibition,
                                     LinkType_protein_state_change,
                                     //LinkType_protein_binding,
                                     //LinkType_protein_dissociation,
                                     LinkType_gene_expression,
                                     LinkType_gene_repression,
                                     LinkType_phosphorylation,
                                     LinkType_dephosphorylation,
                                     LinkType_glycosylation,
                                     LinkType_ubiquitination,
                                     LinkType_deubiquitination,
                                     LinkType_methylation,
                                     LinkType_demethylation,
                                     LinkType_acetylation,
                                     LinkType_deacetylation,
                                     LinkType_sumoylation,
                                     LinkType_desumoylation),
                      title        = "Cancer related genes",
                      organism     = Organism_Homo_sapiens)

lstU = CSV2IDList(table1    = allFenn.sets @require,
                  regexp1   = "ID=(NOT_set1_AND_NOT_set3_AND_set2|set1_AND_NOT_set3_AND_set2)",
                  isList    = true,
                  columnIn  = "Members",
                  columnOut = ".GeneId")
lstD = CSV2IDList(table1    = allFenn.sets @require,
                  regexp1   = "ID=(NOT_set1_AND_set3_AND_NOT_set2|set1_AND_set3_AND_NOT_set2)",
                  isList    = true,
                  columnIn  = "Members",
                  columnOut = ".GeneId")
lstA = TableQuery(table1 = topsU.bioentities @require,
                  table2 = topsN.bioentities @require,
                  table3 = topsD.bioentities @require,
                  table4 = topsS.bioentities @require,
                  query  = """\
                           (SELECT * FROM table1) UNION
                           (SELECT * FROM table2) UNION
                           (SELECT * FROM table3) UNION
                           (SELECT * FROM table4)
                           """)
gNames = PiispanhiippaAnnotator(sourceKeys = lstA,
                                connection = moksiskaanInit.connection,
                                inputDB    = XrefType_Ensembl_gene,
                                keyColumn  = "xref"+XrefType_Ensembl_gene,
                                organism   = Organism_Homo_sapiens,
                                targetDB   = "BioentityName")
status = TableQuery(table1 = lstU @require,
                    table2 = lstD @require,
                    query  = """\
                             (SELECT ".GeneId",  '1' AS "status" FROM table1)
                             UNION
                             (SELECT ".GeneId", '-1' AS "status" FROM table2)
                             """)

// Compare cancer genes with the survival genes

kasnainSets = CSV2SetList(table1 = lstD,
                          table2 = lstU,
                          table3 = topsN.scores,
                          table4 = topsS.scores,
                          cols   = "table1=.GeneId,table2=.GeneId,table3=.GeneId,table4=.GeneId",
                          lists  = "table1=TSG,table2=oncogene,table3=relevant,table4=survival")
kasnainVenn = VennDiagram(sets         = kasnainSets,
                          sets1        = "TSG,oncogene,relevant,survival",
                          names1       = "TSG,oncogene,associated,survival",
                          types        = "squares",
                          sectionTitle = "Comparison with the survival and the cancer genes")

// Compare tumor suppressor list against the TSGene results

tsgG = PiispanhiippaAnnotator(connection = moksiskaanInit.connection,
                              keys       = study.TSGene.tsg_human,
                              inputDB    = "HitStudyId",
                              organism   = Organism_Homo_sapiens,
                              targetDB   = XrefType_Ensembl_gene)
tsgSets = CSV2SetList(table1 = lstD,
                      table2 = lstU,
                      table3 = tsgG.bioAnnotation,
                      cols   = "table1=.GeneId,table2=.GeneId,table3=xref"+XrefType_Ensembl_gene,
                      lists  = "table1=tsgKasnain,table2=oncoKasnain,table3=tsgTSGene")
tsgVenn = VennDiagram(sets         = tsgSets,
                      sets1        = "tsgKasnain,tsgTSGene,oncoKasnain",
                      names1       = "KasnainTSG,TSGene,KasnainOncogene",
                      sectionTitle = "Comparison with the TSGene database")
tsgOverlapD = CSV2IDList(tsgVenn.sets, columnIn="Members", regexp1="ID=tsgKasnain_AND_tsgTSGene_AND_NOT_oncoKasnain", isList=true)
tsgOverlapU = CSV2IDList(tsgVenn.sets, columnIn="Members", regexp1="ID=NOT_tsgKasnain_AND_tsgTSGene_AND_oncoKasnain", isList=true)
tsgOAnnotD  = KorvasieniAnnotator(sourceKeys = tsgOverlapD,
                                  connection = ensembl,
                                  inputDB    = ".GeneId",
                                  targetDB   = ".GeneName,.DNARegion,.GeneDesc,.Biotype")
tsgOAnnotU  = KorvasieniAnnotator(sourceKeys = tsgOverlapU,
                                  connection = ensembl,
                                  inputDB    = ".GeneId",
                                  targetDB   = ".GeneName,.DNARegion,.GeneDesc,.Biotype")
tsgOTableR  = XrefLinkRule(moksiskaan = moksiskaanInit.connection,
                           xrefTypes  = XrefType_Ensembl_gene,
                           columns    = "Members=Members",
                           @doc       = 'Ensembl~\cite{Flicek2008} links for the TSG overlapping genes.')
tsgOTableD  = CSV2Latex(tabledata = tsgOAnnotD,
                        refs      = tsgOTableR @require,
                        caption   = 'TSGene overlap with Kasnain TSGs.',
                        colFormat = "lllp{2.6cm}p{6cm}",
                        columns   = "Members,.GeneName,.DNARegion,.Biotype,.GeneDesc",
                        listCols  = ".Biotype",
                        rename    = "Members=ID,.GeneName=name,.DNARegion=locus,.Biotype=type,.GeneDesc=description",
                        countRows = true,
                        skipEmpty = true)
tsgOTableU  = CSV2Latex(tabledata = tsgOAnnotU,
                        refs      = tsgOTableR @require,
                        caption   = 'TSGene overlap with Kasnain oncogenes.',
                        colFormat = "lllp{2.6cm}p{6cm}",
                        columns   = "Members,.GeneName,.DNARegion,.Biotype,.GeneDesc",
                        listCols  = ".Biotype",
                        rename    = "Members=ID,.GeneName=name,.DNARegion=locus,.Biotype=type,.GeneDesc=description",
                        countRows = true,
                        skipEmpty = true)
tsgOverlapF = PrepareFigure(genes        = tsgOverlapD,
                            force status = status,
                            linkTypes    = std.concat(sep=",",
                                            LinkType_positive_regulation,
                                            LinkType_negative_regulation,
                                            LinkType_protein_activation,
                                            LinkType_protein_inhibition,
                                            LinkType_protein_state_change,
                                            LinkType_protein_binding,
                                            LinkType_protein_dissociation,
                                            LinkType_gene_expression,
                                            LinkType_gene_repression,
                                            LinkType_phosphorylation,
                                            LinkType_dephosphorylation,
                                            LinkType_glycosylation,
                                            LinkType_ubiquitination,
                                            LinkType_deubiquitination,
                                            LinkType_methylation,
                                            LinkType_demethylation,
                                            LinkType_acetylation,
                                            LinkType_deacetylation,
                                            LinkType_sumoylation,
                                            LinkType_desumoylation),
                            eTypes       = BioentityType_gene,
                            expand       = "both",
                            gap          = 1,
                            statusFilter = "NA",
                            description  = "TSGene overlap with Kasnain TSGs and their down stream targets")
figFolders["TSGOverlap"] = tsgOverlapF.report

figRegReasons = PrepareFigure(force genes  = status,
                              force status = status,
                              force scope  = lstA,
                              linkTypes    = std.concat(sep=",",
                                             LinkType_protein_activation,
                                             LinkType_protein_inhibition,
                                             LinkType_protein_state_change,
                                             LinkType_gene_expression,
                                             LinkType_gene_repression,
                                             LinkType_phosphorylation,
                                             LinkType_dephosphorylation,
                                             LinkType_glycosylation,
                                             LinkType_ubiquitination,
                                             LinkType_deubiquitination,
                                             LinkType_methylation,
                                             LinkType_demethylation,
                                             LinkType_acetylation,
                                             LinkType_deacetylation,
                                             LinkType_sumoylation,
                                             LinkType_desumoylation),
                              eTypes       = BioentityType_gene,
                              expand       = "connected",
                              gap          = 0,
                              description  = "Relationships between cancer activated and inactivated genes")
figFolders["UpAndDown"] = figRegReasons.report

figPPd = PrepareFigure(force genes  = lstD,
                       force status = status,
                       linkTypes    = std.concat(sep=",",
                                      LinkType_protein_protein_interaction,
                                      LinkType_protein_binding),
                       eTypes       = BioentityType_gene,
                       expand       = "connected",
                       gap          = 0,
                       description  = "Protein-protein interactions between the tumor suppressors")
figFolders["PPdown"] = figPPd.report

figPPu = PrepareFigure(force genes  = lstU,
                       force status = status,
                       linkTypes    = std.concat(sep=",",
                                      LinkType_protein_protein_interaction,
                                      LinkType_protein_binding),
                       eTypes       = BioentityType_gene,
                       expand       = "connected",
                       gap          = 0,
                       description  = "Protein-protein interactions between the oncogenes")
figFolders["PPup"] = figPPu.report

figPPn = PrepareFigure(force genes  = topsN.scores,
                       force status = status,
                       linkTypes    = std.concat(sep=",",
                                      LinkType_protein_protein_interaction,
                                      LinkType_protein_binding),
                       eTypes       = BioentityType_gene,
                       expand       = "connected",
                       gap          = 0,
                       description  = "Protein-protein interactions between the cancer associated genes")
figFolders["PPneutral"] = figPPn.report

figRegSurvi = PrepareFigure(force genes  = topsS.scores,
                            force status = status,
                            linkTypes    = std.concat(sep=",",
                                           LinkType_protein_activation,
                                           LinkType_protein_inhibition,
                                           LinkType_protein_state_change,
                                           LinkType_protein_binding,
                                           LinkType_protein_dissociation,
                                           LinkType_gene_expression,
                                           LinkType_gene_repression,
                                           LinkType_phosphorylation,
                                           LinkType_dephosphorylation,
                                           LinkType_glycosylation,
                                           LinkType_ubiquitination,
                                           LinkType_deubiquitination,
                                           LinkType_methylation,
                                           LinkType_demethylation,
                                           LinkType_acetylation,
                                           LinkType_deacetylation,
                                           LinkType_sumoylation,
                                           LinkType_desumoylation),
                            eTypes       = BioentityType_gene,
                            expand       = "connected",
                            gap          = 1,
                            description  = "Prognostic survival associated genes and their connectivity")
figFolders["Survival"] = figRegSurvi.report

lstProcesses = PiispanhiippaAnnotator(sourceKeys = status,
                                      connection = moksiskaanInit.connection,
                                      inputDB    = XrefType_Ensembl_gene,
                                      linkTypes  = LinkType_positive_regulation+","+
                                                   LinkType_negative_regulation,
                                      organism   = Organism_Homo_sapiens,
                                      targetDB   = "BioentityId,BioentityTypeId,BioentityName,"+
                                                   XrefType_Gene_Ontology)
processCount = TableQuery(table1 = lstProcesses.bioAnnotation @require,
                          query  = """\
                                   SELECT "BioentityId",
                                          count("sourceKey")                   AS "count",
                                          "BioentityName"                      AS "name",
                                          "xref"""+XrefType_Gene_Ontology+"""" AS "GO"
                                   FROM   table1
                                   WHERE  ("BioentityTypeId" = """+BioentityType_biological_process+""")
                                   GROUP  BY "BioentityId", "BioentityName", "xref"""+XrefType_Gene_Ontology+""""
                                   HAVING (count("sourceKey") > 5)
                                   ORDER  BY "count" DESC
                                   """)
processGO = TableQuery(table1 = geneGO @require,
                       table2 = lstA   @require,
                       query  = """\
                                SELECT A."sourceKey" AS "BioentityId",
                                       G."GO"        AS "GO"
                                FROM   table1 G, table2 A
                                WHERE  (G."xref"""+XrefType_Ensembl_gene+"""" = A."xref"""+XrefType_Ensembl_gene+"""") AND
                                       (G."GO" IS NOT NULL)
                                """)
for process : std.itercsv(processCount) {
  andurilSuffix = std.quote(process.GO,type="Anduril")
  related = GOSearch(force bioAnnotation = processGO,
                     go                  = go,
                     parentFilter        = true,
                     terms               = process.GO,
                     @name               = "related"+andurilSuffix)
  processScope = CSV2IDList(table1    = related.hits @require,
                            constants = process.BioentityId,
                            @name     = "processScope"+andurilSuffix)
  figProcess = PrepareFigure(genes        = StringInput(content="GO\n"+process.GO,
                                                        @name  ="id"+andurilSuffix),
                             force status = status,
                             scope        = processScope,
                             xrefType     = XrefType_Gene_Ontology,
                             linkTypes    = std.concat(sep=",",
                                            LinkType_positive_regulation,
                                            LinkType_negative_regulation,
                                            LinkType_protein_activation,
                                            LinkType_protein_inhibition,
                                            LinkType_protein_state_change,
                                            LinkType_protein_binding,
                                            LinkType_protein_dissociation,
                                            LinkType_gene_expression,
                                            LinkType_gene_repression,
                                            LinkType_phosphorylation,
                                            LinkType_dephosphorylation,
                                            LinkType_glycosylation,
                                            LinkType_ubiquitination,
                                            LinkType_deubiquitination,
                                            LinkType_methylation,
                                            LinkType_demethylation,
                                            LinkType_acetylation,
                                            LinkType_deacetylation,
                                            LinkType_sumoylation,
                                            LinkType_desumoylation),
                             eTypes       = BioentityType_gene+","+
                                            BioentityType_biological_process,
                             expand       = "up",
                             gap          = 3,
                             description  = "Cancer relevant genes in the context of "+
                                            process.name,
                             @name        = andurilSuffix)
  figFoldersGO[andurilSuffix] = figProcess.report
}

// Gene clusters

clusterSets = record(
  active     = record(matrix=topsU.scores),
  inactive   = record(matrix=topsD.scores),
  associated = record(matrix=topsN.scores),
  survival   = record(matrix=topsS.scores)
)
for name, clusterSet : clusterSets {
  g = SimilarityGraph(matrix      = clusterSet.matrix,
                      annotations = gNames.bioAnnotation,
                      threshold   = 0.1,
                      colExclude  = 'pScore,pEntity,scores,position',
                      @name       = "clusterSet_"+name)
  j = VertexJoin(graph    = g.graph,
                 nameAttr = "BioentityName",
                 idPrefix = "group",
                 @name    = "clusterSet_"+name+"_join")
  @out.figure.filename="similarity-name"
  p = GraphVisualizer(graph          = j.graph,
                      layout         = "spring2",
                      titleAttribute = "BioentityName,label",
                      reportHeight   = 20,
                      reportWidth    = 23,
                      minSize        = 1,
                      @name          = "clusterSet_"+name+"_plot")
  figFoldersSim[name] = p.figure
  figDescs["clusterSet_"+name+"_plot-graph.pdf"] = "Clusters of "+name+
                                                   " genes observed at the same studies"
}

// Image gallery

gallery    = FolderCombiner(folders=figFolders,    exclude="document.tex")
galleryGO  = FolderCombiner(folders=figFoldersGO,  exclude="document.tex")
gallerySim = FolderCombiner(folders=figFoldersSim, exclude="document.tex")

imgDesc = "\"File\"\t\"Desc\"\n"
for name, desc : figDescs {
  imgDesc = imgDesc+'"'+name+"\"\t\""+desc+"\"\n"
}
imgDescs        = StringInput(content=imgDesc)
galleryAbstract = StringInput(content="""\
Kasnain visualizations of the relationships between the cancer related genes.
""")
gSite = ImageGallery(folderRoot    = gallery.folder,
                     folder1       = galleryGO.folder,
                     folder2       = gallerySim.folder,
                     csvRoot       = imgDescs,
                     csv1          = imgDescs,
                     csv2          = imgDescs,
                     title1        = "Biological processes",
                     title2        = "Gene similarities",
                     infoRoot      = galleryAbstract,
                     annotationCol = "Desc",
                     parent        = "index.html")

// --------------
// REPORT OUTPUTS
// --------------

cfgReport = ConfigurationReport(showCategories   = false,
                                inlineComponents = false,
                                @enabled         = true)

propertiesDoc = Properties2Latex(moksiskaanInit.connection,
                                 ensembl,
                                 section = "Database configurations",
                                 hide    = "database.password")

rConfig = RConfigurationReport(packages    = "base,csbl.go",
                               sectionType = "subsection")

studyDescs = DescribeStudies(useStudies = studiesS+","+
                                          studiesN+","+
                                          studiesU+","+
                                          studiesD+","+
                                          study.TSGene.tsg_human,
                             moksiskaan = moksiskaanInit.connection)

reportTemplate = LatexTemplate(bibtex1  = rConfig.citations,
                               bibtex2  = bibtexMoksiskaan,
                               authors  = authors,
                               printTOC = true,
                               title    = "Kasnain --- Summary Report")

@out.document.filename = "KasnainWorkflow.pdf"
cfgReportPDF = LatexPDF(header   = reportTemplate.header,
                        footer   = reportTemplate.footer,
                        document = cfgReport.report,
                        useRefs  = true)
cfgAttach    = LatexAttachment(file1        = cfgReportPDF.document,
                               caption1     = "Pipeline configuration",
                               sectionTitle = "System configuration")

reportBody = LatexCombiner(array={
  allFenn.report,
  kasnainVenn.report,
  tsgVenn.report,
  tsgOTableD,
  tsgOTableU,
  cfgAttach,
  studyDescs.report,
  rConfig.report,
  propertiesDoc.report,
  moksiskaanInit.report
})

summaryReport = LatexPDF(header   = reportTemplate.header,
                         footer   = reportTemplate.footer,
                         document = reportBody.document,
                         useRefs  = true)

// Web site

wwwTableSQL = """\
              SELECT S."position"      AS "rank",
                     G."BioentityName" AS "name",
                     S."pScore"        AS "pScore",
                     S."pEntity"       AS "pEntity",
                     S."scores"        AS "score",
                     S.".GeneId"       AS "ensembl"
              FROM   table1 S, table2 G
              WHERE  (S.".GeneId" = G."sourceKey")
              ORDER  BY "rank"
              """
wwwTableUg = TableQuery(topsU.scores, gNames.bioAnnotation, query=wwwTableSQL)
wwwTableDg = TableQuery(topsD.scores, gNames.bioAnnotation, query=wwwTableSQL)
wwwTableNg = TableQuery(topsN.scores, gNames.bioAnnotation, query=wwwTableSQL)
wwwTableSg = TableQuery(topsS.scores, gNames.bioAnnotation, query=wwwTableSQL)

wwwTableU = HTMLTable(data=wwwTableUg, title="Oncogenes")
wwwTableD = HTMLTable(data=wwwTableDg, title="Tumor suppressors")
wwwTableN = HTMLTable(data=wwwTableNg, title="Cancer associated genes")
wwwTableS = HTMLTable(data=wwwTableSg, title="Survival associated genes")

sourceCodeHTML = SyntaxHighlight(code  = sourceCode,
                                 title = "Kasnain AndurilScript")

fJoin = FolderCombiner(file1       = summaryReport.document,
                       files       = {
                                     'genesU.html'     = wwwTableU.table,
                                     'genesD.html'     = wwwTableD.table,
                                     'genesN.html'     = wwwTableN.table,
                                     'genesS.html'     = wwwTableS.table,
                                     'genesU.csv'      = topsU.scores,
                                     'genesD.csv'      = topsD.scores,
                                     'genesN.csv'      = topsN.scores,
                                     'genesS.csv'      = topsS.scores,
                                     'KasnainAnd.html' = sourceCodeHTML.HTML
                                     },
                       folders     = {
                                     'gallery' = gSite
                                     },
                       asDirs      = "gallery",
                       fname1      = "KasnainSummaryReport.pdf",
                       keysAsNames = true)
mainAnnot = StringInput(content=std.concat(sep="\n",
                                "file\tdescription",
                                "KasnainSummaryReport.pdf\tResult summary and the methodological description of the project",
                                "gallery\tMoksiskaan pathway models",
                                "genesU.csv\tStudy statistics for the active genes",
                                "genesD.csv\tStudy statistics for the inactive genes",
                                "genesN.csv\tStudy statistics for the associated genes",
                                "genesS.csv\tStudy statistics for the prognostic genes",
                                "genesU.html\tOriginal summary statistics for the gene activations",
                                "genesD.html\tOriginal summary statistics for the gene inactivations",
                                "genesN.html\tOriginal summary statistics for the cancer associations",
                                "genesS.html\tOriginal summary statistics for the prognostic survival associations",
                                "KasnainAnd.html\tAndurilScript source code for the data analysis"
                                ))

www = SimpleWebPage(folder        = fJoin     @require,
                    annotation    = mainAnnot @require,
                    style         = wwwStyle,
                    title         = "Moksiskaan &#8212; Kasnain",
                    index         = "index.html",
                    contentString = """\
                                    Kasnain is an <a href="http://www.anduril.org/">Anduril</a> pipeline that integrates
                                    cancer data from multiple sources and infers tumour suppressors and oncogenes based on
                                    the alterations in their activity. Data integration and analysis is performed with
                                    <a href="http://csbi.ltdk.helsinki.fi/moksiskaan/">Moksiskaan</a> software for systems
                                    biology.
                                    <hr>
                                    """)

// -------
// OUTPUTS
// -------

/** Result web site */
@out.out.filename = "Kasnain"
OUTPUT(www, link=true)

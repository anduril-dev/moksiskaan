#!/bin/bash

if [ -z $2 ]; then
    echo "syntax: $0 in/out exportfile"
    exit 1
fi

if [ $1 == 'in' ]
then
   echo "Removing old database..."
   sudo -u postgres psql <<EOF
DROP DATABASE moksiskaan;
CREATE DATABASE moksiskaan WITH OWNER=moksiskaan encoding='UTF-8' LC_CTYPE = 'en_US.utf8' LC_COLLATE = 'en_US.utf8' TEMPLATE template0;   
EOF
   if [ ! $? = 0 ]; then
      echo 2>'- remove failed!'
      exit 1
   fi
   echo "Importing $2..."
   sudo -u postgres psql moksiskaan -f $2
elif [ $1 == 'out' ]
then
   echo "Exporting database to $2..."
   sudo -u postgres pg_dump moksiskaan -f $2
else
   echo "Invalid operation: $1"
fi

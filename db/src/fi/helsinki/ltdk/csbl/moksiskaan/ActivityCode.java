package fi.helsinki.ltdk.csbl.moksiskaan;

/**
 * Constants used to represent activity statuses of various bioentities.
 * These constants are compatible with the ActivityTable data type in Anduril.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.11
 */
public enum ActivityCode {

	/** This entity does not exist in the context. */
	ABSENT(-2),

	/** The activity is below the base line. */
	DOWN(-1),

	/** Basal activity */
	STABLE(0),

	/** The activity is above the base line. */
	UP(1);

	/** Default column name for the status codes within Anduril ActivityTables. */
	public static final String COLUMN_NAME_STATUS = "status";

	public  final Integer status;
	private final String  code;

	private ActivityCode(Integer status) {
		this.status = status;
		this.code   = status.toString();
	}

	/** Provides the status code. */
	@Override
	public String toString() { return code; }

    /**
     * Decode the given string to its original meaning.
     */
    static public ActivityCode parse(String code) {
    	if (code == null)             return null;
    	if (STABLE.code.equals(code)) return STABLE;
    	if (UP    .code.equals(code)) return UP;
    	if (DOWN  .code.equals(code)) return DOWN;
    	if (ABSENT.code.equals(code)) return ABSENT;
    	throw new IllegalArgumentException(code+" is not an acceptable activity code.");
    }

}
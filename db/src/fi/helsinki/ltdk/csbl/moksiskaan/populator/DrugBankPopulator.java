package fi.helsinki.ltdk.csbl.moksiskaan.populator;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.hibernate.Session;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import fi.helsinki.ltdk.csbl.asser.MainArgument;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.BioentityType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Organism;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Xref;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType;

/**
 * Populates Moksiskaan database with DrugBank drugs.
 *
 * @since {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.05
 */
public class DrugBankPopulator {

	/** Link annotation name for the data source used by this application */
	static public final String LINK_ANNOTATION_SOURCE = "DrugBank";

	private final boolean verbose;
	private final PrintStream out = System.out;

	static final String ELEMENT_DRUGS      = "drugs";
    static final String ELEMENT_DRUG       = "drug";
    static final String ELEMENT_ID         = "drugbank-id";
    static final String ELEMENT_IDENTIFIER = "identifier";
    static final String ELEMENT_NAME       = "name";
    static final String ELEMENT_PARTNERS   = "partners";
    static final String ELEMENT_PARTNER    = "partner";
    static final String ELEMENT_D_INTER    = "drug-interaction";
    static final String ELEMENT_RESOURCE   = "resource";
    static final String ELEMENT_ROLE       = "action";
    static final String ELEMENT_XREF       = "external-identifier";

    static final String ELEMENT_CARRIER     = "carrier";
    static final String ELEMENT_ENZYME      = "enzyme";
    static final String ELEMENT_TARGET      = "target";
    static final String ELEMENT_TRANSPORTER = "transporter";

    private final Map<String,Bioentity[]> entityCache = new HashMap<String,Bioentity[]>(5000);

    private final BioentityType typeDrug;

    private final XrefType typeKEGGDrug;
    private final XrefType typeDrugBankDrug;

    private final Organism organism;

    public DrugBankPopulator(boolean useVerbose) {
    	verbose = useVerbose;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        organism         = (Organism)     session.get(     Organism.class, IDConstants.Organism_Homo_sapiens);
        typeDrug         = (BioentityType)session.get(BioentityType.class, IDConstants.BioentityType_drug);
        typeDrugBankDrug = (XrefType)     session.get(     XrefType.class, IDConstants.XrefType_DrugBank_drug);
        typeKEGGDrug     = (XrefType)     session.get(     XrefType.class, IDConstants.XrefType_KEGG_drug);
        session.getTransaction().commit();
    }

	public void populate(String exportURL) throws IOException, ParserConfigurationException, SAXException {
		URL url = new URL(exportURL);
		out.println("DrugBank import from "+url);

		// Read database dump...
		URLConnection      con     = url.openConnection();
		ZipInputStream     zin     = new ZipInputStream(con.getInputStream());
		ZipEntry           ze      = zin.getNextEntry();
		SAXParser          parser  = SAXParserFactory.newInstance().newSAXParser();
		DrugBankXMLHandler handler = new DrugBankXMLHandler();

		out.println("- parsing "+ze.getName());
		parser.parse(zin, handler);
		zin.close();

		HibernateUtil.addLogEntry("DrugBankImport",
		                          "Total of "+handler.targets.size()+" drugs identified from "+url+'.',
		                          "Law2013,Knox2011");
		HibernateUtil.getSessionFactory().close();
	}

	@SuppressWarnings("unchecked")
	private Bioentity save(DrugBankTemplate template, Session session) {
		String keggId = template.props.get("KEGG Drug");
		out.println("  - saving "+template+" ("+(keggId==null ? "new" : keggId)+") associations to "+
				    template.targets);
		// Prepare drug
		Bioentity drug = null;
		Xref      xref = null;
		if (keggId != null) {
		    List<Bioentity> drugs = (List<Bioentity>)
	                                session.createQuery("SELECT X.bioentity "+
                                                        "FROM   Xref X "+
                                                        "WHERE  (X.xrefType.id = "+IDConstants.XrefType_KEGG_drug+") AND "+
                                                        "       (X.value       = :value)")
                                           .setString("value", keggId)
                                           .list();
		    if (drugs.size() > 1) {
		        out.println("    There are "+drugs.size()+" drugs for the KEGG ID "+keggId+'!');
		    }
		    if (drugs.isEmpty()) {
		       if (verbose) {
                  out.println("    WARNING: local database does not have a drug with KEGG identifier "+keggId+'.');
               }
		    } else {
		       drug = drugs.get(0);
		       xref = new Xref(typeDrugBankDrug, drug, template.id);
		    }
		}
		if (drug == null) {
			List<Bioentity> drugs = (List<Bioentity>)
	                                session.createQuery("SELECT X.bioentity "+
                                                        "FROM   Xref X "+
                                                        "WHERE  (X.xrefType.id = "+IDConstants.XrefType_DrugBank_drug+") AND "+
                                                        "       (X.value       = :value)")
                                           .setString("value", template.id)
                                           .list();
            if (drugs.isEmpty()) {
                String name = template.props.get(ELEMENT_NAME);
                drugs = (List<Bioentity>)
                        session.createQuery("FROM   Bioentity "+
                                            "WHERE  (bioentityType.id = "+IDConstants.BioentityType_drug+") AND "+
                                            "       (name             = :name)")
                                           .setString("name", name)
                                           .list();
                if (drugs.size() > 1) {
                   out.println("    There are "+drugs.size()+" drugs called "+name+'.');
                }
            }
			for (int i=drugs.size()-1; i>=0; i--) { // Notice the reverse order that preserves the first xref entity right
				drug = drugs.get(i);
	            if (keggId != null) {
	               xref = new Xref(typeKEGGDrug, drug, keggId);
		        }
	            if (i > 0) {
	            	Bioentity main = drugs.get(0); 
	            	out.println("    WARNING: "+drug.getName()+
	            			    " ("+drug.getBioentityId()+") is a duplicate of "+
	            			    main.getName()+" ("+main.getBioentityId()+").");
	            	if (keggId != null) { session.saveOrUpdate(xref); } // NOPMD
	            }
	        }
		}
		if (drug == null) {
			String name = template.props.get(ELEMENT_NAME);
	        drug = new Bioentity();
	        drug.setName(name);
	        drug.setBioentityType(typeDrug);
	        drug.setOrganism(organism);
	        session.saveOrUpdate(drug);
	        if (keggId != null) {
	        	xref = new Xref(typeKEGGDrug, drug, keggId);
	            session.saveOrUpdate(xref);
	        }
	        xref = new Xref(typeDrugBankDrug, drug, template.id);
		}
		if (xref != null) session.saveOrUpdate(xref);

		// Save links
		for (DrugBankTemplate.DrugTarget target : template.targets) {
		    String uniprot = DrugBankTemplate.ID_UNIPROT.get(target.id);
		    if (uniprot == null) continue;
			Bioentity[] genes = HibernateUtil.geneByUniprot(uniprot, session, entityCache);
			if (genes == null) {
				if (verbose) out.println("    No gene mappings for "+uniprot+" ("+
				                         DrugBankTemplate.ID_NAME.get(target.id)+").");
			} else for (Bioentity gene : genes) {
				HibernateUtil.saveLink(drug, gene, 
                                       (LinkType)session.get(LinkType.class, target.role),
                                       LINK_ANNOTATION_SOURCE,
                                       target.host,
                                       session, null);
			}
		}
		return drug;
	}

	/**
	 * Executes the stand-alone populator.
	 *
	 * @param argv An optional URL for the database export (the zip file) 
	 */
    static public void main(String[] argv) throws IOException,
                                                  ParserConfigurationException,
                                                  SAXException {
        Map<String, MainArgument> args = new HashMap<String, MainArgument>();
        MainArgument argXML = MainArgument.add("data", "url",  "URL of the DrugBank XML export", args);
        MainArgument argV   = MainArgument.add("v",            "Verbose output mode", args);
        try { MainArgument.parseInput(argv, args, 0, 0); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            System.err.println();
            MainArgument.printArguments(args, System.err);
            System.exit(-1); return;
        }
        if (!argXML.isUsed()) {
        	System.err.println(argXML.getDescription()+" is missing.");
        	System.exit(-2); return;
        }

       new DrugBankPopulator(argV.isUsed()).populate(argXML.getValue());
    }

    class DrugBankXMLHandler extends DefaultHandler {

    	Session          session  = HibernateUtil.getSessionFactory().getCurrentSession();
        DrugBankTemplate tpl      = null;
        boolean          keep     = false;
        String           key      = null;
        StringBuffer     cBuffer  = new StringBuffer(512); // NOPMD
        Stack<String>    level    = new Stack<String>();
        String           resource = null;
        Integer          partner  = null;

        DrugBankTemplate.DrugTarget  target  = null;
        Collection<DrugBankTemplate> targets = new LinkedList<DrugBankTemplate>();

        @Override
        public void startDocument() throws SAXException {
            session.beginTransaction();
        }

        @Override
        public void endDocument() throws SAXException {
            Map<String,Bioentity> drugEntities = new HashMap<String,Bioentity>(1000);

        	for (DrugBankTemplate tpl: targets) {
        	    Bioentity d = save(tpl, session);
        	    drugEntities.put(tpl.id, d);
        	}

            out.println("- Saving drug interactions...");
        	LinkType interactionLT = (LinkType)session.get(LinkType.class, IDConstants.LinkType_drug_interaction);
        	for (DrugBankTemplate tpl: targets) {
        	    int iCount = tpl.inters.size(); 
        	    if (iCount < 1) continue;
        	    if (verbose) {
        	       out.println("  - saving "+iCount+" interactions for "+
        	                   tpl.props.get(DrugBankPopulator.ELEMENT_NAME));
        	    }
        	    Bioentity s = drugEntities.get(tpl.id);
        	    for (String drugId : tpl.inters) {
        	        Bioentity t = drugEntities.get(drugId);
        	        if (t != null) {
        	           HibernateUtil.saveLink(s, t, interactionLT,
                                              LINK_ANNOTATION_SOURCE,
                                              "drug_interaction",
                                              session, null);
        	        }
        	    }
        	}
    		session.getTransaction().commit();
        }

        @Override
        public void startElement(String     uri,
                                 String     localName,
                                 String     qName,
                                 Attributes attributes) throws SAXException {
        	String parent = level.empty() ? null : level.peek();
        	if ((ELEMENT_DRUG.equals(parent) && (ELEMENT_ID.equals(qName)       || ELEMENT_NAME.equals(qName)))       ||
        	    (ELEMENT_XREF.equals(parent) && (ELEMENT_RESOURCE.equals(qName) || ELEMENT_IDENTIFIER.equals(qName))) ||
        	    (ELEMENT_PARTNER.equals(parent) && ELEMENT_NAME.equals(qName))                                        ||
        		((target != null) && ELEMENT_ROLE.equals(qName))) {
               key = qName;
        	} else
        	if (ELEMENT_CARRIER.equals(qName) ||
        	    ELEMENT_ENZYME.equals(qName)  ||
        	    ELEMENT_TARGET.equals(qName)  ||
        	    ELEMENT_TRANSPORTER.equals(qName)) {
        		target    = tpl.new DrugTarget(qName);
        		target.id = Integer.valueOf(attributes.getValue("partner"));
        	} else
        	if (ELEMENT_DRUGS.equals(parent) && ELEMENT_DRUG.equals(qName)) {
        		tpl = new DrugBankTemplate();
        	} else
        	if (ELEMENT_D_INTER.equals(parent) && ELEMENT_DRUG.equals(qName)) {
        	    key = ELEMENT_D_INTER;
        	} else
            if (ELEMENT_PARTNER.equals(qName)) {
            	partner = Integer.valueOf(attributes.getValue("id"));
            } else
        	if (ELEMENT_PARTNERS.equals(qName)) {
        		out.println("  * loading target proteins");
        	}
        	level.push(qName);
        }

        @Override
        public void characters(char ch[], int start, int length) throws SAXException {
        	if ((key != null) && (length > 0)) {
               cBuffer.append(ch, start, length);
        	}
        }

        /**
         * Process the complete character content of the block.
         */
        private void processCharacters() {
        	if (cBuffer.length() < 1) return;

            String value = cBuffer.toString();
            cBuffer.setLength(0);
            if ((target != null) && ELEMENT_ROLE.equals(key)) {
               value = value.trim().toLowerCase();
               target.setRole(value);
            } else
            if (ELEMENT_RESOURCE.equals(key)) {
               resource = value;
            } else
            if ((resource != null) && ELEMENT_IDENTIFIER.equals(key)) {
               if (partner == null) {
                  assert (tpl != null) : "Drug template has not been initialized for the resource.";
                  String oldV = tpl.setProperty(resource, value);
                  if (oldV != null) {
            	     out.println("  * WARNING: replacing "+key+" identifier "+oldV+" with "+value+" in "+tpl+'.');
                  }
               } else {
                  if ("UniProtKB".equals(resource)) {
                     DrugBankTemplate.ID_UNIPROT.put(partner, value);
                  }
               }
            } else
            if (ELEMENT_D_INTER.equals(key)) {
               tpl.addInteraction(value);
            } else
            if ((partner != null) && ELEMENT_NAME.equals(key)) {
               DrugBankTemplate.ID_NAME.put(partner, value);
            } else {
               assert (tpl != null) : "Drug template has not been initialized for the key.";
               String oldV  = tpl.setProperty(key, value);
               if (oldV != null) {
            	  out.println("  * WARNING: replacing "+key+" value "+oldV+" with "+value+" in "+tpl+'.');
               }
            }
            key = null;       	
        }

        @Override
        public void endElement(String uri,
                               String localName,
                               String qName) throws SAXException {
        	processCharacters();

        	level.pop();
        	String parent = level.empty() ? null : level.peek();
        	if (ELEMENT_DRUGS.equals(parent) && ELEMENT_DRUG.equals(qName)) {
        		if (tpl.isValid()) {
                   if (!tpl.targets.isEmpty()) {
    				  targets.add(tpl);
                   }
    			} else {
    				out.println("  * WARNING: an incomplete drug definition found for "+tpl+'.');
    			}
    			tpl = null;
        	} else
        	if (ELEMENT_CARRIER.equals(qName) ||
        	    ELEMENT_ENZYME.equals(qName)  ||
        	    ELEMENT_TARGET.equals(qName)  ||
        	    ELEMENT_TRANSPORTER.equals(qName)) {
        	    // Skip targets with unknown roles (=actions)
        	    if (target.role >= 0) tpl.targets.add(target);
        		target = null;
        	} else
        	if (ELEMENT_XREF.equals(qName)) {
        		resource = null;
        	} else
            if (ELEMENT_PARTNER.equals(qName)) {
              	partner = null;
            }
        }

    }

}

/**
 * This reusable template is used to accumulate details about an individual drug and its relations.
 * The content of the template may be cleaned as a new drug entry is encountered while the
 * previous drug may be processed before doing this.
 */
class DrugBankTemplate {

    static final Map<Integer,String> ID_NAME    = new HashMap<Integer,String>(10000);
    static final Map<Integer,String> ID_UNIPROT = new HashMap<Integer,String>(10000);

    static final Map<String,Integer> ACTION_ROLE = new HashMap<String,Integer>(100);
    static {
    	try {
    	  CSVParser actionsIn = new CSVParser(ClassLoader.getSystemResource("fi/helsinki/ltdk/csbl/moksiskaan/populator/DrugBankActions.csv").getFile());
    	  for (String[] line : actionsIn) {
    		  ACTION_ROLE.put(line[0],
    				          (line[1]==null) ? Integer.MIN_VALUE : Integer.valueOf(line[1]));
    	  }
    	  actionsIn.close();
    	} catch (IOException err) {
    		throw new RuntimeException("Cannot map DrugBank target actions to their link types.", err);
    	}
    }

	class DrugTarget {

		final String host;

        Integer id   = null;
		int     role = -1;

		DrugTarget(String hostTag) {
			host = hostTag;
		}

		public String toString() {
			String roleS;
			if (role == IDConstants.LinkType_drug_inhibits)   roleS = "(-)"; else
			if (role == IDConstants.LinkType_drug_promotes)   roleS = "(+)"; else
			if (role == IDConstants.LinkType_drug_target)     roleS = "(o)"; else
			if (role == IDConstants.LinkType_drug_metabolism) roleS = "(%)"; else
            if (role == IDConstants.LinkType_biomarker)       roleS = "(M)"; else
				                                              roleS = "(?)";
			return roleS+getName();
		}

		public String getName() {
			String name = ID_UNIPROT.get(id);
			if (name == null) {
			   name = (id==null) ? "[undefined]" : id.toString();
			}
			return name;
		}

		void setRole(String action) {
			Integer r = ACTION_ROLE.get(action);
			if (r == null) {
				System.err.println("  * WARNING: unknown drug target role '"+action+"' used in "+DrugBankTemplate.this+'.');
			} else
			if (r != Integer.MIN_VALUE) {
				role = r;
			}
		}
	}

	String                 id;
	Map<String,String>     props   = new HashMap<String,String>();
	Collection<DrugTarget> targets = new ArrayList<DrugTarget>();
	Collection<String>     inters  = new HashSet<String>();

	void setId(String value) {
		if (id != null)
			throw new IllegalStateException("Multiple identifiers for the same drug: "+id+", "+value);
		id = value;
	}

	String setProperty(String key, String value) {
		if (DrugBankPopulator.ELEMENT_ID.equals(key)) {
			setId(value);
			return null;
		} else
	    return props.put(key, value);
	}

    void addInteraction(String drugId) {
        inters.add(drugId);
    }

	boolean isValid() {
		return (id != null) && props.containsKey(DrugBankPopulator.ELEMENT_NAME);
	}

	public String toString() {
	    String name = props.get(DrugBankPopulator.ELEMENT_NAME);
	    if (name == null) {
	       name = (id == null) ? "[undefined]" : id; 
	    }
		return name;
	}

}
package fi.helsinki.ltdk.csbl.moksiskaan.populator;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.rpc.ServiceException;

import org.hibernate.Session;
import org.pathvisio.core.model.ConverterException;
import org.pathvisio.core.model.DataNodeType;
import org.pathvisio.core.model.ObjectType;
import org.pathvisio.core.model.Pathway;
import org.pathvisio.core.model.PathwayElement;
import org.pathvisio.wikipathways.webservice.WSPathway;
import org.pathvisio.wikipathways.webservice.WSPathwayInfo;
import org.wikipathways.client.WikiPathwaysClient;

import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.MainArgument;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.BioentityType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Organism;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Xref;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType;

/**
 * Populates Moksiskaan database with
 * <a href="http://www.wikipathways.org/index.php/WikiPathways">WikiPathways</a> pathways.
 * 
 * @author Riku Louhimo
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.12
 */
public class WikiPathwaysPopulator {
    /** Link annotation name for the data source used by this application */
    static public final String LINK_ANNOTATION_SOURCE = "WikiPathways";

    private final boolean verbose;
    private final PrintStream out = System.out;

    static final String DB_URL = "http://www.wikipathways.org/wpi/webservice/webservice.php";
    
    private final BioentityType typePathway;
    
    private final XrefType typeWikiPathway;
    
    private final LinkType typePathwayContains;

    public WikiPathwaysPopulator(boolean useVerbose) {
        verbose = useVerbose;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        typePathway         = (BioentityType)session.get(BioentityType.class, IDConstants.BioentityType_pathway);
        typePathwayContains = (LinkType)     session.get(     LinkType.class, IDConstants.LinkType_pathway_contains);
        typeWikiPathway     = (XrefType)     session.get(     XrefType.class, IDConstants.XrefType_WikiPathways);
        session.getTransaction().commit();
    }
    
    @SuppressWarnings("unchecked")
	public void populate(String   wpLog,
	                     String[] ids,
	                     String[] orgs) throws IOException, ServiceException, ConverterException {
    	// WikiPathways messages
    	if (wpLog == null) {
           org.pathvisio.core.debug.Logger.log.setLogLevel(false, false, false, false, true, true);   
    	} else {
    	   org.pathvisio.core.debug.Logger.log.setStream(new PrintStream(wpLog));
    	}
    	
        // Create a client to the WikiPathways web service
    	out.println("Connecting to: "+DB_URL);
        WikiPathwaysClient client = new WikiPathwaysClient(new URL(DB_URL));

        // Select potential organisms from the local database.
        List<Organism> organisms = (List<Organism>)HibernateUtil.selectThese(
                "SELECT DISTINCT B.organism "+
                "FROM   Bioentity B "+
                "WHERE  (B.bioentityType.id = "+IDConstants.BioentityType_gene+')'
        );

        for (Organism organism : organisms) {
            if (orgs != null) {
                if (AsserUtil.indexOf(organism.getOrganismId().toString(), orgs, true, false) < 0)
                   continue;
            }
	        out.println("\nLoading WikiPathways content for "+organism.getName()+"...");
	        org.bridgedb.bio.Organism species = org.bridgedb.bio.Organism.fromLatinName(organism.getName());
	        if (species == null) {
	        	out.println("    * Cannot find any content for this organism!");
	        	continue;
	        }

	        Set<String>     missedRefs  = new HashSet<String>();
	        WSPathwayInfo[] pathwayList = client.listPathways(species);	        
	        int             pwCount     = 0;
	        // Loop over pathways
	        for (int i=0; i<pathwayList.length; i++) {
	            String id = pathwayList[i].getId();
	            if (ids != null) {
	                if (AsserUtil.indexOf(id, ids, true, false) < 0)
	                   continue;
	            }

	            String name     = pathwayList[i].getName();
	            String pathName = name+" ("+id+')';
	            out.println("Processing "+pathName+" ["+(i+1)+'/'+pathwayList.length+"]...");
	            if ((name.indexOf("-delete") > 0) ||
	                (name.indexOf("andbox")  > 0)) {
	                out.println("    * Skipped because the name is associated to dymmy pathways");
	                continue;
	            }
	            WSPathway wsPathway = null;
	            for (int retry=0; (retry<2) && (wsPathway==null); retry++) {
	                if (retry>0) out.println("    * Trying again!");
	                try { wsPathway = client.getPathway(id); }
	                catch (RemoteException err) {
	                    err.printStackTrace();
	                }
	            }
	            if (wsPathway == null) throw new IOException("Pathways cannot be loaded!");

	            Pathway pathway;	            
	            try { pathway = WikiPathwaysClient.toPathway(wsPathway); }
	            catch (ConverterException err) {
	            	out.println("    * Pathway entry is broken and cannot be read");
	            	err.printStackTrace(out);
	            	continue;
	            }
	
	            if (savePathway(pathway, organism, id, name, missedRefs)) {
	            	pwCount++;
	            }
	        }
	        HibernateUtil.addLogEntry("WikiPathways",
	                                  String.format("WikiPathways import produced %d %s pathways.",
	               		              pwCount, organism.getName()),
	                                  "Kelder2012");
        }
    }

    /**
     * Process one pathway.
     *
     * @param organism    Host organism in Moksiskaan
     * @param pathwayId   WikiPathways identifier for the pathway
     * @param pathwayName Moksiskaan name to be used for the pathway
     */
	private boolean savePathway(Pathway     pathway,
                                Organism    organism,
                                String      pathwayId,
                                String      pathwayName,
                                Set<String> unknownIdTypes) throws IOException {

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Map<String, List<Bioentity>> elements = new HashMap<String, List<Bioentity>>(200);
        Map<String, String>          groups   = new HashMap<String, String>(100);
        List<List<Bioentity>>        source   = new ArrayList<List<Bioentity>>(200); 
        List<List<Bioentity>>        target   = new ArrayList<List<Bioentity>>(200);
        List<PathwayElement>         rel      = new ArrayList<PathwayElement>(200);

        // Fetch groups of genes
        for (PathwayElement group : pathway.getDataObjects()){
            if (group.getObjectType() == ObjectType.GROUP){
                String groupId = group.getGroupId();
                String graphId = group.getGraphId();
                if (graphId == null){
                    out.println("    * skipped element group ("+groupId+"): no links to group");
                    continue;
                } else {
                    groups.put(groupId, graphId);
                }
            }
        }

        for (PathwayElement element : pathway.getDataObjects()) {
            if (element.getObjectType() == ObjectType.DATANODE) {
                if (element.getDataNodeType().equals(DataNodeType.PROTEIN.toString())     ||
                    element.getDataNodeType().equals(DataNodeType.GENEPRODUCT.toString()) ||
                    element.getDataNodeType().equals(DataNodeType.RNA.toString())) {
                	String QUERY;
                    String dSource;
                    if (element.getDataSource() == null) continue;
                    dSource = element.getDataSource().getFullName();
                    if ("Entrez Gene".equals(dSource)) {
                    	QUERY = "SELECT B "+
                                "FROM   Xref X, Bioentity B "+
                                "WHERE  (X.xrefType.id  = "+IDConstants.XrefType_Entrez_gene+") AND "+ // NOPMD
                                "       (X.value        = :value) AND "+
                                "       (X.bioentity.id = B.id) AND "+
                                "       (B.organism.id  = "+organism.getOrganismId()+')';
                    } else
                    if ("UniProt".equals(dSource) ||
                        dSource.startsWith("Uniprot")) {
                    	QUERY = "SELECT L.source "+
                                "FROM   Xref X, Link L "+
                                "WHERE  (L.linkType.id = "+IDConstants.LinkType_protein_coding_gene+") AND "+
                                "       (X.xrefType.id = "+IDConstants.XrefType_UniProt_protein+") AND "+
                                "       (X.value       = :value) AND "+
                                "       (X.bioentity   = L.target)";
                    } else
                    if ("HGNC".equals(dSource)) {
                    	QUERY = "FROM Bioentity WHERE (name = :value) AND "+
                                "(organism.id      = "+organism.getOrganismId()+") AND "+
                    			"(bioentityType.id = "+IDConstants.BioentityType_gene+')';
                    } else
                    if (dSource.startsWith("Ensembl") ||
                        "ENSEMBL".equals(dSource)) {
                    	QUERY = "SELECT X.bioentity "+
                                "FROM   Xref X "+
                                "WHERE  (X.xrefType.id = "+IDConstants.XrefType_Ensembl_gene+") AND "+
                                "       (X.value       = :value)";
                    } else
                    if (dSource.startsWith("miRBase")) {
                    	QUERY = "SELECT X.bioentity "+
                                "FROM   Xref X "+
                                "WHERE  (X.xrefType.id = "+IDConstants.XrefType_miRBase_sequence+") AND "+
                                "       (X.value       = :value)";
                    } else
                    if ("ec-code".equals(dSource) ||
                        "Enzyme Nomenclature".equals(dSource) ||
                        "EC Number".equals(dSource)) {
                        QUERY = "SELECT B "+
                                "FROM   Xref X, Bioentity B "+
                                "WHERE  (X.xrefType.id  = "+IDConstants.XrefType_Enzyme_classification+") AND "+
                                "       (X.value        = :value) AND "+
                                "       (X.bioentity.id = B.id) AND "+
                                "       (B.organism.id  = "+organism.getOrganismId()+')';
                    } else {
                        if (unknownIdTypes.add(dSource)) {
                            out.println("    * Unsupported identifier type: "+dSource+
                                        " ("+element.getElementID()+')');
                        }
                        continue;
                    }

                    @SuppressWarnings("unchecked")
                    List<Bioentity> entities = (List<Bioentity>)
                                               session.createQuery(QUERY)
                                                      .setString("value",
                                                                 element.getElementID().trim())
                                                      .list();
                    if (!entities.isEmpty()) {
                        elements.put(element.getGraphId(), entities);
                        // Dissect groups
                        if (element.getGroupRef() != null){
                            if (elements.get(groups.get(element.getGroupRef())) == null) {
                                // Do not add null groups
                                if (groups.get(element.getGroupRef()) != null) {
                                    elements.put(groups.get(element.getGroupRef()), entities);
                                }
                            } else {
                                List<Bioentity> newGroup = elements.get(groups.get(element.getGroupRef()));
                                newGroup.addAll(entities);
                                elements.put(groups.get(element.getGroupRef()), newGroup);
                            }
                        }
                    }
                }
            } else if (element.getObjectType() == ObjectType.LINE ||
                       element.getObjectType() == ObjectType.GRAPHLINE) {
            	List<Bioentity> sources = elements.get(element.getStartGraphRef());
            	List<Bioentity> targets = elements.get(element.getEndGraphRef());
            	if (sources == null || targets == null) continue;
                source.add(sources);
                target.add(targets);
                rel.add(element);
            }
        }
        if (elements.size() < 2) {
            session.getTransaction().commit();
            out.println("    * skipped ("+pathwayId+") in the absence of suitable entities");
            return false;
        }

        int lCount = 0;
        WikiPathwaysLinkTypes ltMap = WikiPathwaysLinkTypes.getDefaultInstance();
        for (int link=0; link < source.size(); link++) {
            List<Bioentity> sources = source.get(link);
            List<Bioentity> targets = target.get(link);
            LinkType        type    = ltMap.asLinkType(rel.get(link),out);

            if (type==null) {
                if (!sources.isEmpty() && !targets.isEmpty())
                   out.println("    * unknown relationship between "+sources+" and "+targets);
                continue;
            }

            // Create all connections...
            for (Bioentity sEntity : sources) {
                for (Bioentity tEntity : targets) {
                	// Skip self joins
                	if (sEntity.getBioentityId() == tEntity.getBioentityId()) continue;
                    HibernateUtil.saveLink(sEntity, tEntity, type, LINK_ANNOTATION_SOURCE, pathwayId, session,
                    		               verbose ? out : null);
                    lCount++;
                }
            }
        }

        // Create pathway if not already in db
        if (lCount > 0){
            Bioentity bePathway = createPathway(pathwayId, pathwayName, organism, session);
            // Link entities to pathway
            for (List<Bioentity> entities : elements.values()) {
                for (Bioentity entity : entities) {
                    HibernateUtil.saveLink(bePathway, entity, typePathwayContains, null, null, session, null);
                }
            }
        } else {
            session.getTransaction().commit();
            out.println("    * skipped ("+pathwayId+") in the absence of links");
            return false;
        }
        out.println("    * accepted "+lCount+" links");
        session.getTransaction().commit();
        return true;
    }
    
    private Bioentity createPathway(String   id,
                                    String   name,
                                    Organism organism,
                                    Session  session) {
        Bioentity pathway;

        pathway = (Bioentity)session.createQuery("SELECT X.bioentity "+
                                                 "FROM   Xref X "+
                                                 "WHERE  (X.xrefType.id = "+IDConstants.XrefType_WikiPathways+") AND "+
                                                 "       (X.value       = :value)")
                                    .setString("value", id)
                                    .uniqueResult();
        if (pathway == null) {
           out.println("    save("+id+", "+typePathway.getName()+')');
           pathway = new Bioentity();
           pathway.setName(name);
           pathway.setBioentityType(typePathway);
           pathway.setOrganism(organism);
           session.saveOrUpdate(pathway);
           Xref xref = new Xref(typeWikiPathway, 
                                pathway,
                                id);
           session.saveOrUpdate(xref);
        }
        return pathway;
    }

    static public void main(String[] argv) throws ConverterException,
                                                  ServiceException,
                                                  IOException {
        Map<String, MainArgument> args = new HashMap<String, MainArgument>();
        MainArgument argIds  = MainArgument.add("ids", "WPlist", "Comma separated WP IDs", args);
        MainArgument argOrgs = MainArgument.add("o",   "ids",    "Comma separated organism IDs", args);
        MainArgument argVerb = MainArgument.add("v",             "Verbose output",         args);
        try { MainArgument.parseInput(argv, args, 0, 0); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            System.err.println();
            MainArgument.printArguments(args, System.err);
            System.exit(-1); return;
        }

        WikiPathwaysPopulator populator = new WikiPathwaysPopulator(argVerb.isUsed());
        populator.populate(null, argIds.getValues(), argOrgs.getValues());
        HibernateUtil.getSessionFactory().close();
    }
}

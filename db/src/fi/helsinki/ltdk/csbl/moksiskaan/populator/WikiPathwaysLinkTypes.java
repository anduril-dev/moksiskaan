package fi.helsinki.ltdk.csbl.moksiskaan.populator;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.pathvisio.core.model.PathwayElement;

import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;

/**
 * Provides the mapping between Moksiskaan LinkTypes and edges in
 * WikiPathways.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.12
 */
public final class WikiPathwaysLinkTypes {

    static private final LinkType UNDEFINED_LINK_TYPE = new LinkType();

	static private WikiPathwaysLinkTypes defaultInstance = null;

	private final Map<WPLTEntity,LinkType> types = new HashMap<WPLTEntity,LinkType>(100);

	private WikiPathwaysLinkTypes() {}

	/**
	 * Provides a Moksiskaan link type that matches the given edge of
	 * the WikiPathways graph.
	 *
	 * @param element An edge of a WikiPathways pathway
	 * @param out     An optional debug output writer for the missing mappings.     
	 */
	@SuppressWarnings("PMD.CompareObjectsWithEquals")
	public LinkType asLinkType(PathwayElement element, PrintStream out) {
		WPLTEntity key = new WPLTEntity(element.getLineStyle(),
				                        element.getColor(),
				                        element.getEndLineType().getName());
		LinkType lt = asLinkType(key);
		if (lt == null && out != null) {
            types.put(key, UNDEFINED_LINK_TYPE);
			out.println("- Unsupported link type: "+key);
		}
		if (lt == UNDEFINED_LINK_TYPE) lt = null;
		return lt;
	}

    public LinkType asLinkType(WPLTEntity key) {
        LinkType lt = types.get(key);
        if (lt == null && key.style != null) {
            lt = asLinkType(new WPLTEntity(null, key.color, key.relType));
        }
        if (lt == null && key.color != null) {
            lt = asLinkType(new WPLTEntity(key.style, null, key.relType));
        }
        if (lt == null && key.relType != null) {
            lt = asLinkType(new WPLTEntity(key.style, key.color, null));
        }
        return lt;
    }

	static public WikiPathwaysLinkTypes createInstance(InputStream config) throws IOException {
		WikiPathwaysLinkTypes instance = new WikiPathwaysLinkTypes();
		Map<String,LinkType>  lts      = new HashMap<String,LinkType>(50);

		CSVParser in = new CSVParser(config,
				                     CSVParser.DEFAULT_DELIMITER,
				                     0,
				                     CSVParser.DEFAULT_MISSING_VALUE_SYMBOL,
				                     true);
		int s = in.getColumnIndex("style");
		int c = in.getColumnIndex("color");
		int r = in.getColumnIndex("relation");
		int t = in.getColumnIndex("linktype");
		for (String[] line : in) {
			Color      color = (line[c]==null) ? null : new Color(Integer.parseInt(line[c], 16));
			LinkType   tl    = lts.get(line[t]);
			if (tl == null) {
				tl = new LinkType();
				tl.setLinkTypeId(Integer.parseInt(line[t]));
				tl.setName("LinkType:"+line[t]);
				lts.put(line[t], tl);
			}
			WPLTEntity entry = new WPLTEntity(line[s]==null ? null : Integer.parseInt(line[s]),
					                          color,
					                          line[r]);
			instance.types.put(entry, tl);
		}
		in.close();
		return instance;
	}

	static public WikiPathwaysLinkTypes getDefaultInstance() throws IOException {
		synchronized (WikiPathwaysLinkTypes.class) {
			if (defaultInstance == null) {
				final String RN = "fi/helsinki/ltdk/csbl/moksiskaan/populator/WikiPathwaysLinks.csv";
				InputStream stream = ClassLoader.getSystemResourceAsStream(RN);
				if (stream == null) throw new FileNotFoundException(RN);
				defaultInstance = createInstance(stream);
			}
		}
		return defaultInstance;
	}

	/** An edge type in WikiPathways. */
	static private class WPLTEntity {

		Integer style;
		Color   color;
		String  relType;

		public WPLTEntity(Integer s, Color c, String r) {
			style   = s;
			color   = c;
			relType = r;
		}

		@Override
		public int hashCode() {
			return (style  ==null ? 0 : style.hashCode())^
				   (color  ==null ? 0 : color.hashCode())^
				   (relType==null ? 0 : relType.hashCode());
		}

		@Override
		public boolean equals(Object obj) {
			WPLTEntity e = (WPLTEntity)obj;
			return (style   == null || style.equals(e.style)) &&
				   (color   == null || color.equals(e.color)) &&
				   (relType == null || relType.equals(e.relType));
		}
		
		@Override
		public String toString() {
			StringBuffer b = new StringBuffer(255);
			if (style   != null) b.append("style=").append(style);
			if (color   != null) b.append(",color=").append(Integer.toHexString(color.getRGB()));
			if (relType != null) b.append(",type=") .append(relType);
			return b.toString();
		}
	}
}
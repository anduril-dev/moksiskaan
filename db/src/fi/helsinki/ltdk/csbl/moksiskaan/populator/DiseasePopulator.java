package fi.helsinki.ltdk.csbl.moksiskaan.populator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.anduril.core.utils.Pair;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.BioentityType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Organism;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Xref;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType;

/**
 * Imports diseases with their gene and drug associations from
 * the KEGG DISEASE database.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.09
 */
public class DiseasePopulator {

    /** This LinkAnnotation name is used to store reasoning of links provided by KEGG. */
    static public final String LINK_ANNOTATION_SOURCE = "KEGGDisease";

	private final URL exportFile;

    private final PrintStream out;

	public DiseasePopulator(String      exportURL,
	                        PrintStream log) throws MalformedURLException {
		exportFile = new URL(exportURL);
		out        = log;
	}

	public void populate() throws IOException {
        URLConnection         con      = exportFile.openConnection();
        BufferedReader        in       = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String                key      = null;
        String                value    = null;
        DiseaseTemplate       template = new DiseaseTemplate(System.err);
        List<DiseaseTemplate> diseases = new LinkedList<DiseaseTemplate>();
        String                line;

        while ((line=in.readLine()) != null) {
        	if (line.isEmpty()) continue;
            char c = line.charAt(0);
            if (Character.isWhitespace(c)) {
            	value = line.trim();
            } else {
            	if (line.startsWith("///")) {
            	    if (template.isInteresting()) {
            		   diseases.add(template);
            		}
            		key      = null;
            		value    = null;
            		template = new DiseaseTemplate(out);
            		continue;
            	}
            	int sep = Narggari.whitespace(line);
            	if (sep < 0) {
            		key = line;
            	} else {
            	    key   = line.substring(0,sep);
            	    value = line.substring(sep+1).trim();
            	}
            }
    	    if ("CATEGORY".equals(key)) {
    	        template.setCategories(value);
    	    } else
    	    if ("NAME".equals(key)) {
    	        template.setName(value);
    	    }
       	    if ("GENE".equals(key)) {
       	        template.addGenes(value, true);
    	    } else
    	    if ("MARKER".equals(key)) {
    	        template.addGenes(value, false);
    	    } else
    	    if ("DRUG".equals(key)) {
    	        template.addDrugs(value);
    	    } else
    	    if ("PATHWAY".equals(key)) {
    	        template.setPathway(value);
    	    } else
            if ("DBLINKS".equals(key)) {
    	        template.setICDs(value);
    	    }
        }
        in.close();

        saveDiseases(diseases);

        HibernateUtil.addLogEntry("DiseaseImport",
                                  "Imported "+diseases.size()+" diseases from the KEGG DISEASE database.",
                                  "Kanehisa2009,Kanehisa2011");
        HibernateUtil.getSessionFactory().close();
	}

    private void saveDiseases(List<DiseaseTemplate> diseases) throws IOException {
      out.println("Total of "+diseases.size()+" interesting diseases were found.");

      Set<String> drugsUnknown = new TreeSet<String>();

      final KEGGGeneCache geneCache = new KEGGGeneCache();
      final File          fileCache = new File("etc/keggCache"+IDConstants.Organism_Homo_sapiens+".csv");
      geneCache.setVerbose(out);
      geneCache.prepareGeneCache(fileCache, 500); // Import human data

      Session session = HibernateUtil.getSessionFactory().getCurrentSession();
      session.beginTransaction();

      BioentityType typeDisease  = (BioentityType)session.get(BioentityType.class, IDConstants.BioentityType_disease);
      Organism      organism     = (Organism)     session.get(     Organism.class, IDConstants.Organism_Homo_sapiens);
      LinkType      ltIndication = (LinkType)     session.load(    LinkType.class, IDConstants.LinkType_drug_indication);
      LinkType      ltModel      = (LinkType)     session.load(    LinkType.class, IDConstants.LinkType_pathway_describes);
      XrefType      typeClass    = (XrefType)     session.get(     XrefType.class, IDConstants.XrefType_Disease_classification);
      XrefType      typeCategory = (XrefType)     session.get(     XrefType.class, IDConstants.XrefType_Disease_category);

      Iterator<DiseaseTemplate> dIter = diseases.iterator();
      while (dIter.hasNext()) {
    	  DiseaseTemplate template = dIter.next();
    	  for (String gene : template.getGenes()) {
    	       Bioentity[] entities = geneCache.asBioentities(gene, session);
    	       if (entities.length < 1) {
    	    	   template.removeGene(gene);
    	       }
    	  }
    	  if (!template.isInteresting()) {
    		  dIter.remove();
    		  continue;
    	  }
    	  Bioentity disease = (Bioentity)session.createQuery("FROM  Bioentity "+
                                                             "WHERE (bioentityType.id = "+IDConstants.BioentityType_disease+") AND "+
                                                             "      (name             = :name)")
                                                .setString("name", template.getName())
                                                .uniqueResult();
    	  if (disease == null) {
    		  disease = new Bioentity();
              disease.setBioentityType(typeDisease);
              disease.setOrganism(organism);
    		  disease.setName(template.getName());
    		  session.saveOrUpdate(disease);
    		  for (String category : template.getCategories()) {
    		      Xref xref = new Xref(typeCategory, disease, category);
                  session.saveOrUpdate(xref);
    		  }    		  
    		  for (String icd : template.getICDs()) {
    		      if ((icd == null) || icd.isEmpty()) continue;
    		      Xref xref = new Xref(typeClass, disease, icd);
                  session.saveOrUpdate(xref);
    		  }
    	  }
    	  for (String gene : template.getGenes()) {
   	          Bioentity[]          entities = geneCache.asBioentities(gene, session);
   	          Pair<Integer,String> role     = template.getLinkType(gene);
   	          for (Bioentity source : entities) {
   	        	  LinkType lt = (LinkType)session.load(LinkType.class, role.get1());
   	              HibernateUtil.saveLink(source, disease, lt, LINK_ANNOTATION_SOURCE, role.get2(), session, out);
   	          }
    	  }
    	  for (String drugId : template.getDrugs()) {
    		  if (drugsUnknown.contains(drugId)) continue;
    		  @SuppressWarnings("unchecked")
              List<Bioentity> drugL = (List<Bioentity>)
                                      session.createQuery("SELECT X.bioentity "+
                                                          "FROM   Xref X "+
                                                          "WHERE  (X.xrefType.id = "+IDConstants.XrefType_KEGG_drug+") AND "+
                                                          "       (X.value       = :value)")
                                             .setString("value", drugId)
                                             .list();
    		  if (drugL.isEmpty()) {
    			  drugsUnknown.add(drugId);
    		  } else
    		  for (Bioentity drug : drugL) {
    		      HibernateUtil.saveLink(disease, drug, ltIndication, LINK_ANNOTATION_SOURCE, "[drug entry]", session, out);
    		  }
    	  }
    	  if (template.getPathway() != null) {
              Bioentity pw = (Bioentity)session.createQuery("SELECT X.bioentity "+
                                                            "FROM   Xref X "+
                                                            "WHERE  (X.xrefType.id = "+IDConstants.XrefType_KEGG_pathway+") AND "+
                                                            "       (X.value       = :value)")
                                               .setString("value", template.getPathway())
                                               .uniqueResult();
              if (pw != null) {
                 HibernateUtil.saveLink(pw, disease, ltModel, LINK_ANNOTATION_SOURCE, "[pathway entry]", session, out);
              }
    	  }
      }
      session.getTransaction().commit();
      geneCache.write(fileCache);

      if (!drugsUnknown.isEmpty()) {
         out.println("These drug identifiers were not found from the Moksiskaan database:");
         out.println(drugsUnknown);
      }
    }

    static public void main(String[] argv) throws IOException {
    	if (argv.length != 1) {
    		throw new IllegalArgumentException("Requires one argument that specifies the KEGG export URL.");
    	}
        new DiseasePopulator(argv[0], System.err).populate();
    }

}

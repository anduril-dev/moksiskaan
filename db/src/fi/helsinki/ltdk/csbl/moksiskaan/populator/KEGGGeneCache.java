package fi.helsinki.ltdk.csbl.moksiskaan.populator;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.MainArgument;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.moksiskaan.EntityUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.Version;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;

/**
 * This class is used to convert KEGG gene identifiers to Moksiskaan bioentities.
 *
 * @since {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 0.19
 */
public class KEGGGeneCache {

    private Map<String,Object> geneCache;

    private PrintStream out;

	public void setVerbose(PrintStream output) {
		out = output;
	}

	/**
	 * Prepares the cache for a new set of entities.
	 *
	 * @param localCache An optional file for mapping between KEGG and Ensembl IDs. 
	 * @param newLimit   Positive number determines the number of new ID mappings
	 *                   before they are flushed to the localCache. Negative values
	 *                   inactivate the autosave.
	 */
    public void prepareGeneCache(File localCache,
    		                     int  newLimit) throws IOException {
        println("* preparing gene identifier cache...");
        if (geneCache == null) {
           geneCache = new HashMap<String, Object>(100000);
        } else {
           geneCache.clear();
        }

        if ((localCache != null) && (localCache.exists())) {
        	CSVParser in = new CSVParser(localCache);
        	for (String[] line : in) {
        		String[] ids = AsserUtil.split(line[1]);
        		geneCache.put(line[0], (ids.length==1) ? ids[0] : ids);
        	}
        	in.close();
        }
        println("  - got "+geneCache.size()+" mappings for the Ensembl identifiers");
    }

	private Object getCacheItem(String keggId) {
        Object obj = geneCache.get(keggId);
        if (obj == null) {
           geneCache.put(keggId, EntityUtil.EMPTY_LIST_BIOENTITY);
           println("  - no Ensembl mapping for "+keggId);
        }
        return obj;
    }

    public Bioentity[] asBioentities(String[] genes, Session session) {
        Set<Bioentity> set = null;
        for (String keggId : genes) {
            for (Bioentity entity : asBioentities(keggId, session)) {
                if (set == null) set = new HashSet<Bioentity>();
                set.add(entity);
            }
        }
        if (set == null) {
            return null;
        } else {
            return set.toArray(new Bioentity[set.size()]);
        }
    }

    public Bioentity[] asBioentities(String keggId, Session session) {
        final String FIND_BIOENTITY =
            "SELECT X.bioentity "+
            "FROM   Xref X "+
            "WHERE  (X.xrefType.id = "+IDConstants.XrefType_Ensembl_gene+") AND "+
            "       (X.value       ";

        Object obj = getCacheItem(keggId);
        if (obj == null) {
            obj = EntityUtil.EMPTY_LIST_BIOENTITY;
        } else {
            if (!(obj instanceof Bioentity[])) {
                List<?> findings;
                if (obj instanceof String) {
                    findings = session.createQuery(FIND_BIOENTITY+"= :value)")
                                      .setString("value", (String)obj)
                                      .list();
                } else {
                	findings = session.createQuery(FIND_BIOENTITY+"IN (:values))")
                                      .setParameterList("values", (String[])obj)
                                      .list();
                }
                if (findings.isEmpty()) {
                   println("  - no bioentities for: "+
                		   ((obj instanceof String) ? obj : Arrays.toString((String[])obj)));
                   geneCache.remove(keggId);
                   obj = getCacheItem(keggId);
                   if (obj == null) obj = EntityUtil.EMPTY_LIST_BIOENTITY;
                } else {
                   obj = findings.toArray(new Bioentity[findings.size()]);
                   geneCache.put(keggId, obj);
                }
            }
        }
        return (Bioentity[])obj;
    }

    @SuppressWarnings("unchecked")
	public void write(File file) throws IOException {
    	println("  - saving the cache file: "+file);

    	final String QUERY          = "SELECT DISTINCT value FROM Xref WHERE (xrefType.id = "+
    	                              IDConstants.XrefType_Ensembl_gene+
    	                              ") AND (bioentity IN (:entities))";
    	CSVWriter    fileOut        = new CSVWriter(new String[]{"KEGG","Ensembl"}, file);
    	Session      session        = HibernateUtil.getSessionFactory().getCurrentSession();
        boolean      useTransaction = !session.getTransaction().isActive();

        if (useTransaction) session.beginTransaction();
        String[] keys = geneCache.keySet().toArray(new String[geneCache.size()]);
        Arrays.sort(keys);
    	for (int i=0; i<keys.length; i++) {
    		Object value = geneCache.get(keys[i]);
    		if (value instanceof Bioentity[]) {
    			Bioentity[] v = (Bioentity[])value;
    			if (v.length < 1) continue;
    			try {
    			  List<String> list = (List<String>)session.createQuery(QUERY)
                                                           .setParameterList("entities", v)
                                                           .list();
    			  String[] values = list.toArray(new String[list.size()]);
    			  Arrays.sort(values);
    			  fileOut.write(keys[i], false);
    			  fileOut.write(AsserUtil.collapse(values, AsserUtil.DEFAULT_DELIMITER), false);
    			} catch (Exception err) {
    		      println("Could not get Ensembl identifiers for: "+Arrays.toString(v));
    			  println(err.toString());
    			}
    		} else
    		if (value instanceof String) {
    			fileOut.write(keys[i], false);
    			fileOut.write((String)value, false);
    		} else
    		if (value instanceof String[]) {
    			String[] values = (String[])value;
    			Arrays.sort(values);
    			fileOut.write(keys[i], false);
    			fileOut.write(AsserUtil.collapse(values, AsserUtil.DEFAULT_DELIMITER), false);
        	}
    	}
    	fileOut.close();
    	if (useTransaction) session.getTransaction().commit();
    }

    private void println(String message) {
    	if (out != null) out.println(message);
    }
    
    /**
     * Adds the given id to the target collection.
     * Multiple identifiers are added if they are in a form of a list like
     * HSA:1128 1129 1131 1132.
     */
    static public void split(String text, Collection<String> target) {
    	String id = text.trim();
    	int pos = id.indexOf(' ');
    	if (pos > 0) {
    		String pref = id.substring(0, id.indexOf(':')+1);
    		target.add(id.substring(0,pos));
    		for (String term : id.substring(pos+1).split(" ")) {
    			if (term.indexOf(':') < 0)
    			   target.add(pref+term);
    			else
    			   target.add(term);
    		}
    	} else {
    		target.add(id);
    	}
    }

    static private void printSynopsis(Map<String, MainArgument> args, String msg) {
        System.err.println("KEGGGeneCache v."+Version.getVersion()+" by Marko Laakso");
        System.err.println("------------------------------------");
        System.err.println("KEGGGeneCache [options]");
        MainArgument.printArguments(args, System.err);
        
        if (msg != null) {
            System.err.println();
            System.err.println(msg);
            System.exit(-1);
        }
    }

    static public void main(String[] argv) throws IOException {
       Map<String, MainArgument> args = new HashMap<String, MainArgument>();
       MainArgument argCache          = MainArgument.add("f", "cache", "Ensembl ID cache file", args);
       MainArgument argUpdateN        = MainArgument.add("u",     "n", "Maximum updates before a save", "500", args);
       try { MainArgument.parseInput(argv, args, 0, 0); }
       catch (IllegalArgumentException e) {
           printSynopsis(args, e.getMessage());
           return;
       }
       if (!argCache.isUsed()) {
           printSynopsis(args, "No cache file specified!");
           return;
       }

       File          fileCache = new File(argCache.getValue());
       KEGGGeneCache cache     = new KEGGGeneCache();
       cache.setVerbose(System.out);
       cache.prepareGeneCache(fileCache, argUpdateN.getInt());
       
       Session session = HibernateUtil.getSessionFactory().getCurrentSession();
       session.beginTransaction();
       
       for (Object keggId : new ConcurrentSkipListSet<Object>(cache.geneCache.keySet())) {
           cache.asBioentities((String)keggId, session);
       }
       session.getTransaction().commit();

       cache.write(fileCache);
    }

}
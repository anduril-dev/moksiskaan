package fi.helsinki.ltdk.csbl.moksiskaan.populator;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Organism;

/**
 * This stand-alone Java application replicates protein-protein interactions
 * from PINA to Moksiskaan.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 */
public class PINAPopulator {

    static public final String LINK_ANNOTATION_SOURCE = "PINA";

    private final PrintStream out = System.out;

    private final Map<String,Bioentity[]> entityCache = new HashMap<String,Bioentity[]>();

    public void populate(String exportURL) throws IOException,
                                                  URISyntaxException {
        URL    url         = new URL(exportURL);
        String speciesName = url.toURI().getPath();
        int    separator   = speciesName.lastIndexOf('-');

        if (separator < 0) separator = speciesName.lastIndexOf('.');
        speciesName = speciesName.substring(speciesName.lastIndexOf('/')+1,
                                            separator);

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Organism organism = (Organism)session.createQuery("FROM Organism WHERE name LIKE :name")
                                             .setString("name", speciesName)
                                             .uniqueResult();
        session.getTransaction().commit();
        if (organism == null) {
           throw new IllegalStateException("Local definition of "+speciesName+" is missing.");
        }

        out.println("Trying to fetch data for "+speciesName+'.');
        CSVParser input = new CSVParser(url.openStream(),
                                        CSVParser.DEFAULT_DELIMITER,
                                        0,
                                        "-",
                                        true);
        int count = processPINADump(input);
        input.close();
        HibernateUtil.addLogEntry("PINAImport",
                                  "Total of "+count+" protein-protein interactions obtained from "+url+'.',
                                  "Cowley2012");
        HibernateUtil.getSessionFactory().close();
    }

    private int processPINADump(CSVParser in) {
       int srcCol = in.getColumnIndex("ID(s) interactor A");
       int trgCol = in.getColumnIndex("ID(s) interactor B");
       int refCol = in.getColumnIndex("Interaction identifier(s)");

       Session session = HibernateUtil.getSessionFactory().getCurrentSession();
       session.beginTransaction();

       LinkType linkType = (LinkType)session.load(LinkType.class, IDConstants.LinkType_protein_protein_interaction);
       int      count    = 0;
       out.println("Reading data... "+(in.hasNext()?"[content detected]":"[no content]"));
       while (in.hasNext()) {
          String[] line   = in.next();
          String   source = asUniprot(line[srcCol]);
          String   target = asUniprot(line[trgCol]);

          Bioentity[] sourceEnts = HibernateUtil.geneByUniprot(source, session, entityCache);
                                   if (sourceEnts == null) continue;
          Bioentity[] targetEnts = HibernateUtil.geneByUniprot(target, session, entityCache);
                                   if (targetEnts == null) continue;

          String refs = line[refCol];
          if (refs != null) {
             refs = refs.trim();
             refs = refs.isEmpty() ? null : refs.replace('|', ',');
          }

          for (Bioentity sourceEnt : sourceEnts) {
              for (Bioentity targetEnt : targetEnts) {
                  if (sourceEnt.equals(targetEnt)) {
                      // Skip interactions with the protein itself.
                      // WE ARE LOSING SOME INFORMATION IN HERE!
                      continue;
                  }

                  boolean created;
                  created = HibernateUtil.saveLink(sourceEnt, targetEnt, linkType, LINK_ANNOTATION_SOURCE, refs, session, null) |
                            HibernateUtil.saveLink(targetEnt, sourceEnt, linkType, LINK_ANNOTATION_SOURCE, refs, session, null);
                  if (created) {
                     count++;
                     if (count % 1000 == 0) {
                        session.getTransaction().commit();
                        out.println("- saved links: "+count+" (cache size="+entityCache.size()+')');
                        session = HibernateUtil.getSessionFactory().getCurrentSession();
                        session.beginTransaction();
                     }
                  }
              }
          }
       }
       session.getTransaction().commit();
       out.println("Total of "+count+" protein-protein interactions were saved.");
       return count;
    }

    static private String asUniprot(String text) {
       return text.substring(text.lastIndexOf(':')+1);
    }

    static public void main(String[] argv) throws IOException,
                                                  URISyntaxException {
        new PINAPopulator().populate(argv[0]);
    }

}

package fi.helsinki.ltdk.csbl.moksiskaan.populator;

import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;

/**
 * Interaction rules for Simple Interaction Format (SIF).
 * 
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.00
 */
public enum SIFInteraction {

	/** LinkType mapping is missing */
	COMPONENT_OF                  (true,  null),
	/** LinkType mapping is missing */
	CO_CONTROL                    (false, null),
	/** Directed {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants#LinkType_gene_repression} links. */
	DOWNREGULATE_EXPRESSION       (true,  IDConstants.LinkType_gene_repression),
	/** Undirected {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants#LinkType_protein_binding} links. */
	IN_SAME_COMPONENT             (false, IDConstants.LinkType_protein_binding),
	/** Undirected {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants#LinkType_protein_protein_interaction} links. */
	INTERACTS_WITH                (false, IDConstants.LinkType_protein_protein_interaction),
	/** Directed {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants#LinkType_pathway_precedence} links. */
	METABOLIC_CATALYSIS           (true,  IDConstants.LinkType_pathway_precedence),
	/** LinkType mapping is missing */
	REACTS_WITH                   (false, null),
	/** Directed {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants#LinkType_pathway_precedence} links. */
	SEQUENTIAL_CATALYSIS          (true,  IDConstants.LinkType_pathway_precedence),
	/** Directed {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants#LinkType_protein_state_change} links. */
	STATE_CHANGE                  (true,  IDConstants.LinkType_protein_state_change),
	/** Directed {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants#LinkType_gene_expression} links. */
	UPREGULATE_EXPRESSION         (true,  IDConstants.LinkType_gene_expression),
    ;

	/** True for the rules, which respect their source-target direction. */
	public final boolean isDirected;

	/** Moksiskaan LinkType identifier that shall be used to represent these rules. */
	public final Integer linkTypeId;

	private SIFInteraction(boolean directed,
			               Integer linkType) {
		isDirected = directed;
		linkTypeId = linkType;
	}

}

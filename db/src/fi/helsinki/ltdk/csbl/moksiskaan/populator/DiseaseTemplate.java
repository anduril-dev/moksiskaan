package fi.helsinki.ltdk.csbl.moksiskaan.populator;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import fi.helsinki.ltdk.csbl.anduril.core.utils.Pair;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;

/**
 * Represents a disease as fetched from the KEGG DISEASE database.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.09
 */
public class DiseaseTemplate {

	static public final String[] WORDS_INACTIVE = {
		"abnormal splice variants",
		"absent",
		"decreased expression",
		"deletion",
		"downregulation",
		"inactivation",
		"inversion",
		"LOH",
		"loss",
		"mutation",
		"null alleles",
		"underexpression"
	};
	static public final String[] WORDS_ACTIVE = {
		"activating mutation",
		"amplification",
		"duplication",
		"expression",
		"increased expression",
		"overexpression",
	};

	private String name;

	private String[] categories;

	private String[] icd;

	private String pathway;

	private Set<String> genes;

	private Map<String,Pair<Integer,String>> roleMap;

	private Set<String> drugs;

	private final PrintStream out;

	public DiseaseTemplate(PrintStream out) {
		this.out = out;
	}

	public String getName() { return name; }

	public void setName(String name) {
		int pos = name.indexOf(", ");
		String newName = (pos<0) ? name : name.substring(0,pos);
		pos = newName.indexOf(';');
		if (pos >= 0) {
			newName = newName.substring(0,pos);
		}

		if (this.name == null) {
			this.name = newName;
		} else {
			out.println(newName+" is called "+this.name+'.');
		}
	}

	public String[] getCategories() {
		return (categories==null) ? AsserUtil.EMPTY_ARRAY_STRING : categories;
	}

	public void setCategories(String list) {
		assert(this.categories == null);

		Set<String> cTmp = new HashSet<String>();
		for (String c : AsserUtil.split(list, "; ")) {
			if (!c.isEmpty()) cTmp.add(c);
		}
		this.categories = cTmp.toArray(new String[cTmp.size()]);
	}

	public String[] getICDs() {
		return (icd==null) ? AsserUtil.EMPTY_ARRAY_STRING : icd;
	}

	public void setICDs(String ref) {
		if (ref.indexOf("ICD-10:") < 0) return;
		assert(this.icd == null);
		this.icd = AsserUtil.split(ref.substring(8),"[, ;-]");
	}

	public String getPathway() { return pathway; }

	public void setPathway(String text) {
		int pos = text.indexOf("hsa");
		if (pos >= 0) {
			int pos2 = text.indexOf(' ');
			pathway = (pos2 < 0) ? text : text.substring(pos, pos2); 
			pos     = pathway.indexOf('(', 3);
			if (pos > 0) pathway = pathway.substring(0, pos);
			pathway = "path:"+pathway;
		}
	}

	public Pair<Integer,String> getLinkType(String gene) {
	    final Pair<Integer,String> DEFAULT_ROLE = new Pair<Integer,String>(IDConstants.LinkType_biomarker,
	                                                                       "[unknown association type]");

		Pair<Integer,String> t = (roleMap==null) ? null : roleMap.get(gene);
		return (t==null) ? DEFAULT_ROLE : t;
	}

	public String[] getGenes() {
		if ((genes == null) || genes.isEmpty())
			return AsserUtil.EMPTY_ARRAY_STRING;
		return genes.toArray(new String[genes.size()]);
	}

	public void removeGene(String id) {
		genes.remove(id);
		if (roleMap != null) roleMap.remove(id);
	}

	public void addGenes(String text, boolean isGene) {
		int pos = text.indexOf("[HSA:");
		if (pos < 0) {
			if (isGene) out.println("No KEGG identifier in "+text+" list given to "+name+'.');
			return;
		}

		String gene = text.substring(pos+1, text.indexOf(']',pos+5));
		gene = gene.toLowerCase(Locale.US);

		final String ROLE_PREFIX = " (";
		String role = text.substring(0, pos);
		pos = role.indexOf(ROLE_PREFIX);
		if (pos > 0) {
			role = role.substring(pos+2, role.indexOf(')',pos+3));
			pos  = role.indexOf(ROLE_PREFIX);
			if (pos > 0) role = role.substring(0, pos);
		} else {
			role = null;
		}
		Integer linkT = linkTypeOf(role);

		if (genes == null) {
			genes = new HashSet<String>();
		}
		if (linkT == null) {
		   KEGGGeneCache.split(gene, genes);
		} else {
			if (roleMap == null) roleMap = new HashMap<String,Pair<Integer,String>>();
			Set<String> tmpSet = new HashSet<String>();
			KEGGGeneCache.split(gene, tmpSet);
			Pair<Integer,String> ltr = new Pair<Integer,String>(linkT,role);
			for (String g : tmpSet) {
				Pair<Integer,String> oldRole = roleMap.put(g, ltr);
				if ((oldRole != null) && !oldRole.get1().equals(linkT)) {
					out.println("Warning: link type "+oldRole+" replaced with "+linkT+" in "+name+'.');
				}
				genes.add(g);
			}
		}
	}

	public String[] getDrugs() {
		return (drugs==null) ? AsserUtil.EMPTY_ARRAY_STRING : drugs.toArray(new String[drugs.size()]);
	}

	public void addDrugs(String text) {
		int pos = text.indexOf("[D");
		if (pos < 0) {
			out.println("No drug identifier in "+text+" given to "+name+'.');
			return;
		}

		String drug = text.substring(pos+1, text.indexOf(']',pos+4));
		drug = drug.replaceAll("DR:", AsserUtil.EMPTY_STRING);

		if (drugs == null) {
			drugs = new HashSet<String>();
		}
		KEGGGeneCache.split(drug, drugs);		
	}

	public boolean isInteresting() {
		return ((genes != null) && !genes.isEmpty());
	}

	@Override
	public String toString() {
		StringBuffer s = new StringBuffer(512);
		s.append(name);
		if (categories != null) {
			s.append(' ').append(Arrays.toString(categories));
		}
		if (genes != null) {
			s.append("\n  genes: [");
			boolean isFirst = true;
			for (String g : genes) {
				if (isFirst) { isFirst = false; } else { s.append(", "); }
				s.append(g);
				if (roleMap != null) {
					Pair<Integer,String> r = roleMap.get(g);
					if (r != null) {
						s.append('(').append(r).append(')');
					}
				}
			}
			s.append(']');
		}
		if (drugs != null) {
			s.append("\n  drugs: ").append(drugs);
		}
		return s.toString();
	}

	private Integer linkTypeOf(String text) {
		if (text == null) return null;

		boolean inactive = false;
		for (String word : WORDS_INACTIVE) {
			if (text.indexOf(word) >= 0) {
				inactive = true;
				break;
			}
		}

		boolean active = false;
		for (String word : WORDS_ACTIVE) {
			if (text.indexOf(word) >= 0) {
				active = true;
				break;
			}
		}

		if (inactive && !active)   return IDConstants.LinkType_negative_regulation;
		if (active   && !inactive) return IDConstants.LinkType_positive_regulation;
		if (!active  && !inactive) out.println("No roles assigned for '"+text+"'.");
		return null;
	}

}

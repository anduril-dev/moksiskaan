package fi.helsinki.ltdk.csbl.moksiskaan.populator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.anduril.component.Tools;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.MainArgument;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;

/**
 * Import pathway models from Pathway Commons.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.00
 */
public final class PCPopulator {

	/** This LinkAnnotation name is used to store original sources of links provided by Pathway Commons. */
	static public final String LINK_ANNOTATION_SOURCE = "PathwayCommons";

	private final PrintStream out = System.out;

	private boolean skipInteractsWith;

	public void populate(String  edgeURL) throws MalformedURLException, IOException {
		out.println("Loading pathway connections...");
		loadEdges(edgeURL);
		out.println("Pathway Commons import completed!");
    }

	private void loadEdges(String file) throws MalformedURLException, IOException {
	    final String            UNIPROT  = "uniprot";
		Map<String,Bioentity[]> nodes    = new HashMap<String,Bioentity[]>(30000);
		URL                     url      = new URL(file);
		URLConnection           con      = url.openConnection();
		boolean                 useCache = !"file".equals(url.getProtocol());
		File                    tmpFile  = null;
		InputStream             inStream;

		if (useCache) {
			tmpFile = File.createTempFile("PCPopulatorCache", ".gz");
			Tools.copyStreamToFile(con.getInputStream(), tmpFile);
			inStream = new FileInputStream(tmpFile);
		} else {
			inStream = con.getInputStream();
		}
		GZIPInputStream zin = new GZIPInputStream(inStream);

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        // Load LinkTypes for all SIF rules
        Map<Integer,LinkType> lTypes = new HashMap<Integer,LinkType>();
        for (SIFInteraction sif : SIFInteraction.values()) {
        	if ((sif.linkTypeId != null) && !lTypes.containsKey(sif.linkTypeId)) {
        	   lTypes.put(sif.linkTypeId, (LinkType)session.get(LinkType.class, sif.linkTypeId));
        	}
        }

        if (skipInteractsWith) out.println("- skipping "+SIFInteraction.INTERACTS_WITH+" relations");
		out.println("- parsing "+file);
		CSVParser in  = new CSVParser(zin,
				                      CSVParser.DEFAULT_DELIMITER,
				                      0,
				                      CSVParser.DEFAULT_MISSING_VALUE_SYMBOL,
				                      false);
		int colGeneS = AsserUtil.indexOf("PARTICIPANT_A",           in.getColumnNames(), true, true);
		int colGeneT = AsserUtil.indexOf("PARTICIPANT_B",           in.getColumnNames(), true, true);
		int colLinkT = AsserUtil.indexOf("INTERACTION_TYPE",        in.getColumnNames(), true, true);
		int colSrc   = AsserUtil.indexOf("INTERACTION_DATA_SOURCE", in.getColumnNames(), true, true);
		int cNew     = 0;
		int cAll     = 0;
		int cIter    = 0;
		for (String[] line : in) {
		    // Take Uniprot IDs...
		    if (line[colGeneS].indexOf(UNIPROT) < 0 ||
		        line[colGeneT].indexOf(UNIPROT) < 0)  continue;
		    line[colGeneS] = line[colGeneS].substring(line[colGeneS].lastIndexOf('/')+1);
		    line[colGeneT] = line[colGeneT].substring(line[colGeneT].lastIndexOf('/')+1);

			SIFInteraction sifRule = SIFInteraction.valueOf(line[colLinkT]);
			if ((sifRule.linkTypeId==null) ||
				(skipInteractsWith && (sifRule==SIFInteraction.INTERACTS_WITH)))
			   continue;

			Bioentity[] geneS = HibernateUtil.geneByUniprot(line[colGeneS], session, nodes); if (geneS==null) continue;
			Bioentity[] geneT = HibernateUtil.geneByUniprot(line[colGeneT], session, nodes); if (geneT==null) continue;
			LinkType    lType = lTypes.get(sifRule.linkTypeId);
			for (int s=0; s<geneS.length; s++) for (int t=0; t<geneT.length; t++) {
                if (HibernateUtil.saveLink(geneS[s], geneT[t], lType, LINK_ANNOTATION_SOURCE, line[colSrc], session, null)) {
				   cNew++;
		    	}
			    if (!sifRule.isDirected &&
			        HibernateUtil.saveLink(geneT[t], geneS[s], lType, LINK_ANNOTATION_SOURCE, line[colSrc], session, null)) {
				   cNew++;
			    }
			}
			cIter++;
			cAll += sifRule.isDirected ? 1 : 2;
			if ((cIter%1000)==0) {
				session.getTransaction().commit();
                out.println("  "+cNew+" out of "+cAll+" possible links saved as new (by line "+in.getLineNumber()+')');
                session = HibernateUtil.getSessionFactory().getCurrentSession();
                session.beginTransaction();
			}
		}
		in.close();
		zin.close();
		if (useCache) tmpFile.delete();

		session.getTransaction().commit();
		out.println("- total of "+cNew+" new links were found");
		out.println("  "+(cAll-cNew)+" links were already present in local database");
		HibernateUtil.addLogEntry("PathwayCommonsImport",
				                  "Total of "+cNew+" new links obtained from "+file+'.',
				                  "Cerami2010");
		HibernateUtil.getSessionFactory().close();
	}

	/**
	 * Save Pathway Commons export for the given organism into the local Moksiskaan database.
	 */
    static public void main(String[] argv) throws MalformedURLException, IOException {
        Map<String, MainArgument> args = new HashMap<String, MainArgument>();
        MainArgument argEurl = MainArgument.add("edges",   "URL", "URL for the edge attribute export", args);
        MainArgument argPiP  = MainArgument.add("iw",             "Import 'interacts with' relations", args);
        try { MainArgument.parseInput(argv, args, 0, 0); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            System.err.println();
            MainArgument.printArguments(args, System.err);
            System.exit(-1); return;
        }
        if (!argEurl.isUsed()) {
        	System.err.println(argEurl.getDescription()+" is missing.");
        	System.exit(-2); return;
        }

        PCPopulator populator = new PCPopulator();
        populator.skipInteractsWith = !argPiP.isUsed();
        populator.populate(argEurl.getValue());
    }

}
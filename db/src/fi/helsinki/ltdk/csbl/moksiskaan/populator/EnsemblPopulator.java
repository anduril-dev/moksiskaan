package fi.helsinki.ltdk.csbl.moksiskaan.populator;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.asser.annotation.IDConverter;
import fi.helsinki.ltdk.csbl.asser.db.JDBCConnection;
import fi.helsinki.ltdk.csbl.asser.db.ResultSetProcessor;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.*;

/**
 * Populates Moksiskaan database with Ensembl genes and proteins.
 */
public class EnsemblPopulator {

    static private final int MAX_COUNT = 100000;

    static private final String SELECT_GENES =
    "SELECT X.display_label, G.stable_id, "+
    "       SR.name, G.seq_region_start, G.seq_region_end, G.seq_region_strand, "+
    "       G.gene_id "+
    "FROM   gene           AS G, "+
    "       xref           AS X, "+
    "       seq_region     AS SR, "+
    "       coord_system   AS CS "+
    "WHERE  (G.display_xref_id  = X.xref_id)          AND "+
    "       (G.is_current       = 1)                  AND "+
  //"       (G.status           = 'KNOWN')            AND "+
    "       (CS.coord_system_id = SR.coord_system_id) AND "+
    "       (CS.name            = 'chromosome')       AND "+
    "       (CS.attrib          = 'default_version')  AND "+
    "       (SR.seq_region_id   = G.seq_region_id)    AND "+
    "       (SR.name              NOT LIKE '%\\_%')";

    static private final String SELECT_XREFS =
    "SELECT DISTINCT OX.ensembl_id, X.dbprimary_acc "+
    "FROM   object_xref OX, xref X, external_db ED "+
    "WHERE  (OX.ensembl_object_type = 'Gene')           AND "+
    "       (OX.xref_id             = X.xref_id)        AND "+
    "       (ED.external_db_id      = X.external_db_id) AND "+
    "       (ED.db_name             = ?)";
    
    static private final String SELECT_GENE_PROTEINS =
    "SELECT DISTINCT X.display_label, X.dbprimary_acc, T.gene_id "+
    "FROM   xref         AS X, "+
    "       object_xref  AS OX, "+
    "       external_db  AS ED, "+
    "       seq_region   AS SR, "+
    "       coord_system AS CS, "+
    "       transcript   AS T "+
    "WHERE  (OX.ensembl_object_type = 'Translation')              AND "+
    "       (OX.xref_id             = X.xref_id)                  AND "+
    "       (OX.ensembl_id          = T.canonical_translation_id) AND "+
    "       (ED.external_db_id      = X.external_db_id)           AND "+
    "       (ED.db_name             = 'Uniprot/SWISSPROT')        AND "+
    "       (ED.status              = 'KNOWN')                    AND "+
    "       (T.is_current           = 1)                          AND "+
    "       (T.status               = 'KNOWN')                    AND "+
    "       (CS.coord_system_id     = SR.coord_system_id)         AND "+
    "       (CS.name                = 'chromosome')               AND "+
    "       (CS.attrib              = 'default_version')          AND "+
    "       (SR.seq_region_id       = T.seq_region_id)            AND "+
    "       (SR.name                  NOT LIKE '%\\_%')";

    private final PrintStream out = System.out;

    private final String cfgFile;
    private final int    organism;

    private EnsemblPopulator(int organismId, String cfg) {
        cfgFile  = cfg;
        organism = organismId;
    }

	public void populate() throws SQLException,
                                  IOException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
		Organism org = (Organism)session.get(Organism.class, organism);
		session.getTransaction().commit();

        if (org == null)
            throw new IllegalArgumentException("No organisms ("+organism+") found from the local database.");
        out.println("Ensembl data import for Moksiskaan");
        out.println("*** Processing "+org.getName()+"...");
        String message = populate(org);

        HibernateUtil.addLogEntry("EnsemblImport", message, "Flicek2011");
        HibernateUtil.getSessionFactory().close();
    }

    public String populate(Organism species) throws SQLException,
                                                    IOException {
        JDBCConnection   db = new JDBCConnection(new File(cfgFile));
        Session          session;
        EntityProcessor  ep;
        XrefProcessor    ap1;
        ProteinProcessor pp;
        BioentityType    bType;
        XrefType         xType;
        LinkType         lType;
        Map<Long, Xref>  genes;

        out.println("    - source database: "+db.getURL());

        // Get all genes
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        bType = (BioentityType)session.get(BioentityType.class, IDConstants.BioentityType_gene);
        xType =      (XrefType)session.get(     XrefType.class, IDConstants.XrefType_Ensembl_gene);
        genes = new HashMap<Long,Xref>(50000);
        ep    = new EntityProcessor(species, bType, xType, session, genes);
        db.doSelect(SELECT_GENES, ep);
        session.getTransaction().commit();

        // Add external references to the genes
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        xType = (XrefType)session.get(XrefType.class, IDConstants.XrefType_Entrez_gene);
        ap1   = new XrefProcessor(xType, session, genes);
        db.doSelect(SELECT_XREFS, ap1, "EntrezGene");
        session.getTransaction().commit();
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        xType = (XrefType)session.get(XrefType.class, IDConstants.XrefType_miRBase_sequence);
        ap1   = new XrefProcessor(xType, session, genes);
        db.doSelect(SELECT_XREFS, ap1, "miRBase");
        session.getTransaction().commit();

        // Add genes to their target proteins
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        out.println("    - binding genes to their proteins...");
        bType = (BioentityType)session.get(BioentityType.class, IDConstants.BioentityType_protein);
        xType =      (XrefType)session.get(     XrefType.class, IDConstants.XrefType_UniProt_protein);
        lType = (LinkType)session.get(LinkType.class, IDConstants.LinkType_protein_coding_gene);
        pp    = new ProteinProcessor(species, bType, xType, lType, session, genes);
        db.doSelect(SELECT_GENE_PROTEINS, pp);
        session.getTransaction().commit();
        out.println("      total of "+pp.count+" proteins were created");

        db.close();
        return db.getURL()+" ("+genes.size()+" genes, "+pp.count+" proteins)";
    }

    static public void main(String[] argv) throws IOException, SQLException {
        new EnsemblPopulator(Integer.parseInt(argv[0]),
        		             argv.length==2 ? argv[1] : IDConverter.CFG_FILE)
        .populate();
    }

    /**
     * Imports the requested bioentities into the local database.
     */
    private class EntityProcessor implements ResultSetProcessor {

        final Organism       species;
        final BioentityType  bType;
        final XrefType       xType;
        final Session        session;
        final Map<Long,Xref> results;

        EntityProcessor(Organism       organism,
                        BioentityType  type,
                        XrefType       xref,
                        Session        transaction,
                        Map<Long,Xref> resMap) {
            this.species = organism;
            this.bType   = type;
            this.xType   = xref;
            this.session = transaction;
            this.results = resMap;
        }

        protected Xref saveEntity(ResultSet rs) throws SQLException {
            Bioentity entity = new Bioentity();
            Xref      link   = new Xref();

            entity.setName(rs.getString(1));
            entity.setOrganism(species);
            entity.setBioentityType(bType);
            link.setValue(rs.getString(2));
            link.setBioentity(entity);
            link.setXrefType(xType);

            session.save(entity);
            session.save(link);
            return link;
        }

        public void readResults(ResultSet rs) throws SQLException {
            out.println("    - saving "+bType.getName()+" entities...");
            int count = 0;
            while (rs.next()) {
                if (++count > MAX_COUNT) {
                    out.println("      obtained first "+MAX_COUNT+" and the rest were skipped");
                    return;
                }

                Xref      link   = saveEntity(rs);
                Bioentity entity = link.getBioentity();
                DNARegion region = new DNARegion(entity,
                                                 rs.getString(3),
                                                 rs.getLong(4),
                                                 rs.getLong(5),
                                                 rs.getInt(6)==1);
                session.save(region);
                results.put(rs.getLong(7), link);
            }
            out.println("      total of "+count+" items were found");
        }

    }

    /**
     * Imports additional external references into the local database.
     */
    private class XrefProcessor implements ResultSetProcessor {

        final XrefType       xType;
        final Session        session;
        final Map<Long,Xref> bioentities;

        XrefProcessor(XrefType       xref,
                      Session        transaction,
                      Map<Long,Xref> entities) {
            this.xType       = xref;
            this.session     = transaction;
            this.bioentities = entities;
        }

        public void readResults(ResultSet rs) throws SQLException {
          out.println("    - saving "+xType.getName()+" references...");
          while (rs.next()) {
            Long geneId = rs.getLong(1);
            Xref ensg   = bioentities.get(geneId);

            if (ensg == null) continue;

            Bioentity entity = ensg.getBioentity();
            Xref      ref    = new Xref();
            ref.setBioentity(entity);
            ref.setXrefType(xType);
            ref.setValue(rs.getString(2));
            session.save(ref);
          }
        }

    }

    /**
     * Creates proteins for the listed genes.
     */
    private class ProteinProcessor extends EntityProcessor {

        private final LinkType        lType;
        private final Map<Long, Xref> genes;
        private int                   count = 0;

        ProteinProcessor(Organism       organism,
                         BioentityType  type,
                         XrefType       xref,
                         LinkType       ltype,
                         Session        transaction,
                         Map<Long,Xref> geneMap) {
        	super(organism, type, xref, transaction, null);
        	this.lType = ltype;
        	this.genes = geneMap;
        }

        public void readResults(ResultSet rs) throws SQLException {
            while (rs.next()) {
            	Long gId  = rs.getLong(3);
            	Xref ensg = genes.get(gId);
            	if (ensg == null) {
            		out.println("      "+gId+" is not available locally");
                    continue;
            	}

                Xref uniprot = saveEntity(rs);

                Link link = new Link();
                link.setLinkType(lType);
                link.setSource(ensg.getBioentity());
                link.setTarget(uniprot.getBioentity());
                session.save(link);
                count++;
            }
        }

    }

}

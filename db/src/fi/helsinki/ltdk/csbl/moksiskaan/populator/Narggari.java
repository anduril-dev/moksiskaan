package fi.helsinki.ltdk.csbl.moksiskaan.populator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.BioentityType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Link;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Organism;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Xref;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType;

/**
 * Populates Moksiskaan database with KEGG drugs.
 *
 * @since {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 0.19
 */
public class Narggari {

	static private final String GENE_PREFIX = "[HSA:";

	static private final String[] WORDS_SKIP = {
		"-mutation"
	};

	static private final String[] WORDS_INHIBITION = {
		"antagonist", "antibody", "blocker", "inhibition", "inhibitor"
	};

	static private final String[] WORDS_PROMOTION = {
		" activator", " agonist", "analogue", "induction", "opener", "reactivator", "stimulant"
	};

	private final URL exportFile;

    private final XrefType typeKEGGDrug;
    private final XrefType typeDrugBankDrug;

    private final BioentityType typeDrug;

    private final LinkType typeDrugInhibits;
    private final LinkType typeDrugMetabolism;
    private final LinkType typeDrugPromotes;
    private final LinkType typeDrugTarget;
    private final LinkType typeBiomarker;

    private final Organism organism;

    private final KEGGGeneCache geneCache;

    private final Map<String,Bioentity> formulaCache = new HashMap<String,Bioentity>(5000);

	public Narggari(String exportURL) throws MalformedURLException {
		exportFile = new URL(exportURL);

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        organism           = (Organism)     session.get(     Organism.class, IDConstants.Organism_Homo_sapiens);
        typeDrugInhibits   = (LinkType)     session.get(     LinkType.class, IDConstants.LinkType_drug_inhibits);
        typeDrugMetabolism = (LinkType)     session.get(     LinkType.class, IDConstants.LinkType_drug_metabolism);
        typeDrugPromotes   = (LinkType)     session.get(     LinkType.class, IDConstants.LinkType_drug_promotes);
        typeDrugTarget     = (LinkType)     session.get(     LinkType.class, IDConstants.LinkType_drug_target);
        typeBiomarker      = (LinkType)     session.get(     LinkType.class, IDConstants.LinkType_biomarker);
        typeDrug           = (BioentityType)session.get(BioentityType.class, IDConstants.BioentityType_drug);
        typeDrugBankDrug   = (XrefType)     session.get(     XrefType.class, IDConstants.XrefType_DrugBank_drug);
        typeKEGGDrug       = (XrefType)     session.get(     XrefType.class, IDConstants.XrefType_KEGG_drug);
        session.getTransaction().commit();

        geneCache = new KEGGGeneCache();
        geneCache.setVerbose(System.out);
	}

	public void populate() throws IOException {
        URLConnection      con   = exportFile.openConnection();
        BufferedReader     in    = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String             key   = null;
        String             value = null;
        final DrugTemplate drug  = new DrugTemplate();
        int                pos;
        String             line;

        final File fileCache = new File("etc/keggCache"+IDConstants.Organism_Homo_sapiens+".csv");
        geneCache.prepareGeneCache(fileCache, 500); // Import human data

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        while ((line=in.readLine()) != null) {
        	if (line.isEmpty()) continue;
            char c = line.charAt(0);
            if (Character.isWhitespace(c)) {
            	value = line.trim();
            } else {
            	if (line.startsWith("///")) {
            		saveDrug(drug, session);
            		key   = null;
            		value = null;
            		drug.reset();
            		continue;
            	}
            	int sep = whitespace(line);
            	if (sep < 0) {
            		key = line;
            	} else {
            	    key   = line.substring(0,sep);
            	    value = line.substring(sep+1).trim();
            	    if ("ENTRY".equals(key)) {
            	    	drug.setId(value);
            	    	continue;
            	    } else
            	    if ("NAME".equals(key)) {
            	    	drug.setName(value);
            	    	continue;
            	    }
            	}
            }
            if ("FORMULA".equals(key)) {
                drug.setFormula(value);
            } else
            if ("DBLINKS".equals(key)) {
            	pos = value.indexOf("DrugBank:");
            	if (pos >= 0) {
            		drug.drugBank = value.substring(10);
            	}
            } else
            if (value != null) {
               Bioentity[] genes = getGenes(value, session, in);
               if (genes != null && "TARGET".equals(key)){
                  asLinks(drug, genes, key, value);
               }
            }
        }
        in.close();

        session.getTransaction().commit();
        HibernateUtil.addLogEntry("Narggari",
                                  "Drug target import from the KEGG DRUG database.",
                                  "Kanehisa2009,Kanehisa2011");
        geneCache.write(fileCache);
        HibernateUtil.getSessionFactory().close();
	}

	private void saveDrug(DrugTemplate template, Session session) {
	    if (!template.isValid()) return;

	    String key = template.getCacheKey();

        Bioentity drug = formulaCache.get(key);
        if (drug == null) {
           drug = (Bioentity)session.createQuery("SELECT X.bioentity "+
                                                 "FROM   Xref X "+
                                                 "WHERE  (X.xrefType.id = "+IDConstants.XrefType_KEGG_drug+") AND "+
                                                 "       (X.value       = :value)")
                                    .setString("value", template.id)
                                    .uniqueResult();
        } else {
           System.out.println("  - "+template.name+" ("+template.id+") replaced with "+
                              drug.getName());
           String name = drug.getName();
           int    pos  = name.indexOf(' ');
           if (pos > 0) {
               name = name.substring(0, pos);
               if (template.name.startsWith(name)) {
                  System.out.println("    Renaming "+drug.getName()+" to "+name+'.');
                  drug.setName(name);
                  session.saveOrUpdate(drug);
               } else {
                  System.out.println("    Incompatible names of "+drug.getName()+" and "+template.name+'.');
               }
               Xref xref = new Xref(typeKEGGDrug, drug, template.id);
               session.saveOrUpdate(xref);
               if (template.drugBank != null) {
                  xref = (Xref)session.createQuery("FROM Xref WHERE "+
                                                   "(bioentity.id = :id)   AND "+
                                                   "(xrefType.id  = :type) AND "+
                                                   "(value        = :value)")
                                                  .setLong(   "id",    drug.getBioentityId())
                                                  .setInteger("type",  IDConstants.XrefType_DrugBank_drug)
                                                  .setString( "value", template.drugBank)
                                                  .uniqueResult();
                  if (xref == null) {
                     xref = new Xref(typeDrugBankDrug, drug, template.drugBank);
                     session.saveOrUpdate(xref);
                  }
               }
           }
           key = null;
        }
        if ((drug == null) && (template.drugBank != null)) {
           @SuppressWarnings("unchecked")
           List<Bioentity> drugs = (List<Bioentity>)
                                   session.createQuery("SELECT X.bioentity "+
                                                       "FROM   Xref X "+
                                                       "WHERE  (X.xrefType.id = "+IDConstants.XrefType_DrugBank_drug+") AND "+
                                                       "       (X.value       = :value)")
                                          .setString("value", template.drugBank)
                                          .list();
           if (!drugs.isEmpty()) {
        	   drug = drugs.get(0);
        	   if (drugs.size() > 1) {
        		   System.err.println("warning: multiple DrugBank drugs for "+template.drugBank+
        				              ":\n         "+drugs+"\n         using "+drug+'.');
        	   }
           }
        }
        if (drug == null) {
           System.out.println("    save("+template.id+", "+template.name+')');
           drug = new Bioentity();
           drug.setName(template.name);
           drug.setBioentityType(typeDrug);
           drug.setOrganism(organism);
           session.saveOrUpdate(drug);
           Xref xref = new Xref(typeKEGGDrug, drug, template.id);
           session.saveOrUpdate(xref);
           if (template.drugBank != null) {
        	   xref = new Xref(typeDrugBankDrug, drug, template.drugBank);
               session.saveOrUpdate(xref);
           }
        }
        if (key != null) formulaCache.put(key, drug);

        for (Link link : template.links) {
        	LinkType lt = link.getLinkType();
        	if (typeBiomarker.equals(lt)) {
	            HibernateUtil.saveLink(link.getTarget(), drug,
                                       lt,
                                       null, null,
                                       session, null);        		
        	} else {
	            HibernateUtil.saveLink(drug, link.getTarget(),
	                                   lt,
	                                   null, null,
	                                   session, null);
        	}
        }
    }

	private void asLinks(DrugTemplate drug,
			             Bioentity[]  genes,
			             String       key,
			             String       value) {
	    if (contains(value, WORDS_SKIP)) {
		   System.err.println("Skipped "+drug.name+" ("+drug.id+") "+key+": "+value);
		   return;
		}

		LinkType type      = null;
		boolean  ambiguous = false;
		if (contains(value, WORDS_INHIBITION)) {
		   type = typeDrugInhibits;
		}
        if (contains(value, WORDS_PROMOTION)) {
            if (type == null) {
                type = typeDrugPromotes;
            } else {
                type      = null;
                ambiguous = true;
            }
        }
		if (type == null) {
			if ("METABOLISM".equals(key) ||
				("INTERACTION".equals(key) && (value.indexOf("Enzyme") >= 0))) {
				type = typeDrugMetabolism;
			} else
			if (("INTERACTION".equals(key) || "PRODUCT".equals(key))
			   && (value.indexOf("Genomic biomarker") >= 0)) {
				type = typeBiomarker;
			} else
		    if (ambiguous || !"BRITE".equals(key)) {
                System.err.println(drug.name+" ("+drug.id+") has an "+
                                      (ambiguous ? "ambiguous " : "unknown ")+
                                      key+" role in: "+value);
		    	if (ambiguous) {
		    		type = typeDrugTarget;
		    	} else {
		    	    return;
		    	}
			} else {
                return; // No link type
			}
		}

		for (Bioentity gene : genes) {
			Link link = new Link();
			link.setLinkType(type);
			link.setTarget(gene);
			drug.addLink(link);
		}
	}

	private Bioentity[] getGenes(String         textIn,
			                     Session        session,
			                     BufferedReader in) throws IOException {
		final int    P_LEN = GENE_PREFIX.length();
		List<String> genes = null;
		String       text  = textIn;

		geneLoop: for (int i=text.indexOf(GENE_PREFIX); i>0; i=text.indexOf(GENE_PREFIX, i+1)) {
			if (genes == null) genes = new ArrayList<String>();
			int e;
			while ((e=text.indexOf(']', i+P_LEN)) < 0) {
				String line = in.readLine();
				if (Character.isWhitespace(line.charAt(0))) {
					text = text+' '+line.trim();
				} else {
					System.err.println("Invalid gene reference: "+text);
					System.err.println("      ...next line was: "+line);
					break geneLoop;
				}
			}
            KEGGGeneCache.split(text.substring(i+1,e).toLowerCase(), genes);
            i = e;
		}
		return (genes == null) ? null :
			                     geneCache.asBioentities(genes.toArray(new String[genes.size()]), session);
	}

	/**
	 * Tells if the given text contains any of the given substrings.
	 */
	static public boolean contains(String text, String[] words) {
	    for (String word : words) {
            if (text.indexOf(word) >= 0) {
                return true;
            }
        }
	    return false;
	}

	static public int whitespace(String text) {
		int length = text.length();
		for (int i=0; i<length; i++) {
			if (Character.isWhitespace(text.charAt(i)))
				return i;
		}
		return -1;
	}

    static public void main(String[] argv) throws IOException {
    	if (argv.length != 1) {
    		throw new IllegalArgumentException("Requires one argument that specifies the KEGG export URL.");
    	}
        new Narggari(argv[0]).populate();
    }

}

/**
 * This reusable template is used to accumulate details about an individual drug and its relations.
 * The content of the template may be cleaned as a new drug entry is encountered while the
 * previous drug may be processed before doing this.
 */
class DrugTemplate {

    static private final String[] WORDS_NO_JOIN = {
        "alfa", "beta", "delta", "gamma", "lambda"
    };

	String    id;
    String    formula;
	String    name;
	String    drugBank;
	Set<Link> links = new HashSet<Link>();

	void reset() {
		id       = null;
		name     = null;
		formula  = null;
		drugBank = null;
		links.clear();
	}

	void addLink(Link nLink) {
		Long bId = nLink.getTarget().getBioentityId();
		Iterator<Link> linkIt = links.iterator();
		while (linkIt.hasNext()) {
			Link oLink = linkIt.next();
			if (bId.equals(oLink.getTarget().getBioentityId())) {
				int oType = oLink.getLinkType().getLinkTypeId();
				int nType = nLink.getLinkType().getLinkTypeId(); 
				if ((nType == IDConstants.LinkType_drug_target) || (oType == nType))
					return;
				if (oType == IDConstants.LinkType_drug_target)
					linkIt.remove();
			}
		}
		links.add(nLink);
	}

	void setId(String value) {
		if (id != null) throw new IllegalStateException("Multiple identifiers for the same drug: "+id+", "+value);
		int pos = Narggari.whitespace(value);
		if (pos > 0) {
			id = value.substring(0, pos);
		} else {
			id = value;
		}
	}

	void setName(String value) {
		if (name != null) return; // Keep the first alias!
		String text = value;
		int    pos  = text.indexOf('(');
		if (pos > 0) text = text.substring(0, pos-1);
		pos = text.indexOf(';');
		if (pos > 0) text = text.substring(0, pos);
		name = text.trim();
	}

    void setFormula(String value) {
        String text = value;
        int    pos  = text.indexOf('.');
        if (pos > 0) text = text.substring(0, pos);
        formula = text.trim();
    }

    String getCacheKey() {
        if (formula == null) {
            return name;
        } else {
            String t = name;
            if ((formula.charAt(0) != '(') && !Narggari.contains(t, WORDS_NO_JOIN)) {
               int pos = t.indexOf(' ');
               if (pos > 0) t = t.substring(0, pos);
            }
            return formula+'_'+t;
        }
    }

	boolean isValid() {
		return (id      != null) &&
		       (name    != null) &&
		       (!links.isEmpty());
	}

}
package fi.helsinki.ltdk.csbl.moksiskaan.schema;
// Generated by Hibernate Tools
// THIS CODE SHALL NOT BE MODIFIED DIRECTLY!

import java.util.HashSet;
import java.util.Set;

/**
 * Plain Old Java Object representing Organism. This code is generated by hbm2java.
 */
public class Organism extends MoksiskaanEntity implements java.io.Serializable
{
    /** Moksiskaan default persistence as defined in {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.MoksiskaanEntity MoksiskaanEntity}. */
    static private final long serialVersionUID = MoksiskaanEntity.serialVersionUID;

    // Constants for the field names:
    static public final String FIELD_ORGANISMID = "organismId";
    static public final String FIELD_NAME = "name";
    static public final String FIELD_AUTHOR = "author";
    static public final String FIELD_YEAR = "year";
    static public final String FIELD_STUDIES = "studies";

    private Integer organismId;
    private String name;
    private String author;
    private Integer year;
    private Set<Study> studies = new HashSet<Study>(0);

    /** Default constructor */
    public Organism() {
      // Do nothing.
    }
	
    public Organism(Integer organismId, String name, String author, Integer year) {
        this.organismId = organismId;
        this.name = name;
        this.author = author;
        this.year = year;
    }
    public Organism(Integer organismId, String name, String author, Integer year, Set<Study> studies) {
       this.organismId = organismId;
       this.name = name;
       this.author = author;
       this.year = year;
       this.studies = studies;
    }

    public Integer getOrganismId() {
        return this.organismId;
    }
    
    public void setOrganismId(Integer organismId) {
        this.organismId = organismId;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return this.author;
    }
    
    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getYear() {
        return this.year;
    }
    
    public void setYear(Integer year) {
        this.year = year;
    }

    public Set<Study> getStudies() {
        return this.studies;
    }
    
    public void setStudies(Set<Study> studies) {
        this.studies = studies;
    }


}

package fi.helsinki.ltdk.csbl.moksiskaan.schema;

/**
 * Common interface for POJOs representing results of studies.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.00
 */
public interface StudyResult<E extends MoksiskaanEntity> {

    /** Returns the host study that produced the results. */
    public Study getStudy();

    /** Defines the host study that produced the results. */
    public void setStudy(Study study);

    /** Evidence score as defined by the score type of the host study. */
    public double getWeight();

    /** Assigns a score for the evidence. */
    public void setWeight(double weight);

    /**
     * True indicates a positive evidence and false means that the
     * study has produced contradictive evidence against the relevance
     * of the target relationship in its context.
     */
    public boolean isEvidence();

   /**
    * True indicates a positive evidence and false means that the
    * study has produced contradictive evidence against the relevance
    * of the target relationship in its context.
    */
    public void setEvidence(boolean evidence);

    /**
     * Returns a database item that represents the actual finding.
     * This item may be for example a bioentity or a connection between
     * such entities.
     */
    public E getEntity();

    /**
     * Defines the actual finding.
     */
    public void setEntity(E entity);

}

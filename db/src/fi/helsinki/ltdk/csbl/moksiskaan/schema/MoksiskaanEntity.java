package fi.helsinki.ltdk.csbl.moksiskaan.schema;

import java.io.Serializable;

/**
 * This is a common base class for the plain old Java objects used to
 * represent tuples of the Moksiskaan database.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 */
public class MoksiskaanEntity implements Serializable {

    /** Hibernate is used for long time persistence. Default serialization is used for runtime constructs. */
    static protected final long serialVersionUID = 1L;

}

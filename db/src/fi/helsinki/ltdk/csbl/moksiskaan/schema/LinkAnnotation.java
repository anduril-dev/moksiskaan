package fi.helsinki.ltdk.csbl.moksiskaan.schema;
// Generated by Hibernate Tools
// THIS CODE SHALL NOT BE MODIFIED DIRECTLY!


/**
 * Plain Old Java Object representing LinkAnnotation. This code is generated by hbm2java.
 */
public class LinkAnnotation extends MoksiskaanEntity implements java.io.Serializable
{
    /** Moksiskaan default persistence as defined in {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.MoksiskaanEntity MoksiskaanEntity}. */
    static private final long serialVersionUID = MoksiskaanEntity.serialVersionUID;

    // Constants for the field names:
    static public final String FIELD_LINK = "link";
    static public final String FIELD_NAME = "name";
    static public final String FIELD_VALUE = "value";

    private Link link;
    private String name;
    private String value;

    /** Default constructor */
    public LinkAnnotation() {
      // Do nothing.
    }
    public LinkAnnotation(Link link, String name, String value) {
       this.link = link;
       this.name = name;
       this.value = value;
    }

    public Link getLink() {
        return this.link;
    }
    
    public void setLink(Link link) {
        this.link = link;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return this.value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }

    public boolean equals(Object other) {
         if (this  == other) return true;
         if (other == null)  return false;
         if (!(other instanceof LinkAnnotation)) return false;

         LinkAnnotation castOther = (LinkAnnotation)other; 
         return ( (this.getLink()==castOther.getLink()) || ( this.getLink()!=null && castOther.getLink()!=null && this.getLink().equals(castOther.getLink()) ) )
 && ( (this.getName()==castOther.getName()) || ( this.getName()!=null && castOther.getName()!=null && this.getName().equals(castOther.getName()) ) );
    }

    public int hashCode() {
         int result = 17;

         result = 37 * result + ( getLink() == null ? 0 : this.getLink().hashCode() );
         result = 37 * result + ( getName() == null ? 0 : this.getName().hashCode() );
         return result;
   }   

}

package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.LatexTools;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.BioentityType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Hit;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Link;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkEvidence;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.MoksiskaanEntity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Organism;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.ScoreType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Study;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.StudyResult;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Xref;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType;

/**
 * This Anduril component is used to save studies and their results into Moksiskaan database.
 * Empty inputs can be used to remove studies.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 */
public class SaveStudy extends SkeletonComponent {

    static private final String SELECT_BIOENTITY =
        "SELECT B "+
        "FROM  "+Bioentity.class.getSimpleName()+" AS B, "+
                 Xref.class.getSimpleName()+"      AS X "+
        "WHERE (B.bioentityType = :bioentityType) AND "+
        "      (B.organism      = :organism)      AND "+
        "      (X.bioentity     = B)              AND "+
        "      (X.xrefType      = :xrefType)      AND "+
        "      (X.value         = :value)";

    @SuppressWarnings("unchecked")
	protected ErrorCode runImpl(CommandFile cf) throws IOException {
        boolean     lowest    = cf.getBooleanParameter("weightLowest");
        boolean     clean     = cf.getBooleanParameter("clean");
        boolean     evidence  = cf.getBooleanParameter("evidence");
        boolean     directed  = cf.getBooleanParameter("directed");
        boolean     acceptNU  = cf.getBooleanParameter("acceptNonUnique");
        boolean     hasLimit  = !AsserUtil.onlyWhitespace(cf.getParameter("threshold"));
        double      threshold = hasLimit ? cf.getDoubleParameter("threshold") : 0;
        String      xref2C    = cf.getParameter("xrefCol2");
        boolean     asLink    = !AsserUtil.onlyWhitespace(xref2C);
        File        reportDir = cf.getOutput("report");
        String      desc      = cf.getParameter("description");
        String      refs      = cf.getParameter("refs");
        String      annotN    = cf.getParameter("linkAnnot");
        PrintWriter report;
        String      id;

        if (!reportDir.exists() && !reportDir.mkdirs()) {
            cf.writeError("Cannot create output folder: "+reportDir.getAbsolutePath());
            return ErrorCode.ERROR;
        }

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        id = cf.getParameter("organism");
        Organism organism = (Organism)session.get(Organism.class, Integer.parseInt(id));
        if (organism == null)
            throw new IllegalArgumentException("Invalid organism identifier: "+id);

        id = cf.getParameter("bioentityType");
        BioentityType bioentityType = (BioentityType)session.get(BioentityType.class, Integer.parseInt(id));
        if (bioentityType == null)
            throw new IllegalArgumentException("Invalid bioentity type identifier: "+id);

        id = cf.getParameter("scoreType");
        ScoreType scoreType = (ScoreType)session.get(ScoreType.class, Integer.parseInt(id));
        if (scoreType == null)
            throw new IllegalArgumentException("Invalid score type identifier: "+id);

        id = cf.getParameter("xrefType");
        XrefType xrefType = (XrefType)session.get(XrefType.class, Integer.parseInt(id));
        if (xrefType == null)
            throw new IllegalArgumentException("Invalid external reference type identifier: "+id);

        // Link relations
        BioentityType bioentityType2;
        XrefType      xrefType2;
        LinkType      dLinkType;
        if (asLink) {
           id             = cf.getParameter("bioentityType2");
           bioentityType2 = (BioentityType)session.get(BioentityType.class, Integer.parseInt(id));
           if (bioentityType2 == null)
              throw new IllegalArgumentException("Invalid target bioentity type identifier: "+id);
           id             = cf.getParameter("xrefType2");
           xrefType2      = (XrefType)session.get(XrefType.class, Integer.parseInt(id));
           if (xrefType2      == null)
              throw new IllegalArgumentException("Invalid external target reference type identifier: "+id);
           dLinkType = (LinkType)session.get(LinkType.class, Integer.valueOf(cf.getParameter("linkTypeDefault")));
	       if (dLinkType == null)
	          throw new IllegalArgumentException("Invalid link type identifier: "+cf.getParameter("linkTypeDefault"));
        } else {
           bioentityType2 = null;
           xrefType2      = null;
           dLinkType      = null;
        }

        Integer studyId = Integer.valueOf(cf.getParameter("id"));
        Study   study   = (Study)session.get(Study.class, studyId);
        if (study == null) {
           // Prepare a new study...
           study = new Study();
           study.setStudyId(studyId);
        } else if (clean) {
           // Remove old results...
           for (Hit hit : study.getHits()) {
               session.delete(hit);
           }
           for (LinkEvidence hit : study.getLinkEvidences()) {
               session.delete(hit);
           }
        }
        if (AsserUtil.onlyWhitespace(desc)) desc = null;
        if (AsserUtil.onlyWhitespace(refs)) refs = null;
        study.setTitle(cf.getParameter("title"));
        study.setDescription(desc);
        study.setRef(refs);
        study.setOrganism(organism);
        study.setScoreType(scoreType);
        session.saveOrUpdate(study);

        Map<String,StudyResult<?>>           hits   = new HashMap<String,StudyResult<?>>(10000);
        Map<MoksiskaanEntity,StudyResult<?>> beHits = new HashMap<MoksiskaanEntity,StudyResult<?>>(hits.size());

        CSVParser     hitsIn = new CSVParser(cf.getInput("hits"));
        int           colW   = getColumnIndex(cf.getParameter("weightCol"), hitsIn.getColumnNames(), -1);
        int           colId  = getColumnIndex(cf.getParameter(  "xrefCol"), hitsIn.getColumnNames(),  0);
        List<String>  missed = null;
        int           colIdT = asLink ? AsserUtil.indexOf(xref2C, hitsIn.getColumnNames(), true, true) : -1;

        int colAnn = -1;
        if (asLink) {
 	       if (AsserUtil.onlyWhitespace(annotN)) {
	    	   annotN = null;
	       } else {
	    	   String[] annot = AsserUtil.split(annotN, "=");
	    	   annotN = annot[0];
	    	   colAnn = hitsIn.getColumnIndex(annot[1]);
	       }
        }
        while (hitsIn.hasNext()) {
            String[]        row = hitsIn.next();
            List<Bioentity> tmpList;
			StudyResult<?>  hit;
            double          score;

            if ((row[colId] == null)                 || // Xref is missing
                ((colW >= 0) && (row[colW] == null)) || // Weight is missing although required
                (asLink && (row[colIdT] == null)))      // Link target is missing
               continue;

            if (colW >= 0) {
                score = Double.parseDouble(row[colW]);
                if (hasLimit &&
                    (( lowest && score > threshold) ||
                     (!lowest && score < threshold)))
                    continue;
            } else {
                score = Double.NaN;
            }

            String hitKey = asLink ? row[colId]+'_'+row[colIdT] : row[colId];
            hit = hits.get(hitKey);
            if (hit != null) { // Already created
                if (colW >= 0) {
                    double sOld = hit.getWeight();
                    double sNew = lowest ? Math.min(sOld, score):
                                           Math.max(sOld, score);
                    hit.setWeight(sNew);
                    if (!directed) {
                    	hits.get(row[colIdT]+'_'+row[colId]).setWeight(sNew);
                    }
                }
                continue;
            }

            tmpList = (List<Bioentity>)session.createQuery(SELECT_BIOENTITY)
                                              .setInteger("bioentityType", bioentityType.getBioentityTypeId())
                                              .setInteger("organism",      organism.getOrganismId())
                                              .setInteger("xrefType",      xrefType.getXrefTypeId())
                                              .setString ("value",         row[colId])
                                              .list();
            if (tmpList.isEmpty()) {
                System.err.println("Entity not found: "+row[colId]);
                if (missed == null) missed = new LinkedList<String>();
                missed.add(row[colId]);
                continue;
            }
            if (!acceptNU && tmpList.size() > 1) {
                cf.writeError("Input ID maps to more than one item: "+tmpList);
                return ErrorCode.INVALID_INPUT;
            }

            for (Bioentity entity : tmpList) {
                if (asLink) {
                   String annotV = (annotN==null) ? null : row[colAnn];
                   tmpList = (List<Bioentity>)session.createQuery(SELECT_BIOENTITY)
                                                     .setInteger("bioentityType", bioentityType2.getBioentityTypeId())
                                                     .setInteger("organism",      organism.getOrganismId())
                                                     .setInteger("xrefType",      xrefType2.getXrefTypeId())
                                                     .setString ("value",         row[colIdT])
                                                     .list();
                   if (tmpList.isEmpty()) {
                      System.err.println("Entity not found: "+row[colIdT]);
                      if (missed == null) missed = new LinkedList<String>();
                      missed.add(row[colIdT]);
                      continue;
                   }
                   Bioentity target = (Bioentity)AsserUtil.selectOnly(tmpList);
                   Link      link;
                   if (!directed) {
                	   link = HibernateUtil.prepareLink(target, entity, dLinkType, annotN, annotV, session);
                       hits.put(row[colIdT]+'_'+row[colId],
                    		    new LinkEvidence(study, link, (colW>=0)?score:-1.0, evidence));
                   }
                   link = HibernateUtil.prepareLink(entity, target, dLinkType, annotN, annotV, session);
                   hit = new LinkEvidence();
                   ((StudyResult<MoksiskaanEntity>)hit).setEntity(link);
                } else {
                   hit = new Hit();
                   ((StudyResult<MoksiskaanEntity>)hit).setEntity(entity);
                }

                hit.setWeight((colW>=0) ? score : -1.0);

                StudyResult<?> hitOld = beHits.get(hit.getEntity());
                if (hitOld != null) {
                	hitOld.setWeight(lowest ? Math.min(hit.getWeight(), hitOld.getWeight()):
                                              Math.max(hit.getWeight(), hitOld.getWeight()));
                	continue;
                }
                beHits.put(hit.getEntity(), hit);

                hit.setStudy(study);
                hit.setEvidence(evidence);
                hits.put(hitKey, hit);
            }
        }
        hitsIn.close();

        if (hits.isEmpty()) {
            session.delete(study);
        } else {
	        for (StudyResult<?> hit : hits.values()) {
	            session.save(hit);
	        }
        }

        session.getTransaction().commit();

        try { report = new PrintWriter(new FileWriter(new File(reportDir, LatexTools.DOCUMENT_FILE))); }
        catch (IOException e) {
            cf.writeError(e);
            return ErrorCode.ERROR;
        }
        printStudy(study,
                   bioentityType,
                   xrefType,
                   xrefType2,
                   hits.size(),
                   missed,
                   report);
        report.close();
        return ErrorCode.OK;
    }

    static private void printStudy(Study         study,
                                   BioentityType biotype,
                                   XrefType      reftype,
                                   XrefType      reftype2,
                                   int           hits,
                                   List<String>  missed,
                                   PrintWriter   out) {
        Organism org = study.getOrganism(); 
        out.append("\\textbf{")
           .append(LatexTools.quote(study.getTitle()));
        if (study.getRef() != null)
        	out.append("}~\\cite{").append(study.getRef());
        out.append("} data set represents ")
           .append(LatexTools.quote(biotype.getName()))
           .append(" hits of \\textit{")
           .append(LatexTools.quote(org.getName()))
           .append("\\/} ")
           .append(LatexTools.quote(org.getAuthor()))
           .append(", ")
           .append(org.getYear().toString())
           .append(". Total of ")
           .append(String.valueOf(hits))
           .append(" hits were accepted based on their ")
           .append(LatexTools.quote(reftype.getName()));
        if (reftype2 != null) {
           out.append(" and ").append(LatexTools.quote(reftype2.getName()));
        }
        out.append(" identifiers.");
        if (missed != null) {
            int mSize = missed.size();
            if (mSize == 1) {
               out.append(" One hit (\\textbf{")
                  .append(LatexTools.quote(missed.get(0)))
                  .append("}) was rejected as the identifier did not match the local database.");
            } else {
               out.append(" Total of ")
                  .append(String.valueOf(mSize))
                  .append(" hits were rejected because their identifiers did not match the local database. "+
                          "The rejected identifiers were: ");
               for (String id : missed) {
                   out.append("\\textit{").append(LatexTools.quote(id));
                   mSize--;
                   if (mSize >= 1) {
                	  out.append("\\/}, ");
                   }
                   if (mSize == 1) {
                      out.append("and ");
                   }
               }
               out.append("\\/}.");
            }
        }
        if (study.getDescription() != null)
           out.append("\n\n").append(LatexTools.quote(study.getDescription()));
        if (hits == 0) {
            out.append(" \\textcolor{red}{This study has not been saved as it would have been empty!}");
        }
        out.append('\n');
    }

    static private int getColumnIndex(String   name,
                                      String[] cols,
                                      int      def) {
        if (AsserUtil.onlyWhitespace(name))
            return def;
        return AsserUtil.indexOf(name, cols, true, true);
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new SaveStudy().run(argv);
    }

}

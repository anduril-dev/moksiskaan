package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.File;
import java.io.IOException;
import java.util.*;

import org.hibernate.Query;
import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.db.TypeConverter;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.moksiskaan.ActivityCode;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Link;

/**
 * Provides a hypothetical regulation network that is based on the given time series.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.11
 */
public class TimeSeriesGraph extends SkeletonComponent {

	@SuppressWarnings("unchecked")
	protected ErrorCode runImpl(CommandFile cf) throws IOException {
		Integer[][]      linkTs = prepareLinkTypes(cf);
        String           myName = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);
        boolean          fuzzy  = cf.getBooleanParameter("fuzzy");
        Map<Long,String> idMap  = new HashMap<Long,String>(2000);
		BioentityGraphML graph  = new BioentityGraphML();
		StringBuffer     msg    = new StringBuffer(1024);

		CSVParser   dataIn    = new CSVParser(cf.getInput("activities"));
		String[]    colNames  = dataIn.getColumnNames();
		int         tCount    = colNames.length-1;
		Set<Long>[] genesUp   = new Set[tCount];
		Set<Long>[] genesDown = new Set[tCount];
		msg.append("This data consists of ").append(tCount).append(" time points: ");
		for (int t=0; t<tCount; t++) {
			if (t > 0) msg.append(", ");
			msg.append(t).append('=').append(colNames[t+1]);
			genesUp[t]   = new HashSet<Long>(1000);
			genesDown[t] = new HashSet<Long>(1000);
		}
		msg.append('.'); // NOPMD
        cf.writeLog(msg.toString());

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        for (String[] line : dataIn) {
        	Long entityId = null;
        	for (int t=0; t<tCount; t++) {
        		ActivityCode s = ActivityCode.parse(line[t+1]);
    			if (entityId == null && (s==ActivityCode.DOWN||s==ActivityCode.UP))
    				entityId = loadEntity(line[0], session);        		
        		if (s == ActivityCode.DOWN) {
                   genesDown[t].add(entityId);
                   idMap.put(entityId, line[0]);
        		} else
        		if (s == ActivityCode.UP) {
        			genesUp[t].add(entityId);
        			idMap.put(entityId, line[0]);
        		}
        	}
        }
		dataIn.close();

		CSVWriter statsOut = new CSVWriter(new String[]{"time","up","down"}, cf.getOutput("stats"));
		msg.setLength(0); // NOPMD
		msg.append("Number of up and down regulated genes at each time point: ");
		for (int t=0; t<tCount; t++) {
			int sU = genesUp  [t].size();
			int sD = genesDown[t].size();
			if (t > 0) msg.append(", ");
			msg.append(t).append('=').append(sU)
			             .append('/').append(sD);
			statsOut.write(colNames[t+1]);
			statsOut.write(sU);
			statsOut.write(sD);
		}
		msg.append(". Total number of DEGs: ").append(idMap.size());
		cf.writeLog(msg.toString());
		statsOut.close();

		if (fuzzy) {
			for (int t=tCount-1; t>0; t--) {
				genesUp  [t].addAll(genesUp  [t-1]);
				genesDown[t].addAll(genesDown[t-1]);
			}
		}

		Set<Link>              linkPool  = new HashSet<Link>(2048);
		Map<Long,ActivityCode> statusOut = new HashMap<Long,ActivityCode>(1000);
		Map<Long,Integer>      times     = new HashMap<Long,Integer>(1000);
		for (int t=1; t<tCount; t++) {
			boolean sUp   = !genesUp  [t-1].isEmpty();
			boolean sDown = !genesDown[t-1].isEmpty();
			boolean tUp   = !genesUp  [t]  .isEmpty();
			boolean tDown = !genesDown[t]  .isEmpty();
			boolean inhi  = false;
			boolean acti  = false;
			if ((!sUp && !sDown) || (!tUp && !tDown)) continue; // No possible out comes
		    msg.setLength(0); // NOPMD
		    msg.append("SELECT L FROM Link L WHERE (L.source.id <> L.target.id) AND (");
		    if (tUp) {
		    	msg.append("((");
		    	if (sUp)   { msg.append("(L.source.id IN (:sUp)   AND L.linkType.id IN (:activation))"); acti = true; }
		    	if (sUp && sDown) msg.append(" OR ");
		    	if (sDown) { msg.append("(L.source.id IN (:sDown) AND L.linkType.id IN (:inhibition))"); inhi = true; }
		    	msg.append(") AND L.target.id IN (:tUp))");
		    }
		    if (tUp && tDown) msg.append(" OR ");
		    if (tDown) {
		    	msg.append("((");
		    	if (sUp)   { msg.append("(L.source.id IN (:sUp)   AND L.linkType.id IN (:inhibition))"); inhi = true; }
		    	if (sUp && sDown) msg.append(" OR ");
		    	if (sDown) { msg.append("(L.source.id IN (:sDown) AND L.linkType.id IN (:activation))"); acti = true; }
		    	msg.append(") AND L.target.id IN (:tDown))");
		    }
		    msg.append(')');

	        Query query = session.createQuery(msg.toString());
            if (acti)  query.setParameterList("activation", linkTs[0]);
            if (inhi)  query.setParameterList("inhibition", linkTs[1]);
            if (sUp)   query.setParameterList("sUp",        genesUp  [t-1]);
            if (sDown) query.setParameterList("sDown",      genesDown[t-1]);
            if (tUp)   query.setParameterList("tUp",        genesUp  [t]);
            if (tDown) query.setParameterList("tDown",      genesDown[t]);
            List<Link> links = (List<Link>)query.list();

            if (linkPool.addAll(links)) {
            	ActivityCode ac;
            	Long         bId;
            	Integer      timeS = Integer.valueOf(t-1);
            	Integer      timeT = Integer.valueOf(t);
            	for (Link l : links) {
            		bId = l.getSource().getBioentityId();
            	    ac  = (sUp && genesUp[t-1].contains(bId)) ? ActivityCode.UP : ActivityCode.DOWN;
            	    updateStatus(bId, ac, idMap, statusOut, times, timeS);
            	    bId = l.getTarget().getBioentityId();
            	    ac  = (tUp && genesUp[t]  .contains(bId)) ? ActivityCode.UP : ActivityCode.DOWN;
            	    updateStatus(bId, ac, idMap, statusOut, times, timeT);
            	}
            }
		}
        for (Link link : linkPool) {
            graph.addLink(link, session);
        }

        cf.writeLog("Total of "+graph.size()+" bioentities included into the time series graph.");
        graph.addIdAttributes("EnsemblGeneId",
	                          IDConstants.XrefType_Ensembl_gene,
	                          new Integer[]{ IDConstants.BioentityType_gene },
	                          session);
        for (Long id : times.keySet()) {
        	Bioentity entity = graph.getBioentity(id);
        	Integer   time   = times.get(id);
        	graph.addEntityProperty(entity, "fillcolor", "0.0,"+(1.0-time/(tCount-1.0))+",1.0");
        }
        graph.readLinkStyles(cf.getInput("linkStyles"));
        graph.save(cf.getOutput("graph"), myName);
        graph.saveLegend(cf.getOutput("legend"), myName+"Legend");

        session.getTransaction().commit();

        writeStatusOutput(idMap,
        		          statusOut,
        		          cf.getOutput("status"),
        		          colNames[0]);
		return ErrorCode.OK;
	}

	private static Integer[][] prepareLinkTypes(CommandFile cf) {
		String[]  posS = AsserUtil.split(cf.getParameter("ltPos"));
		String[]  anyS = AsserUtil.split(cf.getParameter("ltAny"));
		String[]  negS = AsserUtil.split(cf.getParameter("ltNeg"));
		Integer[] any;
		if ((anyS.length == 1) && anyS[0].isEmpty()) {
			any = null;
		} else {
			any = TypeConverter.toIntegerArray(anyS);
		}

		Integer[][] linkTypes = new Integer[2][];
		linkTypes[0] = prepareLinkTypes(any, posS);
		linkTypes[1] = prepareLinkTypes(any, negS);
		return linkTypes;
	}

	private static Integer[] prepareLinkTypes(Integer[] any, String[] values) {
		Integer[] linkTypes;
		boolean   isEmpty = ((values.length == 1) && values[0].isEmpty()); 
		if (any == null) {
			if (isEmpty) {
				linkTypes = new Integer[0];
			} else {
				linkTypes = TypeConverter.toIntegerArray(values);
		    }
		} else {
			if (isEmpty) {
				linkTypes = any;
		    } else {
				linkTypes = new Integer[any.length+values.length];
				for (int i=0; i<values.length; i++) {
				    linkTypes[i] = Integer.valueOf(values[i]);
				}
				System.arraycopy(any, 0, linkTypes, values.length, any.length);
		    }
		}
		return linkTypes;
	}

	private static void updateStatus(Long                   id,
			                         ActivityCode           code,
			                         Map<Long,String>       idMap,
			                         Map<Long,ActivityCode> statuses,
			                         Map<Long,Integer>      times,
			                         Integer                time) {
		if (!times.containsKey(id)) times.put(id, time);

		ActivityCode old = statuses.put(id, code);
		if ((old != null) && !old.equals(code)) {
		   idMap.remove(id);
		}
	}

	private static void writeStatusOutput(Map<Long,String>       idMap,
                                          Map<Long,ActivityCode> statuses,
			                              File                   file,
			                              String                 idCol) throws IOException {
		CSVWriter out = new CSVWriter(new String[]{idCol, ActivityCode.COLUMN_NAME_STATUS}, file);
		for (Long bId : statuses.keySet()) {
			String name = idMap.get(bId);
			if (name != null) {
				out.write(name);
				out.write(statuses.get(bId).toString(), false);
			}
		}
		out.close();
	}

	private static Long loadEntity(String id, Session session) {
		final String QUERY = "SELECT bioentity.id FROM Xref WHERE (value = :value) AND (xrefType.id = "+
		                     IDConstants.XrefType_Ensembl_gene+')';
		return (Long)session.createQuery(QUERY).setString("value", id).uniqueResult();
	}

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new TimeSeriesGraph().run(argv);
    }

}
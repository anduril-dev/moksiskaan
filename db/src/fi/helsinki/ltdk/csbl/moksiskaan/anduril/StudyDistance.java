package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Study;

/**
 * This Anduril component produces matrix of pairwise distances between the studies.
 */
public class StudyDistance extends SkeletonComponent {

    private static final String STUDY_HITS =
        "SELECT bioentity.id FROM Hit WHERE (study.id = :study)";

    @SuppressWarnings("unchecked")
	protected ErrorCode runImpl(CommandFile cf) throws IOException {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        boolean    asRanks = cf.getBooleanParameter("asRanks");
        Study[]    studies = ScoreMatrix.loadStudies(AsserUtil.split(cf.getParameter("useStudies")), session);
        String[]   names   = new String[studies.length+1];
        double[][] dists   = new double[studies.length][studies.length];
        Object[]   hits    = new Object[studies.length];
        for (int s=0; s<studies.length; s++) {
        	if (asRanks) {
        	   hits[s] = ScoreMatrix.loadHits(studies[s], null, true, false, session, null);
        	} else {
               hits[s] = session.createQuery(STUDY_HITS)
                                .setInteger("study", studies[s].getStudyId())
                                .list();
        	}
        }
        for (int s=0; s<studies.length; s++) {
            names[s+1] = studies[s].getTitle();
            for (int t=s+1; t<studies.length; t++) {
                double dist;
                if (asRanks)
                	dist = distance((Map<Long,Double>)hits[s], (Map<Long,Double>)hits[t]);
                else
                	dist = distance((Collection<?>)hits[s], (Collection<?>)hits[t]);
                dists[s][t] = dist;
                dists[t][s] = dist;
            }
        }

        session.getTransaction().commit();

        // Save distances...
        names[0] = "study";
        CSVWriter out = new CSVWriter(names, cf.getOutput("distances"));
        for (int s=0; s<studies.length; s++) {
            out.write(names[s+1]);
            for (int i=0; i<dists[s].length; i++) {
                out.write(dists[s][i]);
            }
        }
        out.close();

        return ErrorCode.OK;
    }

    @SuppressWarnings("PMD.AvoidReassigningParameters")
    public static double distance(Map<Long,Double> a, Map<Long,Double> b) {
    	int    sizeA = a.size();
        int    sizeB = b.size();
        double dist  = 0.0;
        int    inter = 0;

        if ((sizeA < 1) || (sizeB < 1))
            return 1.0;
        if (sizeA > sizeB) {
        	Map<Long,Double> tmp = a;
            a = b;   sizeA = sizeB;
            b = tmp; sizeB = b.size();
        }

        for (Long o : a.keySet()) {
        	Double aV = a.get(o);
        	Double bV = b.remove(o); 
            if (bV == null) {
            	dist += aV;
            } else {
            	dist += Math.abs(aV-bV);
            	inter++;
            }
        }
        for (Double bV : b.values()) {
        	dist += bV;
        }
    	return dist/(sizeA+sizeB-inter);
    }

    /**
     * Calculates the Czekanowski-Dice distance between the given sets.
     */
    @SuppressWarnings("PMD.AvoidReassigningParameters")
    public static double distance(Collection<?> a, Collection<?> b) {
        double sizeA = a.size();
        double sizeB = b.size();
        double inter = 0.0;

        if ((sizeA < 1) || (sizeB < 1))
            return 1.0;
        if (sizeA > sizeB) {
            Collection<?> tmp = a;
            a = b;   sizeA = sizeB;
            b = tmp; sizeB = b.size();
        }

        // Size of the intersection
        for (Object o : a) {
            if (b.contains(o)) inter++;
        }

        return ((sizeA-inter)+(sizeB-inter))/((sizeA+sizeB-inter)+inter);
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new StudyDistance().run(argv);
    }

}
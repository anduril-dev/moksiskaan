package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.IOException;
import java.sql.SQLException;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.db.JDBCConnection;
import fi.helsinki.ltdk.csbl.asser.db.TypeConverter;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa;

/**
 * Provides a CSV2Latex compatible template for the hyperlinks of the external references.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.13
 */
public class XrefLinkRuleComponent extends SkeletonComponent {

	public static final String PARAMETER_TYPES      = "xrefTypes";
	public static final String PARAMETER_COLUMNS    = "columns";
	public static final String PARAMETER_CONNECTION = "moksiskaan";

	protected ErrorCode runImpl(CommandFile cf) throws IOException,
                                                       SQLException {
		Integer[]      types   = TypeConverter.toIntegerArray(AsserUtil.split(cf.getParameter(PARAMETER_TYPES)));
		String[][]     columns = AsserUtil.prepareMapping(cf.getParameter(PARAMETER_COLUMNS));
		CSVWriter      out     = new CSVWriter(new String[] { "URL", "refCol", "valueCol" },
				                               cf.getOutput("refs"));
		JDBCConnection con     = cf.inputDefined(PARAMETER_CONNECTION) ?
				                 new JDBCConnection(cf.getInput(PARAMETER_CONNECTION)) :
				                 new JDBCConnection(Piispanhiippa.CFG_FILE);
        for (int i=0; i<types.length; i++) {
        	Object[] entry = con.fetchObject("SELECT \"www\", \"name\" "+
                                             "FROM   \"XrefType\" "+
        			                         "WHERE  (\"xrefTypeId\"=?)",
        			                         true, types[i]);
        	if (entry[0] == null) {
        		cf.writeLog("No WWW links for "+entry[1]+'.');
        	} else {
        		out.write((String)entry[0]);
        		out.write(columns[i][0]);
        		out.write(columns[i][1]==null ? columns[i][0] : columns[i][1]);
        	}
        }
	    con.close();
	    out.close();
		return ErrorCode.OK;
	}

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new XrefLinkRuleComponent().run(argv);
    }

}

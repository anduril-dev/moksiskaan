package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.db.TypeConverter;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.*;

/**
 * This Anduril component constructs a hypothetical pathway for the given set of bioentities.
 * The pathway is based on the combined set of connections found from Moksiskaan database
 * and it tries to connect the given entities together.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 */
public class CandidatePathway extends SkeletonComponent {

    /** These modes are used to express the selection of neighbors that are related to the original candidates. */
    static public enum ExpandMode {
       /** Include only those neighbors that belong to a path that starts from a candidate gene and end to
           a candidate gene. The end point may also be the starting gene if the path forms a loop.*/
       connected,
       /** Expand network by using the up stream entities of the candidates. */
       up,
       /** Expand network by using the up stream entities of the candidates. */
       down,
       /** Expand network by using the down and up stream entities of the candidates. */
       both
    }

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        String           myName      = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);
        ExpandMode       mode        = ExpandMode.valueOf(cf.getParameter("expand"));
        Integer[]        linkTypes   = TypeConverter.toIntegerArray(AsserUtil.split(cf.getParameter("linkTypes"))); // NOPMD
        Integer[]        targetTypes;
        BioentityGraphML graph;
        String           id;

        id = cf.getParameter("bioentityTypes");
        if (AsserUtil.onlyWhitespace(id)) {
        	targetTypes = new Integer[]{ IDConstants.BioentityType_gene };
        } else {
        	targetTypes = TypeConverter.toIntegerArray(AsserUtil.split(id)); // NOPMD
        }

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        id = cf.getParameter("organism");
        Organism organism = (Organism)session.get(Organism.class, Integer.parseInt(id));
        if (organism == null)
            throw new IllegalArgumentException("Invalid organism identifier: "+id);

        id = cf.getParameter("xrefType");
        XrefType xrefType = (XrefType)session.get(XrefType.class, Integer.parseInt(id));
        if (xrefType == null)
            throw new IllegalArgumentException("Invalid external reference type identifier: "+id);

        Set<Long> oSource = readEntities(cf.getInput("hits"),
                                         cf.getParameter("xrefCol"),
                                         organism,
                                         xrefType,
                                         targetTypes,
                                         session,
                                         cf.getLogWriter());
        cf.writeLog("Total of "+oSource.size()+" bioentities found for the initial seeds.");

        File      searchSpaceF = cf.getInput("within");
        Set<Long> searchSpace  = null;
        if (searchSpaceF != null) {
        	searchSpace = new HashSet<Long>(10000);
        	CSVParser ssIn = new CSVParser(searchSpaceF);
        	for (String[] entityId : ssIn) {
        		searchSpace.add(Long.valueOf(entityId[0]));
        	}
        	ssIn.close();
        	cf.writeLog("Search space contains "+searchSpace.size()+" bioentities.");
        }

        Map<String,String> gapProps   = AsserUtil.parseMap(cf.getParameter("gapProperties"));
        Map<String,String> hitProps   = AsserUtil.parseMap(cf.getParameter("hitProperties"));
        String[][]         annotRules = AsserUtil.prepareMapping(cf.getParameter("annotRules"));
        graph = loadGraph(oSource,
        		          searchSpace,
                          linkTypes,
                          targetTypes,
                          cf.getIntParameter("maxGap"),
                          mode,
                          annotRules,
                          session);
        for (Bioentity node : graph.bioentities()) {
        	if (oSource.contains(node.getBioentityId()))
                graph.addEntityProperties(node, hitProps);
            else
                graph.addEntityProperties(node, gapProps);
        }
        graph.addIdAttributes("EnsemblGeneId",
        		              IDConstants.XrefType_Ensembl_gene,
        		              new Integer[]{ IDConstants.BioentityType_gene },
        		              session);
        graph.addIdAttributes("GO",
	                          IDConstants.XrefType_Gene_Ontology,
	                          new Integer[]{ IDConstants.BioentityType_biological_process,
        		                             IDConstants.BioentityType_molecular_function },
	                          session);
        graph.readLinkStyles(cf.getInput("linkStyles"));

        cf.writeLog("Total of "+graph.size()+" bioentities included into the candidate pathway.");
        graph.save(cf.getOutput("graph"), myName);
        graph.saveLegend(cf.getOutput("legend"), myName+"Legend");

        session.getTransaction().commit();
        return ErrorCode.OK;
    }

    @SuppressWarnings("unchecked")
    static private BioentityGraphML loadGraph(Set<Long>  oSources,
    		                                  Set<Long>  searchSpace,
                                              Integer[]  linkTypes,
                                              Integer[]  targetTypes,
                                              int        maxGap,
                                              ExpandMode mode,
                                              String[][] annotRules,
                                              Session    session) {
    	String arSQL;
    	if (annotRules.length < 1) {
    		arSQL = "";
    	} else {
    		StringBuffer sql = new StringBuffer(512);
    		sql.append(" AND (L.id IN (SELECT A.link.id FROM LinkAnnotation A WHERE ");
    		for (int i=0; i<annotRules.length; i++) {
    			if (i > 0)                    sql.append(" OR ");
    			if (annotRules[i][1] != null) sql.append('(');
    			sql.append("(A.name = '").append(annotRules[i][0]).append("')");
    			if (annotRules[i][1] != null) {
    				sql.append(" AND (A.value LIKE '").append(annotRules[i][1]).append("'))");
    			}
    		}
    		sql.append("))");
    		arSQL = sql.toString();
    	}

        final String QUERY_EXPAND = "SELECT DISTINCT L.target.id "+
                                    "FROM   Link L "+
                                    "WHERE  (L.source.id               IN (:keys))      AND "+
                                    "       (L.target.id NOT           IN (:keys))      AND "+
                                    "       (L.linkType.id             IN (:linkTypes)) AND "+
                                    "       (L.target.bioentityType.id IN (:targetTypes))"+
                                    arSQL;
        final String QUERY_PRUNE  = "SELECT DISTINCT L.source.id "+
                                    "FROM   Link L "+
                                    "WHERE  (L.target.id               IN (:keys))      AND "+
                                    "       (L.source.id NOT           IN (:keys))      AND "+
                                    "       (L.linkType.id             IN (:linkTypes)) AND "+
                                    "       (L.source.bioentityType.id IN (:targetTypes))"+
                                    arSQL;
        final String QUERY_LINKS  = "SELECT L "+
                                    "FROM   Link L "+
                                    "WHERE  (L.source.id   IN (:sources))   AND "+
                                    "       (L.target.id   IN (:sources))   AND "+
                                    "       (L.linkType.id IN (:linkTypes)) AND "+
                                    "       (L.source.id <> L.target.id)"+
                                    arSQL;

        Set<Long> sources = new HashSet<Long>();
        sources.addAll(oSources);

        // Expand the source set with all possible gaps...
        if (maxGap > 0) {
            String query;
            if (mode == ExpandMode.connected || mode == ExpandMode.down)
               query = QUERY_EXPAND;
            else if (mode == ExpandMode.up)
               query = QUERY_PRUNE;
            else if (mode == ExpandMode.both)
               query = "SELECT B.id FROM Bioentity B WHERE B.id IN ("+QUERY_EXPAND+") OR B.id IN ("+QUERY_PRUNE+')';
            else throw new UnsupportedOperationException("No implementation for the given expansion method: "+mode.name());

            Set<Long>[] levelsDown = new Set[maxGap];
            for (int i=0; i<maxGap; i++) {
                levelsDown[i] = new HashSet<Long>();

                Set<Long> setIn = (i==0) ? sources : levelsDown[i-1];
                if (setIn.isEmpty()) continue;

                // Split possible oversize queries into sub-queries of 10000 bioentities
                int subsets       = (int)Math.ceil(((double)setIn.size()/10000.0));
                int setSize       = setIn.size();               
                List<Long> listIn = new ArrayList<Long>(setIn);
                for (int s=0; s < subsets; s++) {
                    int coef           = Math.min(10000, setSize);
                    int toIndex        = Math.min((s+1)*coef, setSize);
                    Set<Long> setInSub = new HashSet<Long>(listIn.subList(s*coef, toIndex));
                    Iterator<Long> res = (Iterator<Long>)session.createQuery(query)
                                                                .setParameterList("keys",        setInSub)
                                                                .setParameterList("linkTypes",   linkTypes)
                                                                .setParameterList("targetTypes", targetTypes)
                                                                .iterate();
                    while (res.hasNext()) {
                        Long    newId  = res.next();
                        boolean canAdd = ((searchSpace == null) || searchSpace.contains(newId)); 

                        if (canAdd) {
                            levelsDown[i].add(newId);
                            if (mode != ExpandMode.connected) {
                                sources.add(newId);
                            }
                        }
                    }
                }
            }
            // Prune gaps that are not terminated with a hit...
            if (mode == ExpandMode.connected && !sources.isEmpty())
            for (int i=0; i<maxGap; i++) {
                Iterator<Long> res = (Iterator<Long>)session.createQuery(QUERY_PRUNE)
                                                            .setParameterList("keys",        sources)
                                                            .setParameterList("linkTypes",   linkTypes)
                                                            .setParameterList("targetTypes", targetTypes)
                                                            .iterate();
                while (res.hasNext()) {
                   Long ent = res.next();
                   for (int l=maxGap-1-i; l>=0; l--) {
                       if (levelsDown[l].contains(ent) &&
                          ((searchSpace == null) || searchSpace.contains(ent))) {
                          sources.add(ent);
                          break;
                       }
                   }
                }
            }
        }

        // Create graph...
        BioentityGraphML graph = new BioentityGraphML();
        if (!sources.isEmpty()) {
            int sourSize      = sources.size();
            int subsets       = (int)Math.ceil(((double)sourSize/10000.0));            
            List<Long> listIn = new ArrayList<Long>(sources);

            for (int s=0; s < subsets; s++) {
                int coef           = Math.min(10000, sourSize);
                int toIndex        = Math.min((s+1)*coef, sourSize);
                Set<Long> sourSub  = new HashSet<Long>(listIn.subList(s*coef, toIndex));
                List<Link> links   = (List<Link>)session.createQuery(QUERY_LINKS)
                                                                     .setParameterList("sources",   sourSub)
                                                                     .setParameterList("linkTypes", linkTypes)
                                                                     .list();
                for (Link link : links) {
                    graph.addLink(link, session);
                }
            }
        }
        return graph;
    }

    static public Set<Long> readEntities(File      file,
                                         String    column,
                                         Organism  organism,
                                         XrefType  xrefType,
                                         Integer[] targetTypes,
                                         Session   session,
                                         Writer    log) throws IOException {
    	if (file == null) return null;

        final String QUERY = "SELECT B.id "+
                             "FROM   Bioentity B, Xref X "+
                             "WHERE  (B.id          = X.bioentity) AND "+
                             "       (B.organism.id = :organism)   AND "+
                             "       (X.xrefType.id = :xrefType)   AND "+
                             "       (X.value       = :xref)       AND "+
                             "       (B.bioentityType.id IN (:targetTypes))";
        Set<Long> set   = new HashSet<Long>();
        CSVParser input = new CSVParser(file);
        int       col   = 0;

        if (!AsserUtil.onlyWhitespace(column)) {
            col = AsserUtil.indexOf(column, input.getColumnNames(), false, true);
        }

        for (String[] row : input) {
            @SuppressWarnings("unchecked")
			Iterator<Long> gene = (Iterator<Long>)session
			                      .createQuery(QUERY)
                                  .setInteger(      "organism",    organism.getOrganismId())
                                  .setInteger(      "xrefType",    xrefType.getXrefTypeId())
                                  .setString(       "xref",        row[col])
                                  .setParameterList("targetTypes", targetTypes)
                                  .iterate();
            if ((gene == null) || !gene.hasNext()) {
                log.write("Unknown "+xrefType.getName()+": "+row[col]+'\n');
            } else {
            	while (gene.hasNext()) {
                  set.add(gene.next());
            	}
            }
        }

        input.close();
        return set;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new CandidatePathway().run(argv);
    }

}
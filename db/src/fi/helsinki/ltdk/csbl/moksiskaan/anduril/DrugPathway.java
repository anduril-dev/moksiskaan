package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.File;
import java.io.IOException;
import java.util.*;

import org.hibernate.Query;
import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.moksiskaan.ActivityCode;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.*;

/**
 * This Anduril component tries to find possible drugs that would target
 * some of the given genes.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 0.19
 */
public class DrugPathway extends SkeletonComponent {

	public static final String NODE_ATTR_ENSEMBL_ID   = "EnsemblGeneId";
	public static final String NODE_ATTR_DRUG_BANK_ID = "DrugBankId";
	public static final String NODE_ATTR_KEGG_DRUG_ID = "KEGGDrugId";
	public static final String NODE_ATTR_BORDERWIDTH  = "penwidth";
	public static final String NODE_ATTR_BORDERCOLOR  = "color";
	public static final String NODE_ATTR_FILLCOLOR    = "fillcolor";
	public static final String NODE_ATTR_SHAPE        = "shape";

    public static final String STATUS_COLUMN_ID        = ".GeneId";
	public static final String STATUS_COLUMN_PROMOTER  = "promoters";
	public static final String STATUS_COLUMN_INHIBITOR = "inhibitors";

	@SuppressWarnings("unchecked")
	protected ErrorCode runImpl(CommandFile cf) throws IOException {
        String               myName        = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);
        Map<Long,DrugTarget> targets       = new HashMap<Long,DrugTarget>(1024);
        int                  effectDist    = cf.getIntParameter("effectDist");
        int                  maxSize       = cf.getIntParameter("maxSize");
        boolean              useMetabolism = cf.getBooleanParameter("useMetabolism"); 
        BioentityGraphML     graph;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Organism organism = (Organism)session.get(Organism.class, IDConstants.Organism_Homo_sapiens);
        if (organism == null)
            throw new IllegalStateException("Cannot find Homo sapiens from the database.");

        String   xrefId   = cf.getParameter("xrefType");
        XrefType xrefType = (XrefType)session.get(XrefType.class, Integer.parseInt(xrefId));
        if (xrefType == null)
            throw new IllegalArgumentException("Invalid external reference type identifier: "+xrefId);

        Map<String,String> statusMap;
        final Set<Long>    geneP       = new HashSet<Long>(1000);
        final Set<Long>    geneN       = new HashSet<Long>(1000);
        File               statusInput = cf.getInput("status"); 
        if (statusInput == null) {
        	statusMap = null;
        } else {
        	cf.writeLog("Reading the status input...");
            statusMap       = new HashMap<String,String>(1000);
            CSVParser input = new CSVParser(statusInput);
            int       col   = AsserUtil.indexOf(ActivityCode.COLUMN_NAME_STATUS, input.getColumnNames(), true, true);
            String    xConv = "SELECT bioentity.id FROM Xref WHERE (value = :value) AND (xrefType.id = "+xrefId+')';
            for (String[] line : input) {
            	ActivityCode status = ActivityCode.parse(line[col]);
            	if (ActivityCode.UP == status) {
            		statusMap.put(line[0], "#70FF70");
            		geneP.addAll(session.createQuery(xConv).setString("value", line[0]).list());
            	} else
                if (ActivityCode.DOWN == status) {
            		statusMap.put(line[0], "#7070FF");
            		geneN.addAll(session.createQuery(xConv).setString("value", line[0]).list());
                } else
                if (ActivityCode.STABLE == status) {
                	statusMap.put(line[0], "#999999");
                }
            }
            input.close();
            cf.writeLog("Status mapping completed.");
        }

        Set<Long> drugsIn = loadDrugs(cf.getInput("drugs"),
        		                      cf.getParameter("drugIdType"),
        		                      session);
        Set<Long> oSource = CandidatePathway.readEntities(cf.getInput("targets"),
                                                          cf.getParameter("xrefCol"),
                                                          organism,
                                                          xrefType,
                                                          new Integer[]{ IDConstants.BioentityType_gene },
                                                          session,
                                                          cf.getLogWriter());
        if (oSource == null) {
           if (drugsIn == null) throw new IllegalStateException("No genes nor drugs defined as search keys.");
           cf.writeLog("Total of "+drugsIn.size()+" drugs found for the initial seeds.");
        } else {
           cf.writeLog("Total of "+oSource.size()+" genes found for the initial seeds.");
        }

        graph = loadGraph(oSource,
                          geneP,
                          geneN,
                          drugsIn,
                          targets,
                          useMetabolism,
                          effectDist,
                          session,
                          maxSize);
        graph.addIdAttributes(NODE_ATTR_ENSEMBL_ID,
        		              IDConstants.XrefType_Ensembl_gene,
        		              new Integer[]{ IDConstants.BioentityType_gene },
        		              session);
        graph.addIdAttributes(NODE_ATTR_KEGG_DRUG_ID,
	                          IDConstants.XrefType_KEGG_drug,
	                          new Integer[]{ IDConstants.BioentityType_drug },
	                          session);
        graph.addIdAttributes(NODE_ATTR_DRUG_BANK_ID,
                              IDConstants.XrefType_DrugBank_drug,
                              new Integer[]{ IDConstants.BioentityType_drug },
                              session);
        graph.readLinkStyles(cf.getInput("linkStyles"));
        for (Bioentity node : graph.bioentities()) {
        	if (IDConstants.BioentityType_drug.equals(node.getBioentityType().getBioentityTypeId())) {
        	   graph.addEntityProperty(node, NODE_ATTR_SHAPE, "diamond");
        	} else {
        	   if ((oSource != null) && oSource.contains(node.getBioentityId())) {
        	      graph.addEntityProperty(node, NODE_ATTR_SHAPE, "doubleoctagon");
               }
        	   if (statusMap != null) {
        		  String key    = graph.getEntityProperty(node.getBioentityId(), NODE_ATTR_ENSEMBL_ID);
        	      String status = statusMap.get(key);
        	      if (status != null) graph.addEntityProperty(node, NODE_ATTR_FILLCOLOR, status);
        	   }
        	}
        }

        cf.writeLog("Total of "+graph.size()+" genes included into the candidate pathway.");
        graph.save(cf.getOutput("graph"), myName);
        graph.saveLegend(cf.getOutput("legend"), myName+"Legend");

        writeStatusOutput(cf.getOutput("status"), graph, targets);

        session.getTransaction().commit();
        HibernateUtil.getSessionFactory().close();
        return ErrorCode.OK;
	}

	static private void writeStatusOutput(File                 statusF,
			                              BioentityGraphML     graph,
			                              Map<Long,DrugTarget> targets) throws IOException {
		// Sort items to produce robust and comparable outputs
		Map<String,DrugTarget> sortSet = new TreeMap<String,DrugTarget>();
		for (DrugTarget item: targets.values()) {
			String eId = graph.getEntityProperty(item.id, NODE_ATTR_ENSEMBL_ID);
			sortSet.put(eId, item);
		}

		CSVWriter statusOut = new CSVWriter(new String[] {STATUS_COLUMN_ID,
                                                          ActivityCode.COLUMN_NAME_STATUS,
				                                          STATUS_COLUMN_PROMOTER,
				                                          STATUS_COLUMN_INHIBITOR},
				                            statusF);
		for (String eId: sortSet.keySet()) {
			DrugTarget item = sortSet.get(eId);
			statusOut.write(eId, false);
			statusOut.write(item.getStatus(), false);
			statusOut.write(item.getPromoters());
			statusOut.write(item.getInhibitors());
		}
		statusOut.close();
	}

	@SuppressWarnings("unchecked")
	static private BioentityGraphML loadGraph(Set<Long>            oSources,
			                                  Set<Long>            geneP,
			                                  Set<Long>            geneN,
			                                  Set<Long>            drugsIn,
			                                  Map<Long,DrugTarget> targets,
			                                  boolean              useMetabolism,
			                                  int                  maxDistance,
                                              Session              session,
                                              int                  maxSize) {
        final String QUERY_DOWN   = "SELECT DISTINCT L.target.id, L.linkType.id "+
                                    "FROM   Link L "+
                                    "WHERE  (L.source.id     IN (:keys))      AND "+
                                    "       (L.target.id NOT IN (:keys))      AND "+
                                    "       (L.linkType.id   IN (:linkTypes)) AND "+
                                    "       (L.target.bioentityType.id = "+IDConstants.BioentityType_gene+')';
        final String QUERY_UP     = "SELECT DISTINCT L.source "+
                                    "FROM   Link L "+
                                    "WHERE  (L.target.id     IN (:keys)) AND "+
                                    "       (L.source.id NOT IN (:keys)) AND "+
                                    "       (L.linkType.id   IN (:linkTypes) "+
                (geneP.isEmpty()?"":"OR (L.linkType.id IN (:linkTypeP) AND L.target.id NOT IN (:keyP)) ")+
                (geneN.isEmpty()?"":"OR (L.linkType.id IN (:linkTypeN) AND L.target.id NOT IN (:keyN)) ")+
                                    "       ) AND (L.source.bioentityType.id IN (:bioTypes))";
        final String QUERY_LINKS  = "FROM  Link "+
                                    "WHERE (source.id   IN (:entities))  AND "+
                                    "      (target.id   IN (:entities))  AND "+
                                    "      (linkType.id IN (:linkTypes)) AND "+
                                    "      (source.id <> target.id)";
        final Integer[] linkTypeP = {
        		IDConstants.LinkType_drug_promotes,
        		IDConstants.LinkType_gene_expression,
        		IDConstants.LinkType_protein_activation,
        };
        final Integer[] linkTypeN = {
        		IDConstants.LinkType_drug_inhibits,
        		IDConstants.LinkType_gene_repression,
        		IDConstants.LinkType_protein_inhibition,
        };
        final Set<Integer> linkTypes = new HashSet<Integer>(50);
        final Set<Long>    entities  = new HashSet<Long>(1000);

        linkTypes.add(IDConstants.LinkType_acetylation);
        linkTypes.add(IDConstants.LinkType_deacetylation);
        linkTypes.add(IDConstants.LinkType_demethylation);
        linkTypes.add(IDConstants.LinkType_dephosphorylation);
        linkTypes.add(IDConstants.LinkType_desumoylation);
        linkTypes.add(IDConstants.LinkType_deubiquitination);
        linkTypes.add(IDConstants.LinkType_drug_target);
        linkTypes.add(IDConstants.LinkType_glycosylation);
        linkTypes.add(IDConstants.LinkType_methylation);
        linkTypes.add(IDConstants.LinkType_phosphorylation);
        linkTypes.add(IDConstants.LinkType_protein_binding);
        linkTypes.add(IDConstants.LinkType_protein_dissociation);
        linkTypes.add(IDConstants.LinkType_protein_state_change);
        linkTypes.add(IDConstants.LinkType_sumoylation);
        linkTypes.add(IDConstants.LinkType_ubiquitination);
        if (geneP.isEmpty()) for (Integer l : linkTypeP) linkTypes.add(l);
        if (geneN.isEmpty()) for (Integer l : linkTypeN) linkTypes.add(l);

        Set<Long> genes = new HashSet<Long>(1000);
        if (oSources == null) {
        	entities.addAll(drugsIn);
        } else {
	        Set<Integer> bioTypes = new HashSet<Integer>();
	        bioTypes.add(IDConstants.BioentityType_gene);
	        bioTypes.add(IDConstants.BioentityType_drug);
	        genes.addAll(oSources);
	        for (int d=0; d<maxDistance; d++) {
	            if (genes.isEmpty()) break;
	            Query query = session.createQuery(QUERY_UP)
	                                 .setParameterList("keys",      genes)
	                                 .setParameterList("linkTypes", linkTypes)
	                                 .setParameterList("bioTypes",  bioTypes);
	            if (!geneP.isEmpty())
	            	query.setParameterList("keyP", geneP).setParameterList("linkTypeP", linkTypeP);
	            if (!geneN.isEmpty())
	            	query.setParameterList("keyN", geneN).setParameterList("linkTypeN", linkTypeN);
	            Iterator<Bioentity> res = (Iterator<Bioentity>)query.iterate();
	            genes.clear();
	            while (res.hasNext()) {
	               Bioentity ent = res.next();
	               if (IDConstants.BioentityType_drug.equals(ent.getBioentityType().getBioentityTypeId())) {
	            	   if ((drugsIn == null) || drugsIn.contains(ent.getBioentityId()))
	                      entities.add(ent.getBioentityId());
	               } else {
	                  genes.add(ent.getBioentityId());
	               }
	            }
	            if (d == maxDistance-2) {
	               bioTypes.remove(IDConstants.BioentityType_gene);
	            }
	        }	
	        genes.clear();
        }
        genes.addAll(entities);

        if (useMetabolism) linkTypes.add(IDConstants.LinkType_drug_metabolism);
        if (!geneP.isEmpty()) for (Integer l : linkTypeP) linkTypes.add(l);
        if (!geneN.isEmpty()) for (Integer l : linkTypeN) linkTypes.add(l);
        for (int d=0; d<maxDistance; d++) {
            if (genes.isEmpty()) break;
            Iterator<Object[]> res = (Iterator<Object[]>)session.createQuery(QUERY_DOWN)
                                                                .setParameterList("keys",      genes)
                                                                .setParameterList("linkTypes", linkTypes)
                                                                .iterate();
            genes.clear();
            while (res.hasNext()) {
               Object[] id = res.next();
               if (!IDConstants.LinkType_drug_metabolism.equals(id[1])) {
                  genes.add((Long)id[0]);
               }
               entities.add((Long)id[0]);
            }
            if (entities.size() > maxSize)
                throw new IllegalStateException("Pathway contains more than "+maxSize+" elements.");
        }

        List<Link> links;
        if (entities.isEmpty()) {
           links = Collections.EMPTY_LIST;
        } else {
           if (useMetabolism) {
               linkTypes.add(IDConstants.LinkType_enzymatic_reaction);
               linkTypes.add(IDConstants.LinkType_pathway_precedence);
           }
           links = (List<Link>)session.createQuery(QUERY_LINKS)
                                      .setParameterList("entities",  entities)
                                      .setParameterList("linkTypes", linkTypes)
                                      .list();
        }
        // Create graph...
        BioentityGraphML graph = new BioentityGraphML();
        for (Link link : links) {
            graph.addLink(link, session);
            Integer type = link.getLinkType().getLinkTypeId();
            if (IDConstants.LinkType_drug_inhibits.equals(type)) {
               Long       b      = link.getTarget().getBioentityId();
               DrugTarget target = getTarget(b, targets);
               target.inhibitors.add(link.getSource().getBioentityId());
            } else
            if (IDConstants.LinkType_drug_promotes.equals(type)) {
               Long       b   = link.getTarget().getBioentityId();
               DrugTarget target = getTarget(b, targets);
               target.promoters.add(link.getSource().getBioentityId());
            }
        }
        for (DrugTarget target : targets.values()) {
            if (target.getStatus() == null) {
               Bioentity entity = graph.getBioentity(target.id);
               graph.addEntityProperty(entity, NODE_ATTR_BORDERCOLOR, "#DDDD00");
               graph.addEntityProperty(entity, NODE_ATTR_BORDERWIDTH, "4.0");
            }
        }
        return graph;
	}

	@SuppressWarnings("unchecked")
	static private Set<Long> loadDrugs(File    input,
			                           String  drugIdType,
			                           Session session) throws IOException {
		if (input == null) return null;

		Set<Long> drugs = new HashSet<Long>(100);
		CSVParser in    = new CSVParser(input);

		if (drugIdType.isEmpty()) {
			for (String[] line : in) {
				if (line[0] == null) continue;
				drugs.add(Long.valueOf(line[0]));
			}
		} else {
			Set<String> xrefs = new HashSet<String>(100);
			for (String[] line : in) {
				if (line[0] == null) continue;
				xrefs.add(line[0]);
			}
			List<Long> ids = session.createQuery("SELECT bioentity.id FROM Xref "+
					                             "WHERE  (xrefType.id = :xrefType) AND (value IN (:keys))")
                                    .setInteger("xrefType",   Integer.parseInt(drugIdType))
                                    .setParameterList("keys", xrefs)
                                    .list();
	        drugs.addAll(ids);
		}
		return drugs;
	}

	static private DrugTarget getTarget(Long                 id,
			                            Map<Long,DrugTarget> targets) {
		DrugTarget target = targets.get(id);
		if (target == null) {
			target = new DrugTarget(id);
			targets.put(id, target);
		}
		return target;
	}

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new DrugPathway().run(argv);
    }

}

/**
 * Represents a {@link fi.helsinki.ltdk.csbl.moksiskaan.anduril.DrugPathway drug pathway} entity
 * with its promoters and inhibitors.
 */
final class DrugTarget {

	final Long      id;
	final Set<Long> promoters  = new TreeSet<Long>();
	final Set<Long> inhibitors = new TreeSet<Long>();

	DrugTarget(Long bioentityId) {
		id = bioentityId;
	}

	String getStatus() {
		boolean p = promoters.isEmpty();
		boolean i = inhibitors.isEmpty();

		if (p == i) return null;
		if (p)      return ActivityCode.DOWN.toString();
		else        return ActivityCode.UP.toString();
	}

	String getPromoters() {
		return AsserUtil.collapse(promoters);
	}

	String getInhibitors() {
		return AsserUtil.collapse(inhibitors);
	}

}
package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.annotation.OntologyTerm;
import fi.helsinki.ltdk.csbl.asser.annotation.RegionGO;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.BioentityType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Organism;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Xref;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType;

/**
 * Stores the
 * {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants#LinkType_positive_regulation} and
 * {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants#LinkType_negative_regulation} links.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public class SaveGORegulations extends SkeletonComponent {

	private XrefType      typeGO;
	private BioentityType typeGObp;
	private BioentityType typeGOmf;
	private LinkType      typeLinkP;
	private LinkType      typeLinkN;
	private Organism      organism;

	private Map<String,OntologyTerm> go;

	private final Map<OntologyTerm,Bioentity> goEntities = new HashMap<OntologyTerm,Bioentity>(3000);

	protected ErrorCode runImpl(CommandFile cf) throws IOException {
       go = RegionGO.loadGO(cf.getInput("go").getPath(), true);

       final String ENTITY_QUERY = "SELECT B "+
                                   "FROM   Bioentity B, Xref X "+
                                   "WHERE  (B.id               = X.bioentity)                        AND "+
                                   "       (B.organism.id      = :organism)                          AND "+
                                   "       (B.bioentityType.id = "+IDConstants.BioentityType_gene+") AND "+
                                   "       (X.xrefType.id      = :xrefType)                          AND "+
                                   "       (X.value            = :xref)";

		Set<OntologyTerm> pTargets   = new HashSet<OntologyTerm>();
		Set<OntologyTerm> nTargets   = new HashSet<OntologyTerm>();
		int               xrefTypeId = cf.getIntParameter("xrefType");
		String            id;
		Bioentity         gene;

		id = cf.getParameter("linkAnnot");
		String[] linkAnnot  = AsserUtil.split(id, "=");
		if (((linkAnnot.length == 1) && !linkAnnot[0].isEmpty()) || linkAnnot.length >  2) {
			cf.writeError("Invalid linkAnnot parameter: "+id);
			return ErrorCode.PARAMETER_ERROR;
		}
		if (linkAnnot.length < 2) linkAnnot = new String[2];

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        id = cf.getParameter("organism");
        organism = (Organism)session.get(Organism.class, Integer.parseInt(id));
        if (organism == null)
            throw new IllegalArgumentException("Invalid organism identifier: "+id);

        typeGO    = (XrefType)     session.get(     XrefType.class, IDConstants.XrefType_Gene_Ontology);
        typeGObp  = (BioentityType)session.get(BioentityType.class, IDConstants.BioentityType_biological_process);
        typeGOmf  = (BioentityType)session.get(BioentityType.class, IDConstants.BioentityType_molecular_function);
        typeLinkP = (LinkType)     session.get(     LinkType.class, IDConstants.LinkType_positive_regulation);
        typeLinkN = (LinkType)     session.get(     LinkType.class, IDConstants.LinkType_negative_regulation);

		CSVParser in      = new CSVParser(cf.getInput("bioAnnotation"));
		int       colGO   = in.getColumnIndex(cf.getParameter("goColumn"));
		int       colXref = 0;
		id = cf.getParameter("xrefColumn");
		if (!AsserUtil.onlyWhitespace(id)) {
            colXref = AsserUtil.indexOf(id, in.getColumnNames(), false, true);
        }
		for (String[] line : in) {
			String[] terms = AsserUtil.split(line[colGO]);
			for (String term : terms) {
				processTerm(term, pTargets, nTargets);
			}
			if (pTargets.isEmpty() && nTargets.isEmpty()) continue;

            gene = (Bioentity)session.createQuery(ENTITY_QUERY)
                                     .setInteger("organism", organism.getOrganismId())
                                     .setInteger("xrefType", xrefTypeId)
                                     .setString( "xref",     line[colXref])
                                     .uniqueResult();
            if (gene == null) {
               cf.writeLog("Skipping an unknown gene: "+line[colXref]);
               pTargets.clear();
               nTargets.clear();
               continue;
            }
			saveRegulations(gene, pTargets, linkAnnot, session, cf, true);
			saveRegulations(gene, nTargets, linkAnnot, session, cf, false);
		}
	    in.close();

        session.getTransaction().commit();
        cf.writeLog("Total of "+goEntities.size()+" GO terms saved as regulation targets.");
		return ErrorCode.OK;
	}

	private void saveRegulations(Bioentity         gene,
	                             Set<OntologyTerm> targets,
	                             String[]          linkAnnot,
		                         Session           session,
		                         CommandFile       cf,
		                         boolean           positive) throws IOException {
        final String ENTITY_QUERY = "SELECT B "+
                                    "FROM   Bioentity B, Xref X "+
                                    "WHERE  (B.id          = X.bioentity) AND "+
                                    "       (B.organism.id = :organism)   AND "+
                                    "       (X.xrefType.id = :xrefType)   AND "+
                                    "       (X.value       = :xref)       AND "+
                                    "       (B.bioentityType.id IN ("+IDConstants.BioentityType_biological_process+
                                                                  ","+IDConstants.BioentityType_cellular_component+
                                                                  ","+IDConstants.BioentityType_molecular_function+"))";

		filterParents(targets, go);

		for (OntologyTerm target : targets) {
		    //cf.writeLog(gene+(positive ? " -> " : " -| ")+target.getName());
		    Bioentity targetB = goEntities.get(target);
		    if (targetB == null) {
		       targetB = (Bioentity)session.createQuery(ENTITY_QUERY)
                                           .setInteger("organism", organism.getOrganismId())
                                           .setInteger("xrefType", IDConstants.XrefType_Gene_Ontology)
                                           .setString( "xref",     target.getID())
                                           .uniqueResult();
		       if (targetB == null) {
		          targetB = new Bioentity();
		          targetB.setName(target.getName());
		          targetB.setOrganism(organism);
		    	  if ("biological_process".equals(target.getCategory())) {
			          targetB.setBioentityType(typeGObp);
		    	  } else
		    	  if ("molecular_function".equals(target.getCategory())) {
				      targetB.setBioentityType(typeGOmf);
		    	  } else {
		    		  cf.writeLog("Unsupported type of "+target.getCategory()+" in "+target.getName()+'.');
		    		  continue;
		    	  }
		    	  session.saveOrUpdate(targetB);
		    	  Xref goID = new Xref(typeGO, targetB, target.getID());
		          session.saveOrUpdate(goID);
		       }
		       goEntities.put(target, targetB);
		    }
            HibernateUtil.saveLink(gene, targetB,
                                   positive ? typeLinkP : typeLinkN,
                                   linkAnnot[0], linkAnnot[1],
                                   session, null);
		}
		targets.clear();
	}

	static public void filterParents(Set<OntologyTerm>        terms,
			                         Map<String,OntologyTerm> go) {
		Set<String> allParents = new HashSet<String>(200);
		for (OntologyTerm term : terms) {
			collectParents(term, allParents, go);
		}
		for (String parent : allParents) {
			terms.remove(go.get(parent));
		}
	}

	static public void collectParents(OntologyTerm             term,
	                                  Set<String>              allParents,
	                                  Map<String,OntologyTerm> go) {
        if (term.getParents() == null) return;
        for (String parent : term.getParents()) {
            boolean isNew = allParents.add(parent);
            if (isNew) collectParents(go.get(parent), allParents, go);
        }
    }

	private void processTerm(String            goId,
                             Set<OntologyTerm> pTargets,
                             Set<OntologyTerm> nTargets) throws IOException {
		Set<String> regs = new HashSet<String>();

		findRegulations(goId, go, regs, new HashSet<String>());

        for (String reg : regs) {
        	OntologyTerm rTerm = go.get(reg);
        	
        	for (String[] target : rTerm.getTargets()) {
        		if (target[1].equals(OntologyTerm.ROLE_NEGATIVELY))
        			nTargets.add(go.get(target[0]));
        		else
        		if (target[1].equals(OntologyTerm.ROLE_POSITIVELY))
            		pTargets.add(go.get(target[0]));
        	}
        }
	}

	static private void findRegulations(String                   goId,
                                        Map<String,OntologyTerm> go,
                                        Set<String>              hits,
                                        Set<String>              visited) {
		OntologyTerm term = go.get(goId);
		if (term == null) return;
		visited.add(goId);

        String name = term.getName();
        if (name.endsWith("regulation of biological process") ||
            name.endsWith("regulation of molecular function"))
        	return;
        if (term.hasTargets()) {
           hits.add(goId);
        }
        if (term.getParents() != null)
        for (String parent : term.getParents()) {
        	if (!visited.contains(parent)) {
        		findRegulations(parent, go, hits, visited);
        	}
        }
	}

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new SaveGORegulations().run(argv);
    }

}
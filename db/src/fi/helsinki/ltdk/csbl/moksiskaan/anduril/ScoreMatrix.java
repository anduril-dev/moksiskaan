package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Hit;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Study;

/**
 * This Anduril component produces a table of bioentity rows and study
 * columns representing the weights of the corresponding hits.
 */
public class ScoreMatrix extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        boolean   studyNames = cf.getBooleanParameter("studyNames");
        int       minSupport = cf.getIntParameter("minSupport");
        String[]  studyIds   = AsserUtil.split(cf.getParameter("useStudies"));
        File      matrixFile = cf.getOutput("scores");
        Set<Long> entities   = new TreeSet<Long>();
        String    type       = cf.getParameter("type").toLowerCase();
        boolean   asRanks;
        boolean   normalize;

        if ("weight".equals(type)) {
        	asRanks   = false;
        	normalize = false;
        } else
        if ("rank".equals(type)) {
        	asRanks   = true;
        	normalize = false;
        } else
        if ("nrank".equals(type)) {
        	asRanks   = true;
        	normalize = true;
        } else {
            cf.writeError("Invalid score type: "+type);
            return ErrorCode.PARAMETER_ERROR;
        }

        Map<Integer,double[]> limits = prepareLimits(cf.getParameter("limits"));
        
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Study[]            studies = loadStudies(studyIds, session);
        @SuppressWarnings("unchecked")
        Map<Long,Double>[] scores  = new Map[studies.length];
        for (int i=0; i<studies.length; i++) {
            scores[i] = loadHits(studies[i], entities, asRanks, normalize, session,
            		             limits.get(studies[i].getStudyId()));
        }

        session.getTransaction().commit();

        String[] colNames = new String[studies.length+1];
        colNames[0] = "BioentityId";
        for (int i=0; i<studies.length; i++) {
            colNames[i+1] = studyNames ? studies[i].getTitle() : studies[i].getStudyId().toString();
        }
        CSVWriter out  = new CSVWriter(colNames, matrixFile);
        Double[]  line = new Double[colNames.length-1];
        for (Long id : entities) {
            int sup = 0;
            for (int i=0; i<studies.length; i++) {
                line[i] = scores[i].get(id);
                if (line[i] != null) {
                    sup += 1;
                }
            }
            if (sup < minSupport) continue;
            out.write(id);
            for (int i=0; i<line.length; i++) {
                if (line[i] == null)
                   if (asRanks) out.write(0.0); else out.skip();
                else
                   out.write(line[i].doubleValue());
            }
        }
        out.close();
        return ErrorCode.OK;
    }

    static Map<Integer,double[]> prepareLimits(String def) {
    	String[][]            params = AsserUtil.prepareMapping(def);
    	Map<Integer,double[]> limits = new TreeMap<Integer,double[]>();

    	for (String[] param : params) {
    		limits.put(Integer.valueOf(param[0]),
    				   ActivityStatus.prepareInterval(param[1], "/"));
    	}
    	return limits;
    }

    static Map<Long,Double> loadHits(Study      study,
                                     Set<Long>  entities,
                                     boolean    asRanks,
                                     boolean    normalize,
                                     Session    session,
                                     double[]   limits) {
        Collection<Hit> set = study.getHits();
        if (limits != null) {
        	Iterator<Hit> hi = set.iterator();
        	while (hi.hasNext()) {
        		Hit h = hi.next();
        		double w = h.getWeight();
        		if (w < limits[0] || w > limits[1]) hi.remove();
        	}
        }

        Map<Long,Double> hits = new HashMap<Long,Double>(set.size()+10);
        double           det  = set.size()+1.0;
        double           last = Double.NaN;
        Set<Long>        sims = null;
        int              c    = 1;
        int              inc  = 1;
        double           sum  = 0.0;

        if (asRanks) {
            sims = new TreeSet<Long>();
            final Integer scoreType = study.getScoreType().getScoreTypeId();
            if (IDConstants.ScoreType_Absolute_value.equals(scoreType) ||
                IDConstants.ScoreType_Fold_change   .equals(scoreType)) {
               List<Hit> nSet = (set instanceof List) ? (List<Hit>)set : new ArrayList<Hit>(set);
               Collections.sort(nSet, new Comparator<Hit>() {
            	  boolean useInverse = IDConstants.ScoreType_Fold_change.equals(scoreType);
                  public int compare(Hit h1, Hit h2) {
                     double w1 = Math.abs(h1.getWeight());
                     double w2 = Math.abs(h2.getWeight());
                     if (w1 == w2) return 0;
                     if (useInverse) {
                    	 if (w1 < 1) w1 = 1/w1;
                    	 if (w2 < 1) w2 = 1/w2;
                     }
                     return (w1 < w2) ? -1 : 1;
                  }
               });
               set = nSet;
            } else
            if (study.getScoreType().isAscending()) {
                c   = (int)det+1;
                inc = -1;
            }
        }
        for (Hit hit : set) {
            Long   id   = hit.getBioentity().getBioentityId();
            double rank = hit.getWeight();
            if (asRanks) {
                if (last != rank) {
                   if (!sims.isEmpty()) {
                       asAverages(hits, sims);
                   }
                   sims.clear();
                   last = rank;
                }
                sims.add(id);
                c   += inc;
                rank = c/det;
            }
            hits.put(id, rank);
            sum += rank;
            if (entities != null) entities.add(id);
        }
        if (asRanks && !sims.isEmpty()) {
           asAverages(hits, sims);
        }
        if (normalize) {
           for (Entry<Long,Double> entry : hits.entrySet()) {
               entry.setValue(entry.getValue()/sum);
           }
        }
        return hits;
    }

    static private void asAverages(Map<Long,Double> hits, Set<Long> sims) {
        double sum = 0;
        for (Long lId : sims) {
            sum += hits.get(lId);
        }
        Double avgScore = sum / sims.size(); 
        for (Long lId : sims) {
            hits.put(lId, avgScore);
        }
    }

	static public Study[] loadStudies(String[] ids, Session session) {
        Study[] studies;

        if ((ids.length == 1) && ids[0].equals("*")) {
        	@SuppressWarnings("unchecked")
        	List<Study> sl = (List<Study>)HibernateUtil.selectAll(Study.class);
            studies        = (Study[])sl.toArray(new Study[sl.size()]);
        } else {
            studies = new Study[ids.length];
    
            for (int i=0; i<studies.length; i++) {
                studies[i] = (Study)session.get(Study.class, Integer.valueOf(ids[i]));
                if (studies[i] == null)
                    throw new IllegalArgumentException("Invalid study identitifier: "+ids[i]);
            }
        }
        return studies;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new ScoreMatrix().run(argv);
    }

}

package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.LatexTools;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.text.TextUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.Version;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Log;

/**
 * This Anduril component produces a description of the current
 * Moksiskaan installation and provides JDBC parameters for the direct
 * database access.
 */
public class MoksiskaanConnector extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        createProperties(cf.getOutput("connection"));

        return createDescription(cf);
    }

    static private ErrorCode createDescription(CommandFile cf) throws IOException {
        Configuration cfg       = HibernateUtil.getConfiguration();
        String[]      showLog   = AsserUtil.split(cf.getParameter("showLog"));
        File          reportDir = cf.getOutput("report");
        PrintWriter   report;

        if (!reportDir.exists() && !reportDir.mkdirs()) {
            cf.writeError("Cannot create output folder: "+reportDir.getAbsolutePath());
            return ErrorCode.ERROR;
        }

        try { report = new PrintWriter(new FileWriter(new File(reportDir, LatexTools.DOCUMENT_FILE))); }
        catch (IOException e) {
            cf.writeError(e);
            return ErrorCode.ERROR;
        }

        report.append("This analysis is based on Moksiskaan~\\cite{Laakso2010} (version ")
              .append(Version.getVersion())
              .append(") that is running on top of Hibernate with \\textit{")
              .append(TextUtil.addLineWraps(cfg.getProperty(Environment.DIALECT)))
              .append("\\/} to access the native database.\n\n");

        if (!showLog[0].isEmpty()) printLog(report, showLog);

        report.close();
        return ErrorCode.OK;
    }

    static private void printLog(PrintWriter out, String[] types) {
    	final Format timeFormat = new SimpleDateFormat("'\\texttt{['yyyy-MM-dd'] }'", Locale.US);

    	String query;
    	if ("*".equals(types[0])) {
    		query = "";
    	} else {
    		StringBuffer typeFilters = new StringBuffer(1024);
    		typeFilters.append("WHERE (type IN ('");
    		typeFilters.append(AsserUtil.collapse(types, "','"));
    		typeFilters.append("'))");
    		query = typeFilters.toString();
    	}

    	@SuppressWarnings("unchecked")
		List<Log> entries = (List<Log>)HibernateUtil.selectThese("FROM Log "+query+" ORDER BY time");
    	if (!entries.isEmpty()) {
    		out.append("Database history:\\\\\n");
    	}
    	for (Log entry : entries) {
    		out.append(timeFormat.format(entry.getTime()))
    		   .append(LatexTools.quote(entry.getType()))
    		   .append(": \\textit{")
    		   .append(TextUtil.addLineWraps(entry.getDescription()))
               .append("\\/}");
            if (entry.getRef() != null) {
               out.append(" See also~\\cite{")
                  .append(entry.getRef())
                  .append("}.");
            }
    		out.append("\\\\\n");
    	}
    }

    /**
     * Converts current Hibernate configurations into Asser.jar compatible
     * JDBC properties.
     */
    static public void createProperties(File file) throws IOException {
        Configuration cfg      = HibernateUtil.getConfiguration();
        Properties    conProps = new Properties();
        String        user     = cfg.getProperty(Environment.USER);
        String        pass     = cfg.getProperty(Environment.PASS);

        conProps.setProperty("database.url",     cfg.getProperty(Environment.URL));
        conProps.setProperty("database.driver",  cfg.getProperty(Environment.DRIVER));
        conProps.setProperty("database.timeout", "20");
        conProps.setProperty("database.recycle", "true");
        if (user != null) conProps.setProperty("database.user",     user);
        if (pass != null) conProps.setProperty("database.password", pass);

        Writer propsOut = new FileWriter(file);
        conProps.store(propsOut, "JDBC settings for Moksiskaan database");
        propsOut.close();        
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new MoksiskaanConnector().run(argv);
    }

}
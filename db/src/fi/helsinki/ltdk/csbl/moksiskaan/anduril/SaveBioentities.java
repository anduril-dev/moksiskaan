package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.IOException;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.*;

/**
 * Stores the given set of bioentities into the database.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public class SaveBioentities extends SkeletonComponent {

	protected ErrorCode runImpl(CommandFile cf) throws IOException {
	    final String ENTITY_QUERY = "SELECT B "+
                                    "FROM   Bioentity B, Xref X "+
                                    "WHERE  (B.id               = X.bioentity) AND "+
                                    "       (B.bioentityType.id = :type)       AND "+
                                    "       (B.organism.id      = :organism)   AND "+
                                    "       (X.xrefType.id      = :xrefType)   AND "+
                                    "       (X.value            = :xref)";
        int    saveCount = 0;
        int    oldCount  = 0;
		int    id;
		String colParam;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        id = cf.getIntParameter("organism");
        Organism organism = (Organism)session.get(Organism.class, id);
        if (organism == null)
            throw new IllegalArgumentException("Invalid organism identifier: "+id);

        id = cf.getIntParameter("bioentityType");
        BioentityType bioentityType = (BioentityType)session.get(BioentityType.class, id);
        if (bioentityType == null)
            throw new IllegalArgumentException("Invalid bioentity type identifier: "+id);

        id = cf.getIntParameter("xrefType");
        XrefType xrefType = null;
        if (id != -999999) {
           xrefType = (XrefType)session.get(XrefType.class, id);
           if (xrefType == null)
              throw new IllegalArgumentException("Invalid external reference type identifier: "+id);
        }

        CSVParser in      = new CSVParser(cf.getInput("bioentities"));
        int       colName = 0;
        int       colXref = 0;
        if (xrefType != null) {
        	colParam = cf.getParameter("xrefCol");
            if (!AsserUtil.onlyWhitespace(colParam)) {
                colXref= AsserUtil.indexOf(colParam, in.getColumnNames(), false, true);
            }
        }
        int      colAlias;
        XrefType aliasType;
        colParam = cf.getParameter("aliasCol");
        if (colParam == null) {
        	colAlias  = -1;
        	aliasType = null;
            colParam = cf.getParameter("nameCol");
            if (!AsserUtil.onlyWhitespace(colParam)) {
                colName = AsserUtil.indexOf(colParam, in.getColumnNames(), false, true);
            }
        } else {
        	if (AsserUtil.onlyWhitespace(colParam)) {
        		colAlias = 0;
        	} else {
                colAlias= AsserUtil.indexOf(colParam, in.getColumnNames(), false, true);
            }
        	aliasType = (XrefType)session.get(XrefType.class, cf.getIntParameter("aliasType"));
        }
        int[] colReg;
        colParam = cf.getParameter("regionCol");
        if ((colParam == null) || AsserUtil.onlyWhitespace(colParam)) {
        	colReg = new int[0];
        } else {
        	if (colParam.indexOf(',') < 0) {
        	   colReg = new int[] { AsserUtil.indexOf(colParam, in.getColumnNames(), false, true) };
        	} else {
        	   colReg = new int[4];
        	   String[] regTmp = AsserUtil.strictSplit(colParam, ",");
        	   if (regTmp.length != 4) throw new IllegalArgumentException("Invalid number of region columns: "+colParam);
        	   colReg[0] = AsserUtil.indexOf(regTmp[1], in.getColumnNames(), false, true);
        	   colReg[1] = AsserUtil.indexOf(regTmp[2], in.getColumnNames(), false, true);
        	   colReg[2] = regTmp[2].isEmpty() ? colReg[1] : AsserUtil.indexOf(regTmp[2], in.getColumnNames(), false, true);
        	   colReg[3] = regTmp[3].isEmpty() ? -1 : AsserUtil.indexOf(regTmp[3], in.getColumnNames(), false, true);
        	}
        }
        for (String[] line : in) {
            if ((aliasType == null) && (line[colName] == null)) continue;

            Xref      xref = null;
            Bioentity entity;
            DNARegion reg;
            if ((xrefType != null) && (line[colXref] != null)) {
               entity = (Bioentity)session.createQuery(ENTITY_QUERY)
                                          .setInteger("type",     bioentityType.getBioentityTypeId())
                                          .setInteger("organism", organism.getOrganismId())
                                          .setInteger("xrefType", xrefType.getXrefTypeId())
                                          .setString( "xref",     line[colXref])
                                          .uniqueResult();
               if (aliasType != null) { // This is the implementation of the SaveXrefs component.
            	   if (entity == null) {
            		   cf.writeLog("Unknown "+xrefType.getName()+": "+line[colXref]);
            	   } else {
            	       if (line[colAlias] == null) {
            	           cf.writeLog("No "+aliasType.getName()+" for "+entity.getName()+'.');
            	       } else {
            	           xref = new Xref(aliasType, entity, line[colAlias]);
            	           session.saveOrUpdate(xref);
            	       }
            	   }
            	   continue;
               }
               if (entity != null) {
                   oldCount++;
                   continue;
               }
               xref = new Xref();
               xref.setXrefType(xrefType);
               xref.setValue(line[colXref]);
            }
            entity = new Bioentity();
        	entity.setBioentityType(bioentityType);
        	entity.setOrganism(organism);
        	entity.setName(line[colName]);
        	session.saveOrUpdate(entity);
        	saveCount++;
        	if (xref != null) {
        	   xref.setBioentity(entity);
        	   session.saveOrUpdate(xref);
        	}
        	if ((colReg.length > 0) && (line[colReg[0]] != null)) {
        		if (colReg.length == 1) {
        		   reg = DNARegion.valueOf(line[colReg[0]]);
           		   reg.setBioentity(entity);
        		} else {
        		   reg = new DNARegion(entity,
        				               line[colReg[0]],
        				               Long.parseLong(line[colReg[1]]),
        				               Long.parseLong(line[colReg[2]]),
        				               colReg[3]<0 ? true : Boolean.parseBoolean(line[colReg[3]]));
        		}
        		session.saveOrUpdate(reg);
        	}
        }
        in.close();

        session.getTransaction().commit();
        if (aliasType == null) cf.writeLog("Saved "+saveCount+" bioentities and skipped "+oldCount+" bioentities as duplicates.");
		return ErrorCode.OK;
	}

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new SaveBioentities().run(argv);
    }

}

package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.moksiskaan.EntityUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Link;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkAnnotation;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;

/**
 * This class represents a pathway model that can be exported to GraphML.
 */
public class BioentityGraphML {

	/** An enumeration of data types available for GraphML attribute values. */
    static public enum ATTRIBUTE_TYPE {
        integer_number("int"),
        float_number("double"),
        string("string");

        public final String def;
        private ATTRIBUTE_TYPE(String definition) {
            def = definition;
        }
    }

    static private final String XML_HEADER =
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
        "<graphml xmlns             =\"http://graphml.graphdrawing.org/xmlns\"\n"+
        "         xmlns:xsi         =\"http://www.w3.org/2001/XMLSchema-instance\"\n"+
        "         xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns\n"+
        "                              http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">";
    static private final String XML_FOOTER =
        "</graph>\n</graphml>";

    /** Entity associated properties */
    Map<Long,Map<String,String>> eProps = new HashMap<Long,Map<String,String>>(1000);

    /** Entity property types */
    Map<String,ATTRIBUTE_TYPE> epTypes = new TreeMap<String,ATTRIBUTE_TYPE>();

    /** Link type associated properties */
    Map<Integer,Map<String,String>> ltProps = new HashMap<Integer,Map<String,String>>(20);

    /** Link type property types */
    Map<String,ATTRIBUTE_TYPE> ltpTypes = new TreeMap<String,ATTRIBUTE_TYPE>();

    /** Bioentities of the graph */
    Map<Long,Bioentity> entities = new HashMap<Long,Bioentity>(1000);

    /** Links between the entity nodes */
    List<Link> links = new ArrayList<Link>(1000);

    public BioentityGraphML() {
        epTypes .put("BioentityId", ATTRIBUTE_TYPE.string);
        epTypes .put("label",       ATTRIBUTE_TYPE.string);
        ltpTypes.put("LinkType",    ATTRIBUTE_TYPE.string);
        ltpTypes.put("LinkTypeId",  ATTRIBUTE_TYPE.integer_number);
        ltpTypes.put("LinkWeight",  ATTRIBUTE_TYPE.float_number);
    }

    public void addLink(Link    link,
                        Session session) {
        link.setSource(intern(link.getSource()));
        link.setTarget(intern(link.getTarget()));
        links.add(link);
        for (LinkAnnotation annot : link.getAnnotations()) {
            propertyType(ltpTypes, annot.getName(), annot.getValue());
        }
    }

    /** Returns the number of bioentities available for this graph. */
    public int size() { return entities.size(); }

    public Collection<Bioentity> bioentities() {
        return entities.values();
    }

    public Bioentity getBioentity(Long id) {
        return entities.get(id);
    }

    public String getEntityProperty(Long entityId, String property) {
        Map<String,String> props = eProps.get(entityId);
        return (props == null) ? null : props.get(property);
    }

    public void addEntityProperties(Bioentity          entity,
                                    Map<String,String> properties) {
        Long id = entity.getBioentityId();
        if (!entities.containsKey(id))
            throw new IllegalArgumentException("Bioentity "+id+" ("+entity.getName()+" is not part of the graph.");
        for (String name : properties.keySet()) {
            propertyType(epTypes, name, properties.get(name));
        }
        Map<String,String> props = eProps.get(id);
        if (props == null) {
            props = new HashMap<String,String>();
            eProps.put(id, props);
        }
        props.putAll(properties);
    }

    public void addEntityProperty(Bioentity entity, String property, String value) {
        Long id = entity.getBioentityId();
        if (!entities.containsKey(id))
            throw new IllegalArgumentException("Bioentity "+id+"("+entity.getName()+" is not part of the graph.");

        Map<String,String> props = eProps.get(id);
        if (props == null) {
            props = new HashMap<String,String>();
            eProps.put(id, props);
        }
        props.put(property, value);
        propertyType(epTypes, property, value);
    }

    public void addLinkTypeProperty(Integer linkTypeId, String property, String value) {
        Map<String,String> props = ltProps.get(linkTypeId);

        if (props == null) {
            props = new HashMap<String,String>();
            ltProps.put(linkTypeId, props);
        }
        props.put(property, value);
        propertyType(ltpTypes, property, value);
    }

    private void propertyType(Map<String,ATTRIBUTE_TYPE> defs, String name, String value) {
        ATTRIBUTE_TYPE type = defs.get(name);

        if (ATTRIBUTE_TYPE.string == type) return;

        int length = value.length();
        if (type == null)
            type = ATTRIBUTE_TYPE.integer_number;
        for (int i=0; i<length; i++) { 
            char c = value.charAt(i);
            if (((c>='0') && (c<='9')) || (c=='+') || (c=='-')) continue;
            if (c=='.') {
                type = ATTRIBUTE_TYPE.float_number;
            } else {
                type = ATTRIBUTE_TYPE.string;
                break;
            }
        }
        defs.put(name, type);
    }

    private Bioentity intern(Bioentity entity) {
        Bioentity old = entities.get(entity.getBioentityId());

        if (old == null) {
            entities.put(entity.getBioentityId(), entity);
            old = entity;
        }
        return old;
    }

    public void readLinkStyles(File file) throws IOException {
        if (file == null) return; // No style definitions available.

        CSVParser in    = new CSVParser(file);
        String[]  names = in.getColumnNames();

        for (String[] row : in) {
            Integer id = Integer.valueOf(row[0]);
            for (int i=1; i<row.length; i++) {
                if (row[i] != null)
                    addLinkTypeProperty(id, names[i], row[i]);
            }
        }
        in.close();
    }

    @SuppressWarnings("unchecked")
	public void addIdAttributes(String    xrefAttr,
                                Integer   xrefType,
                                Integer[] entityType,
                                Session   session) {
    	final String  ID_SELECT = "SELECT X.value FROM Xref X "+
                                  "WHERE (X.xrefType.id = "+xrefType+") AND "+
                                  "      (X.bioentity   = :bioentity)";
        for (Bioentity node : bioentities()) {
        	Integer nId    = node.getBioentityType().getBioentityTypeId();
        	boolean accept = false;
        	for (Integer aKey : entityType) {
        		if (nId.equals(aKey)) {
        			accept = true;
        			break;
        		}
        	}
            if (accept) {
            	List<String> xrefs = session.createQuery(ID_SELECT)
                                            .setParameter("bioentity", node)
                                            .list();
            	if (!xrefs.isEmpty()) {
            	   String xref = AsserUtil.collapse(xrefs);
	               addEntityProperty(node, xrefAttr, xref);
            	}
	        }
        }
        if (!epTypes.containsKey(xrefAttr)) {
        	epTypes.put(xrefAttr, ATTRIBUTE_TYPE.string);
        }
    }

    public void save(File file, String id) throws IOException {
        PrintWriter out = new PrintWriter(file);

        out.println(XML_HEADER);
        printKeyDeclarations(epTypes,  "node", out);
        printKeyDeclarations(ltpTypes, "edge", out);
        out.append("<graph id=\"").append(id).println("\" edgedefault=\"directed\">");

        List<Bioentity> genes = new ArrayList<Bioentity>(entities.values());
        Collections.sort(genes, EntityUtil.BIOENTITY_COMPARATOR);
        for (Bioentity gene : genes) {
            out.append("  <node id=\"").print(gene.getBioentityId());
            out.println("\">");
            out.append("    <data key=\"label\">").append(gene.getName()).println("</data>");
            out.append("    <data key=\"BioentityId\">").append(String.valueOf(gene.getBioentityId())).println("</data>");
            printProperties(eProps.get(gene.getBioentityId()), out);
            out.println("  </node>");
        }

        Collections.sort(links, EntityUtil.LINK_COMPARATOR);
        for (Link link : links) {
            out.append("  <edge source=\"").print(link.getSource().getBioentityId());
            out.append(     "\" target=\"").print(link.getTarget().getBioentityId());
            out.println("\">");
            printProperties(edgeProperties(link),out);
            out.println("  </edge>");
        }

        out.println(XML_FOOTER);
        out.close();
    }

    public void saveLegend(File file, String id) throws IOException {
        Set<LinkType>              lTypes = new TreeSet<LinkType>();
        Map<String,ATTRIBUTE_TYPE> nTypes = new HashMap<String,ATTRIBUTE_TYPE>();
        PrintWriter                out    = new PrintWriter(file);

        nTypes.put("label", ATTRIBUTE_TYPE.string);
        nTypes.put("shape", ATTRIBUTE_TYPE.string);

        for (Link link : links) {
            lTypes.add(link.getLinkType());
        }

        out.println(XML_HEADER);
        printKeyDeclarations(nTypes,   "node", out);
        printKeyDeclarations(ltpTypes, "edge", out);
        out.append("<graph id=\"").append(id).println("\" edgedefault=\"directed\">");
        for (LinkType lt : lTypes) {
            out.printf("  <node id=\"%s\"><data key=\"label\">%s</data></node>\n",
                       lt.getLinkTypeId(), lt.getName().replaceAll(" ", "\\\\n"));
            out.printf("  <node id=\"%sH\"><data key=\"shape\">point</data></node>\n",
                       lt.getLinkTypeId());
        }
        for (LinkType lt : lTypes) {
            Integer ltId = lt.getLinkTypeId();
            out.printf("  <edge source=\"%sH\" target=\"%s\">\n", ltId, ltId);
            printProperties(ltProps.get(ltId), out);
            out.println("  </edge>");
        }
        out.println(XML_FOOTER);
        out.close();
    }

    static private void printKeyDeclarations(Map<String,ATTRIBUTE_TYPE> keys,
                                             String                     scope,
                                             PrintWriter                out) {
        for (String name : keys.keySet()) {
            out.printf("<key id=\"%s\" for=\"%s\" attr.name=\"%s\" attr.type=\"%s\"/>\n",
                       name, scope, name, keys.get(name).def);
        }
    }

    static private void printProperties(Map<String,String> properties,
                                        PrintWriter        out) {
        if (properties == null) return;

        for (String key : properties.keySet()) {
            String value = properties.get(key);
            out.printf("    <data key=\"%s\">%s</data>\n", key, value);
        }        
    }

    private Map<String,String> edgeProperties(Link link) {
       Map<String,String> props = new HashMap<String, String>();
       Map<String,String> typeP = ltProps.get(link.getLinkType().getLinkTypeId());

       // Properties defined for the link type
       if (typeP != null) {
           props.putAll(typeP);
       }

       // Database annotations
       for (LinkAnnotation annot : link.getAnnotations()) {
           props.put(annot.getName(), annot.getValue());
       }

       props.put("LinkType",   link.getLinkType().getName());
       props.put("LinkTypeId", String.valueOf(link.getLinkType().getLinkTypeId()));
       props.put("LinkWeight", String.valueOf(link.getWeight()));
       return props;
    }

}

package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.moksiskaan.ActivityCode;

/**
 * Converts numerical measures to gene activity indicators.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.07
 */
public class ActivityStatus extends SkeletonComponent {

	protected ErrorCode runImpl(CommandFile cf) throws IOException {
		String                   naMethod  = cf.getParameter("naMethod");
		boolean                  naOutput  = cf.getBooleanParameter("naOutput");
		CSVParser                in        = new CSVParser(cf.getInput("measures"));
		String[]                 colsIn    = in.getColumnNames();
		int                      colDat    = AsserUtil.indexOf(cf.getParameter("valueColumn"), colsIn, true, true);
		String                   colIdName = cf.getParameter("idColumn").trim();
		List<String>             colsOut   = new ArrayList<String>(50);
		double[][]               intervals = new double[4][];
		Map<String,ActivityCode> resultMap = new TreeMap<String,ActivityCode>();
		int                      colId;

		if (colIdName.isEmpty()) {
		   colId = 0;
		} else {
		   colId = AsserUtil.indexOf(colIdName, colsIn, true, true);
		}

		intervals[0] = prepareInterval(cf.getParameter("defUp"),     AsserUtil.DEFAULT_DELIMITER);
		intervals[1] = prepareInterval(cf.getParameter("defStable"), AsserUtil.DEFAULT_DELIMITER);
		intervals[2] = prepareInterval(cf.getParameter("defDown"),   AsserUtil.DEFAULT_DELIMITER);
		intervals[3] = prepareInterval(cf.getParameter("defAbsent"), AsserUtil.DEFAULT_DELIMITER);

		for (String[] row : in) {
			if (row[colId] == null) continue;
			ActivityCode value; 
			if (row[colDat] == null) {
				if ("remove".equals(naMethod))
			       continue;
				if ("absent".equals(naMethod))
					value = ActivityCode.ABSENT;
				else
				if ("keep".equals(naMethod))
					value = null;
				else
					throw new IllegalArgumentException("Invalid naMethod: "+naMethod); 
			} else {
				value = getStatus(row[colDat], intervals);
			}
			if (!naOutput && (value == null)) continue;

			ActivityCode oldValue = resultMap.put(row[colId], value);
			if ((oldValue != null) && !oldValue.equals(value)) {
				cf.writeError("[line "+(in.getLineNumber()-1)+"] Multiple outcomes for "+row[colId]+": "+oldValue.name()+" versus "+value.name());
				in.close();
				return ErrorCode.INVALID_INPUT;
			}
		}
		in.close();

		colsOut.add(colsIn[colId]);
		colsOut.add(ActivityCode.COLUMN_NAME_STATUS);
		CSVWriter out = new CSVWriter(colsOut.toArray(new String[colsOut.size()]), cf.getOutput("status"));
		for (String key : resultMap.keySet()) {
			ActivityCode status = resultMap.get(key); 
			out.write(key);
			out.write(status==null ? null : status.toString(), false);
		}
		out.close();
		return ErrorCode.OK;
	}

	static public double[] prepareInterval(String def, String delim) {
		String[] parts = AsserUtil.trimAll(def.split(delim, -1));
		if (parts.length == 1) {
			if (parts[0].isEmpty()) return null;
			double v = Double.parseDouble(parts[0]);
			return new double[]{v, v};
		}
		if (parts.length == 2) {
			return new double[]{
				parts[0].isEmpty() ? Double.NEGATIVE_INFINITY : Double.parseDouble(parts[0]),
				parts[1].isEmpty() ? Double.POSITIVE_INFINITY : Double.parseDouble(parts[1])
			};
		}
		throw new IllegalArgumentException("Invalid interval: "+def);
	}

	static private ActivityCode getStatus(String     value,
			                              double[][] intervals) {
		final ActivityCode[] OPTIONS = {
			ActivityCode.UP,
			ActivityCode.STABLE,
			ActivityCode.DOWN,
			ActivityCode.ABSENT
		};
		double v = Double.parseDouble(value);
		for (int i=0; i<OPTIONS.length; i++) {
			if ((intervals[i] == null) ||
				(intervals[i][0] > v)  ||
				(intervals[i][1] < v))
				continue;
			return OPTIONS[i];
		}
		return null;
	}

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new ActivityStatus().run(argv);
    }

}

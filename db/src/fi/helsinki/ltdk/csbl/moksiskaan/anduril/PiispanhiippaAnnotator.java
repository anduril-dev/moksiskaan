package fi.helsinki.ltdk.csbl.moksiskaan.anduril;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.Tools;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa;

/**
 * This Anduril component provides a wrapper to
 * {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa}.
 */
public class PiispanhiippaAnnotator extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws IOException,
                                                       SQLException {
    	String[]      keys     = AsserUtil.split(cf.getParameter("keys"));
        Integer       organism = Integer.valueOf(cf.getParameter("organism"));
        OutputStream  out      = new FileOutputStream(cf.getOutput("bioAnnotation"));
        Piispanhiippa app      = cf.inputDefined("connection") ? new Piispanhiippa(cf.getInput("connection")) :
                                                                 new Piispanhiippa();
        String        sql;
        
        if (cf.inputDefined("sourceKeys") || !keys[0].isEmpty()) {
           sql = convertKeys(cf, keys, organism, app, out);
        } else {
           sql = app.listAll(cf.getParameter("inputDB"), organism, out);
        }
        out.close();

        Tools.writeString(cf.getOutput("query"), sql);

        return ErrorCode.OK;
    }

    static private String convertKeys(CommandFile   cf,
    		                          String[]      keys,
                                      Integer       organism,
                                      Piispanhiippa app,
                                      OutputStream  out) throws IOException,
                                                                SQLException {
        int          keyColumn  = 0;
        List<String> sourceKeys = new LinkedList<String>();
        for (String key : keys) {
        	if (!key.isEmpty()) {
        		sourceKeys.add(key);
        	}
        }

        File file = cf.getInput("sourceKeys");
        if (file != null) {
	        CSVParser in         = new CSVParser(file);
	        boolean   isListCol  = cf.getBooleanParameter("isListKey");
	        String    keyColName = cf.getParameter("keyColumn");
	        if (!AsserUtil.onlyWhitespace(keyColName)) {
	            keyColumn = AsserUtil.indexOf(keyColName, in.getColumnNames(), true, true);
	        }
	        for (String[] row : in) {
	            if ((row[keyColumn] == null) ||
	                AsserUtil.onlyWhitespace(row[keyColumn])) continue;
	            if (isListCol) {
	                for (String key : AsserUtil.split(row[keyColumn])) {
	                    sourceKeys.add(key);
	                }
	            } else {
	                sourceKeys.add(row[keyColumn]);
	            }
	        }
	        in.close();
        }

        return app.process(sourceKeys,
                           cf.getParameter("inputDB"),
                           AsserUtil.split(cf.getParameter("targetDB")),
                           cf.getParameter("linkTypes"),
                           !cf.getBooleanParameter("reverse"),
                           organism,
                           Piispanhiippa.parseOrderBy(cf.getParameter("orderBy")),
                           out);
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new PiispanhiippaAnnotator().run(argv);
    }

}

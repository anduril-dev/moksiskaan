package fi.helsinki.ltdk.csbl.moksiskaan.cytoscape;

import java.awt.Color;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.collections15.Transformer;

import cytoscape.CyEdge;
import cytoscape.CyNetwork;
import cytoscape.CyNode;
import cytoscape.Cytoscape;
import cytoscape.CytoscapeInit;
import cytoscape.data.CyAttributes;
import cytoscape.data.writers.CytoscapeSessionWriter;
import cytoscape.groups.CyGroup;
import cytoscape.groups.CyGroupManager;
import cytoscape.init.CyInitParams;
import cytoscape.layout.algorithms.GridNodeLayout;
import cytoscape.util.ColorUtil;
import cytoscape.visual.ArrowShape;
import cytoscape.visual.CalculatorCatalog;
import cytoscape.visual.NodeShape;
import cytoscape.visual.VisualMappingManager;
import cytoscape.visual.VisualPropertyType;
import cytoscape.visual.VisualStyle;
import cytoscape.visual.calculators.BasicCalculator;
import cytoscape.visual.mappings.DiscreteMapping;
import cytoscape.visual.mappings.ObjectMapping;
import cytoscape.visual.mappings.PassThroughMapping;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseGraph;
import edu.uci.ics.jung.io.GraphIOException;
import edu.uci.ics.jung.io.graphml.AbstractMetadata;
import edu.uci.ics.jung.io.graphml.EdgeMetadata;
import edu.uci.ics.jung.io.graphml.GraphMLReader2;
import edu.uci.ics.jung.io.graphml.GraphMetadata;
import edu.uci.ics.jung.io.graphml.HyperEdgeMetadata;
import edu.uci.ics.jung.io.graphml.NodeMetadata;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.db.TypeConverter;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;

/**
 * This is an Anduril component that converts Moksiskaan pathways (GraphML)
 * to Cytoscape sessions that may be used interactively.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 */
public class Pathway2Cytoscape extends SkeletonComponent {

    static public final String ATTRIBUTE_LINK_TYPE_ID   = "LinkTypeId";
    static public final String ATTRIBUTE_LINK_TYPE_NAME = "LinkTypeName";

    @Override
    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        String     title       = cf.getParameter("title");
        String     nameAttr    = cf.getParameter("nameAttr");
        String     linkAttr    = cf.getParameter("linkAttr");
        String     weightAttr  = cf.getParameter("weightAttr");
        String     tooltipAttr = cf.getParameter("tooltipAttr").trim();
        String[][] edgeCopy    = prepareMapping(cf.getParameter("edgeCopy"));
        String[][] vertexCopy  = prepareMapping(cf.getParameter("vertexCopy"));

        if (tooltipAttr.isEmpty())
           tooltipAttr = null;

        GraphMLReader2<Graph<String,String>,String,String> reader;
        Graph<String,String>                               graph;
        Transformer<GraphMetadata,Graph<String,String>>    grapTransformer = new GraphTransformer();
        MetadataTransformer<NodeMetadata>                  nodeTransformer = new MetadataTransformer<NodeMetadata>();
        MetadataTransformer<EdgeMetadata>                  edgeTransformer = new MetadataTransformer<EdgeMetadata>();
        MetadataTransformer<HyperEdgeMetadata>             hEdgTransformer = new MetadataTransformer<HyperEdgeMetadata>();
        String                                             inGraphFile     = cf.getInput("pathway").getAbsolutePath(); 
        reader = new GraphMLReader2<Graph<String,String>, String, String>(
                     new FileReader(inGraphFile),
                     grapTransformer, nodeTransformer, edgeTransformer, hEdgTransformer
                 );
        try {
			graph = reader.readGraph();
		} catch (GraphIOException e) {
			throw new IOException("Unable to read "+inGraphFile+'.', e);
		}
        //reader.load(cf.getInput("pathway").getAbsolutePath(), graph);

        // Prepare Cytoscape
        final String sessionName = cf.getOutput("session").getCanonicalPath();
        CyInitParams cyParams = new CyInitParams() {
            Properties props    = new Properties();
            Properties vizProps = new Properties();

            public String[]   getArgs() {               return AsserUtil.EMPTY_ARRAY_STRING; }
            public List<?>    getEdgeAttributeFiles() { return Collections.emptyList(); }
            public List<?>    getExpressionFiles() {    return Collections.emptyList(); }
            public List<?>    getGraphFiles() {         return Collections.emptyList(); }
            public int        getMode() {               return TEXT; }
            public List<?>    getNodeAttributeFiles() { return Collections.emptyList(); }
            public List<?>    getPlugins() {            return Collections.emptyList(); }
            public Properties getProps() {              return props; }
            public String     getSessionFile() {        return null; }
            public Properties getVizProps() {           return vizProps; }
        };
        CytoscapeInit initializer = new CytoscapeInit();
        if (!initializer.init(cyParams)) {
            cf.writeError("Could not initialize Cytoscape.");
            return ErrorCode.ERROR;
        }
        Cytoscape.getDesktop().setVisible(false); // THIS IS A HACK! We should not have a GUI.

        toCyNetwork(nodeTransformer,
                    edgeTransformer,
                    graph,
                    title,
                    nameAttr,
                    linkAttr,
                    weightAttr,
                    tooltipAttr,
                    edgeCopy,
                    vertexCopy);
        addGroups(cf.getInput("groups"));
        Cytoscape.getCurrentNetworkView().applyLayout(new GridNodeLayout());
        Cytoscape.getCurrentNetworkView().redrawGraph(false, true);

        CytoscapeSessionWriter sessionWriter = new CytoscapeSessionWriter(sessionName);
        try { sessionWriter.writeSessionToDisk(); }
        catch (Exception e) {
			throw new IOException("Cannot write session file to the disk.", e);
		}

        Cytoscape.getDesktop().dispose(); // We should not have the GUI really!
        return ErrorCode.OK;
    }

    static private void addGroups(File file) throws IOException {
        if (file == null) return;

        CSVParser    in    = new CSVParser(file);
        int          mCol  = AsserUtil.indexOf("Members", in.getColumnNames(), true, true);
        for (String[] line : in) {
            String[] names = AsserUtil.split(line[mCol]);
            CyGroup  group = CyGroupManager.createGroup(line[0], null);
            for (String name : names) {
                name = cleanUp(name);
                CyNode node = Cytoscape.getCyNode(name);
                if (node != null) {
                   group.addNode(node);
                }
            }
            Cytoscape.getCurrentNetworkView().getGraphPerspective().hideNode(group.getGroupNode());
            Cytoscape.firePropertyChange(Cytoscape.NETWORK_MODIFIED, null, Cytoscape.getCurrentNetwork());
            System.out.println("Created a new group ("+group.getGroupName()+") with "+group.getNodes()+'.');
        }
        in.close();
    }

    static private void netStyles(String tooltipAttr) {
        VisualMappingManager vmm       = Cytoscape.getVisualMappingManager();
        CalculatorCatalog    cc        = vmm.getCalculatorCatalog();
        DiscreteMapping      tgtarrows = new DiscreteMapping(ArrowShape.class, ATTRIBUTE_LINK_TYPE_ID);
        DiscreteMapping      colors    = new DiscreteMapping(Color.class,      ATTRIBUTE_LINK_TYPE_ID);

        VisualStyle newStyle = new VisualStyle(vmm.getVisualStyle());
        newStyle.setName("MoksiskaanPathway");
        vmm.getCalculatorCatalog().addVisualStyle(newStyle);
        vmm.setVisualStyle(newStyle);
        Cytoscape.getCurrentNetworkView().setVisualStyle(newStyle.getName());

        for (LinkTypeCy type : LinkTypeCy.values()) {
            tgtarrows.putMapValue(type.id, type.tgtarrow);
            colors   .putMapValue(type.id, type.color);
        }
        addCalculator(VisualPropertyType.EDGE_TGTARROW_SHAPE, tgtarrows, cc, newStyle, true);
        addCalculator(VisualPropertyType.EDGE_TGTARROW_COLOR, colors,    cc, newStyle, true);
        addCalculator(VisualPropertyType.EDGE_COLOR,          colors,    cc, newStyle, true);

        if (tooltipAttr != null) {
           PassThroughMapping tooltips = new PassThroughMapping(String.class, tooltipAttr);
           addCalculator(VisualPropertyType.NODE_TOOLTIP, tooltips, cc, newStyle, false);
        }
        vmm.applyEdgeAppearances();
    }

    static private void addCalculator(VisualPropertyType type,
                                      ObjectMapping      mapping,
                                      CalculatorCatalog  cc,
                                      VisualStyle        style,
                                      boolean            forEdge) {
       BasicCalculator calc = new BasicCalculator("Moksiskaan"+type, mapping, type);
       cc.addCalculator(calc);
       if (forEdge)
          style.getEdgeAppearanceCalculator().setCalculator(calc);
       else
          style.getNodeAppearanceCalculator().setCalculator(calc);
    }

    static public String[][] prepareMapping(String def) {
        String[][] map = AsserUtil.prepareMapping(def);

        for (String[] pair : map) {
        	if (pair[1] == null) pair[1] = pair[0];
        }
        return map;
    }

    static public void toCyNetwork(MetadataTransformer<NodeMetadata> vData,
                                   MetadataTransformer<EdgeMetadata> eData,
                                   Graph<String,String>              graph,
                                   String                            title,
                                   String                            nameAttr,
                                   String                            linkAttr,
                                   String                            weightAttr,
                                   String                            tooltipAttr,
                                   String[][]                        edgeCopy,
                                   String[][]                        vertexCopy) {
        CyNetwork    net    = Cytoscape.createNetwork(title);
        CyAttributes vAttr  = Cytoscape.getNodeAttributes();
        CyAttributes eAttr  = Cytoscape.getEdgeAttributes();
        String       value;

        Cytoscape.ensureCapacity(graph.getVertexCount(), graph.getEdgeCount());

        // PROCESS NODES
        RuntimeException nodeFailure = null;
        for (String vertex : graph.getVertices()) {
            try {
                String name = cleanUp(valueOfV(nameAttr, vertex, vData));
                CyNode node = Cytoscape.getCyNode(name, true);
                String id = node.getIdentifier();
                net.addNode(node);
    
                value = valueOfV("color", vertex, vData);
                if (value != null)
                    vAttr.setAttribute(id, VisualPropertyType.NODE_BORDER_COLOR.getBypassAttrName(), asColor(value));
                value = valueOfV("fillcolor", vertex, vData);
                if (value != null)
                    vAttr.setAttribute(id, VisualPropertyType.NODE_FILL_COLOR.getBypassAttrName(), asColor(value));
                value = valueOfV("fontsize", vertex, vData);
                if (value != null)
                    vAttr.setAttribute(id, VisualPropertyType.NODE_FONT_SIZE.getBypassAttrName(), value);
                value = valueOfV("shape", vertex, vData);
                if (value != null)
                    vAttr.setAttribute(id, VisualPropertyType.NODE_SHAPE.getBypassAttrName(), asShape(value));
                value = valueOfV("penwidth", vertex, vData);
                if (value != null)
                    vAttr.setAttribute(id, VisualPropertyType.NODE_LINE_WIDTH.getBypassAttrName(), value);
    
                for (int i=0; i<vertexCopy.length; i++) {
                    value = valueOfV(vertexCopy[i][0], vertex, vData);
                    if (value != null) {
                       vAttr.setAttribute(id, vertexCopy[i][1], value);
                    }
                }
            } catch (RuntimeException e) {
                if (nodeFailure == null) nodeFailure = e;
                System.err.println("Cannot process vertex '"+vertex+"': "+e.toString());
            }
        }
        if (nodeFailure != null) throw nodeFailure;

        // PROCESS EDGES
        for (String edge : graph.getEdges()) {
            String     source = cleanUp(valueOfV(nameAttr, graph.getSource(edge), vData));
            String     target = cleanUp(valueOfV(nameAttr, graph.getDest(edge),   vData));
            LinkTypeCy type   = LinkTypeCy.forLinkTypeId(valueOfE(linkAttr, edge, eData));

            CyEdge cyEdge = Cytoscape.getCyEdge(source, edge, target, type.interaction.name());
            String id     = cyEdge.getIdentifier();
            net.addEdge(cyEdge);
            eAttr.setAttribute(id, ATTRIBUTE_LINK_TYPE_ID,   type.id);
            eAttr.setAttribute(id, ATTRIBUTE_LINK_TYPE_NAME, type.name());

            value = valueOfE(weightAttr, edge, eData);
            if (value != null) {
                double w = Double.parseDouble(value)+0.1;
                eAttr.setAttribute(id, VisualPropertyType.EDGE_LINE_WIDTH.getBypassAttrName(), String.valueOf(w));
            }

            for (int i=0; i<edgeCopy.length; i++) {
                value = valueOfE(edgeCopy[i][0], edge, eData);
                if (value != null) {
                   eAttr.setAttribute(id, edgeCopy[i][1], value);
                }
            }
        }
        netStyles(tooltipAttr);
    }

    static private String cleanUp(String value) {
    	if (value == null) return null;
        String text = value;
        text = text.replace(' ', '_');
        if (text.indexOf('\\') >= 0) {
            text = text.replaceAll("\\\\n", AsserUtil.EMPTY_STRING);
        }
        return text;
    }

    static private String asShape(String def) {
        if ("box"          .equals(def)) return NodeShape.RECT.getShapeName();
        if ("diamond"      .equals(def)) return NodeShape.DIAMOND.getShapeName();
        if ("doublehaxagon".equals(def)) return NodeShape.HEXAGON.getShapeName();
        if ("doubleoctagon".equals(def)) return NodeShape.OCTAGON.getShapeName();
        if ("ellipse"      .equals(def)) return NodeShape.ELLIPSE.getShapeName();
        if ("hexagon"      .equals(def)) return NodeShape.HEXAGON.getShapeName();
        if ("octagon"      .equals(def)) return NodeShape.OCTAGON.getShapeName();
        if ("point"        .equals(def)) return NodeShape.DIAMOND.getShapeName();
        return null;
    }

    static private String asColor(String def) {
        Color col;
        try {
            if (def.charAt(0)=='#') {
                int r = Integer.parseInt(def.substring(1,3), 16);
                int g = Integer.parseInt(def.substring(3,5), 16);
                int b = Integer.parseInt(def.substring(5,7), 16);
                col = new Color(r, g, b);
            } else {
                float[] terms = TypeConverter.toFloatArray(AsserUtil.split(def));
                col = Color.getHSBColor(terms[0], terms[1], terms[2]);
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid color definition: "+def, e);
        }
        return ColorUtil.getColorAsText(col);
    }

    static private String valueOfV(String                            key,
                                   String                            item,
                                   MetadataTransformer<NodeMetadata> transformer) {
        return valueOfImpl("v_", key, item, transformer);
    }

    static private String valueOfE(String                            key,
                                   String                            item,
                                   MetadataTransformer<EdgeMetadata> transformer) {
        return valueOfImpl("e_", key, item, transformer);
    }

    static private String valueOfImpl(String                 prefix,
                                      String                 key,
                                      String                 item,
                                      MetadataTransformer<?> transformer) {
        AbstractMetadata data = transformer.data.get(item);
        if (data == null) throw new IllegalStateException("Unknown item: "+item);
        String value = data.getProperty(key);
        if (value == null) value = data.getProperty(prefix+key);
        if ((value == null) || value.isEmpty()) return null;
        return value;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new Pathway2Cytoscape().run(argv);
    }

}

/**
 * Provides a graphs for the given metadata.
 */
class GraphTransformer implements Transformer<GraphMetadata,Graph<String,String>> {

    @Override
    public Graph<String,String> transform(GraphMetadata element) {
        return new SparseGraph<String,String>();
    }

}

/**
 * Converts graph {@link edu.uci.ics.jung.io.graphml.AbstractMetadata metadata}
 * objects to {@link java.lang.String strings}.
 *
 * @param <T> Actual metadata type (node, edge, hyper edge)
 */
class MetadataTransformer<T extends AbstractMetadata> implements Transformer<T,String> {

    int           counter = 0;
    Map<String,T> data    = new HashMap<String,T>();

    @Override
    public String transform(T element) {
        String prefix;
        String id;
        switch (element.getMetadataType()) {
          case EDGE      : id     = ((EdgeMetadata)element).getId();
                           prefix = "edge";
                           break;
          case HYPEREDGE : id     = ((HyperEdgeMetadata)element).getId();
                           prefix = "hedge";
                           break;
          case NODE      : id     = ((NodeMetadata)element).getId();
                           prefix = "node";
                           break;
          default        : return "element"+counter++; 
        }
        if (id == null) {
            id = prefix+counter++;
        }
        data.put(id, element);
        return id;
    }

}

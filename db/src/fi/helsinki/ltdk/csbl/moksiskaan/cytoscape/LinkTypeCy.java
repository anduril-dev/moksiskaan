package fi.helsinki.ltdk.csbl.moksiskaan.cytoscape;

import java.awt.Color;

import cytoscape.visual.ArrowShape;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;

/** Supported Moksiskaan LinkTypes and their Cytoscape mappings **/
public enum LinkTypeCy {
    ProteinActivation        (IDConstants.LinkType_protein_activation,
                              Interaction.pp,
                              ArrowShape.ARROW,
                              Color.ORANGE),
    ProteinInhibition        (IDConstants.LinkType_protein_inhibition,
                              Interaction.pp,
                              ArrowShape.T,
                              Color.ORANGE),
    ProteinStateChange       (IDConstants.LinkType_protein_state_change,
                              Interaction.pp,
                              ArrowShape.ARROW,
                              Color.ORANGE),
    ProteinBinding           (IDConstants.LinkType_protein_binding,
                              Interaction.pp,
                              ArrowShape.ARROW,
                              Color.ORANGE),
    ProteinDissociation      (IDConstants.LinkType_protein_dissociation,
                              Interaction.pp,
                              ArrowShape.ARROW,
                              Color.ORANGE),
    GeneExpression           (IDConstants.LinkType_gene_expression,
                              Interaction.pd,
                              ArrowShape.ARROW,
                              Color.BLACK),
    GeneRepsession           (IDConstants.LinkType_gene_repression,
                              Interaction.pd,
                              ArrowShape.T,
                              Color.BLACK),
    Phosphorylation          (IDConstants.LinkType_phosphorylation,
                              Interaction.pp,
                              ArrowShape.CIRCLE,
                              Color.ORANGE),
    Dephosphorylation        (IDConstants.LinkType_dephosphorylation,
                              Interaction.pp,
                              ArrowShape.DIAMOND,
                              Color.ORANGE),
    Glycosylation            (IDConstants.LinkType_glycosylation,
                              Interaction.pp,
                              ArrowShape.CIRCLE,
                              Color.ORANGE),
    Ubiquitination           (IDConstants.LinkType_ubiquitination,
                              Interaction.pp,
                              ArrowShape.CIRCLE,
                              Color.ORANGE),
    Methylation              (IDConstants.LinkType_methylation,
                              Interaction.pp,
                              ArrowShape.CIRCLE,
                              Color.ORANGE),
    ChemicalReaction         (IDConstants.LinkType_chemical_reaction,
                              Interaction.mp,
                              ArrowShape.ARROW,
                              Color.CYAN),
    PathwayPrecedence        (IDConstants.LinkType_pathway_precedence,
                              Interaction.prp,
                              ArrowShape.DELTA,
                              Color.CYAN),
    ProteinProteinInteraction(IDConstants.LinkType_protein_protein_interaction,
                              Interaction.pp,
                              ArrowShape.NONE,
                              Color.ORANGE),
    DrugActivation           (IDConstants.LinkType_drug_promotes,
                              Interaction.mp,
                              ArrowShape.ARROW,
                              Color.GREEN),
    DrugInhibition           (IDConstants.LinkType_drug_inhibits,
                              Interaction.mp,
                              ArrowShape.T,
                              Color.GREEN),
    DrugTarget               (IDConstants.LinkType_drug_target,
                              Interaction.mp,
                              ArrowShape.CIRCLE,
                              Color.GREEN),
    DrugMetabolism           (IDConstants.LinkType_drug_metabolism,
                              Interaction.mp,
                              ArrowShape.ARROW,
                              Color.CYAN),
    DrugIndication           (IDConstants.LinkType_drug_indication,
                              Interaction.go,
                              ArrowShape.CIRCLE,
                              Color.GREEN),
    PositiveRegulation       (IDConstants.LinkType_positive_regulation,
                              Interaction.go,
                              ArrowShape.ARROW,
                              Color.GRAY),
    NegativeRegulation       (IDConstants.LinkType_negative_regulation,
                              Interaction.go,
                              ArrowShape.T,
                              Color.GRAY),
    Biomarker                (IDConstants.LinkType_biomarker,
                              Interaction.go,
                              ArrowShape.CIRCLE,
                              Color.GRAY),
    Coexist                  (IDConstants.LinkType_coexist,
                              Interaction.xx,
                              ArrowShape.NONE,
                              Color.LIGHT_GRAY);

    final Integer     id;
    final Interaction interaction;
    final ArrowShape  tgtarrow;
    final Color       color;

    LinkTypeCy(Integer     id,
               Interaction interact,
               ArrowShape  endArrow,
               Color       col) {
        this.id          = id;
        this.interaction = interact;
        this.tgtarrow    = endArrow;
        this.color       = col;
    }

    static public LinkTypeCy forLinkTypeId(String ltId) {
        Integer key = Integer.parseInt(ltId);
        for (LinkTypeCy item : values()) {
            if (item.id.equals(key))
                return item;
        }
        throw new IllegalArgumentException("Unsupported LinkType: "+ltId);
    }

    /** Cytoscape relationships between the pathway elements */
    static public enum Interaction {
     /* Some commonly used edge types:
        pp .................. protein  -> protein interaction
        pd .................. protein  -> DNA   
        pr .................. protein  -> reaction
        rc .................. reaction -> compound
        cr .................. compound -> reaction
        gl .................. genetic lethal relationship
        pm .................. protein-metabolite interaction
        mp .................. metabolite-protein interaction
     */
        /** Protein-protein interaction */
        pp,
        /** Protein-DNA interaction such as a transcriptional action */
        pd,
        /** Metabolic reaction between two entzymes (protein-reaction-protein) */
        prp,
        /** A chemical compound interfering with a protein */
        mp,
        /** A Gene Ontology / disease link (a Moksiskaan specific extension) */
        go,
        /** Non-biological association (a Moksiskaan specific extension) */
        xx
    }

}
package fi.helsinki.ltdk.csbl.moksiskaan.test;

import java.util.List;

import org.hibernate.Session;

import fi.helsinki.ltdk.csbl.moksiskaan.HibernateUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Organism;

/**
 * This stand-alone application can be used to check if Moksiskaan database is available
 * for use. This test focuses on Hibernate configuration.
 */
public final class DbValidator {

  /** No instantiation */
  private DbValidator() {}

  static public void main(String[] argv) {
     tryConnection();
     printOrganisms();
     HibernateUtil.getSessionFactory().close();
  }

  static private void tryConnection() {
     print("Connecting to Moksiskaan database...");

     Session session = HibernateUtil.getSessionFactory().getCurrentSession();
     session.beginTransaction();
     if (session.getStatistics().getEntityCount() > 0)
         throw new IllegalStateException("Too many entities: "+session.getStatistics().getEntityCount());
     session.getTransaction().commit();
     print("...OK");
  }

  static private void printOrganisms() {
	  @SuppressWarnings("unchecked")
      List<Organism> list = (List<Organism>)HibernateUtil.selectAll(Organism.class);

      if (list.isEmpty()) {
          print("No organisms found from the database.");
      } else {
          print("Organisms available:");
          for (Object obj : list) {
              Organism org = (Organism)obj;
              print(" * "+org.getName()+" "+org.getAuthor()+", "+org.getYear());
          }
      }
  }

  /**
   * Write messages to the user.
   */
  static public void print(String msg) {
     System.out.println(msg);
  }

}
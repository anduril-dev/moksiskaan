package fi.helsinki.ltdk.csbl.moksiskaan;

import java.util.Comparator;

import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Link;

/**
 * Miscellaneous utilities for the database schema POJOs.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.05
 */
public final class EntityUtil {

	 static public final Bioentity[] EMPTY_LIST_BIOENTITY = new Bioentity[0];

	 /** No instantiation */
	 private EntityUtil() {};

	 static public final Comparator<Bioentity> BIOENTITY_COMPARATOR = new Comparator<Bioentity>() {
		@Override
		public int compare(Bioentity arg0, Bioentity arg1) {
			String name1 = arg0.getName();
			String name2 = arg1.getName();
			return name1.compareToIgnoreCase(name2);
		}
	 };

	 static public final Comparator<Link> LINK_COMPARATOR = new Comparator<Link>() {
			@Override
			public int compare(Link arg0, Link arg1) {
				int value = arg0.getLinkType().getLinkTypeId()-arg1.getLinkType().getLinkTypeId();
				if (value == 0) {
					value = BIOENTITY_COMPARATOR.compare(arg0.getSource(), arg1.getSource());
				}
				if (value == 0) {
					value = BIOENTITY_COMPARATOR.compare(arg0.getTarget(), arg1.getTarget());
				}
				if (value == 0) {
					value = (int)(arg0.getLinkId()-arg1.getLinkId());
				}
				return value;
			}		 
	 };

}

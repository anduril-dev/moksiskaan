package fi.helsinki.ltdk.csbl.moksiskaan.query;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.MainArgument;
import fi.helsinki.ltdk.csbl.asser.annotation.IDConverter;
import fi.helsinki.ltdk.csbl.asser.annotation.InterestingRegion;
import fi.helsinki.ltdk.csbl.asser.db.JDBCConnection;
import fi.helsinki.ltdk.csbl.asser.db.ResultSet2CSV;
import fi.helsinki.ltdk.csbl.asser.db.ResultSetProcessor;
import fi.helsinki.ltdk.csbl.asser.db.TypeConverter;
import fi.helsinki.ltdk.csbl.asser.text.TextUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.Version;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Organism;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType;

/**
 * This the executable class of Piispanhiippa.
 * Piispanhiippa is a bioentity query interface for Moksiskaan database.
 *  
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 */
public class Piispanhiippa {

    /** Name of the default configuration file. This file should be located into the class path. */
    static public final String CFG_FILE = "moksiskaandb.properties"; 

    /** Synthetic reference types */
    static public enum PseudoXref {
        BioentityId(      false, "Internal Moksiskaan identifier of the bioentity",
        		                 "@.\"bioentityId\""),
        BioentityName(    true,  "Human readable name of the bioentity",
                                 "@.\"name\""),
        BioentityTypeId(  true,  "Internal Moksiskaan identifier of the bioentity type",
        		                 "@.\"bioentityTypeId\""),
        BioentityTypeName(true,  "Human readable name of the bioentity type",
                                 "@.\"name\""),
        DNARegion(        false, "Genome locus of the bioentity (chr:start-end)",
                                 "@.\"chromosome\"||':'||@.\"start\"||'-'||@.\"stop\""),
        DNAStrand(        false, "Chromosome strand for the bioentity",
                                 "CASE @.\"strand\" WHEN TRUE THEN '1' WHEN FALSE THEN '-1' ELSE NULL END"),
        HitEvidence(      false, "Boolean that is true if the study supports this finding",
                                 "@.\"evidence\""),
        HitStudyId(       false, "Internal Moksiskaan identifier of the bioentity associated studies",
                                 "@.\"studyId\""),
        HitStudyName(     true,  "Human readable names of the bioentity associated studies",
                                 "@.\"title\""),
        HitWeight(        false, "Hit score",
                                 "@.\"weight\"");

        public final String  sqlDef;
        public final String  description;
        public final boolean useBioentity;

        PseudoXref(boolean useEntity,
        		   String  desc,
        		   String  valueSelect) {
            useBioentity = useEntity;
            description  = desc;
            sqlDef       = valueSelect;
        }
        
        public String getColumnSelect(String table) {
        	return sqlDef.replaceAll("@", table);
        }
    };

    /** Ensembl connection */
    private JDBCConnection con;

    private int limit = -1;

    /**
     * Creates an instance using the default configurations
     * ({@link #CFG_FILE}).
     */
    public Piispanhiippa() throws IOException, SQLException {
        this(CFG_FILE);
    }

    /**
     * Creates an instance using the given configuration file.
     */
    public Piispanhiippa(String configFile) throws IOException, SQLException {
        con = new JDBCConnection(configFile);
    }

    /**
     * Creates an instance using the given configuration file.
     */
    public Piispanhiippa(File configFile) throws IOException, SQLException {
        con = new JDBCConnection(configFile);
    }

    /**
     * Creates an instance using the given configuration file.
     */
    public Piispanhiippa(URL configFile) throws IOException, SQLException {
        con = new JDBCConnection(configFile.openStream(), configFile.toString());
    }

    /**
     * Limit the maximum number of result items to this value or show them
     * all by setting this limit to a negative value.
     */
    public void setResultLimit(int v) { limit = v; }

    /** JDBC URL for Moksiskaan connection */
    public String getURL() {
        return con.getURL();
    }

    /**
     * Provides a shared access to the underlying database.
     */
    public JDBCConnection getConnection() {
    	return con;
    }

    /**
     * Prints all references of the given type.
     *
     * @param Rs            Reference type
     * @param organism      Organism of interest
     * @param streamOut     Target stream for the output
     * @return              SQL query that produced the results
     * @throws SQLException If the database query fails.
     */
    public String listAll(String       Rs,
                          Integer      organism,
                          OutputStream streamOut) throws SQLException {
        ResultSet2CSV out = new ResultSet2CSV(streamOut);
        out.setUseQuotes(false);
		String sql = listAll(Rs, organism, out);
		out.close();
		return sql;
    }

    /**
     * Prints all references of the given type.
     *
     * @param Rs            Reference type
     * @param organism      Organism of interest
     * @param out           Output handler
     * @return              SQL query that produced the results
     * @throws SQLException If the database query fails.
     */
    public String listAll(String             Rs,
    		              Integer            organism,
    		              ResultSetProcessor out) throws SQLException {
        boolean    useSourceXref = Character.isDigit(Rs.charAt(0));
        PseudoXref pXref;
        String     bioTable;

        // Input validation...
        validate(organism, "Organism", "organismId");
        if (useSourceXref) {
           validate(Rs, "XrefType", "xrefTypeId");
           bioTable = "X";
           pXref    = null;
        } else {
           try { pXref = PseudoXref.valueOf(Rs); }
           catch (IllegalArgumentException e) {
              throw new IllegalArgumentException("Invalid reference type: "+Rs, e);
           }
           if ((PseudoXref.BioentityId     == pXref)   ||
               (PseudoXref.BioentityName   == pXref) ||
               (PseudoXref.BioentityTypeId == pXref)) {
              bioTable = "B";
           } else {
              bioTable = "T";
           }
        }

    	StringBuffer sqlBuffer = new StringBuffer(1000);
    	sqlBuffer.append("-- All elements with the reference type of ")
                 .append(Rs)
                 .append("\nSELECT DISTINCT ")
                 .append(useSourceXref ? "X.\"value\"" : pXref.getColumnSelect(bioTable))
                 .append(" AS \"")
                 .append(asColumnName(Rs))
                 .append("\"\nFROM   \"Bioentity\" B");
        if (useSourceXref) {
           sqlBuffer.append(", \"Xref\" X\nWHERE  (X.\"xrefTypeId\"  = ?)               AND\n     ");
        } else if (PseudoXref.BioentityTypeName == pXref) {
           sqlBuffer.append(", \"BioentityType\" T\nWHERE");
        } else if ((PseudoXref.DNARegion == pXref) ||
                   (PseudoXref.DNAStrand == pXref)) {
           sqlBuffer.append(", \"DNARegion\" T\nWHERE");
        } else if ((PseudoXref.HitEvidence == pXref) ||
                   (PseudoXref.HitStudyId  == pXref) ||
                   (PseudoXref.HitWeight   == pXref)) {
           sqlBuffer.append(", \"Hit\" T\nWHERE");
        } else if (PseudoXref.HitStudyName == pXref) {
           sqlBuffer.append(", \"Hit\" H, \"Study\" T\nWHERE  (T.\"studyId\"     = H.\"studyId\")     AND\n     ");
           bioTable = "H";
        }
        if ("B".equals(bioTable)) {
           sqlBuffer.append("\nWHERE  (B.\"organismId\" = ?)");
        } else if (PseudoXref.BioentityTypeName == pXref) {
           sqlBuffer.append("  (T.\"bioentityTypeId\" = B.\"bioentityTypeId\") AND\n       (B.\"organismId\"      = ?)");
        } else {
           sqlBuffer.append("  (")
                    .append(bioTable)
                    .append(".\"bioentityId\" = B.\"bioentityId\") AND\n       (B.\"organismId\"  = ?)");
        }
        sqlBuffer.append("\nORDER  BY 1");
        if (limit >= 0) sqlBuffer.append("\nLIMIT  ").append(limit);

        String sql = sqlBuffer.toString();
        if (useSourceXref)
           con.doSelect(sql, out, Integer.valueOf(Rs), organism);
        else
           con.doSelect(sql, out, organism);
    	return sql;
    }

    public String process(List<String>       keys,
                          String             Rs,
                          String[]           Rt,
                          String             linkTypes,
                          boolean            isDirect,
                          Integer            organism,
                          int[]              orderBy,
                          OutputStream       out) throws SQLException {
        ResultSet2CSV processor = new ResultSet2CSV(out);
        processor.setUseQuotes(false);
        String sql = process(keys,
                             Rs,
                             Rt,
                             linkTypes,
                             isDirect,
                             organism,
                             orderBy,
                             processor);
        if (keys.isEmpty()) {
            processor.prepareOutput(con.getMetaData(sql));
        }
        processor.close();
        return sql;
    }

    public String process(List<String>       keys,
                          String             Rs,
                          String[]           Rt,
                          String             linkTypes,
                          boolean            isDirect,
                          Integer            organism,
                          int[]              orderBy,
                          ResultSetProcessor processor) throws SQLException {
        if (Rt.length < 1)    throw new IllegalArgumentException("No target references of interest.");
        if (organism == null) throw new IllegalStateException("The organism has not been defined.");

        boolean      useSourceXref = Character.isDigit(Rs.charAt(0));
        boolean      useLink       = (linkTypes != null) && !AsserUtil.onlyWhitespace(linkTypes);
        boolean      useBigInt     = false;
        String       sIdColumn     = useLink ? "L.\""+(isDirect ? "target\"" : "source\"") : "SB.\"bioentityId\"";
        PseudoXref[] pseudoRefs    = new PseudoXref[Rt.length];
        boolean      tbUsed        = false;
        boolean      shUsed        = false;
        boolean      ssUsed        = false;
        boolean      sbtUsed       = false;
        boolean      hitUsed       = false;
        boolean      regUsed       = false;
        boolean      srUsed        = false;
        PseudoXref   pXref         = null;

        // Input validation...
        validate(organism, "Organism", "organismId");
        if (useSourceXref) {
           validate(Rs, "XrefType", "xrefTypeId");
        } else {
           try { pXref = PseudoXref.valueOf(Rs); }
           catch (IllegalArgumentException e) {
              throw new IllegalArgumentException("Invalid source reference type: "+Rs, e);
           }
           if (PseudoXref.BioentityTypeName == pXref) { sbtUsed = true; } else
           if (PseudoXref.DNARegion         == pXref) { srUsed  = true; } else
           if (PseudoXref.HitStudyId        == pXref) { shUsed  = true; } else
           if (PseudoXref.HitStudyName      == pXref) { shUsed  = ssUsed = true; }
        }

        StringBuffer sqlBuffer = new StringBuffer(1000);
        sqlBuffer.append("-- Expected source reference type is ")
                 .append(Rs)
                 .append(".\nSELECT DISTINCT ? AS \"sourceKey\"");
        if (useLink) {
           sqlBuffer.append(", L.\"linkTypeId\", L.\"weight\" AS \"linkWeight\"");
        }
        for (int i=0; i<Rt.length; i++) {
            String rtColName = asColumnName(Rt[i]);
            sqlBuffer.append(",\n       ");
            if (Character.isDigit(Rt[i].charAt(0))) {
               sqlBuffer.append("TX").append(i).append(".\"value\"");
            } else {
               pseudoRefs[i] = PseudoXref.valueOf(Rt[i]);
               if (useLink && !tbUsed && pseudoRefs[i].useBioentity)
                  tbUsed = true;
               if (PseudoXref.BioentityId == pseudoRefs[i]) {
                  sqlBuffer.append(sIdColumn);
               } else
               if ((PseudoXref.BioentityName   == pseudoRefs[i]) ||
            	   (PseudoXref.BioentityTypeId == pseudoRefs[i])) {
                  sqlBuffer.append(pseudoRefs[i].getColumnSelect(useLink ? "TB" : "SB"));
               } else
               if (PseudoXref.BioentityTypeName == pseudoRefs[i]) {
                  sqlBuffer.append("BT.\"name\"");
               } else
               if ((PseudoXref.DNARegion == pseudoRefs[i]) ||
                   (PseudoXref.DNAStrand == pseudoRefs[i])) {
                  sqlBuffer.append(pseudoRefs[i].getColumnSelect("R"));
               } else
               if ((PseudoXref.HitEvidence == pseudoRefs[i]) ||
                   (PseudoXref.HitStudyId  == pseudoRefs[i]) ||
                   (PseudoXref.HitWeight   == pseudoRefs[i])) {
                  sqlBuffer.append(pseudoRefs[i].getColumnSelect("H"));
               } else
               if (PseudoXref.HitStudyName == pseudoRefs[i]) {
                  sqlBuffer.append(pseudoRefs[i].getColumnSelect("S"));
               }
            }
            sqlBuffer.append(" AS \"").append(rtColName).append('"');
        }
        sqlBuffer.append("\nFROM   ");
        if (useSourceXref) {
           sqlBuffer.append("\"Xref\" SX, ");
        } else {
           if (sbtUsed) { sqlBuffer.append(useLink ? "\"BioentityType\" SBT, " : "\"BioentityType\" BT, "); }
           if (srUsed)  { sqlBuffer.append(useLink ? "\"DNARegion\" SR, "      : "\"DNARegion\" R, "); regUsed = !useLink; }
           if (shUsed)  { sqlBuffer.append(useLink ? "\"Hit\" SH, "            : "\"Hit\" H, "); hitUsed = !useLink; }
           if (ssUsed)  { sqlBuffer.append(useLink ? "\"Study\" SS, "          : "\"Study\" S, "); }
        }
        sqlBuffer.append("\"Bioentity\" SB");
        if (useLink) sqlBuffer.append(", \"Link\" L");
        if (tbUsed)  sqlBuffer.append("\n       JOIN \"Bioentity\" AS TB ON (TB.\"bioentityId\" = ")
                              .append(sIdColumn).append(')');
        for (int i=0; i<Rt.length; i++) {
            if (pseudoRefs[i] == null) {
               sqlBuffer.append("\n       LEFT OUTER JOIN \"Xref\" AS TX").append(i)
                        .append(" ON ((TX").append(i).append(".\"bioentityId\" = ").append(sIdColumn)
                        .append(") AND (TX").append(i).append(".\"xrefTypeId\" = ").append(Rt[i]).append("))");
            } else
            if (PseudoXref.BioentityTypeName == pseudoRefs[i] && !(sbtUsed && !useLink)) {
               sqlBuffer.append("\n       LEFT OUTER JOIN \"BioentityType\" AS BT ON (BT.\"bioentityTypeId\" = ")
                        .append(useLink ? "TB.\"bioentityTypeId\")" : "SB.\"bioentityTypeId\")");
            } else
            if (!regUsed && (
                PseudoXref.DNARegion == pseudoRefs[i] ||
                PseudoXref.DNAStrand == pseudoRefs[i])) {
               sqlBuffer.append("\n       LEFT OUTER JOIN \"DNARegion\" AS R ON (R.\"bioentityId\" = ")
                        .append(sIdColumn).append(')');
               regUsed = true;
            } else
            if (!hitUsed && (
                PseudoXref.HitEvidence  == pseudoRefs[i] ||
                PseudoXref.HitStudyId   == pseudoRefs[i] ||
                PseudoXref.HitStudyName == pseudoRefs[i] ||
                PseudoXref.HitWeight    == pseudoRefs[i])) {
               sqlBuffer.append("\n       LEFT OUTER JOIN \"Hit\" AS H ON (H.\"bioentityId\" = ")
                        .append(sIdColumn).append(')');
               hitUsed = true;
            }
            if (PseudoXref.HitStudyName == pseudoRefs[i] && !(ssUsed && !useLink)) {
               sqlBuffer.append("\n       LEFT OUTER JOIN \"Study\" AS S ON ((S.\"studyId\" = H.\"studyId\") AND (S.\"organismId\" = ")
                        .append(organism).append("))");
            }
        }
        if (useSourceXref) {
           sqlBuffer.append("\nWHERE  (SX.\"xrefTypeId\" = ")
                    .append(Rs)
                    .append(") AND\n       (SX.\"bioentityId\" = SB.\"bioentityId\") AND\n       (SX.\"value\" = ?) AND \n");
        } else {
           if (PseudoXref.BioentityId == pXref) {
              sqlBuffer.append("\nWHERE  (SB.\"bioentityId\" = ?) AND\n");
              useBigInt = true;
           } else
           if (PseudoXref.BioentityName == pXref) {
              sqlBuffer.append("\nWHERE  (SB.\"name\" = ?) AND\n");
           } else
           if (PseudoXref.BioentityTypeId == pXref) {
              sqlBuffer.append("\nWHERE  (SB.\"bioentityTypeId\" = ?) AND\n");
              useBigInt = true;
           } else
           if (PseudoXref.BioentityTypeName == pXref) {
              sqlBuffer.append(useLink ?
                               "\nWHERE  (SB.\"bioentityTypeId\" = SBT.\"bioentityTypeId\") AND (SBT.\"name\" = ?) AND\n" :
                               "\nWHERE  (SB.\"bioentityTypeId\" = BT.\"bioentityTypeId\") AND (BT.\"name\" = ?) AND\n");
           } else
           if (PseudoXref.DNARegion == pXref) {
              String table = useLink ? "SR" : "R";
              sqlBuffer.append(String.format("\nWHERE  (SB.\"bioentityId\" = %s.\"bioentityId\") AND (%s.\"chromosome\" = ?) AND (%s.\"stop\" >= ?) AND (%s.\"start\" <= ?) AND\n",
                                             table, table, table, table));
           } else
           if (PseudoXref.HitStudyId == pXref) {
              sqlBuffer.append(useLink ?
                               "\nWHERE  (SB.\"bioentityId\" = SH.\"bioentityId\") AND (SH.\"studyId\" = ?) AND\n" :
                               "\nWHERE  (SB.\"bioentityId\" = H.\"bioentityId\") AND (H.\"studyId\" = ?) AND\n");
              useBigInt = true;
           } else
           if (PseudoXref.HitStudyName == pXref) {
              sqlBuffer.append(useLink ?
                               "\nWHERE  (SB.\"bioentityId\" = SH.\"bioentityId\") AND (SH.\"studyId\" = SS.\"studyId\") AND (SS.\"title\" = ?) AND\n" :
                               "\nWHERE  (SB.\"bioentityId\" = H.\"bioentityId\") AND (H.\"studyId\" = S.\"studyId\") AND (S.\"title\" = ?) AND\n");
           } else
              throw new UnsupportedOperationException("Source keys cannot be of type "+pXref+'.');
        }
        sqlBuffer.append("       (SB.\"organismId\" = ").append(organism).append(')');
        if (useLink) {
        	prepareLinkSQL(linkTypes, isDirect, sqlBuffer);
        }
        if ((orderBy != null) && (orderBy.length > 0)) {
        	sqlBuffer.append("\nORDER  BY ");
        	int oShift = useLink ? 3 : 1;
        	for (int o=0; o<orderBy.length; o++) {
        		if (o > 0) sqlBuffer.append(", ");
        		if (orderBy[o] < 0) {
        		    sqlBuffer.append(-1*orderBy[o]+oShift).append(" DESC");
        		} else {
        			sqlBuffer.append(orderBy[o]+oShift); // NOPMD
        		}
        	}
        }
        if (limit >= 0) sqlBuffer.append("\nLIMIT  ").append(limit);

        String sql = sqlBuffer.toString();
        for (String key : keys) {
            if (PseudoXref.DNARegion == pXref) {
               InterestingRegion reg = new InterestingRegion(key);
               con.doSelect(sql, processor, key, reg.getChromosome(), reg.getStart(), reg.getEnd());
            } else {
               Object dbKey;
               if (useBigInt) {
            	   try { dbKey = Long.valueOf(key); }
            	   catch (NumberFormatException e) {
            		   throw new IllegalArgumentException(Rs+" values should be numeric: "+key); // NOPMD
            	   }
               } else {
            	   dbKey = key;
               }
               con.doSelect(sql, processor, key, dbKey);
            }
        }
        return sql;
    }

    private void prepareLinkSQL(String       linkTypes,
    		                    boolean      isDirect,
    		                    StringBuffer sqlBuffer) throws SQLException {
        Set<Long>    linkTypeIds = new TreeSet<Long>();
        List<long[]> linkRanges  = new ArrayList<long[]>();
        long[]       range;

        for (String link : AsserUtil.split(linkTypes)) {
     	   int hPos = link.indexOf('-'); 
     	   if (hPos < 0) {
     		  linkTypeIds.add(validate(link, "LinkType", "linkTypeId"));
     	   } else {
     		   range = new long[2];
     		   try {
     			   range[0] = Long.parseLong(link.substring(0, hPos));
     			   range[1] = Long.parseLong(link.substring(hPos+1));
     		   } catch (NumberFormatException e) {
     			   throw new IllegalArgumentException("Invalid link type range: "+link, e);
     		   }
     		   if (range[0] > range[1]) {
     			   long tmp = range[0];
     			   range[0] = range[1];
     			   range[1] = tmp;
     		   }
     		   if (range[0] == range[1]) {
     			   linkTypeIds.add(range[0]);
     		   } else {
     			   linkRanges.add(range);
     		   }
     	   }
        }

        int     sizeIds    = linkTypeIds.size();
        int     sizeRanges = linkRanges.size();
        boolean useOR      = (sizeRanges > 1) || (sizeIds > 0 && sizeRanges > 0);
        sqlBuffer.append(" AND\n       (SB.\"bioentityId\" = L.")
                 .append(isDirect ? "\"source\"" : "\"target\"")
                 .append(") AND\n");
        if (useOR) sqlBuffer.append("       (\n ");
        if (sizeIds == 1) {
     	   sqlBuffer.append("       (L.\"linkTypeId\" = ")
     	            .append(linkTypeIds.iterator().next())
     	            .append(')');
        } else
        if (sizeIds > 1) {
            sqlBuffer.append("       (L.\"linkTypeId\" IN (")
                     .append(AsserUtil.collapse(linkTypeIds))
                     .append("))");
        }
        for (int r=0; r<sizeRanges; r++) {
     	   range = linkRanges.get(r);
     	   if ((sizeIds > 0) || (r > 0)) sqlBuffer.append(" OR\n ");
     	   sqlBuffer.append("       ((L.\"linkTypeId\" >= ")
                     .append(range[0])
                     .append(") AND (L.\"linkTypeId\" <= ")
                     .append(range[1])
                     .append("))");
        }
        if (useOR) sqlBuffer.append("\n       )");
    }

    private Long validate(Object key, String relation, String field) throws SQLException {
        Long id = TypeConverter.toLong(key);
        int count = ((Number)con.fetchValue("SELECT count(*) FROM \""+
                                            relation+"\" WHERE (\""+field+"\"=?)",
                                            true, id))
                                .intValue();
        if (count == 0)
           throw new IllegalArgumentException("Invalid "+relation+" identifier: "+key);
        return id;
    }

    static private String asColumnName(String refType) {
        if (Character.isDigit(refType.charAt(0))) {
           return "xref"+refType;
        } else {
           return refType;
        }
    }

    /**
     * Produces a list of XrefTypes available in Moksiskaan database.
     */
    public List<XrefType> getReferenceTypes() throws SQLException {
        List<XrefType> types = new LinkedList<XrefType>();

        List<Object[]> results = con.fetchObjects("SELECT \"xrefTypeId\", \"name\", \"www\" FROM \"XrefType\" ORDER BY 1");
        for (Object[] row : results) {
        	XrefType xt = new XrefType((Integer)row[0], (String)row[1]);
        	xt.setWww((String)row[2]);
            types.add(xt);
        }
        return types;
    }

    /**
     * Produces a list of LinkTypes available in Moksiskaan database.
     */
    public List<LinkType> getLinkTypes() throws SQLException {
        List<LinkType> types = new LinkedList<LinkType>();

        List<Object[]> results = con.fetchObjects("SELECT \"linkTypeId\", \"name\", \"description\" FROM \"LinkType\" ORDER BY 1");
        for (Object[] row : results) {
            types.add(new LinkType((Integer)row[0], (String)row[1], (String)row[2]));
        }
        return types;
    }

    /**
     * Produces a list of organisms available in Moksiskaan database.
     */
    public List<Organism> getOrganisms() throws SQLException {
        List<Organism> types = new LinkedList<Organism>();

        List<Object[]> results = con.fetchObjects("SELECT \"organismId\", \"name\", \"author\", \"year\" FROM \"Organism\" ORDER BY 1");
        for (Object[] row : results) {
            types.add(new Organism((Integer)row[0], (String)row[1], (String)row[2], (Integer)row[3]));
        }
        return types;
    }

    /** Close database connection */
    public void close() {
        con.close();
    }

    /**
     * This is the startup method of Piispanhiippa.
     */
    static public void main(String[] argv) throws IOException, SQLException {
        Map<String, MainArgument> args = new HashMap<String, MainArgument>();
        MainArgument argShowHelp       = MainArgument.add("help",         "Print this usage information",args);
        MainArgument argSQL            = MainArgument.add("sql",          "Print SQL query to the standard error stream",args);
        MainArgument argShowURL        = MainArgument.add("d",            "Print current database address",args);
        MainArgument argListAll        = MainArgument.add("L",            "List all elements available for the source reference type",args);
        MainArgument argListReferences = MainArgument.add("Lr",           "List supported reference types",args);
        MainArgument argListOrganisms  = MainArgument.add("Lo",           "List supported organisms",args);
        MainArgument argListLinks      = MainArgument.add("Ll",           "List supported bioentity link types",args);
        MainArgument argRaw            = MainArgument.add("r",            "Use raw input with the formating characters",args);
        MainArgument argListDirection  = MainArgument.add("rev",          "Use reverse links from targets to sources",args);
        MainArgument argUseURL         = MainArgument.add("D",   "file",  "Database configurations to be used",CFG_FILE,args);
        MainArgument argLinkType       = MainArgument.add("l",   "ids",   "Comma separated list of link types combining bioentities",args);
        MainArgument argOrderBy        = MainArgument.add("s",   "cols",  "Comma separated list of ordering Rt columns (<0 for descending)",args);
        MainArgument argRs             = MainArgument.add("Rs",  "id",    "Source reference type",args);
        MainArgument argRt             = MainArgument.add("Rt",  "ids",   "Comma separated list of target reference types","BioentityId,BioentityName",args);
        MainArgument argOrganism       = MainArgument.add("o",   "id",    "Organism of interest",IDConstants.Organism_Homo_sapiens.toString(),args);
        MainArgument argBound          = MainArgument.add("b",   "limit", "Maximum result size or <0 for all","-1",args);
        String[] argLeft;
        try { argLeft = MainArgument.parseInput(argv, args, 0, -1); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            System.err.println();
            printSynopsis(args);
            return;
        }
        if (argShowHelp.isUsed()) {
            printSynopsis(args);
            return;
        }

        Piispanhiippa app;
        try { app = new Piispanhiippa(argUseURL.getValue()); }
        catch (MissingResourceException e) {
           System.err.println("Configuration error in "+argUseURL.getValue()+": "+e.getKey()+" is not defined.");
           return;
        }

        try { app.limit = argBound.getInt(); }
        catch (NumberFormatException e) {
            System.err.println("Invalid upper boundary for the number of result items: "+argBound.getValue());
            return;
        }

        if (argShowURL.isUsed()) {
            System.out.println(app.getURL());
        } else
        if (argListLinks.isUsed()) {
            System.out.println("Link types supported:");
            for (LinkType type : app.getLinkTypes()) {
                System.out.printf("%s = %s (%s)\n",
                		         type.getLinkTypeId(),
                		         type.getName(),
                		         TextUtil.firstSentenceOf(type.getDescription(), 80, "-no definition-"));
            }
            System.out.println("---");
            System.out.println("You may use a hyphen (-) to define ranges like:");
            System.out.println("200-210,300-310,440");
        } else
        if (argListReferences.isUsed()) {
            System.out.println("Reference types supported:");
            for (XrefType type : app.getReferenceTypes()) {
                System.out.println(type.getXrefTypeId()+" = "+type.getName());
            }
            System.out.println("---");
            for (PseudoXref px : PseudoXref.values()) {
                System.out.println(px.name()+" = "+px.description);
            }
        } else
        if (argListOrganisms.isUsed()) {
            System.out.println("Organisms supported:");
            for (Organism o : app.getOrganisms()) {
                System.out.println(o.getOrganismId()+" = "+o.getName()+" ("+o.getAuthor()+", "+o.getYear()+')');
            }
        } else
        if (argRs.isUsed()) {
        	String sql;
        	if (argListAll.isUsed()) {
        		sql = app.listAll(argRs.getValue(),
        				          argOrganism.getInt(),
                                  System.out);
        	} else {
	            List<String> keys = new LinkedList<String>();
	            if (argLeft.length < 1) {
	                readKeyInput(System.in, keys, !argRaw.isUsed());
	            } else {
	                readKeyInput(argLeft,   keys, !argRaw.isUsed());
	            }
	            sql = app.process(keys,
	                              argRs.getValue(),
	                              AsserUtil.split(argRt.getValue()),
	                              argLinkType.getValue(),
	                              !argListDirection.isUsed(),
	                              argOrganism.getInt(),
	                              parseOrderBy(argOrderBy.getValue()),
	                              System.out);
        	}
            if (argSQL.isUsed()) {
               System.err.println();
               System.err.println(sql);
            }
        } else {
       		System.err.println(argRs.getDescription()+" is missing.\n");
            printSynopsis(args);
        }

        app.close();
    }

    static public int[] parseOrderBy(String value) {
    	if ((value == null) || AsserUtil.onlyWhitespace(value)) return null;
    	try { return TypeConverter.toIntArray(AsserUtil.split(value)); }
    	catch (RuntimeException err) {
    		throw new IllegalArgumentException("Invalid selection of the output ordering columns: "+value, err);
    	}
    }

    static private void readKeyInput(InputStream  in,
    		                         List<String> keys,
    		                         boolean      useCleaning) {
        Scanner scan = new Scanner(in);

        while (scan.hasNext()) {
        	String key = scan.next();
        	if (useCleaning) {
        		key = IDConverter.clean(key);
        	}
            keys.add(key);
        }
        scan.close();
    }

    static private void readKeyInput(String[]     argv,
    		                         List<String> keys,
    		                         boolean      useCleaning) {
        for (String arg : argv) {
        	if (useCleaning) {
        		arg = IDConverter.clean(arg);
        	}
            keys.add(arg);
        }
    }

    static private void printSynopsis(Map<String, MainArgument> args) {
        System.err.println("Piispanhiippa v."+Version.getVersion()+" by Marko Laakso");
        System.err.println("------------------------------------");
        System.err.println("piispanhiippa [options] [sourceKey...]");
        MainArgument.printArguments(args, System.err);
        System.err.println("Source keys are read from the standard input if the argument");
        System.err.println("list contains none.");
    }

}

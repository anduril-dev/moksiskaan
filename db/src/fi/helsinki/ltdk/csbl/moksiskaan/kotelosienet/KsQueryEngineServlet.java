package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.helsinki.ltdk.csbl.moksiskaan.Version;
import fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.DNARegion;

public class KsQueryEngineServlet extends HttpServlet {

	private static final long serialVersionUID = -5838136990346266233L;

	private Piispanhiippa piispanhiippa;

	private void open() throws ServletException {
		if (piispanhiippa == null) {
			try {
				URL cfg = getServletContext().getResource("/WEB-INF/"+Piispanhiippa.CFG_FILE);
				piispanhiippa = new Piispanhiippa(cfg);
			} catch (Exception err) {
				throw new ServletException("Cannot establish Piispanhiippa.", err);
			}
		}
	}

	private void close() {
		if (piispanhiippa != null) {
			piispanhiippa.close();
			piispanhiippa = null;
		}
	}

	public void destroy() {
		close();
	}

	@Override
	public String getServletInfo() {
		return "Moksiskaan Kotelosienet query engine version "+Version.getVersion()+'.';
	}

	@SuppressWarnings("unchecked")
	private Object process(String            method,
	                       ObjectInputStream data) throws ServletException,
	                                                      IOException,
	                                                      ClassNotFoundException {
		synchronized (this) {
		open();
		try {
		  if ("getRegion".equals(method)) {
		     return SimpleQueryEngine.getRegion((Long)data.readObject(),
		                                        piispanhiippa.getConnection());
		  } else
		  if ("getRegions".equals(method)) {
		     return SimpleQueryEngine.getRegions((String)data.readObject(),
		                                         (Long)data.readObject(),
		                                         (Long)data.readObject(),
		                                         (Integer)data.readObject(),
		                                         piispanhiippa.getConnection());
		  } else
		  if ("getNextRegion".equals(method)) {
		     return SimpleQueryEngine.getNextRegion((DNARegion)data.readObject(),
		                                            (Boolean)data.readObject(),
		                                            (Integer)data.readObject(),
		                                            piispanhiippa.getConnection());
		  } else
		  if ("getLinkAnnotations".equals(method)) {
		      return SimpleQueryEngine.getLinkAnnotations((Long)data.readObject(),
		                                                  (Long)data.readObject(),
		                                                  (Integer)data.readObject(),
		                                                  piispanhiippa.getConnection());
		  } else
		  if ("getNeighbors".equals(method)) {
		      return SimpleQueryEngine.getNeighbors((Long)data.readObject(),
		                                            (Boolean)data.readObject(),
		                                            (String)data.readObject(),
		                                            piispanhiippa,
		                                            (Integer)data.readObject());
		  } else
		  if ("getBioentity".equals(method)) {
		      return SimpleQueryEngine.getBioentity((List<String>)data.readObject(),
		                                            (String)data.readObject(),
		                                            piispanhiippa,
		                                            (Integer)data.readObject());
		  } else
		  if ("getStudies".equals(method)) {
		      return SimpleQueryEngine.getStudies((Long)data.readObject(),
		                                          piispanhiippa,
		                                          (Integer)data.readObject());
		  } else
		  if ("getStudyIdentifiers".equals(method)) {
		      return SimpleQueryEngine.getStudyIdentifiers(piispanhiippa,
		                                                   (Integer)data.readObject());
		  } else
		  if ("getStudy".equals(method)) {
		      return SimpleQueryEngine.getStudy((Integer)data.readObject(),
		                                        piispanhiippa.getConnection());
		  } else
          if ("getBioentityTypes".equals(method)) {
			  return SimpleQueryEngine.getBioentityTypes(piispanhiippa.getConnection());
          } else
		  if ("getLinkTypes".equals(method)) {
			  return piispanhiippa.getLinkTypes();
		  } else
		  if ("getOrganisms".equals(method)) {
			  return piispanhiippa.getOrganisms();
          } else
		  if ("getXrefs".equals(method)) {
		      Long id = (Long)data.readObject();
		      if (id == null)
			     return piispanhiippa.getReferenceTypes();
			  else
			     return SimpleQueryEngine.getXrefs(id, piispanhiippa.getConnection());
		  } else
          if ("getLinkTypeStatistics".equals(method)) {
		      return SimpleQueryEngine.getLinkTypeStatistics((Integer)data.readObject(),
			                                                 piispanhiippa.getConnection());
		  } else
          if ("getBioentityTypeStatistics".equals(method)) {
		      return SimpleQueryEngine.getBioentityTypeStatistics((Integer)data.readObject(),
			                                                      piispanhiippa.getConnection());
		  } else
          if ("getXrefTypeStatistics".equals(method)) {
              return SimpleQueryEngine.getXrefTypeStatistics((Integer)data.readObject(),
                                                             piispanhiippa.getConnection());
          } else
          if ("getChromosomeStatistics".equals(method)) {
              return SimpleQueryEngine.getChromosomeStatistics((Integer)data.readObject(),
                                                               piispanhiippa.getConnection());
          } else
		  if ("getLogEntities".equals(method)) {
			  return SimpleQueryEngine.getLogEntities(piispanhiippa.getConnection());
          }
		} catch (SQLException err) {
			close();
			throw new ServletException(err);
		} catch (RuntimeException err) {
			err.printStackTrace();
			return err;
		}
		}
		throw new ServletException("Unsupported method: "+method);
	}

	@Override
	protected void doPost(HttpServletRequest  req,
                          HttpServletResponse res) throws ServletException,
                                                          IOException {
		try {
		  ObjectInputStream  in     = new ObjectInputStream(req.getInputStream());
		  String             method = in.readUTF();
		  Object             result = process(method, in);
          ObjectOutputStream out    = new ObjectOutputStream(res.getOutputStream());

          out.writeObject(result);
          out.close();
		  in.close();
		  return;
		}
		catch (ClassNotFoundException err) {
			getServletContext().log("Unsupported input data.", err);
		}
		throw new ServletException("Access denied!");
	}

	@Override
	protected void doGet(HttpServletRequest  req,
                         HttpServletResponse res) throws ServletException,
                                                         IOException {
		res.setContentType("text/plain");
		PrintWriter out = res.getWriter();
		out.println(getServletInfo());
		out.println();
		out.println("Piispanhiippa in currently "+(piispanhiippa==null ? "available." : "unavailable."));
		out.close();
	}

}
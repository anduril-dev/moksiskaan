package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.db.ArrayListProcessor;
import fi.helsinki.ltdk.csbl.asser.db.JDBCConnection;
import fi.helsinki.ltdk.csbl.moksiskaan.Version;
import fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.*;

/**
 * Local implementation of {@link fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet.KsQueryEngine}.
 * This class hides the actual usage of Piispanhiippa and Korvasieni.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public class SimpleQueryEngine extends AbstractQueryEngine {

	private final Piispanhiippa piispanhiippa;

	public SimpleQueryEngine() throws SQLException, IOException {
		piispanhiippa = new Piispanhiippa();
		piispanhiippa.setResultLimit(MAX_RESULT_SIZE);
	}

	@Override
	public void close() {
		super.close();
		piispanhiippa.close();
	}

	public URL getUserGuideAddress() {
		return ClassLoader.getSystemResource("doc/userguide/userguide.html");
	}

    /**
     * Produces a list of organisms available in Moksiskaan database.
     */
	@Override
    public List<Organism> getOrganisms() throws SQLException {
        return piispanhiippa.getOrganisms();
    }

    @Override
    public Log[] getLogEntities() throws SQLException {
    	return getLogEntities(piispanhiippa.getConnection());
    }

    static public Log[] getLogEntities(JDBCConnection con) throws SQLException {
		final String SQL = "SELECT \"time\", \"type\", \"description\" "+
		                   "FROM   \"Log\" "+
		                   "ORDER  BY \"time\"";

		List<Object[]> results = con.fetchObjects(SQL);
		if (results == null) return new Log[0];

		Log[] log = new Log[results.size()];
		int   i   = 0;
		for (Object[] row : results) {
			log[i] = new Log();
			log[i].setTime       (  (Date)row[0]);
			log[i].setType       ((String)row[1]);
			log[i].setDescription((String)row[2]);
		    i++;
		}
		return log;
    }

	@Override
	public Map<Integer,BioentityType> getBioentityTypes() throws SQLException {
		return getBioentityTypes(piispanhiippa.getConnection());
	}

	static public Map<Integer,BioentityType> getBioentityTypes(JDBCConnection con) throws SQLException {
		Map<Integer,BioentityType> types = new HashMap<Integer,BioentityType>();
		final String               SQL   = "SELECT \"bioentityTypeId\", \"name\" FROM \"BioentityType\"";
		for (Object[] row : con.fetchObjects(SQL)) {
			types.put((Integer)row[0], new BioentityType((Integer)row[0], (String)row[1]));
		}
		return types;
	}

	@Override
	public List<LinkType> getLinkTypes() throws SQLException {
		return piispanhiippa.getLinkTypes();
	}

	@Override
	public List<Object[]> getXrefs(Long id) throws SQLException {
		return getXrefs(id, piispanhiippa.getConnection());
	}

	static public List<Object[]> getXrefs(Long id, JDBCConnection con) throws SQLException {
		final String SQL = "SELECT T.\"name\", X.\"value\", X.\"xrefTypeId\" "+
		                   "FROM   \"Xref\" X, \"XrefType\" T "+
		                   "WHERE  (X.\"xrefTypeId\"  = T.\"xrefTypeId\") AND "+
		                   "       (X.\"bioentityId\" = ?) "+
		                   "ORDER  BY 1, 2";

		return con.fetchObjects(SQL, id);
	}

	@Override
	public List<XrefType> getXrefs() throws SQLException {
		return piispanhiippa.getReferenceTypes();
	}

	@Override
	public List<Object[]> getLinkTypeStatistics() throws SQLException {
		return getLinkTypeStatistics(organism, piispanhiippa.getConnection());
	}

	static public List<Object[]> getLinkTypeStatistics(Integer        species,
			                                           JDBCConnection con) throws SQLException {
		final String SQL = "SELECT COUNT(L.\"linkTypeId\"), T.\"name\" "+
                           "FROM   \"Bioentity\" B, \"Link\" L, \"LinkType\" T "+
                           "WHERE  (L.\"source\"     = B.\"bioentityId\") AND "+
                           "       (L.\"linkTypeId\" = T.\"linkTypeId\")  AND "+
                           "       (B.\"organismId\" = ?) "+
                           "GROUP  BY T.\"name\" "+
                           "ORDER  BY 1 DESC, 2";

        return con.fetchObjects(SQL, species);
	}

	@Override
	public List<Object[]> getBioentityTypeStatistics() throws SQLException {
		return getBioentityTypeStatistics(organism, piispanhiippa.getConnection());
	}

	static public List<Object[]> getBioentityTypeStatistics(Integer        species,
			                                                JDBCConnection con) throws SQLException {
		final String SQL = "SELECT COUNT(B.\"bioentityId\"), T.\"name\" "+
                           "FROM   \"Bioentity\" B, \"BioentityType\" T "+
                           "WHERE  (B.\"bioentityTypeId\" = T.\"bioentityTypeId\")  AND "+
                           "       (B.\"organismId\"      = ?) "+
                           "GROUP  BY T.\"name\" "+
                           "ORDER  BY 1 DESC, 2";

        return con.fetchObjects(SQL, species);
	}

	@Override
	public List<Object[]> getXrefTypeStatistics() throws SQLException {
		return getXrefTypeStatistics(organism, piispanhiippa.getConnection());
	}

	static public List<Object[]> getXrefTypeStatistics(Integer        species,
			                                           JDBCConnection con) throws SQLException {
		final String SQL = "SELECT COUNT(B.\"bioentityId\"), T.\"name\" "+
  		                   "FROM   \"Bioentity\" B, \"Xref\" X, \"XrefType\" T "+
		                   "WHERE  (X.\"bioentityId\" = B.\"bioentityId\") AND "+
		                   "       (X.\"xrefTypeId\"  = T.\"xrefTypeId\")  AND "+
		                   "       (B.\"organismId\"  = ?) "+
		                   "GROUP  BY T.\"name\" "+
		                   "ORDER  BY 1 DESC, 2";

        return con.fetchObjects(SQL, species);
	}

    @Override
    public List<Object[]> getChromosomeStatistics() throws SQLException {
        return getChromosomeStatistics(organism, piispanhiippa.getConnection());
    }

    static public List<Object[]> getChromosomeStatistics(Integer        species,
                                                         JDBCConnection con) throws SQLException {
        final String SQL = "SELECT COUNT(R), R.\"chromosome\"||'/'||T.\"name\" "+
                           "FROM   \"Bioentity\" B, \"DNARegion\" R, \"BioentityType\" T "+
                           "WHERE  (R.\"bioentityId\"     = B.\"bioentityId\")     AND "+
                           "       (B.\"bioentityTypeId\" = T.\"bioentityTypeId\") AND "+
                           "       (B.\"organismId\"      = ?) "+
                           "GROUP  BY R.\"chromosome\", T.\"name\" "+
                           "ORDER  BY 1 DESC, 2";

        return con.fetchObjects(SQL, species);
    }

	@Override
	public Integer[] getStudyIdentifiers() throws SQLException {
		return getStudyIdentifiers(piispanhiippa, organism);
	}

	static public Integer[] getStudyIdentifiers(Piispanhiippa con,
			                                    Integer       species) throws SQLException {
		ArrayListProcessor out = new ArrayListProcessor();
		con.listAll(Piispanhiippa.PseudoXref.HitStudyId.name(), species, out);

		List<Object[]> resList  = out.getArray();
		Integer[]      resArray = new Integer[resList.size()];
		int            r        = 0;
		for (Object[] row : resList) {
			resArray[r++] = (Integer)row[0];
		}
		return resArray;
	}

	@Override
	public Study getStudy(Integer id) throws SQLException {
		return getStudy(id, piispanhiippa.getConnection());
	}

	static public Study getStudy(Integer        id,
                                 JDBCConnection con) throws SQLException {
		final String SQL = "SELECT S.\"title\", S.\"description\", "+
				           "       ST.\"scoreTypeId\", ST.\"name\", ST.\"ascending\" "+
		                   "FROM   \"Study\" S, \"ScoreType\" ST "+
		                   "WHERE  (S.\"scoreTypeId\" = ST.\"scoreTypeId\") AND "+
		                   "       (S.\"studyId\"     = ?) ";

		Object[]  obj   = con.fetchObject(SQL, true, id);
		ScoreType score = new ScoreType((Integer)obj[2], (String)obj[3], (Boolean)obj[4]);
		Study     study = new Study(id, (String)obj[0], null, score);
		study.setDescription((String)obj[1]);
		return study;
	}

	@Override
	public List<Object[]> getBioentity(List<String> keys,
			                           String       typeS) throws SQLException {
		return getBioentity(keys, typeS, piispanhiippa, organism);
	}

	static public List<Object[]> getBioentity(List<String>  keys,
                                              String        typeS,
                                              Piispanhiippa con,
                                              Integer       species) throws SQLException {
		ArrayListProcessor queryProcessor = new ArrayListProcessor();
	    if (keys == null) {
	    	con.getConnection()
	    	   .doSelect("SELECT DISTINCT X.\"xrefTypeId\", B.\"bioentityId\", B.\"name\", B.\"bioentityTypeId\" "+
	    			     "FROM   \"Bioentity\" B, \"Xref\" X "+
	    			     "WHERE  (X.\"bioentityId\" = B.\"bioentityId\") AND "+
	    			     "       (X.\"xrefTypeId\"  = ?)                 AND "+
	    			     "       (B.\"organismId\"  = ?) "+
	    			     "ORDER  BY B.\"name\" "+
	    			     "LIMIT  "+MAX_RESULT_SIZE,
	    			     queryProcessor,
	    			     Integer.valueOf(typeS),
	    			     species);
	    } else {
			String[] rT = {	Piispanhiippa.PseudoXref.BioentityId.name(),
					        Piispanhiippa.PseudoXref.BioentityName.name(),
					        Piispanhiippa.PseudoXref.BioentityTypeId.name() };
	        con.process(keys,
			            typeS,
			            rT,
			            null,
	                    true,
			            species,
			            new int[] { 2 },
			            queryProcessor);
	    }
		return queryProcessor.getArray();
	}

	@Override
	public DNARegion getRegion(long bioentityId) throws SQLException {
		return getRegion(bioentityId, piispanhiippa.getConnection());
	}

	static public DNARegion getRegion(long           bioentityId,
			                          JDBCConnection con) throws SQLException {
		final String   SQL = "SELECT \"strand\", \"chromosome\", \"start\", \"stop\" "+
		                     "FROM   \"DNARegion\" R "+
		                     "WHERE  (R.\"bioentityId\" = ?)";

		Object[] result = con.fetchObject(SQL, false, bioentityId);
		if (result == null) return null;

		DNARegion region = new DNARegion();
		region.setStrand((Boolean)result[0]);
		region.setChromosome((String)result[1]);
		region.setStart((Long)result[2]);
		region.setStop((Long)result[3]);
		return region;
	}

	@Override
	public DNARegion[] getRegions(String chromosome, long start, long stop) throws SQLException {
		return getRegions(chromosome, start, stop, organism, piispanhiippa.getConnection());
	}

	static public DNARegion[] getRegions(String         chromosome,
			                             long           start,
			                             long           stop,
			                             Integer        species,
			                             JDBCConnection con) throws SQLException {
		final String SQL = "SELECT B.\"bioentityId\", B.\"name\", "+
			               "       B.\"bioentityTypeId\", "+
		                   "       R.\"strand\", R.\"chromosome\", R.\"start\", R.\"stop\" "+
		                   "FROM   \"DNARegion\" R, \"Bioentity\" B "+
		                   "WHERE  (R.\"chromosome\"  = ?) AND "+
		                   "       (R.\"start\"      <= ?) AND "+
		                   "       (R.\"stop\"       >= ?) AND "+
		                   "       (R.\"bioentityId\" = B.\"bioentityId\") AND "+
		                   "       (B.\"organismId\"  = ?) "+
		                   "ORDER  BY \"start\", \"stop\"";

		List<Object[]> results = con.fetchObjects(SQL, chromosome, stop, start, species);
		if (results == null) return new DNARegion[0];

		Map<Integer,BioentityType> btMap = new HashMap<Integer,BioentityType>();

		DNARegion[] regions = new DNARegion[results.size()];
		int         i       = 0;
		for (Object[] row : results) {
		    BioentityType bioentityType = btMap.get((Integer)row[2]);
		    if (bioentityType == null) {
		       bioentityType = new BioentityType();
               bioentityType.setBioentityTypeId((Integer)row[2]);
               btMap.put(bioentityType.getBioentityTypeId(), bioentityType);
		    }
			Bioentity bioentity = new Bioentity();
			bioentity.setBioentityId((Long)row[0]);
			bioentity.setName((String)row[1]);
			bioentity.setBioentityType(bioentityType);
		    regions[i] = new DNARegion();
		    regions[i].setStrand((Boolean)row[3]);
		    regions[i].setChromosome((String)row[4]);
		    regions[i].setStart((Long)row[5]);
		    regions[i].setStop((Long)row[6]);
		    regions[i].setBioentity(bioentity);
		    i++;
		}
		return regions;
	}

	public DNARegion getNextRegion(DNARegion locus, boolean previous) throws SQLException {
		return getNextRegion(locus, previous, organism, piispanhiippa.getConnection());
	}

	static public DNARegion getNextRegion(DNARegion      locus,
			                              boolean        previous,
			                              Integer        species,
			                              JDBCConnection con) throws SQLException {
		long         pos = previous ? locus.getStart() : locus.getStop();
		final String SQL = "SELECT B.\"bioentityId\", B.\"name\", "+
		                   "       R.\"strand\", R.\"chromosome\", R.\"start\", R.\"stop\" "+
		                   "FROM   \"DNARegion\" R, \"Bioentity\" B "+
		                   "WHERE  (R.\"chromosome\"  = ?) AND (R.\""+
		                   (previous ? "stop\" <" : "start\" >")+
		                   "?) AND (R.\"bioentityId\" = B.\"bioentityId\") AND (B.\"organismId\"  = ?) "+
		                   "ORDER BY \""+(previous ? "stop\" DESC" : "start\"")+" LIMIT 1";

		Object[] row = con.fetchObject(SQL, false, locus.getChromosome(), pos, species);
		if (row == null) return null;

		Bioentity bioentity = new Bioentity();
		bioentity.setBioentityId((Long)row[0]);
		bioentity.setName((String)row[1]);
	    DNARegion region = new DNARegion();
	    region.setStrand((Boolean)row[2]);
	    region.setChromosome((String)row[3]);
	    region.setStart((Long)row[4]);
	    region.setStop((Long)row[5]);
	    region.setBioentity(bioentity);
        return region;
	}

	@Override
	public List<Object[]> getStudies(Long bioentityId) throws SQLException {
		return getStudies(bioentityId, piispanhiippa, organism);
	}

	static public List<Object[]> getStudies(Long          bioentityId,
			                                Piispanhiippa con,
			                                Integer       species) throws SQLException {
		List<String> keys = new ArrayList<String>(1);
		keys.add(bioentityId.toString());

		String[] rT = {	Piispanhiippa.PseudoXref.HitStudyId.name(),
		                Piispanhiippa.PseudoXref.HitStudyName.name(),
		                Piispanhiippa.PseudoXref.HitWeight.name(),
		                Piispanhiippa.PseudoXref.HitEvidence.name() };

		ArrayListProcessor queryProcessor = new ArrayListProcessor();
        con.process(keys,
		            Piispanhiippa.PseudoXref.BioentityId.toString(),
		            rT,
		            null,
                    true,
		            species,
		            new int[] { 2 },
		            queryProcessor);
		return queryProcessor.getArray();
	}

	@Override
	public List<Object[]> getNeighbors(Long    id,
			                           boolean down) throws SQLException {
		return getNeighbors(id, down, getLinkTypeSequence(), piispanhiippa, organism);
	}

	static public List<Object[]> getNeighbors(Long          id,
                                              boolean       down,
                                              String        linkTypes,
                                              Piispanhiippa con,
                                              Integer       species) throws SQLException {
		if (AsserUtil.onlyWhitespace(linkTypes)) return Collections.emptyList();

		List<String> keys = new ArrayList<String>(1);
		keys.add(id.toString());

		String[] rT = {	Piispanhiippa.PseudoXref.BioentityId.name(),
				        Piispanhiippa.PseudoXref.BioentityName.name(),
				        Piispanhiippa.PseudoXref.BioentityTypeId.name() };

		ArrayListProcessor queryProcessor = new ArrayListProcessor();
        con.process(keys,
		            Piispanhiippa.PseudoXref.BioentityId.toString(),
		            rT,
		            linkTypes,
                    down,
		            species,
		            new int[] { 2 },
		            queryProcessor);
		return queryProcessor.getArray();
	}

	@Override
	public LinkAnnotation[] getLinkAnnotations(Long source, Long target, Integer type) throws SQLException {
		return getLinkAnnotations(source, target, type, piispanhiippa.getConnection());
	}

	static public LinkAnnotation[] getLinkAnnotations(Long           source,
			                                          Long           target,
			                                          Integer        type,
			                                          JDBCConnection con) throws SQLException {
		List<Object[]>   data   = con.fetchObjects("SELECT A.\"name\", A.\"value\" "+
				                                   "FROM   \"LinkAnnotation\" A, \"Link\" L "+
				                                   "WHERE  (L.\"linkId\"     = A.\"linkId\") AND "+
				                                   "       (L.\"source\"     = ?)            AND "+
				                                   "       (L.\"target\"     = ?)            AND "+
				                                   "       (L.\"linkTypeId\" = ?) "+
				                                   "ORDER  BY 1, 2",
				                                   source, target, type);
		LinkAnnotation[] annots = new LinkAnnotation[data.size()];
		for (int i=0; i<annots.length; i++) {
			Object[] row = data.get(i);
			annots[i] = new LinkAnnotation();
			annots[i].setName((String)row[0]);
			annots[i].setValue((String)row[1]);
		}
		return annots;
	}

	@Override
	public String getDescription() {
		StringBuffer buffer = new StringBuffer(1024);
		buffer.append("<html><h1>Kotelosienet version ")
		      .append(Version.getVersion())
		      .append("</h1></html>\n\nMoksiskaan: ")
		      .append(piispanhiippa.getURL());
		if (isKorvasieniInUse()) {
			buffer.append("\nEnsembl: ")
			      .append(korvasieni.getURL());
		}
		if (fileGO != null) {
			buffer.append("\nGene Ontology: ")
			      .append(fileGO);
		}
		return buffer.toString();
	}

}
package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;

/**
 * Provides a shopping cart for the bioentities.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public class BioentityBasket extends JPanel implements BioentityListener {

	private static final long serialVersionUID = 1L;

	private final JList list;
	private final DefaultListModel model;

	private final JButton bRemove;
	private final JButton bClear;

	private final Set<BioentityListener> entityListeners = new HashSet<BioentityListener>();

	public BioentityBasket() {
		super(new BorderLayout());

		model = new DefaultListModel();
		list  = new JList(model);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				setButtonsEnabled();
			}
		});
		list.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent ev) {
				if ((ev.getClickCount() == 2) && (ev.getButton() == MouseEvent.BUTTON1)) {
					int i = list.locationToIndex(ev.getPoint());
					if (i < 0) return;
					Bioentity entity = (Bioentity)list.getModel().getElementAt(i);
					for (BioentityListener listener : entityListeners) {
						listener.bioentitySelected(entity);
					}                   
				}
			}
			@Override
			public void mouseEntered(MouseEvent ev)  { /* No action */ }
			@Override
			public void mouseExited(MouseEvent ev)   { /* No action */ }
			@Override
			public void mousePressed(MouseEvent ev)  { /* No action */ }
			@Override
			public void mouseReleased(MouseEvent ev) { /* No action */ }
		});

		bRemove = new JButton(new ImageIcon(BioentityBasket.class.getResource("/fi/helsinki/ltdk/csbl/moksiskaan/kotelosienet/remove.png")));
		bRemove.setToolTipText("Remove");
		bRemove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Object entity = list.getSelectedValue();
				if (entity != null) {
					model.removeElement(entity);
				}
				setButtonsEnabled();
			}
		});
		bClear = new JButton(new ImageIcon(BioentityBasket.class.getResource("/fi/helsinki/ltdk/csbl/moksiskaan/kotelosienet/empty.png")));
		bClear.setToolTipText("Empty");
		bClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clear();
			}
		});
		setButtonsEnabled();

		JPanel buttons = new JPanel(new FlowLayout());
		buttons.add(bRemove);
		buttons.add(bClear);
		JScrollPane scroll = new JScrollPane(list);
		scroll.setBorder(new TitledBorder("Bioentities"));
		add(buttons, BorderLayout.NORTH);
		add(scroll,  BorderLayout.CENTER);
	}

	public void addBioentityListener(BioentityListener listener) {
		entityListeners.add(listener);
	}

	/**
	 * Removes everything from the basket.
	 */
	public void clear() {
		model.clear();
		setButtonsEnabled();
	}

	@Override
	public void bioentitySelected(Bioentity entity) {
		if (model.contains(entity)) {
			list.setSelectedValue(entity, true);
		} else {
			model.addElement(entity);
            list.setSelectedIndex(model.getSize()-1);
		}
		setButtonsEnabled();
	}

	@Override
	public void unselectBioentities() {
		list.clearSelection();
		setButtonsEnabled();
	}

	private void setButtonsEnabled() {
		bClear .setEnabled(!model.isEmpty());
		bRemove.setEnabled(list.getSelectedValue() != null);
	}

	@SuppressWarnings("unchecked")
	public Enumeration<Bioentity> elements() {
		return (Enumeration<Bioentity>) model.elements();
	}

}
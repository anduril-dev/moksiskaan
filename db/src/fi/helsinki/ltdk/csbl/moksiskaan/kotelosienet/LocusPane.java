package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.EtchedBorder;

import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.BioentityType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.DNARegion;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;

/**
 * Renders annotation regions along the DNA segment.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.09
 */
public class LocusPane extends JComponent {
	
	private static final long serialVersionUID = 1L;

    static private final int ROW_HEIGHT = 15;
    static private final int MARGIN     = 5;

	private DNARegion[] regions;
	private int[]       rows = new int[10];
	private Rectangle[] positions;
    private long        posMin;
    private long        posMax;
    private int         selection = -1;
    private DNARegion   selectionRange;

	public LocusPane() {
		setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		setPreferredSize(new Dimension(30,70));
		setBackground(Color.WHITE);
		setOpaque(false);
	}

	public DNARegion getSelection() {
		return ((selection<0) || (regions==null) || (regions.length<1)) ?
			   null : regions[selection];
	}

	public DNARegion getFirstRegion() {
		return ((regions==null) || (regions.length<1)) ? null : regions[0];
	}

	public DNARegion getLastRegion() {
		return ((regions==null) || (regions.length<1)) ? null : regions[regions.length-1];
	}

	public DNARegion itemAt(Point point) {
		final int xPrec = 3;

		if (regions == null) return null;

		for (int i=0; i<positions.length; i++) {
			if (positions[i].x > point.x+xPrec) break;
			if ((positions[i].x+positions[i].width  >= point.x-xPrec) &&
				(positions[i].y                     <= point.y) &&
				(positions[i].y+positions[i].height >= point.y)) {
				return regions[i];
			}
		}
		return null;
	}

	public void setItems(DNARegion[] items) {
		selection = -1;
		regions   = items;

		if ((items == null) || (items.length < 1)) {
			positions = null;
			repaint();
			return;
		}

		positions = new Rectangle[items.length];
		if (regions.length > rows.length)
           rows = new int[regions.length];
		long[] rowReserw = new long[rows.length];
		int    rowPos    = 0;
		String chr       = regions[0].getChromosome();
		long   pStart    = regions[0].getStart();

		rows[0]      = 0;
		rowReserw[0] = regions[0].getStop();
		positions[0] = new Rectangle();
		posMin       = pStart;
		posMax       = rowReserw[0]; 
		for (int i=1; i<items.length; i++) {
			long start = items[i].getStart();
			if (start < pStart)
				throw new IllegalArgumentException("Input regions are not in ascending order.");
			if (!chr.equals(regions[i].getChromosome()))
				throw new IllegalArgumentException("Regions from different chromosomes: "+chr+", "+regions[i].getChromosome());
			pStart = start;
			int j;
			for (j=0; (j<=rowPos) && (rowReserw[j] > start); j++);
			if (j > rowPos) rowPos = j;
			rowReserw[j] = items[i].getStop();
			rows[i]      = j;
			positions[i] = new Rectangle();
			if (rowReserw[j] > posMax) posMax = rowReserw[j];
		}

		Dimension ps = getPreferredSize();
		ps.height = (rowPos+1)*ROW_HEIGHT+MARGIN*2; 
		setPreferredSize(ps);
		repaint();
	}

	public void setViewPosition(long start, long end) {
		posMin = start;
		posMax = end;
	}

	public void setSelection(DNARegion region) {
		selectionRange = region;

		if ((region != null)     &&
			(regions != null)    &&
			(regions.length > 0) &&
			region.getChromosome().equals(regions[0].getChromosome())) {
			   long start = region.getStart();
			   long stop  = region.getStop();
	           for (int i=0; i<regions.length; i++) {
	        	    long startI = regions[i].getStart(); 
					if (startI > start) break;
					if ((start == startI) && (stop == regions[i].getStop())) {
						selection      = i;
						selectionRange = null;
						return;
					}
			   }
		}
		selection = -1;
		repaint();
	}

	protected Color getColor(Bioentity be) {
	    if (be == null) return Color.DARK_GRAY;
	    BioentityType t = be.getBioentityType();
	    if (t == null) return Color.GRAY;
	    Integer tid = t.getBioentityTypeId(); 
	    if (IDConstants.BioentityType_gene.equals(tid))                           return Color.ORANGE;
	    if (IDConstants.BioentityType_DNA_binding_site.equals(tid))               return Color.MAGENTA;
	    if (IDConstants.BioentityType_genomic_amplification.equals(tid))          return Color.GREEN;
	    if (IDConstants.BioentityType_genomic_deletion.equals(tid))               return Color.BLUE;
	    if (IDConstants.BioentityType_single_nucleotide_polymorphism.equals(tid)) return Color.BLACK;
	    return Color.YELLOW;
	}

	protected boolean hasStrand(Bioentity be) {
		if (be == null) return false;
	    BioentityType t = be.getBioentityType();
	    if (t == null) return false;
	    Integer tid = t.getBioentityTypeId();
	    return !(IDConstants.BioentityType_genomic_amplification.equals(tid)  ||
	    		 IDConstants.BioentityType_genomic_deletion.equals(tid)       ||
	    		 IDConstants.BioentityType_single_nucleotide_polymorphism.equals(tid));
	}

	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int width  = getWidth();
        int height = getHeight();
        g.setColor(getBackground());
        g.fillRect(0, 0, width, height);
        if ((regions == null) || (regions.length < 1)) return;

        Shape  clip  = g.getClip();
        double scale = (width-MARGIN*2)/(double)(posMax-posMin);

        if (selectionRange != null) {
        	g.setColor(Color.BLUE);
        	int x1 = (int)((selectionRange.getStart()-posMin)*scale)+MARGIN;
        	int x2 = (int)((selectionRange.getStop() -posMin)*scale)+MARGIN;
        	g.fillRect(x1,      height-MARGIN-(ROW_HEIGHT/3),
        			   x2-x1+1, ROW_HEIGHT/4);
        }

        for (int i=0; i<regions.length; i++) {
        	int x1   = (int)((regions[i].getStart()-posMin)*scale)+MARGIN;
        	int x2   = (int)((regions[i].getStop() -posMin)*scale)+MARGIN;
        	int y    = ROW_HEIGHT*rows[i]+MARGIN;
        	int yMid = y+ROW_HEIGHT/2; 

        	positions[i].x      = x1;
        	positions[i].y      = y;
        	positions[i].width  = x2-x1;
        	positions[i].height = ROW_HEIGHT-2;

        	g.setColor(getColor(regions[i].getBioentity()));
        	g.fillRect(x1+1, y+1, x2-x1-1, ROW_HEIGHT-3);
        	g.setColor((i==selection) ? Color.RED : Color.GRAY);
        	if (hasStrand(regions[i].getBioentity())) {
	        	if (regions[i].isStrand()) {
	            	g.drawLine(x2, y,                     x2+3, yMid);
	            	g.drawLine(x2, y+positions[i].height, x2+3, yMid);
	        	} else {
	            	g.drawLine(x1, y,                     x1-3, yMid);
	            	g.drawLine(x1, y+positions[i].height, x1-3, yMid);
	        	}
        	} else {
        		g.fillRect(x1-2, yMid-2, x2-x1+5, 4);
        	}
        	g.drawRect(positions[i].x, positions[i].y, positions[i].width, positions[i].height);
        	g.clipRect(positions[i].x, positions[i].y, positions[i].width, positions[i].height);
        	g.setColor(Color.BLACK);
        	g.drawString(regions[i].getBioentity().getName(), x1+3, y+11);
        	g.setClip(clip);
        }
        g.setColor(Color.GRAY);
        g.drawString(regions[0].getChromosome()+':'+posMin+'-'+posMax, MARGIN, height-MARGIN);
	}

}

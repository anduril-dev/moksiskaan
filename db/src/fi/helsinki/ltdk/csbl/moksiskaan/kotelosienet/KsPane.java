package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;

import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.annotation.DatabaseId;
import fi.helsinki.ltdk.csbl.asser.annotation.OntologyTerm;
import fi.helsinki.ltdk.csbl.asser.annotation.RegionGO;
import fi.helsinki.ltdk.csbl.asser.pipeline.CSV2LatexComponent;
import fi.helsinki.ltdk.csbl.moksiskaan.Version;
import fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.*;

/**
 * Bioentity viewer for Kotelosienet.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public class KsPane extends JPanel implements AnnotationSelectionListener,
                                              BioentityListener {

	private static final long serialVersionUID = 1L;

	private final Cursor WAIT_CURSOR    = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
	private final Cursor DEFAULT_CURSOR = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);

	private static final String BROWSER_DB = "URL";
	private static final String PICK_CMD   = "PICK";

	public static final String PAGE_HEADER = "<html><body>\n";
	public static final String PAGE_FOOTER = "\n</body></html>";
	public static final String PAGE_LF     = "<br/>";

	private static final String DEFAULT_PAGE =
		"<center><h1>Kotelosienet "+
		Version.getVersion()+
		"</h1><img src=\"logo.png\"></center>";

	private static final boolean USE_BROWSER = Desktop.isDesktopSupported() &&
			                                   Desktop.getDesktop().isSupported(Desktop.Action.BROWSE);

	private KsQueryEngine ksEngine;

	private AbstractButton autoPick;
    private AbstractButton studiesB;

	private final String pickLink;

    private final LinkedList<HistoryStep> pageHistory = new LinkedList<HistoryStep>(); // NOPMD
    private int                           pageIndex   = 0;
	private final JButton naviBack;
	private final JButton naviNext;

    private int               searchTypeBaseLimit = 0;
	private final JComboBox   searchType;
	private final JTextField  searchInput;
	private final JCheckBox   isIdInput;
	private final JEditorPane annotPane;

	private final NeighborModel upT;
	private final NeighborModel downT;

	private final Map<Integer,String> xrefURLs = new HashMap<Integer,String>();

	private Map<Integer,BioentityType> entityTypes;
	private final Map<Integer,Study>   studies = new WeakHashMap<Integer,Study>();

	private final Set<BioentityListener> entityListeners = new HashSet<BioentityListener>();
	private Bioentity                    bioentity;

    private final Set<LocusListener> locusListeners = new HashSet<LocusListener>();

	private static final String PROTOCOL_INTERNAL = "kotelosieni";
	private static final String PROTOCOL_EXTERNAL = "browser";

	private static final Object[] FIXED_DATABASES = new Object[] {
		BROWSER_DB,
		fi.helsinki.ltdk.csbl.asser.annotation.IDConverter.DNA_REGION,
		Piispanhiippa.PseudoXref.DNARegion,
		Piispanhiippa.PseudoXref.BioentityId,
		Piispanhiippa.PseudoXref.BioentityName,
		Piispanhiippa.PseudoXref.HitStudyId,
		Piispanhiippa.PseudoXref.HitStudyName
	};

	public KsPane() {
		super(new BorderLayout());

		isIdInput = new JCheckBox("ID", false);
		isIdInput.setToolTipText("Native identifier");
		isIdInput.setEnabled(false);
		searchType = new JComboBox(FIXED_DATABASES);
		searchType.setSelectedIndex(4);
		searchType.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent event) {
				isIdInput.setEnabled(searchType.getSelectedIndex() >= searchTypeBaseLimit);
			}
		});
		searchTypeBaseLimit = searchType.getItemCount();

		searchInput = new JTextField(1);
		searchInput.setBorder(new TitledBorder("Find by"));
		searchInput.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				fireBioentitySearch();
			}
		});

		naviBack = new JButton("<");
		naviBack.setToolTipText("Previous page");
		naviBack.setEnabled(false);
		naviBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				setPage(pageIndex-1);
			}
		});
		naviNext = new JButton(">");
		naviNext.setToolTipText("Next page");
		naviNext.setEnabled(false);
		naviNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				setPage(pageIndex+1);
			}
		});

		JPanel searchTypePane = new JPanel(new FlowLayout());
		searchTypePane.add(searchType);
		searchTypePane.add(isIdInput);
		JPanel naviPane = new JPanel(new FlowLayout());
		naviPane.add(naviBack);
		naviPane.add(naviNext);
		JPanel searchPane = new JPanel(new BorderLayout());
		searchPane.add(naviPane,       BorderLayout.WEST);
		searchPane.add(searchInput,    BorderLayout.CENTER);
		searchPane.add(searchTypePane, BorderLayout.EAST);
		add(searchPane, BorderLayout.NORTH);

		upT   = new NeighborModel();
		downT = new NeighborModel();
		JSplitPane neighbourPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
               upT  .prepareNeighbourPane(  "up stream neighbors", this, false),
               downT.prepareNeighbourPane("down stream neighbors", this, true)
        );
		neighbourPane.setDividerLocation(0.5);

		annotPane = new JEditorPane();
		annotPane.setContentType("text/html");
		annotPane.setEditable(false);
		annotPane.setPreferredSize(new Dimension(400,500));
		annotPane.addHyperlinkListener(new AnnotPaneListener());
		((HTMLDocument)annotPane.getDocument()).setBase(KsPane.class.getResource("/fi/helsinki/ltdk/csbl/moksiskaan/kotelosienet"));
		annotPane.setText(PAGE_HEADER+DEFAULT_PAGE+PAGE_FOOTER);
		JScrollPane itemP = new JScrollPane(annotPane);

		JSplitPane midSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
			   itemP,
			   neighbourPane
	    );
		midSplit.setDividerLocation(0.7);
		add(midSplit, BorderLayout.CENTER);

		pickLink = "<a href=\""+PROTOCOL_INTERNAL+':'+PICK_CMD+"/\"><img alt=\"Pick\" src=\""+
		           BioentityBasket.class.getResource("/fi/helsinki/ltdk/csbl/moksiskaan/kotelosienet/basket.png")+
		           "\" border=\"0\"></a> ";
	}

	public void init(KsQueryEngine  engine,
			         List<XrefType> xTypes) throws SQLException {
		ksEngine = engine;

		entityTypes = ksEngine.getBioentityTypes();
		upT  .setEntityTypes(entityTypes);
		downT.setEntityTypes(entityTypes);

		Map<Integer,LinkType> linkTypes = new HashMap<Integer,LinkType>();
		for (LinkType lt : ksEngine.getLinkTypes()) {
			linkTypes.put(lt.getLinkTypeId(), lt);
		}
		upT  .setLinkTypes(linkTypes);
		downT.setLinkTypes(linkTypes);

        for (XrefType xrefT : xTypes) {
		    searchType.addItem(xrefT);
		    searchTypeBaseLimit++;
		    if (xrefT.getWww() != null) {
		       xrefURLs.put(xrefT.getXrefTypeId(), xrefT.getWww());
		    }
        }

        dataSourcesSelected(engine.getSelectedKorvasieniDbs());
	}

	public BioentityPrinter getPrinter() {
		return new BioentityPrinter(this, annotPane);
	}

	@Override
	public boolean requestFocusInWindow() {
		return searchInput.requestFocusInWindow();
	}

	/**
	 * Registers a listener for the selections of chromosomal regions.
	 */
	public void addLocusListener(LocusListener listener) {
		locusListeners.add(listener);
		if ((listener != null) && (bioentity != null)) {
			listener.setRegion(bioentity.getLocus());
		}
	}

	/**
	 * Removes an already registered listener of the chromosomal regions.
	 */
	public void removeLocusListener(LocusListener listener) {
		locusListeners.remove(listener);
	}

	/**
	 * Return an array of the search types available.
	 */
	public Object[] getIDTypes() {
		Object[] types = new Object[searchType.getItemCount()-1];
		for (int i=0; i<types.length; i++) {
			types[i] = searchType.getItemAt(i+1);
		}
		return types;
	}

	public Map<Integer,BioentityType> getBioentityTypes() {
		return entityTypes;
	}

	public void pick(boolean enableOnly) {
		for (BioentityListener listener : entityListeners) {
			if (enableOnly)
				listener.unselectBioentities();
			else
			    listener.bioentitySelected(bioentity);
		}
	}

	/**
	 * Registers a listener interested in the collected bioentities
	 * by the user.
	 */
	public void addPickListener(BioentityListener listener) {
		entityListeners.add(listener);
	}

    @Override
    public void dataSourcesSelected(String[] dbs) {
    	for (int i=searchType.getItemCount()-1; i>=searchTypeBaseLimit; i--) {
    		searchType.removeItemAt(i);
    	}
    	for (String db : dbs) {
    		searchType.addItem(db);
    	}
    }

    public void setAutopick(AbstractButton indicator) {
        autoPick = indicator;
    }

    public void setShowStudies(AbstractButton indicator) {
        studiesB = indicator;
    }

	public void fireBioentitySearch() {
		Object type = searchType.getSelectedItem(); 
		String id   = searchInput.getText().trim();

		if (type == null) {
			return;
		}
		if (id.isEmpty()) {
			return;
		}
		if (BROWSER_DB.equals(type)) {
			try { showPage(new URL(id)); }
			catch (MalformedURLException err) {
				showError("Invalid URL: "+id, err);
			}
			return;
		}
		if (isIdInput.isEnabled() && isIdInput.isSelected()) {
			type = '_'+(String)type;
		}
		fireBioentitySearch(id, type);
	}

	/**
	 * Selects a new bioentity for the viewer.
	 *
	 * @param id   Value of the identifier
	 * @param type Identifier type:
	 *             ({@link java.lang.String}
	 *                     for Korvasieni databases,
	 *              {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType} or
	 *              {@link java.lang.Integer}
	 *                     for the external Moksiskaan references,
	 *              {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref}
	 *                     for the internal Moksiskaan references).
	 * @return The bioentity selection or null if the result set was not unique.
	 */
	public Bioentity fireBioentitySearch(String id, Object type) {
		try {
		    List<Object[]> entities = ksEngine.getBioentity(id, type);
		    if ((entities == null) || entities.isEmpty()) {
		    	if (id == null) {
		    	   showMessage("No entities of type: "+type+'.');
		    	} else {
		    	   showMessage(id+
		    		           " not found.<p>Please notice that the queries are <b>case sensitive</b> and you were looking for <b>"+
                               type+"</b> matches."+
                               (isIdInput.isEnabled() ? " ID has been set to <b>"+(isIdInput.isSelected() ? "database identifier" : "human readable")+"</b> mode." : "</p>"));
		    	}
		    	if (type == Piispanhiippa.PseudoXref.DNARegion) {
		    	   DNARegion region = DNARegion.valueOf(id);
		    	   for (LocusListener ll : locusListeners) { ll.setRegion(region); }
		    	}
		    } else
		    if (entities.size() == 1) {
		    	Object[]  item   = entities.get(0);
		    	Bioentity entity = new Bioentity();
				entity.setBioentityId((Long)item[1]);
				entity.setName((String)item[2]);
				entity.setBioentityType(entityTypes.get((Integer)item[3]));
		    	bioentitySelected(entity);
		    	pick(!autoPick.isSelected());
		    } else {
		    	showEntities(entities, id, type);
		    	if (type == Piispanhiippa.PseudoXref.DNARegion) {
		    	   DNARegion region = DNARegion.valueOf(id);
		    	   for (LocusListener ll : locusListeners) { ll.setRegion(region); }
		    	}
		    }
		} catch (Exception e) {
			showError("Could not fetch '"+id+"' from the database.", e);
		}
		return bioentity;
	}

	@Override
	public void unselectBioentities() {
		showMessage(DEFAULT_PAGE);
	}

	@Override
	public void bioentitySelected(Bioentity entity) {
	    Component cursorPane = ((RootPaneContainer)getTopLevelAncestor()).getGlassPane();
		try { // Cursor block start
		cursorPane.setCursor(WAIT_CURSOR);
		cursorPane.setVisible(true);

	    DNARegion      region      = entity.getLocus();
		String         korvaId     = null;
		String         keggPathway = null;
		String         wikiPathway = null;
		List<Object[]> data;
	    List<Object[]> dataUp;
	    List<Object[]> dataDown;

		try {
		   dataUp   = ksEngine.getNeighbors(entity.getBioentityId(), false);
   		   dataDown = ksEngine.getNeighbors(entity.getBioentityId(), true);
		} catch (Exception e) {
		   showError("Cannot fetch neigbors for "+entity+'.', e);
		   return;
		}

		if (region == null) {
			try { region = ksEngine.getRegion(entity.getBioentityId()); }
			catch (Exception e) {
			   showError("Cannot fetch the locus of "+entity+'.', e);
			   return;
			}
			entity.setLocus(region);
		}

		StringBuffer text = new StringBuffer(2048);
		text.append(PAGE_HEADER)
		    .append("<i id=\"").append(PICK_CMD).append("\">")
		    .append(entity.getBioentityType())
		    .append(" - ");
		internalLink(Piispanhiippa.PseudoXref.BioentityId, entity.getBioentityId(), null, text);
		text.append("</i><h1>")
		    .append(entity.getName()).append("</h1>");

		if ((dataDown.size() >= KsQueryEngine.MAX_RESULT_SIZE) ||
			(dataUp  .size() >= KsQueryEngine.MAX_RESULT_SIZE)) {
			text.append("<font color=\"red\">Only ")
			    .append(KsQueryEngine.MAX_RESULT_SIZE)
			    .append(" first up and down stream neighbors are shown!</font><br>");
		}

		try {
		   data = ksEngine.getXrefs(entity.getBioentityId());
		} catch (Exception e) {
		   showError("Cannot fetch external references for "+entity+'.', e);
		   return;
		}
		if (region != null) {
	        text.append("locus = ")
	            .append(region).append(" (").append(region.getLength()).append("bp)")
	            .append(PAGE_LF);
            for (LocusListener ll : locusListeners) { ll.setRegion(region); }
		}
		for (Object[] xref : data) {
			boolean asLink = false;
			text.append(xref[0]).append(" = ");
			if (IDConstants.XrefType_Ensembl_gene.equals(xref[2])) {
			    korvaId = (String)xref[1];
			} else
			if (IDConstants.XrefType_Disease_category.equals(xref[2]) ||
				IDConstants.XrefType_Enzyme_classification.equals(xref[2])) {
				asLink = true;
			} else
			if (IDConstants.XrefType_KEGG_pathway.equals(xref[2])) {
				keggPathway = (String)xref[1];
			} else 
			if (IDConstants.XrefType_WikiPathways.equals(xref[2])) {
                wikiPathway = (String)xref[1];
            }
			String xrefURL = xrefURLs.get(xref[2]);
		    if (xrefURL == null) {
		       if (asLink) {
		    	  internalLink(xref[2], xref[1], null, text);
	           } else {
		          text.append(xref[1]);
	           }
			   text.append(PAGE_LF);
		    } else {
		       externalLink(xrefURL.replaceAll(CSV2LatexComponent.LINK_ID_TAG, (String)xref[1]), xref[1], text, xref[2]);
		    }
		}

        if (keggPathway != null) {
            keggPathway = keggPathway.substring(5);
            text.append("<img alt=\"KEGG pathway\" src=\"http://www.kegg.jp/kegg/pathway/")
                .append(keggPathway.substring(0,3)).append('/').append(keggPathway)
                .append(".png\" /><br/>");
        }
        
        if (wikiPathway != null) {
            text.append("<img alt=\"WikiPathways\" src=\"http://wikipathways.org//wpi/wpi.php?action=downloadFile&type=png&pwTitle=Pathway:")
                .append(wikiPathway)
                .append("\" /><br/>");
        }

        if ((korvaId != null) && ksEngine.isKorvasieniInUse()) {
           List<Set<DatabaseId>> korvaAnnots;
           try { korvaAnnots = ksEngine.geneAnnotations(korvaId); }
           catch (Exception e) {
        	   StringBuffer reload = new StringBuffer(256);
        	   internalLink(Piispanhiippa.PseudoXref.BioentityId, entity.getBioentityId(), "Try again!", reload);
		       showError("Cannot fetch "+korvaId+" from Ensembl.<p>"+reload+"</p>", e);
		       return;
		   }
           if (korvaAnnots == null) {
        	   text.append("No Korvasieni annotations for ")
        	       .append(korvaId)
        	       .append('.');
           } else {
	           text.append(PAGE_LF);
	           text.append(valuesOf(korvaAnnots.get(0)));
	           if (!korvaAnnots.get(2).isEmpty()) {
	        	   text.append(" Band: ").append(valuesOf(korvaAnnots.get(2)));
	           }
	           text.append(" Biotypes: ");
	           text.append(valuesOf(korvaAnnots.get(1)).replace('_', ' '));
        	   text.append(".<br/>");
	           String[]                  dbKeys = ksEngine.getSelectedKorvasieniDbs();
	           Map<String, OntologyTerm> go     = null;
	           if (AsserUtil.indexOf("GO", dbKeys, true, false) >= 0) {
	        	   try { go = ksEngine.getGO(); }
	        	   catch (IOException e) {
	        		   showError("Cannot access the Gene Ontology.", e);
	    		       return;
	        	   }
	           }
	           boolean hasExtraAnnot = false;
			   for (int i=3; i<korvaAnnots.size(); i++) {
				   Set<DatabaseId> ids = korvaAnnots.get(i);
				   if (ids.isEmpty()) continue;
				   if (!hasExtraAnnot) {
					   hasExtraAnnot = true;
					   text.append("<dl>");
				   }
				   text.append("<dt>").append(dbKeys[i]).append("</dt>");
			       for (DatabaseId idEntry : ids) {
					    text.append("<dd>")
					        .append(idEntry.getLabel());
					    if (!idEntry.getId().equals(idEntry.getLabel())) {
					    	text.append(" (").append(idEntry.getId()).append(')');
					    }
					    if ((go != null) && ("GO".equals(dbKeys[i]))) {
					    	OntologyTerm term = go.get(idEntry.getId());
					    	if (term != null) {
					    	   text.append(' ')
					    	       .append(term.getName())
					    	       .append(" (").append(RegionGO.goCategory(term.getCategory())).append(')');
					    	}
					    }
					    text.append("</dd>");
			       }
			   }
			   if (hasExtraAnnot) text.append("</dl>");
           }
        }

        if ((studiesB != null) && studiesB.isSelected())
		try {
		   data = ksEngine.getStudies(entity.getBioentityId());
           if (!data.isEmpty() && (data.get(0)[2] != null)) { // Has some studies
			  text.append("\n<h2>Related studies</h2>\n<dl>");
              for (Object[] hit : data) {
                  Study study = getStudy((Integer)hit[1]);
			      text.append("<dt>")
			          .append(hit[2])
			          .append(" - ");
			      internalLink(Piispanhiippa.PseudoXref.HitStudyId, hit[1], null, text);
			      text.append("</dt><dd>")
			          .append(study.getDescription())
			          .append("</dd><dd>")
			          .append(study.getScoreType().getName())
			          .append(" score of this bioentity is ")
			          .append(hit[3]);
			      if (!(Boolean)hit[4]) {
			         text.append(" <font color=\"red\">for contradictive evidence</font>");
			      }
			      text.append(".</dd>");
              }
              text.append("</dl>");
           }
		} catch (Exception e) {
		   showError("Cannot fetch studies for "+entity+'.', e);
		   return;
		}

		text.append(PAGE_FOOTER);
		setPage(text.toString(), entity, dataUp, dataDown);
		if (!autoPick.isSelected()) {
			HTMLDocument doc = (HTMLDocument)annotPane.getDocument();
			try { doc.insertAfterEnd(doc.getElement(PICK_CMD), pickLink); }
			catch (Exception err) { err.printStackTrace(); }
		}

		} finally { // Cursor block end
		  cursorPane.setVisible(false);
		  cursorPane.setCursor(DEFAULT_CURSOR);
		}
	}

	static private void internalLink(Object       type,
			                         Object       id,
			                         Object       label,
			                         StringBuffer text) {
		text.append("<a href=\"").append(PROTOCOL_INTERNAL).append(':')
            .append(type).append('/').append(id).append("\">")
            .append((label==null) ? id : label)
            .append("</a>");
	}

	static private void externalLink(String       url,
			                         Object       label,
			                         StringBuffer text,
			                         Object       type) {
	    if (USE_BROWSER) {
	       if (type == null) {
	          text.append(label);
	       } else {
	          internalLink(type, label, null, text);
	       }
		   text.append(" (<a href=\"").append(PROTOCOL_EXTERNAL).append(':')
		       .append(url).append("\">browser</a>)");
        } else {
           text.append(label);
        }
	    text.append(PAGE_LF);
	}

	private void setPage(int index) {
		pageIndex = index;
		pageHistory.get(index).show(); 

		naviBack.setEnabled(index > 0);
		naviNext.setEnabled(index+1 < pageHistory.size());
	}

	private void setPage(String         content,
			             Bioentity      entity,
			             List<Object[]> dataUp,
			             List<Object[]> dataDown) {
		while (pageIndex+1 < pageHistory.size()) {
			pageHistory.removeLast();
		}
		if (pageHistory.size() > 10) pageHistory.removeFirst();
		HistoryStep step = new HistoryStep(content, entity, dataUp, dataDown);
		pageHistory.add(step);
		step.show();
		pageIndex = pageHistory.size()-1;
		naviBack.setEnabled(pageIndex > 0);
		naviNext.setEnabled(false);
	}

	private static String valuesOf(Set<DatabaseId> set) {
		String[] values = new String[set.size()];
		int      i      = 0;
		for (DatabaseId id : set) {
			values[i++] = id.getLabel();
		}
		return AsserUtil.collapse(values, ", ");
	}

	public LinkAnnotation[] getLinkDetails(Long    target,
			                               Integer linkType,
			                               boolean direct) {
		try {
			Long s = direct ?  bioentity.getBioentityId() : target;
			Long t = direct ?  target : bioentity.getBioentityId();
			return ksEngine.getLinkAnnotations(s, t, linkType);
		} catch (SQLException err) {
			showError("Could not load link annotations.", err);
		    return new LinkAnnotation[0];
		}
	}

    private Study getStudy(Integer id) throws SQLException {
       synchronized (studies) {
	       Study study = studies.get(id);
	       if (study == null) {
	          study = ksEngine.getStudy(id);
	          studies.put(id, study);
	       }
	       return study;
       }
    }

    public void showStudies(Integer[] ids) {
		StringBuffer text = new StringBuffer(4096);
		text.append(PAGE_HEADER)
		    .append("<b>List of ")
		    .append(ids.length)
		    .append(" studies:</b><br/>");
		try {
			for (int i=0; i<ids.length; i++) {
				Study study = getStudy((Integer)ids[i]);
				text.append("<br/><b>");
			    internalLink(Piispanhiippa.PseudoXref.HitStudyId, ids[i], study.getTitle(), text);
			    text.append("</b> (")
			        .append(ids[i])
			        .append(")<br/>")
			        .append(study.getDescription())
			        .append(" Scores are stored in ")
			        .append(study.getScoreType().getName())
			        .append(" format.<br/>");
			}
        } catch (Exception e) {
		   showError("Cannot show studies: "+Arrays.toString(ids), e);
		   return;
		}
		text.append(PAGE_FOOTER);
		setPage(text.toString(), null, null, null);        
    }

	private void showEntities(List<Object[]> entities,
			                  String         key,
			                  Object         type) { // NOPMD
		if (type instanceof Integer) {
			int tId = ((Integer)type).intValue();
			for (int i=0; i<searchTypeBaseLimit; i++) {
				Object item = searchType.getItemAt(i);
				if ((item instanceof XrefType) && (((XrefType)item).getXrefTypeId() == tId)) {
					type = item;
					break;
				}
			}
		}
		
		StringBuffer text = new StringBuffer(2048);
		text.append(PAGE_HEADER)
		    .append("<b>List of ")
		    .append(entities.size());
		if (entities.size() >= KsQueryEngine.MAX_RESULT_SIZE) {
		    text.append(" (=<font color=\"red\">LIMITED TO THE MAXIMUM OUTPUT SIZE</font>) ");
		}
	    text.append(" bioentities matching ")
		    .append(type).append('=').append((key==null)?"*":key)
		    .append(":</b><br/>");
		for (Object[] entity : entities) {
			internalLink(Piispanhiippa.PseudoXref.BioentityId, entity[1], entity[2], text);
		    text.append(" (")
		        .append(entityTypes.get((Integer)entity[3]))
		        .append(':')
		        .append(entity[1])
		        .append(")<br/>");
		}
		text.append(PAGE_FOOTER);
		setPage(text.toString(), null, null, null);
	}

	public void showPage(URL url) {
		HyperlinkEvent event = new HyperlinkEvent(annotPane,
				                                  HyperlinkEvent.EventType.ACTIVATED,
				                                  url);
		annotPane.fireHyperlinkUpdate(event);
	}

	public void showMessage(String message) {
		StringBuffer text = new StringBuffer(2048);
		text.append(PAGE_HEADER)
		    .append(message)
		    .append(PAGE_FOOTER);
		annotPane.setText(text.toString());
		annotPane.setCaretPosition(0);
		upT  .setData(null);
		downT.setData(null);
		bioentity = null;
	}

	/**
	 * Displays an error message to the user.
	 *
	 * @param message An HTML fragment that described the error
	 * @param err     The associated stack trace if available
	 */
	public void showError(String    message,
			              Throwable err) {
		StringBuffer text = new StringBuffer(2048);
		text.append(PAGE_HEADER)
		    .append(message);
		if (err != null) {
			ByteArrayOutputStream errOut = new ByteArrayOutputStream(2048);
			err.printStackTrace(new PrintStream(errOut));
			try { errOut.close(); }	catch (Exception e) { e.printStackTrace(); }
		    text.append("<small><pre>").append(errOut.toString()).append("</pre></small>");
		}
		text.append(PAGE_FOOTER);
		annotPane.setText(text.toString());
		annotPane.setCaretPosition(0);
		bioentity = null;
	}

    /**
     * Provides the navigation logic for the view.
     */
    class AnnotPaneListener implements HyperlinkListener {

        public void hyperlinkUpdate(HyperlinkEvent event) {
            if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                JEditorPane pane = (JEditorPane) event.getSource();
                if (event instanceof HTMLFrameHyperlinkEvent) {
                    HTMLFrameHyperlinkEvent evt = (HTMLFrameHyperlinkEvent)event;
                    HTMLDocument            doc = (HTMLDocument)pane.getDocument();
                    doc.processHTMLFrameHyperlinkEvent(evt);
                } else {
                	String description = event.getDescription(); 
                	if ((description != null) && description.startsWith(PROTOCOL_INTERNAL)) {
                	    int    delim = description.indexOf('/');
                	    String type  = description.substring(PROTOCOL_INTERNAL.length()+1, delim);
                		String id    = description.substring(delim+1);
                		if (PICK_CMD.equals(type)) {
                			pageHistory.getLast().show();
                			pick(false);
                		} else
                	    fireBioentitySearch(id,
                	                        Character.isDigit(type.charAt(0)) ?
                	                        Integer.valueOf(type) :
                	                        Piispanhiippa.PseudoXref.valueOf(type));
                	} else
                	if ((description != null) && description.startsWith(PROTOCOL_EXTERNAL)) {
                	    String url = description.substring(description.indexOf(':')+1);
                	    try {
                           Desktop.getDesktop().browse(new URI(url));
                        } catch (Exception err) {
                           showError("Web browser access failed.", err);
                        }
                	} else
                    try {
                    	Document doc = pane.getDocument();
                    	doc.putProperty(Document.StreamDescriptionProperty, null);
                        pane.setPage(event.getURL());
                    } catch (Throwable t) {
                        showError("Cannot open "+event.getURL()+'.', t);
                    }
                }
            }
        }
    }

    private class HistoryStep {

    	final String         content;
    	final Bioentity      entity;
    	final List<Object[]> neighborsUp;
    	final List<Object[]> neighborsDown;

    	HistoryStep(String         pageContent,
    			    Bioentity      activeEntity,
                    List<Object[]> up,
                    List<Object[]> down) {
    		content       = pageContent;
    		entity        = activeEntity;
    		neighborsUp   = up;
    		neighborsDown = down;
    	}

    	void show() {
    		annotPane.setText(content);
    		annotPane.setCaretPosition(0);

    		bioentity = entity;
    		upT  .setData(neighborsUp);
    		downT.setData(neighborsDown);
    	}

    }

}
package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import fi.helsinki.ltdk.csbl.moksiskaan.Version;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.BioentityType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.DNARegion;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkAnnotation;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Log;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Organism;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Study;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType;

/**
 * Remote server connector for the client.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.11
 */
public class RemoteQueryEngine extends AbstractQueryEngine {

	static private final String UNCHECKED = "unchecked";

	private final URL serverAddr;

	public RemoteQueryEngine(URL address) {
		serverAddr = address;
	}

	/**
	 * Implements the communication protocol between the client and the server.
	 * This method can be used to call server side routines.
	 *
	 * @param method Name of the service that is provided by the server
	 * @param data   Optional inputs for the service
	 * @return       Results that are dependent on the service request
	 * @throws       RuntimeException Used to indicate service failures. 
	 */
	private Object sendMessage(String method, Serializable... data) {
		Object result;
		try {
	        URLConnection con = serverAddr.openConnection();
            con.setDoOutput(true);
            ObjectOutputStream out = new ObjectOutputStream(con.getOutputStream());
            out.writeUTF(method);
            if (data.length < 1) {
               out.writeObject(null);
            } else
            for (Object obj : data) {
               out.writeObject(obj);
            }

            ObjectInputStream in = new ObjectInputStream(con.getInputStream());
            result = in.readObject();
            in.close();
            out.close();
		} catch (ClassNotFoundException err) {
			throw new RuntimeException("Cannot open the server response.", err);
		} catch (IOException err) {
			throw new RuntimeException("Cannot access the database server.", err);
		}
        if (result instanceof RuntimeException) throw (RuntimeException)result; 
		return result;
	}

	public URL getUserGuideAddress() {
		try {
			return new URL("http://csbi.ltdk.helsinki.fi/moksiskaan/userguide/userguide.html");
		} catch (MalformedURLException e) {
			return null;
		}
	}

    @Override
    public void setKorvasieniCfg(Integer organismId, String filename) {
    	try { korvasieniCfgs.put(organismId, new URL(serverAddr, filename)); }
    	catch (MalformedURLException err) {
    		throw new IllegalArgumentException("Invalid configuration file name: "+filename, err);
    	}
    }

	@Override
	@SuppressWarnings(UNCHECKED)
	protected List<Object[]> getBioentity(List<String> keys, String typeS) throws SQLException {
		return (List<Object[]>)sendMessage("getBioentity", (Serializable)keys, typeS, organism);
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	public Map<Integer,BioentityType> getBioentityTypes() throws SQLException {
        return (Map<Integer,BioentityType>)sendMessage("getBioentityTypes");
	}

	@Override
	public String getDescription() {
		StringBuffer buffer = new StringBuffer(1024);
		buffer.append("<html><h1>Kotelosienet version ")
		      .append(Version.getVersion())
		      .append("</h1></html>\n\nMoksiskaan: ")
		      .append(serverAddr);
		if (isKorvasieniInUse()) {
			buffer.append("\nEnsembl: ")
			      .append(korvasieni.getURL());
		}
		if (fileGO != null) {
			buffer.append("\nGene Ontology: ")
			      .append(fileGO);
		}
		return buffer.toString();
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	public List<LinkType> getLinkTypes() throws SQLException {
		return (List<LinkType>)sendMessage("getLinkTypes");
	}

	@Override
	public Log[] getLogEntities() throws SQLException {
		return (Log[])sendMessage("getLogEntities");
	}

	@Override
	public LinkAnnotation[] getLinkAnnotations(Long source, Long target, Integer type) throws SQLException {
		return (LinkAnnotation[])sendMessage("getLinkAnnotations", source, target, type);
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	public List<Object[]> getNeighbors(Long id, boolean down) throws SQLException {
		String lts = getLinkTypeSequence();
		if (lts.isEmpty()) return Collections.emptyList();
		return (List<Object[]>)sendMessage("getNeighbors", id, down, lts, organism);
	}

	@Override
	public DNARegion getNextRegion(DNARegion locus, boolean previous) throws SQLException {
		return (DNARegion)sendMessage("getNextRegion", locus, previous, organism);
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	public List<Organism> getOrganisms() throws SQLException {
		return (List<Organism>)sendMessage("getOrganisms");
	}

	@Override
	public DNARegion getRegion(long bioentityId) throws SQLException {
		return (DNARegion)sendMessage("getRegion", bioentityId);
	}

	@Override
	public DNARegion[] getRegions(String chromosome, long start, long stop)	throws SQLException {
		return (DNARegion[])sendMessage("getRegions", chromosome, start, stop, organism);
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	public List<Object[]> getStudies(Long bioentityId) throws SQLException {
		return (List<Object[]>)sendMessage("getStudies", bioentityId, organism);
	}

	@Override
	public Study getStudy(Integer id) throws SQLException {
		return (Study)sendMessage("getStudy", id);
	}

	@Override
	public Integer[] getStudyIdentifiers() throws SQLException {
		return (Integer[])sendMessage("getStudyIdentifiers", organism);
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	public List<Object[]> getXrefs(Long id) throws SQLException {
		return (List<Object[]>)sendMessage("getXrefs", id);
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	public List<XrefType> getXrefs() throws SQLException {
		return (List<XrefType>)sendMessage("getXrefs");
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	public List<Object[]> getLinkTypeStatistics() throws SQLException {
		return (List<Object[]>)sendMessage("getLinkTypeStatistics", organism);
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	public List<Object[]> getBioentityTypeStatistics() throws SQLException {
		return (List<Object[]>)sendMessage("getBioentityTypeStatistics", organism);
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	public List<Object[]> getXrefTypeStatistics() throws SQLException {
		return (List<Object[]>)sendMessage("getXrefTypeStatistics", organism);
	}

    @Override
    @SuppressWarnings(UNCHECKED)
    public List<Object[]> getChromosomeStatistics() throws SQLException {
        return (List<Object[]>)sendMessage("getChromosomeStatistics", organism);
    }

}
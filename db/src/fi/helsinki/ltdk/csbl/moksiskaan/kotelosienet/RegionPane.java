package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.DNARegion;

/**
 * A genome browser like GUI component that can be used to browse
 * bioentities along the chromosome co-ordinates.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.09
 */
public class RegionPane extends JPanel implements LocusListener {
	
	private static final long serialVersionUID = 1L;

	private final LocusPane locusP;
	private final JButton   movePrevB;
	private final JButton   moveNextB;

	private final RegionUpdate worker;

	private final KsQueryEngine engine;
	private KsPane ksPane;

	private long windowUp   = 50000;
	private long windowDown = 50000;

	public RegionPane(KsQueryEngine queryEngine) {
		super(new BorderLayout());

		engine = queryEngine;
		worker = new RegionUpdate();

		locusP = new LocusPane();
		locusP.addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent ev) { /* No action */ }
			@Override
			public void mousePressed(MouseEvent ev) { /* No action */ }
			@Override
			public void mouseExited(MouseEvent ev) { /* No action */ }
			@Override
			public void mouseEntered(MouseEvent ev) { /* No action */ }
			@Override
			public void mouseClicked(MouseEvent ev) {
				if (ksPane == null) return;
				if (ev.getClickCount() > 1) return;

				DNARegion region = locusP.itemAt(ev.getPoint());
				if (region != null) {
					ksPane.fireBioentitySearch(String.valueOf(region.getBioentity().getBioentityId()),
							                   Piispanhiippa.PseudoXref.BioentityId);
				}
			}
		});
		JScrollPane locusScroll = new JScrollPane(locusP,
				                                  ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				                                  ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		locusScroll.setPreferredSize(locusP.getPreferredSize());

		movePrevB = new JButton("<");
		movePrevB.setToolTipText("Previous bioentity");
		movePrevB.addActionListener(new StepListener(true));
		movePrevB.setEnabled(false);
		moveNextB = new JButton(">");
		moveNextB.setToolTipText("Next bioentity");
		moveNextB.addActionListener(new StepListener(false));
		moveNextB.setEnabled(false);

		add(movePrevB,   BorderLayout.WEST);
		add(locusScroll, BorderLayout.CENTER);
		add(moveNextB,   BorderLayout.EAST);
	}

	public void setRegionExtension(long up, long down) {
		windowUp   = up;
		windowDown = down;
	}

	public void setKsPane(KsPane viewer) {
		ksPane = viewer;
	}

	@Override
	public void setRegion(DNARegion region) {
		worker.setRegion(region);
	}

	private class StepListener implements ActionListener {

		private final boolean previous;

		StepListener(boolean direction) {
			previous = direction;
		}

		@Override
		public void actionPerformed(ActionEvent ev) {
			DNARegion position = locusP.getSelection();
			if (position == null) {
				position = previous ? locusP.getFirstRegion() :
                                      locusP.getLastRegion();
			    if (position == null) return;
			}

			try { position = engine.getNextRegion(position, previous); }
			catch (SQLException err) {
				if (ksPane == null) {
					err.printStackTrace();
				} else {
					ksPane.showError("Cannot move to the "+(previous?"previous":"next")+" position.", err);
				}
				return;
			}
			worker.setRegion(position);
		}

	}

	private class RegionUpdate implements Runnable {

		private DNARegion region;

		public void setRegion(DNARegion region) {
			synchronized (this) {
				this.region = region;
				movePrevB.setEnabled(false);
				moveNextB.setEnabled(false);
			}
			SwingUtilities.invokeLater(worker);
		}

		@Override
		public void run() {
			synchronized (this) {
				if (this.region == null) {
					locusP.setItems(null);
					return;
				}
				DNARegion[] regions;
				try {
					regions = engine.getRegions(region.getChromosome(),
	                                            region.getStart()-windowUp,
	                                            region.getStop() +windowDown);
				} catch (SQLException err) {
					err.printStackTrace();
					return;
				}
				locusP.setItems(regions);
				locusP.setViewPosition(region.getStart()-windowUp,
						               region.getStop() +windowDown);
				locusP.setSelection(region);
				if (locusP.getFirstRegion() != null) {
					movePrevB.setEnabled(true);
					moveNextB.setEnabled(true);					
				}
				this.region = null;
			}
		}

	}

}
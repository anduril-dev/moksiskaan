package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.*;

/**
 * Provides a dialog that can be used to select Korvasieni annotation sources.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public class KorvasieniDbDialog {

	private final KsQueryEngine engine;

	private final Set<AnnotationSelectionListener> listeners = new HashSet<AnnotationSelectionListener>();

	public KorvasieniDbDialog(KsQueryEngine db) {
		engine = db;
	}

	public void addAnnotationSelectionListener(AnnotationSelectionListener listener) {
		listeners.add(listener);
	}

	public void select(Component host) throws SQLException {
		List<Object[]>    databases  = engine.getKorvasieniDatabases();
		final JCheckBox[] selections = new JCheckBox[databases.size()];
		final JPanel      panel      = new JPanel();
		Set<String>       old        = new HashSet<String>(databases.size());
		final String[]    searchKeys = new String[databases.size()];

		for (String db : engine.getSelectedKorvasieniDbs()) {
			old.add(db);
		}
		CompleteSelection noneSelector = new CompleteSelection(false);
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		for (int i=0; i<databases.size(); i++) {
			Object[] db = databases.get(i);
			selections[i] = new JCheckBox((String)db[0], old.contains(db[0]));
			selections[i].setToolTipText((String)db[1]);
			panel.add(selections[i]);
			noneSelector.add(selections[i]);
			searchKeys[i] = ((String)db[0]).toLowerCase();
		}

		final JTextField findField = new JTextField(15);
		findField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				String text = findField.getText().toLowerCase();
				for (int i=0; i<selections.length; i++) {
				    selections[i].setVisible(searchKeys[i].indexOf(text) >= 0);
				}
			}
		});

		JButton unselectB = new JButton("clear");
		unselectB.addActionListener(noneSelector);
		JPanel findPane  = new JPanel(new BorderLayout());
		findPane.add(new JLabel("Find"), BorderLayout.WEST);
		findPane.add(findField,          BorderLayout.CENTER);
		findPane.add(unselectB,          BorderLayout.EAST);

		JScrollPane scroll = new JScrollPane(panel);
		scroll.setColumnHeaderView(findPane);
		scroll.setPreferredSize(new Dimension(350,250));
		scroll.getVerticalScrollBar().setUnitIncrement(15);

		final String[] OPTIONS = { "OK", "Cancel" };
		JOptionPane optPane = new JOptionPane(scroll,
				                              JOptionPane.PLAIN_MESSAGE,
				                              JOptionPane.OK_CANCEL_OPTION,
				                              null,
				                              OPTIONS, OPTIONS[0]);
        JDialog dialog = optPane.createDialog(host, "Select annotation types");
        dialog.addWindowFocusListener(new WindowAdapter() {
           public void windowGainedFocus(WindowEvent ev) {
              findField.requestFocusInWindow();
           }
        });
        dialog.setVisible(true);
		if (!OPTIONS[0].equals(optPane.getValue())) return; // Cancel

		List<String> list = new LinkedList<String>();
		for (int i=0; i<databases.size(); i++) {
		    if (selections[i].isSelected()) {
		    	list.add((String)databases.get(i)[0]);
		    }
		}

		String[] dbs = list.toArray(new String[list.size()]);
		for (AnnotationSelectionListener listener : listeners) {
		    listener.dataSourcesSelected(dbs);
		}
	}

}
package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.annotation.DatabaseId;
import fi.helsinki.ltdk.csbl.asser.annotation.IDConverter;
import fi.helsinki.ltdk.csbl.asser.annotation.OntologyTerm;
import fi.helsinki.ltdk.csbl.asser.annotation.RegionGO;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Organism;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType;

/**
 * Provides a Moksiskaan independent implementation framework for the query engines.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.11
 */
public abstract class AbstractQueryEngine implements KsQueryEngine {

	public static final String[] FIXED_DATABASES = {
		IDConverter.ENSEMBL_GENE_DESC,
		IDConverter.BIOTYPE,
		IDConverter.DNA_BAND,
	};

	protected Integer organism = IDConstants.Organism_Homo_sapiens;

	private final Set<Integer> linkTypes     = new HashSet<Integer>();
	private String             linkTypeCache = null;

	protected final Map<Integer,URL> korvasieniCfgs = new HashMap<Integer,URL>();
	private   String[]               korvaDbs       = FIXED_DATABASES;
	protected IDConverter            korvasieni;

	protected String                    fileGO;
	private   Map<String, OntologyTerm> go;

	@Override
	public void close() {
		if (korvasieni != null) {
			korvasieni.close();
		}
	}

    @Override
    public boolean setOrganism(Organism o) throws SQLException, IOException {
    	boolean useKorva = isKorvasieniInUse();

    	organism = o.getOrganismId();
    	setKorvasieniInUse(false);
    	return setKorvasieniInUse(useKorva);
    }

	@Override
	public void setGO(String oboFile) {
		fileGO = oboFile;
	}

	@Override
	public Map<String,OntologyTerm> getGO() throws IOException {
		if (fileGO == null) return null;
		synchronized (fileGO) {
		 if (go == null) {
		   try {
			  if ((fileGO.indexOf(':')<0) || new File(fileGO).exists()) {
				  go = RegionGO.loadGO(fileGO, false);
			  } else {
                  go = RegionGO.loadGO(new URL(fileGO), false);
			  }
           }
		   catch (IOException e) {
		      fileGO = null;
		      throw e;
		   }
		 }
		}
		return go;
	}

	/**
	 * Provides a comma separated list of link type identifiers representing the
	 * current set of interest.
	 *
	 * @see #addLinkType(Integer)
	 * @see #removeLinkType(Integer)
	 */
	protected String getLinkTypeSequence() {
		if (linkTypeCache == null) {
		   linkTypeCache = AsserUtil.collapse(linkTypes);
		}
		return linkTypeCache;
	}

    @Override
    public void addLinkType(Integer id) {
    	linkTypes.add(id);
    	linkTypeCache = null;
    }

    @Override
    public void removeLinkType(Integer id) {
    	linkTypes.remove(id);
    	linkTypeCache = null;
    }

    @Override
    public void dataSourcesSelected(String[] dbs) {
    	if (dbs.length == 0) {
    		korvaDbs = FIXED_DATABASES;
    	} else {
            korvaDbs = new String[FIXED_DATABASES.length+dbs.length];
            System.arraycopy(FIXED_DATABASES, 0, korvaDbs,                      0, FIXED_DATABASES.length);
            System.arraycopy(            dbs, 0, korvaDbs, FIXED_DATABASES.length,             dbs.length);
    	}
    }

    @Override
    public String[] getSelectedKorvasieniDbs() {
        return korvaDbs;
    }

    @Override
    public void setKorvasieniCfg(Integer organismId, String filename) {
    	File file = new File(filename);
    	if (!file.exists())     throw new IllegalArgumentException(filename+" does not exist.");
    	if (file.isDirectory()) throw new IllegalArgumentException(filename+" is a directory.");
    	try { korvasieniCfgs.put(organismId, file.toURI().toURL());	}
    	catch (MalformedURLException e) {
			throw new IllegalArgumentException("Invalid URL: "+filename, e);
		}
    }

    /**
     * Produces a list of the Korvasieni database names and their descriptions.
     */
    @Override
    public List<Object[]> getKorvasieniDatabases() throws SQLException {
    	return korvasieni.getDatabases(false);
    }

    @Override
    public boolean setKorvasieniInUse(boolean status) {
    	if (status) {
    	    if (korvasieni == null) {
    		   URL cfg = korvasieniCfgs.get(organism);
    		   if (cfg != null) {
    		      try { korvasieni = new IDConverter(cfg.openStream(), cfg.toString()); }
    		      catch (Exception err) {
    		         err.printStackTrace();
    		      }
    		   }
    		}
    	} else if (korvasieni != null) {
  			korvasieni.close();
  			korvasieni = null;
    	}
    	return isKorvasieniInUse();
    }

    @Override
    public boolean isKorvasieniInUse() {
    	return (korvasieni != null);
    }

    @Override
	public List<Set<DatabaseId>> geneAnnotations(String ensg) throws SQLException {
		List<Set<DatabaseId>> results;
		results = korvasieni.convert(ensg,
				                     false,
				                     IDConverter.EnsemblType.Gene,
				                     IDConverter.ENSEMBL_GENE_ID,
				                     korvaDbs,
				                     new boolean[korvaDbs.length]);
		return results;
	}

	@Override
	public List<Object[]> getBioentity(String id,
			                           Object type) throws SQLException {
		String       typeS;
		List<String> keys;

		if (type instanceof String) {
			if (!isKorvasieniInUse()) {
				throw new SQLException("Korvasieni is not in use.");
			}
			typeS = type.toString();
			boolean  isId = (typeS.charAt(0)=='_');
			if (isId) typeS = typeS.substring(1);
			korvasieni.checkAvailable(typeS);
			List<Set<DatabaseId>> genes;
			genes = korvasieni.convert(id,
					                   isId,
					                   IDConverter.EnsemblType.Any,
					                   typeS,
					                   new String[] { IDConverter.ENSEMBL_GENE_ID },
					                   new boolean[korvaDbs.length]);
			if (genes == null) {
				return Collections.emptyList();
			}
			keys = new LinkedList<String>();
			for (DatabaseId dbId : genes.get(0)) {
				keys.add(dbId.getLabel());
			}
			typeS = IDConstants.XrefType_Ensembl_gene.toString();
		} else {
			if (id == null) {
				keys = null;
			} else {
				keys = new ArrayList<String>(1);
				keys.add(id);
			}
			if (type instanceof XrefType) {
				typeS = String.valueOf(((XrefType)type).getXrefTypeId());
			} else {
			    typeS = type.toString();
			}
		}

		return getBioentity(keys, typeS);
	}

	/**
	 * Describes the bioentities matching the given query identifiers.
	 *
	 * @param keys  Identitifers of interest or null for all values of the given type
	 * @param typeS Identifier type as defined by {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa}.
	 * @return      Each bioentity is represented as
	 *              {id,
	 *               {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#BioentityId},
	 *               {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#BioentityName},
	 *               {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#BioentityTypeId}}.
	 */
	protected abstract List<Object[]> getBioentity(List<String> keys,
			                                       String       typeS) throws SQLException;

}
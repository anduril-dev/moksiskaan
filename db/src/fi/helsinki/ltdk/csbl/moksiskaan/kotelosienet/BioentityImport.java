package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.moksiskaan.EntityUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.BioentityType;

/**
 * Imports a set of bioentities in the user space.
 * 
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public class BioentityImport implements ActionListener {

	private final KsPane            host;
	private final KsQueryEngine     ksEngine;
	private final BioentityListener target;

	public BioentityImport(KsPane            guiParent,
	                        KsQueryEngine     engine,
			                BioentityListener sink) {
		host     = guiParent;
		ksEngine = engine;
		target   = sink;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
        guiImport();
	}

	public void guiImport() {
        final JFileChooser fc     = new JFileChooser();
		final JPanel       paramP = new JPanel(new BorderLayout());
        final JComboBox    typeCB = new JComboBox(host.getIDTypes());

		paramP.setBorder(new TitledBorder("Import as:"));
		paramP.add(typeCB, BorderLayout.NORTH);
		fc.setAccessory(paramP);

		int returnVal = fc.showOpenDialog(SwingUtilities.getRoot(host));
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			Object type = typeCB.getSelectedItem(); if (type == null) return;
			File   file = fc.getSelectedFile();     if (file == null) return;
			csvImport(file, type, null);
		}
	}

	public void csvImport(String filename, String colName, String type) {
		Object addr;
		if ((filename.indexOf(':')<0) || new File(filename).exists()) {
			addr = new File(filename);
		} else {
		    addr = filename;
		}
		boolean isNumeric = true;
		for (int i=0; i<type.length(); i++) {
			isNumeric = Character.isDigit(type.charAt(i));
			if (!isNumeric) break;
		}
		Object tObj;
		if (isNumeric) {
		    tObj = Integer.valueOf(type);
		} else {
		    tObj = type;
		    for (PseudoXref xr : PseudoXref.values()) {
		        if (xr.name().equals(type)) {
		            tObj = xr;
		            break;
		        }
		    }
		}
		csvImport(addr, tObj, colName);
	}

	private void csvImport(Object addr, Object type, String colNameIn) {
		try {
		  CSVParser in;
		  if (addr instanceof File) {
             in = new CSVParser((File)addr);
		  } else {
		     in = new CSVParser(new InputStreamReader(new URL((String)addr).openStream()),
				                CSVParser.DEFAULT_DELIMITER,            0,
				                CSVParser.DEFAULT_MISSING_VALUE_SYMBOL, true);
		  }
		  int       col      = 0;
		  String[]  colNames = in.getColumnNames();

		  if (colNames.length > 1) {
			  String colName;
		      if (colNameIn == null) {
			      colName = (String)JOptionPane.showInputDialog(
					                   host,
			                           "Select ID column", "CSV Import",
			                           JOptionPane.QUESTION_MESSAGE,
			                           null,
			                           colNames, colNames[0]);
			      if (colName == null) return;
			  } else {
				  colName = colNameIn;
			  }
              col = AsserUtil.indexOf(colName, colNames, true, true);
		  } else if (colNames.length < 1) return;

		  StringBuffer message = new StringBuffer(1024);
		  message.append(type)
		         .append(" import from ")
		         .append(addr)
		         .append('.');

		  Set<Bioentity>             findings    = new TreeSet<Bioentity>(EntityUtil.BIOENTITY_COMPARATOR);
		  Map<Integer,BioentityType> entityTypes = host.getBioentityTypes();
		  for (String[] line : in) {
			  List<Object[]> entities = ksEngine.getBioentity(line[col], type);
			  if (entities.isEmpty()) {
				  message.append("<br/>Unknown ID: ").append(line[col]);
			  } else
			  for (Object[] def : entities) {
				  Bioentity be = new Bioentity();
                  be.setBioentityId((Long)def[1]);
                  be.setName((String)def[2]);
                  be.setBioentityType(entityTypes.get((Integer)def[3]));
				  findings.add(be);
			  }
		  }
		  in.close();

		  sendFindings(findings);
		  host.showMessage(message.toString());
		}
		catch (Exception err) {
			host.showError("Bioentity import failed!", err);
		}
	}

	private void sendFindings(Set<Bioentity> entities) {
		for (Bioentity entity : entities) {
			target.bioentitySelected(entity);
		}
	}

}
package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableRowSorter;

import fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.BioentityType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkAnnotation;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;

/**
 * This table model is used for the up and down stream neighbor links.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08 
 */
public class NeighborModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	private Map<Integer,BioentityType> entityTypes;
	private Map<Integer,LinkType>      linkTypes;

	private final String[] columns = {"name", "type", "link"};
    private List<Object[]> data    = null;

    public void setEntityTypes(Map<Integer,BioentityType> typeMap) {
    	entityTypes = typeMap;
    }

    public void setLinkTypes(Map<Integer,LinkType> typeMap) {
    	linkTypes = typeMap;
    }

	@Override
	public int getColumnCount() {
		return columns.length; 
	}

	@Override
	public int getRowCount() {
		return (data==null) ? 0 : data.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (data == null) return null;

	    switch (columnIndex) {
	       case  0: return data.get(rowIndex)[4];
	       case  1: return entityTypes.get(data.get(rowIndex)[5]);
	       case  2: return linkTypes.get(data.get(rowIndex)[1]);
	       default: throw new IndexOutOfBoundsException("Invalid column: "+columnIndex);
	    } 
	}

    @Override
    public String getColumnName(int column) {
        return columns[column];
    }

    String getId(int row) {
    	if (row < 0) return null;
    	return data.get(row)[3].toString();
    }

    Double getWeight(int row) {
    	if (row < 0) return null;
    	return (Double)data.get(row)[2];
    }

    void setData(List<Object[]> data) {
        this.data = data;
        fireTableDataChanged();
    }

    /**
     * Creates a table for the model.
     *
     * @param title  Visual title of the table.
     * @param view   Host for the list
     * @param direct True if this list represents downstream targets
     * @return       The new component for the GUI
     */
    public JComponent prepareNeighbourPane(String        title,
    		                               final KsPane  view,
    		                               final boolean direct) {
    	final JTable                   table  = new JTable();
    	final RowSorter<NeighborModel> sorter = new TableRowSorter<NeighborModel>(this);

    	table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    	table.setPreferredScrollableViewportSize(new Dimension(200,200));
    	table.setRowSorter(sorter);
    	table.setModel(this);
    	table.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent ev) {
				int cc = ev.getClickCount();
				int b  = ev.getButton();

				if ((cc == 2) && (b == MouseEvent.BUTTON1)) {
					Point p = ev.getPoint();
					int   r = table.rowAtPoint(p);
					if (r < 0) return;
					String id = getId(sorter.convertRowIndexToModel(r));
					view.fireBioentitySearch(id, Piispanhiippa.PseudoXref.BioentityId);
				} else
				if ((cc == 1) && (b != MouseEvent.BUTTON1)) {
					Point p = ev.getPoint();
					int   r = table.rowAtPoint(p);
					if (r < 0) return;
                    table.getSelectionModel().setSelectionInterval(r, r);
					int mr = sorter.convertRowIndexToModel(r);
					LinkAnnotation[] annot = view.getLinkDetails(   (Long)data.get(mr)[3],
					                                             (Integer)data.get(mr)[1],
					                                             direct);
					StringBuffer message = new StringBuffer(1024);
					message.append(getValueAt(mr, 2)).append(" link to ").append(getValueAt(mr, 0))
					       .append("\nweight: ").append(getWeight(mr));
					if (annot.length < 1) {
						message.append("\n[no optional annotations]");
					} else
					for (LinkAnnotation a : annot) {
					    message.append('\n')
					           .append(a.getName()).append(" = ").append(a.getValue());
					}
					JOptionPane.showMessageDialog(view,
					                              message.toString(),
					                              "Link annotations",
					                              JOptionPane.INFORMATION_MESSAGE);
				}
			}
			@Override
			public void mouseEntered(MouseEvent ev)  { /* No action */ }
			@Override
			public void mouseExited(MouseEvent ev)   { /* No action */ }
			@Override
			public void mousePressed(MouseEvent ev)  { /* No action */ }
			@Override
			public void mouseReleased(MouseEvent ev) { /* No action */ }
		});
    	table.getColumn("link").setCellRenderer(new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 1L;
			@Override
    		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    			Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    			((JLabel)comp).setToolTipText("weight: "+getWeight(row));
    			return comp;
    		}
    	});

    	JScrollPane pane = new JScrollPane(table);
    	pane.setBorder(BorderFactory.createTitledBorder(title));
    	return pane;
    }

}
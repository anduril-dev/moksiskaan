package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.helsinki.ltdk.csbl.asser.annotation.DatabaseId;
import fi.helsinki.ltdk.csbl.asser.annotation.OntologyTerm;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.*;

/**
 * Database interface for the classes of Kotelosienet.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public interface KsQueryEngine extends AnnotationSelectionListener {

	/** Maximum number of items fetched from the database. */
    public static final int MAX_RESULT_SIZE = 10000;

	/**
	 * This method is supposed to be called before the program terminates.
	 * Releases the local resources.
	 */
	public void close();

	/**
	 * Defines the file representing Gene Ontology.
	 *
	 * @param oboFile File name pointing to the OBO-file
	 */
	public void setGO(String oboFile);

	/**
	 * Returns a map that represents Gene Ontology terms found from the
	 * file defined by {@link #setGO(String)}.
	 *
	 * @return Keys are GO term identifiers
	 * @throws IOException If the file cannot be read properly
	 */
	public Map<String, OntologyTerm> getGO() throws IOException;

    /**
     * Produces a list of organisms available in Moksiskaan database.
     */
    public List<Organism> getOrganisms() throws SQLException;

    /**
     * Selects the target organism for the subsequent operations.
     * 
     * @param organism Organism to work with
     * @return Tells if Korvasieni will be used for this organism.
     */
    public boolean setOrganism(Organism organism) throws SQLException, IOException;

    /**
     * Provides the database history.
     */
    public Log[] getLogEntities() throws SQLException;

    /**
     * Defines the organism specific Ensembl database connection parameters.
     *
     * @param organismId Moksiskaan identifier for the associated organism
     * @param filename   Database configuration for the Ensembl core installation
     * @see   fi.helsinki.ltdk.csbl.asser.db.JDBCConnection
     */
    public void setKorvasieniCfg(Integer organismId, String filename);

    /**
     * Includes the given link type to the pool of types that are used to fetch
     * neighbors.
     *
     * @see #getNeighbors(Long, boolean)
     */
    public void addLinkType(Integer id);

    /**
     * Excludes the given link type from the pool of types that are used to fetch
     * neighbors.
     *
     * @see #getNeighbors(Long, boolean)
     */
    public void removeLinkType(Integer id);

    /**
     * Produces a list of Korvasieni database names and their descriptions.
     */
    public List<Object[]> getKorvasieniDatabases() throws SQLException;

    /**
     * Defines the set of Korvasieni databases that shall be used.
     *
     * @see #getSelectedKorvasieniDbs()
     */
    @Override
    public void dataSourcesSelected(String[] dbs);

    /** Determines whether the Korvasieni related functionality is in use. */
    public boolean setKorvasieniInUse(boolean status);

    /** Tells if the Korvasieni related functionality is in use. */
    public boolean isKorvasieniInUse();

    /**
     * Provides a list of Korvasieni databases that shall be used.
     */
    public String[] getSelectedKorvasieniDbs();

    /**
     * Provides the Korvasieni annotations for the gene with the given stable ID.
     * The results depend on the databases that are in use.
     *
     * @see #getSelectedKorvasieniDbs()
     */
	public List<Set<DatabaseId>> geneAnnotations(String ensg) throws SQLException;

	/** Provides the chromosomal position of the bioentity. */
	public DNARegion getRegion(long bioentityId) throws SQLException;

	/**
	 * Provides the bioentities within the given chromosomal region.
	 * The entities are ordered by their start and end positions.
	 */
	public DNARegion[] getRegions(String chromosome, long start, long stop) throws SQLException;

	/**
	 * Provides the next bioentity located after the given position.
	 *
	 * @param  previous True if the next region is up stream on the forward strand and
	 *                  false for the down stream lookup.
	 * @return Null if the given locus was the last one. 
	 */
	public DNARegion getNextRegion(DNARegion locus, boolean previous) throws SQLException;

	/**
	 * Provides all bioentity types available.
	 *
	 * @return BioentityType identifiers are used as keys for the actual type definitions. 
	 */
	public Map<Integer,BioentityType> getBioentityTypes() throws SQLException;

	/**
	 * Provides a list of all link types available.
	 */
	public List<LinkType> getLinkTypes() throws SQLException;

	/**
	 * Provides the number of links of various types for the selected organism.
	 * Each list entity consists of a count integer and a string name.
	 */
	public List<Object[]> getLinkTypeStatistics() throws SQLException;

	/**
	 * Provides the number of bioentities of various types for the selected organism.
	 * Each list entity consists of a count integer and a string name.
	 */
	public List<Object[]> getBioentityTypeStatistics() throws SQLException;

	/**
	 * Provides the number of external references of various types for the selected organism.
	 * Each list entity consists of a count integer and a string name.
	 */
	public List<Object[]> getXrefTypeStatistics() throws SQLException;

    /**
     * Provides the number of DNARegions on each chromosome for the selected organism.
     * Each list entity consists of a count integer and a string name.
     */
    public List<Object[]> getChromosomeStatistics() throws SQLException;

	/**
	 * Provides a list of external references for the given bioentity.
	 * 
	 * @param  id Bioentity identifier
	 * @return Each reference is represented as
	 *         {XrefType name, value, XrefType identifier}.
	 */
	public List<Object[]> getXrefs(Long id) throws SQLException;

	/**
	 * Provides a list of all external reference types available.
	 */
	public List<XrefType> getXrefs() throws SQLException;

	/** Provides a study descriptor for the given study identifier. */
	public Study getStudy(Integer id) throws SQLException;

	/** Provides all study identifiers available for the active organism. */
	public Integer[] getStudyIdentifiers() throws SQLException;

	/** Returns an URL pointing to the main page of the user guide. */
	public URL getUserGuideAddress();

	/**
	 * Describes the bioentities matching the given query identifier.
	 *
	 * @param id   Value of the identifier
	 * @param type Identifier type:
	 *             ({@link java.lang.String}
	 *                     for Korvasieni databases,
	 *              {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType}
	 *                     for the external Moksiskaan references,
	 *              {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref}
	 *                     for the internal Moksiskaan references).
	 * @return      Each bioentity is represented as
	 *              {id,
	 *               {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#BioentityId},
	 *               {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#BioentityName},
	 *               {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#BioentityTypeId}}.
	 */
	public List<Object[]> getBioentity(String id, Object type) throws SQLException;

	/**
	 * Produces a list of studies with at least one hit for the given bioentity.
	 *
	 * @param bioentityId Identifier of the target bioentity
	 * @return            Each study is represented as 
	 *                    {bioentityId,
	 *                     {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#HitStudyId},
	 *                     {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#HitStudyName},
	 *                     {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#HitWeight},
	 *                     {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#HitEvidence}}.
	 */
	public List<Object[]> getStudies(Long bioentityId) throws SQLException;

	/**
	 * Produces a list of bioentities linked to the given bioentity.
	 *
	 * @param id   Identifier of the source entity
	 * @param down True for the down stream neighbors and false for the up stream
	 * @return     Each bioentity is described as
	 *             {id, linkTypeId, weight,
	 *              {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#BioentityId},
	 *              {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#BioentityName},
	 *              {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref#BioentityTypeId}}.
	 */
	public List<Object[]> getNeighbors(Long    id,
                                        boolean down) throws SQLException;

	/**
	 * Provides a list of optional annotations associated with the link
	 * between two bioentities.
	 *
	 * @param source Bioentity identifier of the link source
	 * @param target Bioentity identifier of the link target
	 * @param type   Link type identifier that determines the kind of the link
	 */
	public LinkAnnotation[] getLinkAnnotations(Long source, Long target, Integer type) throws SQLException;

	/**
	 * Produces a message dialog string that describes the program and the installation.
	 */
	public String getDescription();

}
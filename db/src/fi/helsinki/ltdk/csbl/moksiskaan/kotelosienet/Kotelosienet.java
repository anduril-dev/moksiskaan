package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.swing.*;

import fi.helsinki.ltdk.csbl.asser.MainArgument;
import fi.helsinki.ltdk.csbl.moksiskaan.Version;
import fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.*;

/**
 * Top-level launcher of Kotelosienet.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public class Kotelosienet {

	static final String ERROR_UNINITIALIZED = "An uninitialized GUI.";

	private final JCheckBoxMenuItem miShowRegions = new JCheckBoxMenuItem("Show regions", false);

	private KsPane ksPane;
	private BioentityBasket basket;
	private KorvasieniDbDialog korvaDbs;

	public void init(final KsQueryEngine engine) throws SQLException, IOException  {
		final JFrame         frame      = new JFrame("Kotelosienet");
		URL                  iconURL    = Kotelosienet.class.getResource("/fi/helsinki/ltdk/csbl/moksiskaan/logo.png");
		final ImageIcon      icon       = new ImageIcon(iconURL);
		final RegionPane     regionPane = new RegionPane(engine);
		final List<XrefType> xTypes     = engine.getXrefs();

        korvaDbs = new KorvasieniDbDialog(engine);
        korvaDbs.addAnnotationSelectionListener(engine);

		basket = new BioentityBasket();

		JMenuBar menuBar    = new JMenuBar();
		JMenu    menuFile   = new JMenu("File");
		JMenu    menuView   = new JMenu("View");
		JMenu    menuSearch = new JMenu("Search");
		JMenu    menuHelp   = new JMenu("Help");

		final JMenuItem miImportBE = new JMenuItem("Import bioentities");
		menuFile.add(miImportBE);
		final JMenuItem miExportBE = new JMenuItem("Export bioentities");
		menuFile.add(miExportBE);
		menuFile.addSeparator();
		final JCheckBoxMenuItem miAutoPick = new JCheckBoxMenuItem("Pick selected",  true);
		final JCheckBoxMenuItem miUseKorva = new JCheckBoxMenuItem("Use Korvasieni", false);
		JMenu       menuLaF     = new JMenu("Look and Feel");
		ButtonGroup lafSelector = new ButtonGroup();
		String      currentLaF  = UIManager.getLookAndFeel().getClass().getName();
		for (final UIManager.LookAndFeelInfo laf : UIManager.getInstalledLookAndFeels()) {
			final JMenuItem miLaF;
			miLaF = new JRadioButtonMenuItem(laf.getName(), currentLaF.equals(laf.getClassName()));
			miLaF.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (miLaF.isSelected()) {
						try {
							UIManager.setLookAndFeel(laf.getClassName());
							SwingUtilities.updateComponentTreeUI(frame);
						}
						catch (Exception err) {
							ksPane.showError("Look and Feel switching failed.", err);
						}
					}
				}
			});
			lafSelector.add(miLaF);
			menuLaF.add(miLaF);
		}
		menuFile.add(miAutoPick);
		menuFile.add(miUseKorva);
		menuFile.add(menuLaF);
		menuFile.addSeparator();
		JMenuItem miPrint = new JMenuItem("Print");
		menuFile.add(miPrint);
		menuFile.addSeparator();
		JMenuItem miExit = new JMenuItem("Exit");
		miExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				engine.close();
				frame.dispose();
			}
		});
		menuFile.add(miExit);
		menuFile.setMnemonic(KeyEvent.VK_F);

		final JMenuItem miKorvasieni = new JMenuItem("Korvasieni databases");
		miKorvasieni.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				try {
					korvaDbs.select(frame);
				} catch (SQLException e) {
					ksPane.showError("Cannot select Korvasieni databases.", e);
				}
			}
		});
		menuView.add(miKorvasieni);
		JMenu menuLinkTypes = new JMenu("Link types");
		CompleteSelection ltAllSelector  = new CompleteSelection(true);
		CompleteSelection ltNoneSelector = new CompleteSelection(false);
		JMenuItem miLTAll = new JMenuItem("Select all");
		miLTAll.addActionListener(ltAllSelector);
		menuLinkTypes.add(miLTAll);
		JMenuItem miLTNone = new JMenuItem("Deselect all");
		miLTNone.addActionListener(ltNoneSelector);
		menuLinkTypes.add(miLTNone);
		menuLinkTypes.addSeparator();
		for (final LinkType lt : engine.getLinkTypes()) {
			final JMenuItem miLinkType = new JCheckBoxMenuItem(lt.getName(), true);
			miLinkType.setToolTipText(lt.getDescription());
			miLinkType.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (miLinkType.isSelected()) {
						engine.addLinkType(lt.getLinkTypeId());
					} else {
						engine.removeLinkType(lt.getLinkTypeId());
					}
				}
			});
			ltAllSelector.add(miLinkType);
			ltNoneSelector.add(miLinkType);
			menuLinkTypes.add(miLinkType);
			engine.addLinkType(lt.getLinkTypeId());
		}
		menuView.add(menuLinkTypes);
		menuView.addSeparator();
		JCheckBoxMenuItem miShowStudies = new JCheckBoxMenuItem("Show studies", true);
		menuView.add(miShowStudies);
		miShowRegions.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
               if (miShowRegions.isSelected()) {
                  ksPane.addLocusListener(regionPane);
                  regionPane.setVisible(true);
               } else {
                  ksPane.removeLocusListener(regionPane);
                  regionPane.setVisible(false);
               }
			}
		});
		menuView.add(miShowRegions);
		menuView.addSeparator();
		JMenu menuOrganisms = new JMenu("Organism");
		ButtonGroup organismSelector = new ButtonGroup();
		for (final Organism o : engine.getOrganisms()) {
			final JMenuItem miOrganism;
			miOrganism = new JRadioButtonMenuItem(o.getName(),
					                              IDConstants.Organism_Homo_sapiens.equals(o.getOrganismId()));
			miOrganism.setToolTipText("ID="+o.getOrganismId());
			miOrganism.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (miOrganism.isSelected()) {
						boolean useKorvasieni;
						try { useKorvasieni = engine.setOrganism(o); }
						catch (Exception err) {
							ksPane.showError("Could not select "+o.getName()+'.', err);
							return;
						}
						miUseKorva.setSelected(useKorvasieni);
						miKorvasieni.setEnabled(useKorvasieni);
						basket.clear();
						regionPane.setRegion(null);
						ksPane.showMessage("Organism changed to <i>"+o.getName()+"</i>.");
					}
				}
			});
			if (miOrganism.isSelected()) {
				engine.setOrganism(o);
				engine.setKorvasieniInUse(true);
			}
			organismSelector.add(miOrganism);
			menuOrganisms.add(miOrganism);
		}
		menuView.add(menuOrganisms);
		menuView.setMnemonic(KeyEvent.VK_V);

		final JMenuItem miAllStudies = new JMenuItem("All studies");
		miAllStudies.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
			   try { ksPane.showStudies(engine.getStudyIdentifiers()); }
			   catch (Exception err) {
                  ksPane.showError("Could not fetch studies from the database.", err);
               }
			}
		});
		menuSearch.add(miAllStudies);
		JMenu menuBTs = new JMenu("All (bioentity type)");
		menuSearch.add(menuBTs);
		JMenu menuXTs = new JMenu("All (external reference)");
		menuSearch.add(menuXTs);

		JMenuItem miManual = new JMenuItem("User Guide");
		miManual.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
			   ksPane.showPage(engine.getUserGuideAddress());
			}
		});
		menuHelp.add(miManual);
		JMenuItem miLog = new JMenuItem("Database Log");
		miLog.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
			   final Format timeFormat = new SimpleDateFormat("'['yyyy-MM-dd']'", Locale.US);
		       Log[]        log;
		       try { log = engine.getLogEntities(); } 
		       catch (Exception err) {
					ksPane.showError("Could not access the database log.", err);
					return;
               }
		       StringBuffer page = new StringBuffer(2048);
		       page.append("<h1>Database history</h1>\n");
		       for (Log item : log) {
		    	   page.append(timeFormat.format(item.getTime()))
		    	       .append(" &nbsp; <b><i>")
		    	       .append(item.getType())
		    	       .append("</i></b><pre>")
		    	       .append(item.getDescription())
		    	       .append("</pre><br>\n");
		       }
			   ksPane.showMessage(page.toString());
			}
		});
		menuHelp.add(miLog);
		JMenuItem miStats = new JMenuItem("Statistics");
		miStats.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
			   String page;
			   try { page = prepareStatisticsPage(engine); }
			   catch (Exception err) {
				   ksPane.showError("Could not access the database statistics.", err);
				   return;
			   }
			   ksPane.showMessage(page);
			}
		});
		menuHelp.add(miStats);
		JMenuItem miAbout = new JMenuItem("About");
		miAbout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				Image image = icon.getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH);
				JOptionPane.showMessageDialog(
                   frame,
                   engine.getDescription(),
                   "About Kotelosienet",
                   JOptionPane.PLAIN_MESSAGE,
                   new ImageIcon(image));
			}
		});
		menuHelp.add(miAbout);
		menuHelp.setMnemonic(KeyEvent.VK_H);

		menuBar.add(menuFile);
		menuBar.add(menuView);
		menuBar.add(menuSearch);
		menuBar.add(Box.createHorizontalGlue());
		menuBar.add(menuHelp);

		ksPane = new KsPane();
		ksPane.init(engine, xTypes);
		ksPane.setAutopick(miAutoPick);
		ksPane.setShowStudies(miShowStudies);
		ksPane.addPickListener(basket);
		korvaDbs.addAnnotationSelectionListener(ksPane);
		basket.addBioentityListener(ksPane);
		regionPane.setKsPane(ksPane);
		miImportBE.addActionListener(new BioentityImport(ksPane, engine, basket));
		miExportBE.addActionListener(new BioentityExport(basket, ksPane));
		miPrint.addActionListener(ksPane.getPrinter());

		miKorvasieni.setEnabled(engine.isKorvasieniInUse());
		miUseKorva.setSelected(engine.isKorvasieniInUse());
		miUseKorva.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				boolean status = miUseKorva.isSelected();
				status = engine.setKorvasieniInUse(status);
				miKorvasieni.setEnabled(status);
				miUseKorva.setSelected(status);
			}
		});

		ActionListener btListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				ksPane.fireBioentitySearch(ev.getActionCommand(),
						                   Piispanhiippa.PseudoXref.BioentityTypeName);
			}
		};
		for (BioentityType bt : ksPane.getBioentityTypes().values()) {
			JMenuItem miBT = new JMenuItem(bt.getName());
			miBT.addActionListener(btListener);
			menuBTs.add(miBT);
		}

		ActionListener xtListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				String name = ev.getActionCommand();
				for (XrefType xt : xTypes) {
					if (name.equals(xt.getName())) {
						ksPane.fireBioentitySearch(null, xt);
						return;
					}
				}
				ksPane.showMessage("Unknown external reference type: "+name);
			}
		};
		for (XrefType xt : xTypes) {
			JMenuItem miXT = new JMenuItem(xt.getName());
			miXT.addActionListener(xtListener);
			menuXTs.add(miXT);
		}

		regionPane.setVisible(false);
		JSplitPane mainSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				                              basket,
		                                      ksPane);
		mainSplit.setDividerLocation(basket.getMinimumSize().width);
		frame.setIconImage(icon.getImage());
		frame.setJMenuBar(menuBar);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().add(mainSplit,  BorderLayout.CENTER);
		frame.getContentPane().add(regionPane, BorderLayout.SOUTH);
		frame.pack();
		frame.setVisible(true);
		ksPane.requestFocusInWindow();
	}

	static private String prepareStatisticsPage(KsQueryEngine engine) throws SQLException {
	   List<Object[]> linkTypeStats      = engine.getLinkTypeStatistics();
	   List<Object[]> bioentityTypeStats = engine.getBioentityTypeStatistics();
	   List<Object[]> xrefTypeStats      = engine.getXrefTypeStatistics();
	   List<Object[]> chromosomeStats    = engine.getChromosomeStatistics();

	   StringBuffer page = new StringBuffer(10000);
	   page.append("<h1>Database statistics</h1>\n<h2>Bioentity types:</h2>\n");
	   printStatistics(page, bioentityTypeStats);
	   page.append("<h2>External references:</h2>\n");
	   printStatistics(page, xrefTypeStats);
	   page.append("<h2>Link types:</h2>\n");
	   printStatistics(page, linkTypeStats);
       page.append("<h2>Chromosome regions:</h2>\n");
       printStatistics(page, chromosomeStats);
       return page.toString();
	}

    static private void printStatistics(StringBuffer output, List<Object[]> data) {
       output.append("<table><tr><th>count</th><th>name of the type</th></tr>\n");
       for (Object[] entry : data) {
		   output.append("<tr><td align=\"right\">")
		         .append(entry[0]).append("</td><td>")
		         .append(entry[1]).append("</td></tr>\n");
	   }
	   output.append("</table>");
    }

    /**
     * Show or hide the region browser pane.
     */
    public void setRegionPaneVisibility(boolean show) {
        if (ksPane == null) throw new IllegalStateException(ERROR_UNINITIALIZED);
        if (miShowRegions.isSelected() != show)
           miShowRegions.doClick();
    }

	/**
	 * Provides a GUI handle for the host frame.
	 */
	public JFrame getUIFrame() {
		if (ksPane == null) throw new IllegalStateException(ERROR_UNINITIALIZED);
		return (JFrame)SwingUtilities.getWindowAncestor(ksPane);
	}

	/**
	 * Selects a new bioentity for the viewer.
	 *
	 * @param id   Value of the identifier
	 * @param type Identifier type:
	 *             ({@link java.lang.String}
	 *                     for Korvasieni databases,
	 *              {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.XrefType}
	 *                     for the external Moksiskaan references,
	 *              {@link fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa.PseudoXref}
	 *                     for the internal Moksiskaan references).
	 * @return The bioentity selection or null if the result set was not unique.
	 */
	public Bioentity fireBioentitySearch(String id, Object type) {
		if (ksPane == null) throw new IllegalStateException(ERROR_UNINITIALIZED);
		return ksPane.fireBioentitySearch(id, type);
	}

	/**
	 * Registers a listener for the selections of chromosomal regions.
	 */
	public void addLocusListener(LocusListener listener) {
		if (ksPane == null) throw new IllegalStateException(ERROR_UNINITIALIZED);
		ksPane.addLocusListener(listener);
	}

	/**
	 * Removes an already registered listener of the chromosomal regions.
	 */
	public void removeLocusListener(LocusListener listener) {
		if (ksPane == null) throw new IllegalStateException(ERROR_UNINITIALIZED);
		ksPane.removeLocusListener(listener);
	}

	/**
	 * Displays an error message to the user.
	 *
	 * @param message An HTML fragment that described the error
	 * @param err     The associated stack trace if available
	 */
	public void showError(String    message,
			              Throwable err) {
		if (ksPane == null) {
			System.err.println("ERROR: "+message);
			if (err != null) err.printStackTrace();
		} else {
			ksPane.showError(message, err);
		}
	}

	static public void main(String[] argv) {
		final String DB_OPT_PREFIX = "d";

        Map<String, MainArgument> args = new HashMap<String, MainArgument>();
        MainArgument argShowHelp = MainArgument.add("help",       "Print this usage information", args);
        MainArgument argShowRegs = MainArgument.add("b",          "Show region browser", args);
        MainArgument argRemote   = MainArgument.add("r",  "url",  "Use a remote database engine", args);
        MainArgument argFileGO   = MainArgument.add("go", "file", "Name/URL of the GO database file (*.obo)", args);
        MainArgument argImport   = MainArgument.add("i", new String[]{"file","c","t"}, "Import file/URL, column name, and the type", args);
        String[] argLeft;
        try { argLeft = MainArgument.parseInput(argv, args, 0, -1, true); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
			System.exit(1);
			return; // Never called!
        }
		final KsQueryEngine engine;
		try {
			if (argRemote.isUsed()) {
				engine = new RemoteQueryEngine(new URL(argRemote.getValue()));
			} else {
			    engine = new SimpleQueryEngine();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			System.exit(2);
			return; // Never called!
		}
        try {
           for (Organism species : engine.getOrganisms()) {
         	   MainArgument.add(DB_OPT_PREFIX+species.getOrganismId(),
         			            "file",
         			            "Database properties for "+species.getName(),
         			            args);
           }
        } catch (SQLException e) {
			e.printStackTrace();
			System.exit(3);
			return; // Never called!
        }
        try { argLeft = MainArgument.parseInput(argLeft, args, 0, -1); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
	        System.err.println();
	        printSynopsis(args);
			System.exit(1);
			return; // Never called!
        }
        if (argShowHelp.isUsed()) {
        	printSynopsis(args);
        	return;
        }

        for (String argName : args.keySet()) {
        	if (argName.startsWith(DB_OPT_PREFIX)) {
        		MainArgument arg = args.get(argName);
        		if (arg.isUsed()) {
        		   Integer orgId = Integer.valueOf(argName.substring(DB_OPT_PREFIX.length()));
        		   engine.setKorvasieniCfg(orgId, arg.getValue());
        		}
        	}
        }
		if (argFileGO.isUsed()) {
			engine.setGO(argFileGO.getValue());
		}

		if (argLeft.length > 0) {
			engine.dataSourcesSelected(argLeft);
		}

		final String[] importRules  = argImport.getValues();
		final boolean  showRBrowser = argShowRegs.isUsed();
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	Kotelosienet app = new Kotelosienet();
        		try { app.init(engine); }
        		catch (Exception e) {
        			e.printStackTrace();
        			System.exit(4);
        		}
        		if (importRules != null) {
        			BioentityImport reader = new BioentityImport(app.ksPane, engine, app.basket);
        			reader.csvImport(importRules[0],importRules[1],importRules[2]);
        		}
        		if (showRBrowser) app.setRegionPaneVisibility(true);
            }
        });
	}

    static private void printSynopsis(Map<String, MainArgument> args) {
        System.err.println("Kotelosienet v."+Version.getVersion()+" by Marko Laakso");
        System.err.println("-----------------------------------");
        System.err.println("kotelosienet [options] [korvasieni DBs]");
        MainArgument.printArguments(args, System.err);
    }

}
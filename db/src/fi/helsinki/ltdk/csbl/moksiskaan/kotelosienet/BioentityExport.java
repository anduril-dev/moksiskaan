package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;

import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.moksiskaan.query.Piispanhiippa;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;

/**
 * Export the content of the bioentity basket to a file.
 * 
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.14
 */
public class BioentityExport implements ActionListener {

	private final BioentityBasket basket;
	private final KsPane          host;

	public BioentityExport(BioentityBasket basket,
			                KsPane          host) {
		this.basket = basket;
		this.host   = host;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
        guiExport();
	}

	public void guiExport() {
        final JFileChooser fc = new JFileChooser();

		int returnVal = fc.showSaveDialog(SwingUtilities.getRoot(basket));
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file  = fc.getSelectedFile();
			if (file != null) csvExport(file);
		}
	}

	public void csvExport(File file) {
		List<String> colNames = new ArrayList<String>();
        colNames.add(Piispanhiippa.PseudoXref.BioentityName.name());
        colNames.add(Piispanhiippa.PseudoXref.BioentityId.name());
        colNames.add(Piispanhiippa.PseudoXref.BioentityTypeId.name());

        try {
		  CSVWriter out = new CSVWriter(colNames.toArray(new String[colNames.size()]), file);
		  Enumeration<Bioentity> entities = basket.elements();
		  while (entities.hasMoreElements()) {
			  Bioentity entity = entities.nextElement();
			  out.write(entity.getName());
			  out.write(entity.getBioentityId());
			  out.write(entity.getBioentityType().getBioentityTypeId());
		  }
		  out.close();
        }
		catch (Exception err) {
			host.showError("Bioentity import failed!", err);
		}
	}

}

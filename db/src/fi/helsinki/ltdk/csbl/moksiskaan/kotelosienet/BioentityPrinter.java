package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;

import javax.swing.JEditorPane;

/**
 * Printer support for {@link fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet.KsPane}.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.11
 */
public class BioentityPrinter implements ActionListener {
	
	private final KsPane      view;
	private final JEditorPane document;
	
	public BioentityPrinter(KsPane viewer, JEditorPane doc) {
		view     = viewer;
		document = doc;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
        try { document.print(); }
        catch (PrinterException err) {
           view.showError("Could not print!", err);
        }
	}

}
package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;

/**
 * Event interface for the bioentity selections.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public interface BioentityListener {

	/**
	 * The given entity has been selected.
	 */
	public void bioentitySelected(Bioentity entity);

	/**
	 * All bioentity selections can be removed.
	 */
	public void unselectBioentities();

}
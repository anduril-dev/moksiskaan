package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

/**
 * Common interface for objects responding to annotation source modifications.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public interface AnnotationSelectionListener {

	/**
	 * Update the set of selected databases.
	 * @param ids database identifiers
	 */
	public void dataSourcesSelected(String[] ids);

}

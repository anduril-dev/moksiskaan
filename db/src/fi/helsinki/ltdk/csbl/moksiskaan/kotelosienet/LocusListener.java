package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import fi.helsinki.ltdk.csbl.moksiskaan.schema.DNARegion;

/**
 * Event interface for the selections of chromosome regions.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.11
 */
public interface LocusListener {

	/** The given location has been selected. */
	public void setRegion(DNARegion locus);

}
package fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.AbstractButton;

/**
 * Represents a list of selectable components like check boxes that can be chosen at once.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link fi.helsinki.ltdk.csbl.moksiskaan.Version Version} 1.08
 */
public class CompleteSelection extends LinkedList<Component> implements ActionListener {

	private static final long serialVersionUID = 1L;

	private final boolean baseValue;

	public CompleteSelection(boolean response) {
		baseValue  = response;
	}

	/**
	 * Set all components to the given state.
	 */
	public void setSelection(boolean state) {
		for (Component c : this) {
			if (c instanceof AbstractButton) {
				AbstractButton s = (AbstractButton)c;
				if (s.isSelected() != state) {
				   s.doClick();
				}
			}
		}

	}

	@Override
	public void actionPerformed(ActionEvent event) {
		setSelection(baseValue);
	}

}
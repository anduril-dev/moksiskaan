package fi.helsinki.ltdk.csbl.moksiskaan;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IdentifierType;
import org.hibernate.type.Type;

import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Bioentity;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.IDConstants;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Link;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkAnnotation;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.LinkType;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.Log;
import fi.helsinki.ltdk.csbl.moksiskaan.schema.MoksiskaanEntity;

/**
 * Hibernate connectivity class.
 */
public final class HibernateUtil {

  /** No instantiation */
  private HibernateUtil() {};

  /** A singleton for the session factory */
  private static final SessionFactory SESSION_FACTORY;

  private static final Configuration CONFIG;

  static {
      try {
          // Create the SessionFactory from hibernate.cfg.xml
          StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                                             .build();

          CONFIG          = new Configuration().configure();
          SESSION_FACTORY = CONFIG.buildSessionFactory(registry);
      } catch (Throwable ex) {
          // Make sure you log the exception, as it might be swallowed
          System.err.println("Initial SessionFactory creation failed." + ex);
          throw new ExceptionInInitializerError(ex);
      }
  }

  /** Returns the current Hibernate settings. */
  static public Configuration getConfiguration() {
      return CONFIG;
  }

  /** Obtain the session factory that can be used for the database connectivity. */
  static public SessionFactory getSessionFactory() {
      return SESSION_FACTORY;
  }

  static public List<? extends MoksiskaanEntity> selectAll(Class<? extends MoksiskaanEntity> entity) {
      return selectThese("from "+entity.getSimpleName());
  }

  @SuppressWarnings("unchecked")
  static public List<? extends MoksiskaanEntity> selectThese(String hsql) {
      Session                session        = HibernateUtil.getSessionFactory().getCurrentSession();
      boolean                useTransaction = !session.getTransaction().isActive();
      List<MoksiskaanEntity> list;

      if (useTransaction) session.beginTransaction();
      list = (List<MoksiskaanEntity>)session.createQuery(hsql).list();
      if (useTransaction) session.getTransaction().commit();
      return list;
  }

  /**
   * Convert the given string value into its proper Java representation.
   */
  static public Object toObject(String value, Type type) {
      if (value == null || type == null)
          return value;
      if (type instanceof IdentifierType<?>) {
          try { return ((IdentifierType<?>)type).stringToObject(value); }
          catch (RuntimeException e) {
        	  throw e;
          }
          catch (Exception e) {
              throw new RuntimeException(value+" cannot be intepreted as "+type.getName()+'.', e);
          }
      } else
      if (type instanceof DoubleType) {
          return Double.valueOf(value);
      }
      return value;
  }

  /**
   * Adds a new log message to the database.
   *
   * @param type        Message source identifier
   * @param description A short description of the database changes.
   *                    You may list the information sources and their versions in here.
   * @param refs        A comma separated list of BibTeX reference keys for the associated
   *                    data sources. Use null to omit this information.
   */
  static public Log addLogEntry(String type, String description, String refs) {
	  Log     entry          = new Log(type, description, refs);
      Session session        = HibernateUtil.getSessionFactory().getCurrentSession();
      boolean useTransaction = !session.getTransaction().isActive();

      if (useTransaction) session.beginTransaction();
      session.save(entry);
      if (useTransaction) session.getTransaction().commit();

      return entry;
  }

  /**
   * Returns the gene entities encoding the given Uniprot protein.
   *
   * @param xrefId      Uniprot/SWISSPROT identifier of the protein
   * @param session     Open Hibernate session for the Moksiskaan database
   * @param entityCache Shortcut for the entities (this is updated by the call)
   * @return            Null if no genes were found
   */
  @SuppressWarnings("unchecked")
  static public Bioentity[] geneByUniprot(String                  xrefId,
		                                  Session                 session,
		                                  Map<String,Bioentity[]> entityCache) {
      final String FIND_BIOENTITY =
          "SELECT L.source "+
          "FROM   Xref X, Link L "+
          "WHERE  (L.linkType.id = "+IDConstants.LinkType_protein_coding_gene+") AND " +
          "       (X.xrefType.id = "+IDConstants.XrefType_UniProt_protein+") AND "+
          "       (X.value       = :value) AND "+
          "       (X.bioentity   = L.target)";

      Bioentity[] entities = (entityCache==null) ? null : entityCache.get(xrefId);
      if (entities == null) {
         List<Bioentity> list = session.createQuery(FIND_BIOENTITY)
                                       .setString("value", xrefId)
                                       .list();
         entities = list.isEmpty() ? EntityUtil.EMPTY_LIST_BIOENTITY :
        	                         list.toArray(new Bioentity[list.size()]);
         if (entityCache != null) {
            entityCache.put(xrefId, entities);
         }
      }
      if (entities.length < 1) return null;
      return entities;
  }

    /**
     * Save a link between two bioentities if that does not exist already.
     *
     * @param source     Source entity
     * @param target     Target entity
     * @param type       Meaning (the kind) of the link
     * @param annotName  An optional key for an annotation
     * @param annotValue An optional value for the annotation key
     * @param session    Database connection
     * @param out        An optional output stream for debug messages
     * @return           True if the link was actually created
     */
	static public boolean saveLink(Bioentity   source,
	                               Bioentity   target,
	                               LinkType    type,
	                               String      annotName,
	                               String      annotValue,
	                               Session     session,
	                               PrintStream out) {
       return saveLink(source, target, type, annotName, annotValue, session, out, null);
    }

    /**
     * Save a link between two bioentities if that does not exist already.
     *
     * @param source     Source entity
     * @param target     Target entity
     * @param type       Meaning (the kind) of the link
     * @param annotName  An optional key for an annotation
     * @param annotValue An optional value for the annotation key
     * @param session    Database connection
     * @return           The link object, which matches the criterion
     */
	static public Link prepareLink(Bioentity   source,
	                               Bioentity   target,
	                               LinkType    type,
	                               String      annotName,
	                               String      annotValue,
	                               Session     session) {
       Link[] l = new Link[1];
       saveLink(source, target, type, annotName, annotValue, session, null, l);
       return l[0];
    }

	static private boolean saveLink(Bioentity   source,
	                                Bioentity   target,
	                                LinkType    type,
	                                String      annotName,
	                                String      annotValue,
	                                Session     session,
	                                PrintStream out,
	                                Link[]      ref /* this is a private hack */) {
	    final String FIND_LINK =
	        "FROM  Link "+
	        "WHERE (source.id   = :source) AND "+
	        "      (target.id   = :target) AND "+
	        "      (linkType.id = :type)";
	
	    Link link = (Link)session.createQuery(FIND_LINK)
	                             .setLong("source", source.getBioentityId())
	                             .setLong("target", target.getBioentityId())
	                             .setLong("type",   type.getLinkTypeId())
	                             .uniqueResult();
	    boolean createNew = (link == null);
	    if (createNew) {
	        if (out != null)
	            out.println("    save("+type.getName()+", "+source+", "+target+')');
	        link = new Link();
	        link.setSource(source);
	        link.setTarget(target);
	        link.setLinkType(type);
	        session.save(link);
	        if (annotValue != null) {
	            LinkAnnotation annot = new LinkAnnotation(link, annotName, annotValue);
	            session.save(annot);
	        }
	    } else if (annotValue != null) {
	        LinkAnnotation old = (LinkAnnotation)session.createQuery("FROM LinkAnnotation WHERE (link.id="+
	                                                                 link.getLinkId()+") AND (name=:name)")
	                                                    .setString("name", annotName)
	                                                    .uniqueResult();
	        if (old == null) {
	            LinkAnnotation annot = new LinkAnnotation(link, annotName, annotValue);
	            session.save(annot);
	        } else {
	            if (AsserUtil.indexOf(annotValue, AsserUtil.split(old.getValue()), true, false) < 0) {
	                old.setValue(old.getValue()+','+annotValue);
	                session.update(old);
	            }
	        }
	    }
	    if (ref != null) ref[0] = link;
	    return createNew;
	}

}
function TimeSeriesReport {

  /** Visualization configuration for the gene interactions */
  linkStyles = INPUT(path="../CandidateReport/LinkTypeProperties.csv")

  graphT = TimeSeriesGraph(activities = activities,
                           linkStyles = linkStyles,
                           fuzzy      = fuzzy,
                           ltPos      = ltPos,
                           ltAny      = ltAny,
                           ltNeg      = ltNeg)
  graphE = ExpressionGraph(graph      = graphT.graph,
                           status     = graphT.status)
  graphC = VertexJoin     (graph      = graphE.graph)

  graphPlot  = GraphVisualizer(graph          = graphC.graph,
                               layout         = layout,
                               titleAttribute = "GeneName",
                               reportHeight   = 23,
                               reportWidth    = 18)
  legendPlot = GraphVisualizer(graph         = graphT.legend,
                               layout        = "hierarchical",
                               reportHeight  = 4,
                               reportWidth   = 18,
                               reportCaption = 'This graph represents the known relationships between the genes. '+
                                               'The relationships have been selected so that they match the observed '+
                                               'status transitions between the time points. The colors of the nodes '+
                                               'reflect the order of the events so that \textcolor{red}{the first time point} is red '+
                                               'and the last one is white. The intermediate time points have shades of pink. '+
                                               'Green and blue borders are referring to \textcolor{green}{up} and '+
                                               '\textcolor{blue}{down} regulated genes, respectively.')
  if (attach) {
     metadata        = std.metadata()
     graphAttachment = LatexAttachment(file1    = graphE.graph,
                                       caption1 = 'This attachment provides a GraphML representation of the pathway in Figure~\ref{fig:'+
                                                   metadata.instanceName+"-legendPlot}.")
  } else {
     graphAttachment = null
  }
  table = CSV2Latex(tabledata = graphT.stats,
                    colFormat = "lrr",
                    caption   = "Number of up and down regulated genes at each time point.",
                    skipEmpty = true,
                    attach    = attach)
  report = LatexCombiner(graphPlot,
                         legendPlot,
                         graphAttachment,
                         table,
                         pagebreak    = true,
                         sectionTitle = title)
  return record(report = report,
                graph  = graphE.graph,
                status = graphT.status,
                stats  = graphT.stats)
}

set.seed(12345) # Deterministic behaviour

table1[,-1] <- as.numeric(as.matrix(table1[,-1, drop=FALSE]))

doTestS <- as.numeric(param4)
doTestG <- as.numeric(param5)
sLimit  <- as.numeric(unlist(strsplit(param1,',',fixed=TRUE))) # score,testS,testG
roundD  <- as.numeric(param3)
method  <- eval(parse(text=param2))
scores  <- apply(table1[,-1, drop=FALSE], 1, method)
ords    <- order(scores, decreasing=TRUE)
table1  <- table1[ords,]
scores  <- scores[ords]

if ((doTestG > 0) && (length(ords) > 1)) {
   pGene <- sapply(1:nrow(table1), function(x) {
     pSize     <- doTestG
     sSet      <- (table1[x,-1] > 0)
     scoreDist <- sapply(1:length(sSet), function(s) {
                    if (sSet[s]) {
                       sample(table1[table1[,s+1] > 0, s+1], pSize, replace=TRUE)
                    } else {
                       rep(0, pSize)
                    }
                  })
     scoreDist <- apply(scoreDist, 1, method)
     sum(scoreDist >= scores[x])/pSize
   })
}

if ((doTestS > 0) && (length(ords) > 1)) {
   permTable        <- function() { apply(table1[,-1, drop=FALSE], 2, sample) }
   scoreDist        <- replicate(doTestS, apply(permTable(), 1, method))
   sDLength         <- length(scoreDist)
   table1['pScore'] <- sapply(scores, function(x){sum(scoreDist>=x)/sDLength})
   rm(scoreDist)
}

if (exists("pGene")) {
   table1['pEntity'] <- pGene
   scores            <- scores[pGene<=sLimit[3]]
   table1            <- table1[pGene<=sLimit[3],]
}

if (doTestS > 0) {
   scores <- scores[table1[,'pScore']<=sLimit[2]]
   table1 <- table1[table1[,'pScore']<=sLimit[2],]
}


table1    <- table1[scores >= sLimit[1],]
scores    <- scores[scores >= sLimit[1]]
rowC      <- nrow(table1)
table.out <- cbind(BioentityId = table1[,1],
                                 round(table1[,2:ncol(table1)], roundD),
                   scores      = round(scores,                  roundD),
                   position    = rep(1,rowC))
rm(table1)

if (rowC > 1) {
  posCol <- ncol(table.out)
  for (r in 2:rowC) {
    if (scores[r-1] > scores[r]) {
       table.out[r,posCol] <- table.out[r-1,posCol]+1
    } else {
       table.out[r,posCol] <- table.out[r-1,posCol]
    }
  }
}

write.log(cf, sprintf("Output contains %d hits.", rowC))

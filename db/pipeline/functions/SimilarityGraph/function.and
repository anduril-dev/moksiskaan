function SimilarityGraph {
  /**
    Calculates an adjacency matrix for the rows of the given matrix.
    Param1 represents the minimum threshold for the connection.
    Param2 is a comma separated list of names of columns to be excluded.
  */
  similarityR = INPUT(path="similarities.r")
  distances = REvaluate(table1 = matrix,
                        script = similarityR,
                        param1 = threshold,
                        param2 = colExclude)
  /** Converts the given adjacency matrix to an XML graph suitable for the vertex annotations. */
  graph = CSV2GraphML(matrix       = distances.table,
                      directed     = false,
                      edgeWeights  = false,
                      maxEdgeWidth = 5,
                      type         = "adjacency")
  if (annotations == null) {
     gOut = graph.graph
  } else {
     annot = GraphAnnotator(graph            = graph,
                            vertexAttributes = annotations,
                            idAttrib         = "name")
     gOut = annot.graph
  }
  return gOut
}

sLimit           <- as.numeric(param1)
nC               <- ncol(table1)
rownames(table1) <- table1[,1]

colExclude <- unique(trim(unlist(strsplit(x=param2, split=',', fixed=TRUE))))
table1     <- table1[,setdiff(colnames(table1),colExclude)]

table1[,-1]     <- as.numeric(as.matrix(table1[,-1, drop=FALSE]))
table1          <- table1[,-1]
nC              <- nrow(table1)
table.out       <- matrix(nrow=nC, ncol=nC)
diag(table.out) <- 0

for (a in 2:nC) {
   for (b in 1:(a-1)) {
       s <- sum( abs(table1[a,]-table1[b,]), na.rm=TRUE)/
            sum(pmax(table1[a,],table1[b,]), na.rm=TRUE)
       s <- if (s<sLimit) (1-s) else 0
       table.out[a,b] <- s
       table.out[b,a] <- s
   }
}

colnames(table.out) <- rownames(table1)
nC                  <- (colSums(table.out) > 0)
table.out           <- table.out[nC,nC]
table.out           <- cbind(ID=rownames(table1)[nC], table.out)

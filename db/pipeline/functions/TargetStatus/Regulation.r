# input statuses
table1 <- table1[table1[,1] %in% table2[,1],]

# regulation links
table2                <- table2[,c(1,2,4)]
table2[,'linkTypeId'] <- table3[match(table2[,'linkTypeId'], table3[,'LinkType']),'effect']
colnames(table2)      <- c('idIn','role','idOut')
table2                <- table2[!is.na(table2[,'idOut']),]
rm(table3)

# accepted targets
if (!is.null(table4)) {
   if (param1 == '') param1 <- colnames(table4)[1]
   table4 <- table4[,param1]
   table2 <- table2[table2[,'idOut'] %in% table4,]
   rm(table4)
}

table2   <- table2[!duplicated(table2, MARGIN=1), ]
conflict <- get.parameter(cf, 'param3', 'boolean')
signed   <- get.parameter(cf, 'param2', 'boolean')
if (!signed) {
   unsign          <- function(x){ if (is.na(x) || (x==-1)) 1 else x }
   table1[,-1]     <- apply(table1[,-1], MARGIN=1:2, FUN=unsign)
   table2[,'role'] <- sapply(table2[,'role'], FUN=unsign)
   table2          <- table2[!duplicated(table2, MARGIN=1), ]
} else
if (conflict) {
   conflicts <- table2[duplicated(table2[,c(1,3)], MARGIN=1), c(1,3)]
   if (nrow(conflicts) > 0) {
      for (r in 1:nrow(conflicts)) {
          table2 <- table2[!(table2[,1]==conflicts[r,1] & table2[,3]==conflicts[r,2]), ]
      }
   }
}

# Determines the output status of sample s when
# sources[,s] provides the input statuses and
# roles[,'role'] tells the effects.
regulation <- function(s) {
   if (length(sources[,s]) < length(roles[,'role'])) # ambiquous roles
      return(NA)
   reg <- unique(sources[,s]*roles[,'role'])
   if (length(reg) > 1) {
      if (any(is.na(reg))) return(NA)
      r <- reg[1]
      for (v in reg[-1]) {
          if (v == 0) next
          if (r == 0) r <- v
          else if (v != r) return(NA)
      }
      reg <- r
   }
   if (abs(reg) > 1) { reg <- NA }
   return(reg)
}

# Prepare the status table
targets   <- sort(unique(table2[,'idOut']))
samples   <- colnames(table1)[-1]
table.out <- matrix(nrow=length(targets), ncol=length(table1))
if (nrow(table.out) > 0)
for (tI in 1:length(targets)) {
    roles            <- table2[table2[,'idOut']==targets[tI],]
    sources          <- match(table1[,1],roles[,'idIn'])
    sources          <- table1[!is.na(sources),]
    table.out[tI, 1] <- targets[tI]
    table.out[tI,-1] <- sapply(samples, FUN=function(s) {
      regulation(s)
    })
}
colnames(table.out) <- c('idOut', samples)

roleOut <- table2
if (signed && !conflict) {
   # Prepare the roles output by removing genes with ambiquous roles.
   conflicts <- roleOut[duplicated(roleOut[,c(1,3)], MARGIN=1), c(1,3)]
   if (nrow(conflicts) > 0) {
      for (r in 1:nrow(conflicts)) {
          roleOut <- roleOut[!(roleOut[,1]==conflicts[r,1] & roleOut[,3]==conflicts[r,2]), ]
      }
   }
}
CSV.write(get.output(cf, 'optOut1'), roleOut)
rm(optOut1)

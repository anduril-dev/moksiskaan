prepareTable <- function(x) {
 if (is.null(x) || (nrow(x) < 1)) {
    x <- data.frame(.GeneId=character(0), status=integer(0))
 } else {
    x
 }
}
table1 <- prepareTable(table1)
table2 <- prepareTable(table2)
table3 <- prepareTable(table3)
genes  <- sort(union(table1[,1], union(table2[,1], table3[,1])))

table.out <- data.frame(.GeneId=genes, stringsAsFactors=FALSE)
if (nrow(table1) > 0) {
   table.out[match(table1[,1],genes),'expr'] <- table1[,'status']
   rm(table1)
} else {
   table.out[,'expr'] <- NA
}
if (nrow(table2) > 0) {
   table.out[match(table2[,1],genes),'copy'] <- table2[,'status']
   rm(table2)
} else {
   table.out[,'copy'] <- NA
}
if (nrow(table3) > 0) {
   table.out[match(table3[,1],genes),'meth'] <- table3[,'status']
   rm(table3)
} else {
   table.out[,'meth'] <- NA
}

if (!is.null(table4)) {
   table.out <- table.out[table.out[,'.GeneId'] %in% table4[,1],]
   rm(table4)
}

table.out[, 'status'] <- table.out[,'expr']
i <- (table.out[,'copy']<0) & !is.na(table.out[,'copy'])
table.out[i, 'status'] <- -2
i <- (is.na(table.out[,'status']) & (table.out[,'copy']==1))
i[is.na(i)] <- FALSE
table.out[i, 'status'] <- 1
i <- is.na(table.out[,'status'])
table.out[i, 'status'] <- table.out[i,'meth']

i <- (table.out[,'status']==-1) & !is.na(table.out[,'status'])
optOut1 <- paste(c('.GeneId', table.out[i,1]), collapse='\n')

table.out <- table.out[,c('.GeneId','status')] # Leave the extra columns out

param1      <- unlist(strsplit(param1, ",", fixed=TRUE))
idAttribute <- param1[1]
colColumn   <- param1[2]
signalRcol  <- param1[3]
signalGcol  <- param1[4]
signalBcol  <- param1[5]
txtColumn   <- 'fontcolor'
minSignal   <- as.numeric(param2)
defSignal   <- as.numeric(param3)

readColor <- function(data, colName, varName, signalCol, signalVar) {
  color <- rep(minSignal, nrow(table1))
  if (is.null(data)||nrow(table1)<1) {
     assign(varName, idAttribute, inherits=TRUE)
  } else {
     cMatch <- match(table1[,idAttribute], data[,1], nomatch=0)
     if (colName == '') {
        color[cMatch != 0] <- defSignal
     } else {
        maxVal <- max(data[cMatch,colName])
        minVal <- min(data[cMatch,colName])
        color[cMatch != 0] <- minSignal+
                              (255-minSignal)*
                              (data[cMatch,colName]-minVal)/
                              (maxVal-minVal)
        color[is.nan(color)] <- 255
        color <- round(color)
        if (nchar(signalCol) > 0) {
           signalV              <- rep(NA, nrow(table1))
           signalV[cMatch != 0] <- data[cMatch,colName]
           assign(signalVar, signalV, inherits=TRUE)
        }
     }
     assign(varName,
            paste(c(idAttribute, data[cMatch,1]), collapse='\n'),
            inherits = TRUE)
  }
  color
}

rCol <- readColor(table2, param4, 'optOut1', signalRcol, 'signalR')
gCol <- readColor(table3, param5, 'optOut2', signalGcol, 'signalG')
bCol <- readColor(table4, param6, 'optOut3', signalBcol, 'signalB')

if (colColumn == 'fillcolor') {
   tCol <- ((rCol+gCol+bCol)<320)
   tCol <- sapply(tCol, function(x){if(x) 'white' else 'black'})
}

toHexString <- function(x) {
  hRep <- as.character(as.hexmode(x), upper.case=TRUE)
  if (nchar(hRep) < 2)
     hRep <- paste('0', hRep, sep='')
  hRep
}
rCol <- sapply(rCol, toHexString)
gCol <- sapply(gCol, toHexString)
bCol <- sapply(bCol, toHexString)

if (nrow(table1) > 0){
  table.out             <- table1[,c('Vertex',idAttribute)]  
  table.out[,colColumn] <- paste('#',rCol,gCol,bCol,sep="")

  if (exists('signalR')) table.out[,signalRcol] <- signalR
  if (exists('signalG')) table.out[,signalGcol] <- signalG
  if (exists('signalB')) table.out[,signalBcol] <- signalB

  if (colColumn == 'fillcolor') {
     table.out[,txtColumn] <- tCol
     colAttrDesc           <- '\\textbf{Vertex fill colors:}\\\\'
  } else
  if (colColumn == 'color') {
     colAttrDesc           <- '\\textbf{Border colors:}\\\\'
  } else {
     colAttrDesc           <- sprintf('\\textbf{Colors used in %s attribute:}\\\\', colColumn)
  }
} else {
     table.out             <- as.data.frame(matrix(nrow=0,ncol=length(idAttribute)+1))
     colnames(table.out)   <- c('Vertex',idAttribute)
     colAttrDesc           <- '\\textbf{Colors omitted}\\\\'
}

legendSep <- ' \\textit{and} '
legendV   <- c('NNN', '\\textit{no matching annotations}', paste(minSignal,minSignal,minSignal,sep=','))
dim(legendV) <- c(1,3)
if (!is.null(table2)) {
   legendV <- rbind(legendV,
              c('PNN', param7, paste(defSignal,minSignal,minSignal,sep=',')))
if (!is.null(table3)) {
   legendV <- rbind(legendV,
              c('PPN',
                paste(param7,param8,sep=legendSep),
                paste(defSignal,defSignal,minSignal,sep=',')))
}
}
if (!is.null(table3)) {
   legendV <- rbind(legendV,
              c('NPN', param8, paste(minSignal,defSignal,minSignal,sep=',')))
if (!is.null(table4)) {
   legendV <- rbind(legendV,
              c('NPP',
                paste(param8,param9,sep=legendSep),
                paste(minSignal,defSignal,defSignal,sep=',')))
}
}
if (!is.null(table4)) {
   legendV <- rbind(legendV,
              c('NNP', param9, paste(minSignal,minSignal,defSignal,sep=',')))
if (!is.null(table2)) {
   legendV <- rbind(legendV,
              c('PNP',
                paste(param7,param9,sep=legendSep),
                paste(defSignal,minSignal,defSignal,sep=',')))
if (!is.null(table3)) {
   legendV <- rbind(legendV,
              c('PPP',
                paste(param7,param8,param9,sep=legendSep),
                paste(defSignal,defSignal,defSignal,sep=',')))
}
}
}
document.out <- c(colAttrDesc,
                  paste('\\definecolor{',myName,legendV[,1],'}{RGB}{',legendV[,3],'}\\fcolorbox{black}{',myName,legendV[,1],'}{\\quad} ',legendV[,2],
                        sep      = '',
                        collapse = '\\\\\n'))

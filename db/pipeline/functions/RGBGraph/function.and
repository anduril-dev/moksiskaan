function RGBGraph {
  /** Convert input values to graph vertex colors. */
  selectColorsR = INPUT(path="SelectColors.r")

  // Extract graph properties to CSV files.
  inData = GraphAnnotator(graph = graph)

  @out.document.doc = 'Color legend'
  @out.table.doc    = 'Color annotations for the graph vertices'
  @out.optOut1.doc  = 'Identifier list of red vertices'
  @out.optOut2.doc  = 'Identifier list of green vertices'
  @out.optOut3.doc  = 'Identifier list of blue vertices'
  colors = REvaluate(table1 = inData.vertexAttributes @require,
                     table2 = red,
                     table3 = green,
                     table4 = blue,
                     script = selectColorsR @require,
                     param1 = std.concat(sep=',',
                              idAttribute,
                              colorAttribute,
                              signalAttributeR,
                              signalAttributeG,
                              signalAttributeB),
                     param2 = black,
                     param3 = defSignal,
                     param4 = colRed,
                     param5 = colGreen,
                     param6 = colBlue,
                     param7 = legendRed,
                     param8 = legendGreen,
                     param9 = legendBlue)
  withColors = GraphAnnotator(graph            = inData.graph,
                              vertexAttributes = colors.table)
  if (collapse) {
     nodeJoin = VertexJoin(graph = withColors.graph)
     graphOut = nodeJoin.graph
  } else {
     graphOut = withColors.graph
  }
  return record( graph            = graphOut,
                 legend           = colors.document,
                 red              = colors.optOut1,
                 green            = colors.optOut2,
                 blue             = colors.optOut3,
                 edgeAttributes   = withColors.edgeAttributes,
                 vertexAttributes = withColors.vertexAttributes )
}

function CorrelationGraph {
  asTables    = GraphAnnotator(graph=pathway, @keep=false)
  pairs       = TableQuery(table1 = asTables.edgeAttributes,
                           table2 = asTables.vertexAttributes,
                           query  = "CREATE VIEW Edges AS "+
                                    "(SELECT DISTINCT \"Vertex1\", \"Vertex2\" FROM table1) "+
                                    "MINUS "+
                                    "(SELECT DISTINCT \"Vertex2\", \"Vertex1\" FROM table1 WHERE (\"Vertex2\" > \"Vertex1\")) "+
                                    "--<statement break>--"+
                                    "SELECT DISTINCT N1.\""+idAttrib+"\" AS \"id1\", "+
                                    "                N2.\""+idAttrib+"\" AS \"id2\" "+
                                    "FROM   Edges E, table2 N1, table2 N2 "+
                                    "WHERE  (E.\"Vertex1\" = N1.\"Vertex\")   AND "+
                                    "       (E.\"Vertex2\" = N2.\"Vertex\")   AND "+
                                    "       (N1.\""+idAttrib+"\" NOT LIKE '') AND "+
                                    "       (N2.\""+idAttrib+"\" NOT LIKE '') "+
                                    "ORDER  BY 1,2")
  correlation = PairCorrelation(data     = data,
                                pairs    = pairs,
                                rUpMin   = rUpMin,
                                rDownMax = rDownMax,
                                pLimit   = pLimit)
  skipped     = TableQuery(table1 = asTables.vertexAttributes,
                           table2 = correlation.skipped,
                           query  = "SELECT * FROM table1 V "+
                                    "WHERE  (V.\""+idAttrib+"\" LIKE '') OR "+
                                    "       (V.\""+idAttrib+"\" IN (SELECT \"ID\" FROM table2)) "+
                                    "ORDER  BY 1")
  annotDataE  = TableQuery(table1 = correlation.stats,
                           query  = "SELECT \"ID1\", \"ID2\", ROUND(\"r\", 2) AS \"label\" "+
                                    "FROM   table1 "+
                                    "ORDER  BY 1,2")
                /** Generates a graph of the correlating edges. The correlations are shown as labels. */
  graph       = CSV2GraphML(matrix       = annotDataE,
                            directed     = false,
                            type         = "edgelist",
                            maxEdgeWidth = 1)
  annotDataV  = CSVTransformer(csv1       = asTables.vertexAttributes,
                               csv2       = correlation.stats,
                               transform1 = "csv1[csv1[,'"+idAttrib+"'] %in% union(csv2[,1],csv2[,2]),"+
                                            "     c('"+idAttrib+"',setdiff(colnames(csv1)[2:length(colnames(csv1))],'"+idAttrib+"'))]")
  annot       = GraphAnnotator(graph            = graph,
                               vertexAttributes = annotDataV,
                               idAttrib         = "name")
  return record(connections = annot.graph,
                stats       = correlation.stats,
                entities    = annot.vertexAttributes,
                skipped     = skipped)
}

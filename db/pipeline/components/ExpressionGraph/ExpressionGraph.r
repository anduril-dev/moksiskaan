library(componentSkeleton)
library(igraph)

execute <- function(cf) {
    graph    <- read.graph(get.input(cf, 'graph'), format='graphml')
    status   <- CSV.read(get.input(cf, 'status'))
    predictS <- get.parameter(cf, 'predict', type='boolean')
    idAttr   <- get.parameter(cf, 'idAttr')
    linkAttr <- get.parameter(cf, 'linkAttr')
    exprAttr <- split.trim(get.parameter(cf, 'exprAttr'), ',')
    predAttr <- split.trim(get.parameter(cf, 'predAttr'), ',')
    verts    <- V(graph)
    ids      <- get.vertex.attribute(graph, idAttr, verts)

    if (input.defined(cf, 'linkTypes')) {
       lTypes <- CSV.read(get.input(cf, 'linkTypes'))
    } else {
       lTypes <- data.frame(role=character(0), effect=numeric(0))
    }

    # Remove absent genes
    rmThese <- intersect(status[status[,'status']==-2,1], ids)
    if (length(rmThese) > 0) {
       graph <- delete.vertices(graph, verts[ids %in% rmThese])
       verts <- V(graph)
       ids   <- get.vertex.attribute(graph, idAttr, verts)
    }

    # Get status vectors for vertices (vStats) and edges (eStats)
    edgs   <- E(graph)
    eStats <- get.edge.attribute(graph, linkAttr, edgs)
    eStats <- lTypes[match(eStats, lTypes[,1]), 'effect']
    vStats <- status[match(ids,    status[,1]), 'status']

    # Propagate status information
    hasChanges <- TRUE
    fedgs      <- edgs[!is.na(eStats) & (abs(eStats)==1)] # Functional edges
    isPred     <- c()
    while (predictS && hasChanges) {
       hasChanges <- FALSE
       candis     <- fedgs[verts %->% verts[is.na(vStats)]]
       candis     <- cbind(get.edges(graph, candis), eStats[edgs %in% candis])
       candis[,3] <- apply(candis, 1, function(x){x[3]*vStats[verts==x[1]]})
       for (n in unique(candis[,2])) {
           nStats <- unique(candis[candis[,2]==n,3])
           if ((length(nStats)==1) && !is.na(nStats) && (abs(nStats)==1)) {
              hasChanges       <- TRUE
              vStats[verts==n] <- nStats
              isPred           <- c(isPred, n)
              #cat("updating",ids[verts==n],"to",nStats,"based on",ids[verts %in% candis[candis[,2]==n,1]],"\n")
           }
       }
    }

    # Remove conflicting edges
    vsOK    <- !is.na(vStats)
    rmThese <- c(edgs[(verts[vsOK & vStats== 1] %->% verts[vsOK &     vStats == 1]) & (    eStats ==-1)],
                 edgs[(verts[vsOK & vStats== 1] %->% verts[vsOK &     vStats ==-1]) & (    eStats == 1)],
                 edgs[(verts[vsOK & vStats==-1] %->% verts[vsOK &     vStats == 1]) & (    eStats == 1)],
                 edgs[(verts[vsOK & vStats==-1] %->% verts[vsOK &     vStats ==-1]) & (    eStats ==-1)],
                 edgs[(verts[vsOK & vStats== 0] %->% verts[vsOK & abs(vStats)== 1]) & (abs(eStats)== 1)])
    rmThese <- rmThese[rmThese %in% fedgs]
    if (length(rmThese) > 0) {
       graph <- delete.edges(graph, rmThese)
    }

    # Add status information to vertices
    exprUp     <- unlist(strsplit(paste(get.parameter(cf, 'exprUp'    ),",",sep=""), ",", fixed=TRUE))
    exprDown   <- unlist(strsplit(paste(get.parameter(cf, 'exprDown'  ),",",sep=""), ",", fixed=TRUE))
    exprStable <- unlist(strsplit(paste(get.parameter(cf, 'exprStable'),",",sep=""), ",", fixed=TRUE))
    exprPredF  <- unlist(strsplit(paste(get.parameter(cf, 'exprPredF' ),",",sep=""), ",", fixed=TRUE))
    exprPredT  <- unlist(strsplit(paste(get.parameter(cf, 'exprPredT' ),",",sep=""), ",", fixed=TRUE))

    if (length(predAttr) > 0)
    for (i in 1:length(predAttr)) {
        if (nchar(exprPredF[i]) < 1) {
           oldValues <- get.vertex.attribute(graph, predAttr[i], index=verts)
           ovMissing <- if (is.null(oldValues)) verts else verts[is.na(oldValues)]
        } else {
           ovMissing <- verts
        }
        graph <- set.vertex.attribute(graph, predAttr[i], index=ovMissing, exprPredF[i])
    }

    if (length(exprAttr) > 0)
    for (i in 1:length(exprAttr)) {
       graph     <- set.vertex.attribute(graph, exprAttr[i], index=verts[vsOK & vStats== 1],    exprUp    [i])
       graph     <- set.vertex.attribute(graph, exprAttr[i], index=verts[vsOK & vStats==-1],    exprDown  [i])
       graph     <- set.vertex.attribute(graph, exprAttr[i], index=verts[vsOK & vStats== 0],    exprStable[i])
       oldValues <- get.vertex.attribute(graph, exprAttr[i], index=verts[is.na(vStats)])
       ovMissing <- is.na(oldValues)
       if (any(ovMissing)) {
          graph <- set.vertex.attribute(graph, exprAttr[i], index=(verts[is.na(vStats)])[ovMissing], "")
       }
    }

    if (length(predAttr) > 0)
    for (i in 1:length(predAttr)) {
        graph <- set.vertex.attribute(graph, predAttr[i], index=isPred, exprPredT[i])
    }

    # Remove genes that are not differentially expressed
    statusFilter <- unlist(strsplit(paste(get.parameter(cf,'statusFilter'),",",sep=""),",",fixed=TRUE))
    if (nchar(statusFilter[1]) > 0) {
       statusFilter[statusFilter=='NA'] <- NA
       statusFilter                     <- as.numeric(statusFilter)
       statusFilter                     <- !is.na(match(vStats,statusFilter))
       rmThese <- verts[statusFilter]
       rmKeep  <- split.trim(get.parameter(cf, 'keepIf'), ',')
       if (length(rmKeep) > 0) {
          for (rmKeep in rmKeep) {
              rmKeep  <- split.trim(rmKeep, '=')
              fieldValues  <- get.vertex.attribute(graph, rmKeep[1], index=rmThese)
              if (!is.null(fieldValues)) {
                 rmThese <- rmThese[fieldValues != rmKeep[2]]
              }
          }
       }
       if (length(rmThese) > 0) {
          graph <- delete.vertices(graph, rmThese)
          verts <- V(graph)
       }
    }

    # Remove genes with no edges
    rmThese <- verts[degree(graph, verts, "total")==0]
    if (length(rmThese) > 0) {
       graph <- delete.vertices(graph, rmThese)
    }

    # Remove special id attribute from vertices
    graph <- my.remove.vertex.attr(graph, 'id')

    write.graph(graph, get.output(cf, 'graph'), format='graphml')
    return(0)
}

my.remove.vertex.attr <- function(graph, attr.name) {
    while (attr.name %in% list.vertex.attributes(graph)) {
        graph <- remove.vertex.attribute(graph, attr.name)
    }
    return(graph)
}

main(execute)

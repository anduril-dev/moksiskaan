-- Expected source reference type is 10.
SELECT DISTINCT ? AS "sourceKey",
       SB."name" AS "BioentityName",
       BT."name" AS "BioentityTypeName"
FROM   "Xref" SX, "Bioentity" SB
       LEFT OUTER JOIN "BioentityType" AS BT ON (BT."bioentityTypeId" = SB."bioentityTypeId")
WHERE  (SX."xrefTypeId" = 10) AND
       (SX."bioentityId" = SB."bioentityId") AND
       (SX."value" = ?) AND 
       (SB."organismId" = 9606)
ORDER  BY 2, 3 DESC
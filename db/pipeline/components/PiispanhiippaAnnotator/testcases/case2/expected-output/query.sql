-- All elements with the reference type of DNAStrand
SELECT DISTINCT CASE T."strand" WHEN TRUE THEN '1' WHEN FALSE THEN '-1' ELSE NULL END AS "DNAStrand"
FROM   "Bioentity" B, "DNARegion" T
WHERE  (T."bioentityId" = B."bioentityId") AND
       (B."organismId"  = ?)
ORDER  BY 1
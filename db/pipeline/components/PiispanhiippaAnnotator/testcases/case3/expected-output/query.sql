-- Expected source reference type is HitStudyId.
SELECT DISTINCT ? AS "sourceKey",
       SB."bioentityId" AS "BioentityId",
       SB."name" AS "BioentityName"
FROM   "Hit" H, "Bioentity" SB
WHERE  (SB."bioentityId" = H."bioentityId") AND (H."studyId" = ?) AND
       (SB."organismId" = 9606)
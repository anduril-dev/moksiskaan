<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>SaveStudy</name>
    <version>2.3</version>
    <doc>Saves results of an individual study into the database.
         Results may represent either a set of bioentities or a set of links between them.
         Identifier of each study should be unique within the database instance.
    </doc>
    <author email="Marko.Laakso@Helsinki.FI">Marko Laakso</author>
    <category>Moksiskaan</category>
    <launcher type="java">
        <argument name="class" value="fi.helsinki.ltdk.csbl.moksiskaan.anduril.SaveStudy" />
    </launcher>
    <requires type="jar">moksiskaan.jar</requires>
    <requires type="jar">log4j-1.2.14.jar</requires>
    <requires type="jar">postgresql.jar</requires>
    <requires type="jar">slf4j-log4j12-1.6.1.jar</requires>
    <inputs>
        <input name="hits" type="CSV">
            <doc>Table of findings and possible scores</doc>
        </input>
    </inputs>
    <outputs>
        <output name="report" type="Latex">
            <doc>Summary report</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="id" type="int">
            <doc>Identifier of the study</doc>
        </parameter>
        <parameter name="title" type="string">
            <doc>A short name for the study</doc>
        </parameter>
        <parameter name="description" type="string" default="">
            <doc>A short description of the study and the associated results</doc>
        </parameter>
        <parameter name="refs" type="string" default="">
            <doc>A comma separated list of BibTeX reference keys for the associated references.
                 These will be cited when the results are refered.</doc>
        </parameter>
        <parameter name="organism" type="int">
            <doc>Organism of interest defined by NCBI Taxonomy identifier</doc>
        </parameter>
        <parameter name="scoreType" type="int">
            <doc>Identifier of the result weight type as specified in ScoreType.csv</doc>
        </parameter>
        <parameter name="bioentityType" type="int">
            <doc>Identifier of the bioentity type as specified in BioentityType.csv</doc>
        </parameter>
        <parameter name="xrefType" type="int">
            <doc>Identifier of the external reference type as specified in XrefType.csv</doc>
        </parameter>
        <parameter name="xrefCol" type="string" default="">
            <doc>Column name for the input identifiers or an empty string for the first column</doc>
        </parameter>
        <parameter name="bioentityType2" type="int" default="-1">
            <doc>Target identifier of the bioentity type as specified in BioentityType.csv.
                 This attribute shall be defined when saving links.</doc>
        </parameter>
        <parameter name="xrefType2" type="int" default="-1">
            <doc>Target identifier of the external reference type as specified in XrefType.csv.
                 This attribute shall be defined when saving links.</doc>
        </parameter>
        <parameter name="xrefCol2" type="string" default="">
            <doc>Column name for the target entity identifiers when saving links or
                 an empty string for the bioentity sets</doc>
        </parameter>
        <parameter name="linkTypeDefault" type="int" default="-1">
            <doc>Default value for the link type used when saving links.
                 Possible values can be found from LinkType.csv.</doc>
        </parameter>
        <parameter name="linkAnnot" type="string" default="">
            <doc>An empty string or an annotName=valueColumn string, which defines an input
                 column name for the link annotation values</doc>
        </parameter>
        <parameter name="directed" type="boolean" default="true">
            <doc>Links are saved in both directions if this flag is false.</doc>
        </parameter>
        <parameter name="weightCol" type="string" default="">
            <doc>Column name for the hits scores or an empty string to omit the information</doc>
        </parameter>
        <parameter name="weightLowest" type="boolean" default="true">
            <doc>Select the lowest weight if the entity is defined multiple times.
                 The highest value is used if this flag is set false.</doc>
        </parameter>
        <parameter name="threshold" type="string" default="">
            <doc>Weight limit for the accepted hits.
                 This is the highest accepted value if weightLowest is true and the lower
                 boundary if the maximum values are used. An empty string accepts all values.</doc>
        </parameter>
        <parameter name="clean" type="boolean" default="false">
            <doc>Override all existing results available for this study</doc>
        </parameter>
        <parameter name="evidence" type="boolean" default="true">
            <doc>This is true if the hits support the connection between the bioentities.
                 False means that the evidence is against the link.</doc>
        </parameter>
        <parameter name="acceptNonUnique" type="boolean" default="false">
            <doc>Accept input identifiers even if they map to multiple Moksiskaan
                 bioentities.</doc>
        </parameter>
    </parameters>
</component>

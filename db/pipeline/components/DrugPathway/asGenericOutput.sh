TEST_FOLDER=../../../component-test/DrugPathway
for CASE in case1 case2
do
  echo "Updating $CASE."
  sed -E 's/\"[0-9,]+\"/\"{{[\\d,]+}}\"/g' $TEST_FOLDER/$CASE/component/status.csv > testcases/$CASE/expected-output/status.csv
  sed -e 's/\(id\|source\|target\)=\"[0-9]*\"/\1=\"{{[\\d]+}}\"/g;s/BioentityId\">[0-9]*</BioentityId\">{{[\\d]+}}</g' $TEST_FOLDER/$CASE/component/graph.xml > testcases/$CASE/expected-output/graph.xml
done

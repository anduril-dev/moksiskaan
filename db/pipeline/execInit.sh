#!/bin/bash

if [ -z "$HIBERNATE_DIR" ]; then
   HIBERNATE_DIR=/opt/hibernate
fi
if [ -z "$MOKSISKAAN_HOME" ]; then
   export MOKSISKAAN_HOME=`pwd`
fi
# You may optimize the performance by enabling this line producing a canonical path.
# export MOKSISKAAN_HOME=`readlink -f $MOKSISKAAN_HOME`

CLASSPATH=$CLASSPATH:$MOKSISKAAN_HOME/etc
for jar in $( find $HIBERNATE_DIR/lib/jpa $HIBERNATE_DIR/lib/required $HIBERNATE_DIR/lib/optional/c3p0 -name '*.jar' ); do
    CLASSPATH=$CLASSPATH:$jar
done
export CLASSPATH
echo "*** Java CLASSPATH=$CLASSPATH"

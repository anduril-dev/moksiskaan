<%@page import="java.net.URL"%><%@page contentType="application/x-java-jnlp-file"%><?xml version="1.0" encoding="UTF-8"?>
<% StringBuffer tmpSB    = request.getRequestURL();
   String       codebase = tmpSB.substring(0,tmpSB.lastIndexOf("/"));
   String       fileIn   = request.getParameter("import");
   String[]     dbs      = request.getParameterValues("db");
%><jnlp spec="1.0+" codebase="<%=codebase%>">
    <information>
        <title>Kotelosienet</title>
        <vendor>University of Helsinki</vendor>
        <description>Browser for the Moksiskaan bioentities</description>
        <description kind="tooltip">Kotelosienet on <%=request.getLocalName()%></description>
        <homepage href="http://csbi.ltdk.helsinki.fi/moksiskaan/"/>
        <shortcut online="true">
          <desktop/>
          <menu/> 
        </shortcut>
        <icon href="http://csbi.ltdk.helsinki.fi/moksiskaan/logo.png"/>
    </information>
    <security>
     <all-permissions/>
    </security>
    <resources>
        <j2se href="http://java.sun.com/products/autodl/j2se" version="1.6+" />
        <jar  href="KotelosienetClient.jar" main="true" />
    </resources>
    <application-desc main-class="fi.helsinki.ltdk.csbl.moksiskaan.kotelosienet.Kotelosienet">
      <argument>-r</argument>
      <argument><%=codebase%>/KsQueryEngine</argument>
      <argument>-go</argument>
      <argument>http://purl.obolibrary.org/obo/go/go-basic.obo</argument>
      <argument>-d4932</argument>
      <argument>ensemblSaccharomycesCerevisiae.properties</argument>
      <argument>-d9615</argument>
      <argument>ensemblCanisFamiliaris.properties</argument>
      <argument>-d9606</argument>
      <argument>ensemblHomoSapiens.properties</argument>
      <argument>-d10090</argument>
      <argument>ensemblMusMusculus.properties</argument><%
   if (fileIn != null) { %>
      <argument>-i</argument>
      <argument><%=fileIn%></argument>
      <argument><%=request.getParameter("col")%></argument>
      <argument><%=request.getParameter("type")%></argument><%
   }
   if (dbs != null) for (String db : dbs) { %>
      <argument><%=db%></argument><%
   } %>
    </application-desc>
    <update check="background" policy="prompt-update"/>
</jnlp>

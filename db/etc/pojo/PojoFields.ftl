    /** Moksiskaan default persistence as defined in {@link fi.helsinki.ltdk.csbl.moksiskaan.schema.MoksiskaanEntity MoksiskaanEntity}. */
    static private final long serialVersionUID = MoksiskaanEntity.serialVersionUID;

    // Constants for the field names:
<#foreach field in pojo.getAllPropertiesIterator()><#if pojo.getMetaAttribAsBool(field, "gen-property", true)>
    static public final String FIELD_${field.name.toUpperCase()} = "${field.name}";
</#if>
</#foreach>

<#-- // Fields -->
<#foreach field in pojo.getAllPropertiesIterator()><#if pojo.getMetaAttribAsBool(field, "gen-property", true)><#if pojo.hasMetaAttribute(field, "field-description")>    /**
     ${pojo.getFieldJavaDoc(field, 0)}
     */
 </#if>    ${pojo.getFieldModifiers(field)} ${pojo.getJavaTypeName(field, jdk5)} ${field.name}<#if pojo.hasFieldInitializor(field, jdk5)> = ${pojo.getFieldInitialization(field, jdk5)}</#if>;
</#if>
</#foreach>

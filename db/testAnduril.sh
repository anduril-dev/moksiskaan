#!/bin/bash

export CLASSPATH=
for jar in $( find $ANDURIL_HOME/microarray/lib/java -name '*.jar' ); do
    CLASSPATH=$CLASSPATH:$jar
done
source pipeline/execInit.sh
anduril test "$@" -log component-test/log -d component-test -b pipeline --auto-bundles

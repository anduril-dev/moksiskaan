mutLimit    <- as.numeric(param1)
sampleLimit <- as.numeric(param2)

sCodes    <- strsplit(as.character(table1[,2]), ',')
sStats    <- strsplit(as.character(table1[,3]), ',')
table.out <- table1[,1,drop=FALSE]
rm(table1)

for (i in 1:length(sCodes)) {
  sStats[[i]] <- sStats[[i]][!duplicated(sCodes[[i]])]
}
rm(sCodes)

table.out[,2] <- unlist(lapply(sStats, length))
table.out[,3] <- unlist(lapply(sStats, function(x){length(x[x=='y'])}))
table.out[,4] <- table.out[,3]/table.out[,2]
table.out     <- table.out[table.out[,4] >= mutLimit,    ]
table.out     <- table.out[table.out[,2] >= sampleLimit, ]

colnames(table.out) <- c('.GeneId', 'samples', 'mutations', 'ratio')

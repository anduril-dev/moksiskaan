// MOKSISKAAN - Import tissue specific gene expression profiles from TiGER
// author: Marko Laakso

function importTiGER() -> (Latex report) {
    /** TiGER import~\cite{Liu2008} */
    tigerRaw = URLInput(url="http://bioinfo.wilmer.jhu.edu/tiger/download/hs2tissue-Table.txt")

    tiger        = BashEvaluate(var1   = tigerRaw @require,
                                script = '''sed 's/[()]//g;s/\t/,/2g;s/_/ /g' @var1@ > @optOut1@''')
    tigerGenes   = KorvasieniAnnotator(sourceKeys  = tiger.optOut1,
                                       connection  = ensembl,
                                       echoColumns = "Tissues",
                                       indicator   = false,
                                       inputDB     = "_UniGene",
                                       inputType   = "Gene",
                                       primary     = true,
                                       rename      = "",
                                       skipLevel   = "any",
                                       targetDB    = ".GeneId",
                                       unique      = true)
    tigerGExp = ExpandCollapse(tigerGenes, listCols=".GeneId,Tissues")
    tissueUse = CSV2IDList(tigerGExp, columnIn="Tissues")
    tissueIds = PiispanhiippaAnnotator(sourceKeys = tissueUse,
                                       connection = moksiskaanInit.connection,
                                       inputDB    = "BioentityName",
                                       organism   = Organism_Homo_sapiens,
                                       targetDB   = XrefType_Brenda_Tissue_Ontology,
                                       @bind      = std.lookup("tissueSave_"+Organism_Homo_sapiens))
    geneTissues = TableQuery(table1 = tigerGExp               @require,
                             table2 = tissueIds.bioAnnotation @require,
                             query  = """\
                                      SELECT DISTINCT G.".GeneId",
                                             T."xref"""+XrefType_Brenda_Tissue_Ontology+"""\" AS "tissue"
                                      FROM   table1 G, table2 T
                                      WHERE  (G."Tissues" = T."sourceKey")
                                      """)
    geneTissueSave = SaveStudy(hits            = geneTissues,
                               acceptNonUnique = true,
                               bioentityType   = BioentityType_tissue,
                               bioentityType2  = BioentityType_gene,
                               clean           = true,
                               directed        = true,
                               evidence        = true,
                               linkTypeDefault = LinkType_context,
                               xrefCol         = "tissue",
                               xrefCol2        = ".GeneId",
                               xrefType        = XrefType_Brenda_Tissue_Ontology,
                               xrefType2       = XrefType_Ensembl_gene,
                               id              = study.tissueExpr.TiGER,
                               title           = "TiGER",
                               description     = "Tissue specific gene expression profiles from TiGER",
                               organism        = Organism_Homo_sapiens,
                               refs            = "Liu2008,Gremse2011",
                               scoreType       = ScoreType_Probability)

    return geneTissueSave.report
}

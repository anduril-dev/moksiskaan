// MOKSISKAAN
// author: Marko Laakso

function importTumorscape() -> (Latex report) {
  downloadAnt = StringInput(content='''
    <project name="TumorscapeFTP" default="download" basedir="${temp.dir}">
     <property name="zip.name" value="Tumorscape_webfiles_20100104.zip"/>
     <target name="download">
      <ftp server                  = "ftp.broadinstitute.org"
           userid                  = "anonymous"
           password                = ""
           action                  = "get"
           passive                 = "true"
           verbose                 = "true"
           retriesAllowed          = "5"
           ignoreNoncriticalErrors = "true"
           remotedir               = "/pub/cancer/gcnmp/Summary_data/Gene_summary_pages">
         <fileset dir="${temp.dir}" includes="${zip.name}"/>
      </ftp>
      <move file="${temp.dir}/${zip.name}" tofile="${output.output0}" />
     </target>
    </project>
  ''')
  ftp = Ant(script=downloadAnt)

  geneData  = TumorscapeReader(database=ftp.output0)
  useKeep   = false
  limitFreq = 0.1
  limitQ    = 0.25

  /* Tumorscape genes are converted to local indentifiers in two steps:
   * 1) Ensembl gene names to Ensembl stable IDs
   * 2) HGNC gene symbols to Ensembl stable IDs
   */
  geneAnnot1 = PiispanhiippaAnnotator(sourceKeys = geneData,
                                      connection = moksiskaanInit.connection,
                                      inputDB    = "BioentityName",
                                      keyColumn  = "ID",
                                      organism   = Organism_Homo_sapiens,
                                      targetDB   = XrefType_Ensembl_gene)
  idData1    = IDConvert(csv              = geneData,
                         conversionTable  = geneAnnot1.bioAnnotation,
                         conversionColumn = "xref"+XrefType_Ensembl_gene,
                         @keep            = useKeep)
  nameLeft   = StandardProcess(input   = idData1,
                               command = "grep -v ^ENSG",
                               @keep   = useKeep)
  geneAnnot2 = KorvasieniAnnotator(force sourceKeys = nameLeft.stdout,
                                   connection       = ensembl,
                                   inputDB          = "HGNC",
                                   inputType        = "Gene",
                                   keyColumn        = "ID",
                                   targetDB         = ".GeneId",
                                   unique           = true,
                                   skipLevel        = "any",
                                   indicator        = false,
                                   primary          = true)
  idData2    = IDConvert(csv              = idData1,
                         conversionTable  = geneAnnot2,
                         conversionColumn = ".GeneId",
                         targetColumn     = ".GeneId",
                         @keep            = useKeep)
  withID     = CSVFilter(csv       = idData2,
                         highBound = "q-value="+limitQ,
                         lowBound  = "PeakFrequency="+limitFreq,
                         regexp    = ".GeneId=^ENSG[\\d]+",
                         @keep     = useKeep)
  idData     = ExpandCollapse(withID, listCols=".GeneId")

  function saveSubset(string studyName,
                      string type,
                      int    ampId,
                      int    delId) ->
                     (Latex  report) {
     bibtexRefs = "Beroukhim2010"
     amp = CSVFilter(csv    = idData,
                     regexp = "AberrationType=amplification,CancerType="+type)
     del = CSVFilter(csv    = idData,
                     regexp = "AberrationType=deletion,CancerType="+type)
     saveAmp = SaveStudy(hits          = amp,
                         clean         = true,
                         id            = ampId,
                         title         = studyName+"a",
                         description   = "Frequent (minFreq="+limitFreq+", maxQ="+limitQ+") chromosomal amplifications in "+type+" tumours",
                         refs          = bibtexRefs,
                         bioentityType = BioentityType_gene,
                         organism      = Organism_Homo_sapiens,
                         xrefCol       = ".GeneId",
                         xrefType      = XrefType_Ensembl_gene,
                         scoreType     = ScoreType_Proportion,
                         weightCol     = "PeakFrequency")
     saveDel = SaveStudy(hits          = del,
                         clean         = true,
                         id            = delId,
                         title         = studyName+"d",
                         description   = "Frequent (minFreq="+limitFreq+", maxQ="+limitQ+") chromosomal deletions in "+type+" tumours",
                         refs          = bibtexRefs,
                         bioentityType = BioentityType_gene,
                         organism      = Organism_Homo_sapiens,
                         xrefCol       = ".GeneId",
                         xrefType      = XrefType_Ensembl_gene,
                         scoreType     = ScoreType_Proportion,
                         weightCol     = "PeakFrequency")
     return LatexCombiner(saveAmp.report, saveDel.report)
  }

  /** Copy number variations in breast cancer */
  subsetBreast   = saveSubset(studyName = "tscapeBC",
                              type      = "Breast",
                              ampId     = study.Tumorscape.BC_a,
                              delId     = study.Tumorscape.BC_d)
  /** Copy number variations in colorectal cancer */
  subsetCRC      = saveSubset(studyName = "tscapeCRC",
                              type      = "Colorectal",
                              ampId     = study.Tumorscape.CRC_a,
                              delId     = study.Tumorscape.CRC_d)
  /** Copy number variations in glioma */
  subsetGlioma   = saveSubset(studyName = "tscapeGlioma",
                              type      = "Glioma",
                              ampId     = study.Tumorscape.Glioma_a,
                              delId     = study.Tumorscape.Glioma_d)
  /** Copy number variations in non-small cell lung cancer */
  subsetNSCLC    = saveSubset(studyName = "tscapeNSCLC",
                              type      = "Lung NSC",
                              ampId     = study.Tumorscape.NSCLC_a,
                              delId     = study.Tumorscape.NSCLC_d)
  /** Copy number variations in small cell lung cancer */
  subsetSCLC     = saveSubset(studyName = "tscapeSCLC",
                              type      = "Lung SC",
                              ampId     = study.Tumorscape.SCLC_a,
                              delId     = study.Tumorscape.SCLC_d)
  /** Copy number variations in melanoma */
  subsetMelanoma = saveSubset(studyName = "tscapeMelanoma",
                              type      = "Melanoma",
                              ampId     = study.Tumorscape.Melanoma_a,
                              delId     = study.Tumorscape.Melanoma_d)
  /** Copy number variations in ovarian cancer */
  subsetOvarian  = saveSubset(studyName = "tscapeOvarian",
                              type      = "Ovarian",
                              ampId     = study.Tumorscape.Ovarian_a,
                              delId     = study.Tumorscape.Ovarian_d)
  /** Copy number variations in prostate cancer */
  subsetProstate = saveSubset(studyName = "tscapeProstate",
                              type      = "Prostate",
                              ampId     = study.Tumorscape.Prostate_a,
                              delId     = study.Tumorscape.Prostate_d)
  /** Copy number variations in hepatocellular carcinoma */
  subsetHCC      = saveSubset(studyName = "tscapeHCC",
                              type      = "Hepatocellular",
                              ampId     = study.Tumorscape.HCC_a,
                              delId     = study.Tumorscape.HCC_d)
  /** Copy number variations in renal cell carcinoma */
  subsetRCC      = saveSubset(studyName = "tscapeRCC",
                              type      = "Renal",
                              ampId     = study.Tumorscape.RCC_a,
                              delId     = study.Tumorscape.RCC_d)

  report = LatexCombiner(subsetBreast,
                         subsetCRC,
                         subsetGlioma,
                         subsetNSCLC,
                         subsetSCLC,
                         subsetMelanoma,
                         subsetOvarian,
                         subsetProstate,
                         subsetHCC,
                         subsetRCC,
                         sectionTitle = "Copy number variation from Tumorscape",
                         sectionType  = "section")
  return report
}

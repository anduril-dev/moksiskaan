SELECT P."bioentityId"        AS "bioentityId",
       P."name"               AS "pathwayName",
       X."value"              AS "pathwayId",
       count(G."bioentityId") AS "size"
FROM   "Bioentity" P, "Bioentity" G, "Link" L, "Xref" X
WHERE  (P."organismId"      = ORGANISM) AND
       (P."bioentityTypeId" = 400) AND -- Pathway
       (G."bioentityTypeId" = 100) AND -- Gene
       (L."linkTypeId"      = 550) AND -- Pathway contains
       (L."source"          = P."bioentityId") AND
       (L."target"          = G."bioentityId") AND
       (X."bioentityId"     = P."bioentityId") AND
       ((X."xrefTypeId"     = 20) OR   -- KEGG pathway
        (X."xrefTypeId"     = 80))     -- WikiPathways
GROUP  BY P."bioentityId", "pathwayName", "pathwayId"
ORDER  BY "size" DESC, "pathwayName"

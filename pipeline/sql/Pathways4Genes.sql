SELECT "pathways",
       count("member")                   AS "bioentities",
       ROUND(count("member")/"total", 2) AS "ratio"
FROM   (SELECT "target"        AS "member",
               count("source") AS "pathways"
        FROM   "Bioentity", "Link"
        WHERE  ("linkTypeId" = 550)           AND -- Pathway contains
               ("source"     = "bioentityId") AND
               ("organismId" = 9606)
        GROUP  BY "target"
       ) AS C,
       (SELECT count(DISTINCT "target")+0.0 AS "total"
        FROM   "Bioentity", "Link"
        WHERE  ("linkTypeId" = 550)           AND -- Pathway contains
               ("source"     = "bioentityId") AND
               ("organismId" = 9606)
       ) AS T
GROUP  BY "pathways", "total"
ORDER  BY "pathways", "bioentities" DESC

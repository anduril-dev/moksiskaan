matr <- read.table(table1, header=TRUE, row.names=1)

plotWidth   <- 18
fnameHC     <- sprintf('%s-clusters.pdf', instance.name)
plotnameHC  <- file.path(document.dir, fnameHC)
fnameMDS    <- sprintf('%s-mds.pdf', instance.name)
plotnameMDS <- file.path(document.dir, fnameMDS)

pdf(plotnameHC, width=plotWidth, height=10)
plot(hclust(as.dist(matr)),
     main='', sub='', xlab='')
dev.off()

pdf(plotnameMDS, width=plotWidth, height=plotWidth)
mds <- cmdscale(matr)
par(mar=rep(0,4))
plot(x=numeric(), y=numeric(),
     xlim=range(mds[,1]), ylim=range(mds[,2]),
     main='', sub='', xlab='', ylab='', axes=FALSE)
text(mds[,1], mds[,2], rownames(mds))
dev.off()

lFigHC  <- latex.figure(fnameHC,
                        caption      = sprintf('Hierarchical clustering of the studies based on %s.', param1),
                        quote.capt   = FALSE,
                        image.width  = plotWidth,
                        image.height = 10,
                        fig.label    = sprintf('fig:%s-clusters', instance.name))
lFigMDS <- latex.figure(fnameMDS,
                        caption      = 'Multidimensional scaling of the study distances.',
                        quote.capt   = FALSE,
                        image.width  = plotWidth,
                        image.height = plotWidth,
                        fig.label    = sprintf('fig:%s-mds', instance.name))

document.out <- c(lFigHC, '\\clearpage{}', lFigMDS, '\\clearpage{}')
table.out    <- data.frame('nothing') # Not in use

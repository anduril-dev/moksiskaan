wget ftp://ftp.ncbi.nih.gov/gene/DATA/gene_info.gz
gunzip gene_info.gz
grep 'Ensembl:ENSG0' gene_info | cut -f 2,6 |sed 's/|\?\(\(HGNC\)\|\(MIM\)\|\(HPRD\)\|\(Vega\)\)\+:[0-9A-Z]\+|\?//g;s/Ensembl://g;s/^/hsa:/g;s/|/,/g' > genemap.txt
rm gene_info

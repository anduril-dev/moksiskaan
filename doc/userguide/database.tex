\part{Database}

This part concentrates on Moksiskaan database schema and its direct manipulation.
Section~\ref{sec:customdb} will show how to extend the base database with your
own relations.

\section{Database schema}
\label{sec:schema}

This section describes the concepts that are represented in Moksiskaan database. We will
discuss about the database architecture but the details about the relations and
their attributes are left out. You will find that information from the database documentation.

Moksiskaan has been designed so that all parts of it should follow a common data model.
The same model is used to generate physical database, documentation, accession code and
the user interface.

Moksiskaan contains three kinds of relations. The content of each category is maintained
differently.
\begin{description}
\item[Static tables]\glsadd{glos:staticTable}
     consist of constant data that is linked to the database. The original data is
     kept and maintained in files under \baseDir{/data}. These files are used to generate
     some code and to populate the database consistently with it.
\item[Base tables]\glsadd{glos:baseTable}
     are semi-static tables that are populated during the installation. The content of
     these tables is fetched from external resources and it may vary depending on the
     intallation time and the settings used.
\item[User tables]\glsadd{glos:userTable}
     contain dynamic data about the studies and biological findings. These relations form
     the instance specific data that is subjected to modifications on the fly.
\end{description}

\section{Key concepts}
\label{sec:dbConcepts}

This section gives an overview of the key relations of Moksiskaan database. Detailed
descriptions of each relation can be found from the database documentation.

\subsection{Bioentity}
\glsadd{glos:bioentity}

Bioentities form the terminology that is used to describe the biological observations.
Bioentities represent concepts such as genes, proteins, pathways, diseases, drugs,
biological processes, cellular components, and molecular functions. The relationships
between the bioentities are represented as directed links each having a type specifying
the kind of the relationship. In addition, the links may have weights assigned to them.

\subsection{Organism}

Moksiskaan database uses a shared database for all organisms\footnote{This approach is
different from Ensembl~\cite{Flicek2011} that has a separate database for each organism
but they all share the same schema.} but each organism has its own set of bioentities.
NCBI Taxonomy identifies are used as native identifiers of the organims in Moksiskaan.

\subsection{Study}
\label{sec:Study}

Each study represents a set of observations. One research project may produce many sets
of findings of various kinds. All these sets belong to their own studies and they are
kept separate in Moksiskaan database. The default installation populates database with
some example studies (including \cite{Carter2006,Bos2009,Santarius2010}) especially useful
in cancer research.

\section{Customisation of the schema}
\label{sec:customdb}

Physical structure of the database is defined in XML files under
\baseDir{/db/etc}. The complete list of the relations in use can be found from
\baseDir{/db/etc/hibernate.cfg.xml}. New relations can be added by defining them
with their own XMLs in
\baseDir{\D{}db\D{}etc\D{}fi\D{}helsinki\D{}ltdk\D{}csbl\D{}moksiskaan\D{}schema}
and by adding the corresponding reference to \texttt{hibernate.cfg.xml}.

Static tables are populated during the installation process (\texttt{ant staticImport})
and their content is propagated to constant files available to the developers and
Anduril users. You can define content for your own static tables by defining their
tuples in CSV format in \baseDir{/data}. Each relation has its own file named after it.
Column names should match attributes of the target relation.

\subsection{Adding new organisms}

New organisms can be introduced by adding them to \baseDir{db/data/Organism.csv}.
These organisms are saved to the database when you run
\texttt{ant staticImport} (also part of \texttt{ant install}) and the corresponding
constants are generated for AndurilScript and Java in order to simplify the accession
of these entities.

You may have to change the build script (\baseDir{/db/build.xml}) in order to
populate the database for your new organisms. Some generic steps
will most probably work out of the box but most of the external
resources have to be configured manually for each organism. This is why we are having
organism specific flags in \baseDir{db/build.properties} and the conditional
steps in \baseDir{/db/build.xml}. You may follow the logic that has been put in place for
the mouse and yeast.


\part{Piispanhiippa}

The following sections tell how to use the Piispanhiippa program to make queries
against Moksiskaan database.

\section{Standalone application}
\label{sec:piispanhiippa}
\glsadd{glos:piispanhiippa}

Piispanhiippa is a standalone Java application that can be used to make queries against Moksiskaan database.
The queries consist of input identifiers for which the annotator is meant to fetch the
properties of interest. The properties may represent various pieces of information stored into the database.
You can obtain a list of the currently supported properties by asking for a list of reference types:
|piispanhiippa -Lr|.
The same reference types are used to describe the types of input keys and output properties,
although some restrictions apply to the input types. The restrictions have been set in order to
prevent mistakes leading to extremely long results. For example, a trial to ask for all genes of the
forward strands of their chromosomes (|piispanhiippa -Rs DNAStrand 1|) will produce an error.

Simple queries consists of a list of source references (parameter |-Rs|) that shall be annotated with their
target references (parameter |-Rt|). The source keys can be defined as command line arguments or as words
in standard input stream. The later method is used if the argument list contains none.
The following query converts bioentity names 'AIP` and 'MUTYH` to their Ensembl gene identifiers (10) and
genomic coordinates (DNARegion):
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
$ piispanhiippa -Rs BioentityName -Rt 10,DNARegion AIP MUTYH
sourceKey  xref10           DNARegion
AIP        ENSG00000110711  11:67250505-67258578
MUTYH      ENSG00000132781  1:45794914-45806142
\end{Verbatim}

For more complicated queries you may want to have a look on the command line parameters available:
\texttt{piispanhiippa -help}. We have already tried the listing of supported reference types. Now, you can
see the options to list the supported organisms (|-Lo|) and the link types (|-Ll|). The use of the
organism selection is pretty self evident but the link types are worth a careful explanation.
These types are refering to relationships between bioentities such as genes, proteins and pathways.
Piispanhiippa can be used to map source entities (those defined by the source reference keys) to
different target entities using the links of interest. Output properties are read from these target
entities. For the simple queries we did not define any links and the source entities were used as
target entities. Link type 100 represents the transcription-translation relationship between a
gene and its proteins. We can use this link to fetch UniProt proteins (30) and the names of the
proteins encoded by our two example genes:
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
$ piispanhiippa -Rs BioentityName -Rt 30,BioentityName -l 100 AIP MUTYH
sourceKey  linkWeight  xref30  BioentityName
AIP        0           O00170  AIP_HUMAN
MUTYH      0           Q9UIF7  MUTYH_HUMAN
\end{Verbatim}

Links between the bioentities are directed and the natural direction is followed by default.
You can make reverse (target$\to$source) queries using |-rev| argument. The next query will
give us all phosphorylating (400) and methylating (440) genes for \gene{TP53}.
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
$ piispanhiippa -Rs BioentityName TP53 -l 400,440 -rev
sourceKey  linkTypeId  linkWeight  BioentityId  BioentityName
TP53       400         0           31433        CHEK2
TP53       400         0           33340        MAPK10
TP53       400         0           37154        MAPK8
TP53       400         0           38726        MAPK12
TP53       400         0           38786        MAPK11
TP53       400         0           41081        MAPK14
TP53       400         0           45578        ATM
TP53       400         0           47704        CHEK1
TP53       400         0           49556        ATR
\end{Verbatim}
It is also possible to declare ranges of link types such as 200-210,300-310,440.

All organism specific values of the source reference type (|-Rs|) can be listed using the
|-L| parameter. The following example shows how to list all names of the bioentity types.
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
$ piispanhiippa -Rs BioentityTypeName -L
BioentityTypeName
drug
gene
pathway
protein
\end{Verbatim}

The input keys may contain some control characters like quotation marks or commas,
which are filtered out. The filtering can be by passed with the |-r| argument.
The filtering is especially useful when the input is fetched from the file that
uses quotations around the strings. Say, we have a file |mutations.csv|:
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
"gene"  "id"              "mutated"
"AHR"   "ENSG00000106546" true
"SRC"   "ENSG00000197122" false
"MUTYH" "ENSG00000132781" true
"AIP"   "ENSG00000110711" false
\end{Verbatim}
We can now fetch the Gene Ontology targets for these genes using the following pipe:
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
$ cut -f 2 mutations.csv | piispanhiippa -Rs 10 -l 800-810 -Rt 60,BioentityName
sourceKey linkTypeId linkWeight xref60 BioentityName
ENSG00000106546 800 0 GO:0051123 RNA polymerase II transcriptional preinitiation complex assembly
ENSG00000197122 800 0 GO:0010467 gene expression
ENSG00000197122 800 0 GO:0007154 cell communication
ENSG00000197122 800 0 GO:0050663 cytokine secretion
ENSG00000197122 800 0 GO:0014909 smooth muscle cell migration
ENSG00000197122 800 0 GO:0006006 glucose metabolic process
ENSG00000197122 800 0 GO:0033622 integrin activation
ENSG00000197122 800 0 GO:0007155 cell adhesion
ENSG00000197122 800 0 GO:0060070 canonical Wnt receptor signaling pathway
ENSG00000197122 800 0 GO:0008286 insulin receptor signaling pathway
ENSG00000197122 800 0 GO:0007243 intracellular protein kinase cascade
ENSG00000197122 800 0 GO:0006915 apoptosis
ENSG00000110711 800 0 GO:0006350 transcription
ENSG00000110711 810 0 GO:0004112 cyclic-nucleotide phosphodiesterase activity
\end{Verbatim}

\section{Anduril component}
\label{sec:piispanhiippaAnnot}
\glsadd{glos:piispanhiippaAnnot}

PiispanhiippaAnnotator is an Anduril~\cite{Ovaska2010} proxy for the actual
Piispanhiippa application. This component takes in a CSV file that contains
a column of query identifiers. The query itself is expressed in terms of
the parameters.

The following example demonstrates how to fetch relevant studies\footnote{Their identifiers,
names and whether they are providing supporting or contradictive evidence about the gene.}
for the given list of Ensembl gene identifiers:
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
geneStudies = PiispanhiippaAnnotator(
                sourceKeys = geneIds,
                connection = moksiskaanInit.connection,
                inputDB    = XrefType_Ensembl_gene,
                organism   = Organism_Homo_sapiens,
                targetDB   = "HitStudyId,HitStudyName,HitEvidence"
              )
\end{Verbatim}

You may omit the |sourceKeys| input in order to list all organism specific values of the
|inputDB|. For example, the previous query would list all Ensembl gene identifiers for
\textit{Homo sapiens} if |geneIds| happened to be null.

\part{Installation}

You can follow these instructions to set up a local Moksiskaan database and
working environment. These instructions are not sufficient for the development
of Moksiskaan distribution but they are intended for those maintaining and
customizing local installations.

\section{Requirements}
\label{sec:requirements}

Moksiskaan has many dependencies on other programs. Most of these dependencies
are implicit as Anduril~\cite{Ovaska2010} dependent on these programs and
Moksiskaan requires Anduril to run. The following list shows what you need in order to
run Moksiskaan. Version numbers are based on the version that has been used in
testing. Some other versions of these programs might work as well or even better
with Moksiskaan.
\begin{itemize}
 \item Anduril         1.2.22  (\url{http://www.anduril.org/})
 \item Apache Ant      1.9.2   (\url{http://ant.apache.org/}) with an FTP support
 \item Hibernate       4.3.6   (\url{https://www.hibernate.org/})
 \item PostgreSQL      9.1.13  (\url{http://www.postgresql.org/})
\end{itemize}

\section{Setup}

This section will explain how to install the Moksiskaan system to a new computer and
how to maintain that installation. We assume that you have already
installed all the required programs that were mentioned in Section~\ref{sec:requirements}.

The very first step in your installation is to download the latest Moksiskaan
installation archive (\url{http://csbi.ltdk.helsinki.fi/moksiskaan/moksiskaan.zip}).
You may want to extract this archive into
a folder that is accessible to all users that are going to work with your database.
In UNIX |/opt/moksiskaan| is a good candidate for that. The archive does not include
a base folder so you will have to make that by yourself. We will use \baseDir{}
notation to refer to this directory of your choice.

Some of the Moksiskaan tasks are using Hibernate together with a C3P0 connection pool,
which requires some addition Java permissions for its JNDI registration. In most systems,
the additional grants can be assigned by editing the |java.policy| file in use. The following
line shall be added for the code base you are using:
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
permission javax.management.MBeanTrustPermission "register";
\end{Verbatim}

\subsection{Preparing PostgreSQL}
\label{sec:PostgreSetup}

Make sure that you have a PostgreSQL database up and running. There are several
options to create new users for that database depending on your platform and
your configurations. What you need is a database account that is dedicated to
Moksiskaan. The following SQL commands\footnote{The SQL clients are and their
use differs between plantforms. In Ubuntu Linux you can usually start SQL
prompt as follows: \texttt{sudo -u postgres psql}.} can be used to create this database.

\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
CREATE USER moksiskaan PASSWORD 'moksiskaan';
CREATE DATABASE moksiskaan WITH OWNER=moksiskaan encoding='UTF-8'
       LC_CTYPE = 'en_US.utf8' LC_COLLATE = 'en_US.utf8' TEMPLATE template0;
\end{Verbatim}

You may select the username and the password as you will but make sure that
you have updated \baseDir{\D{}db\D{}etc\D{}hibernate.properties} accordingly. The relevant
setting are:

\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
## PostgreSQL
hibernate.dialect org.hibernate.dialect.PostgreSQLDialect
hibernate.connection.driver_class org.postgresql.Driver
hibernate.connection.url jdbc:postgresql:moksiskaan
hibernate.connection.username moksiskaan
hibernate.connection.password moksiskaan
\end{Verbatim}

\subsection{Activating the environment}

The actual installation of Moksiskaan can begin once the whole environment has
been set up properly. Installation is based on an Apache Ant script that can be
found from \baseDir{/db/build.xml}. You can list the tasks available by typing:

\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
ant -p
\end{Verbatim}

The most important task for now is \textit{install}, which will carry out the whole
setup. This task will create all necessary source code, compile it and execute
some tests against it. A complete database schema will be installed into the
database and the key tables will be populated using the included data
(\baseDir{/db/data}), Ensembl~\cite{Flicek2011}, KEGG~\cite{Kanehisa2011},
Pathway Commons~\cite{Cerami2010}, DrugBank~\cite{Wishart2008,Knox2011},
GO~\cite{Ashburner2000}, and PINA~\cite{Cowley2012} databases.

You can have local exceptions to the defaults of the installation script.
The script code (\baseDir{/db/build.xml}) defines many Ant properties that are used
to determine source and target locations of the files in use.
Values of these properties can be overriden by defining them in |build.properties|.
Some of the data importing steps are organism specific and their execution is also
controlled within this file by enabling/disabling guardian flags.
Declaration of Hibernate installation folder is a typical customisation like this.
Default folder for the Hibernate is |/opt/hibernate|.
You can also use symbolic links instead of modifying the installation settings.

Some parts of the installation are carried out by the initialization pipeline,
which is explained in Section~\ref{sec:InitAnd}. Configuration of this pipeline
are in \baseDir{/pipeline/Settings.and}. For instance, commercial users may disable
some of the academic resources my setting |moksiskaanAcademic| to false.

Ensembl database is used to fetch gene and protein information for the genomes.
The data is fetched using a direct JDBC connection to the database. You may configure
your favourite mirror or use the official site by modifying the JDBC properties accordingly.
Human settings are in \baseDir{/pipeline/annotationdb.properties} and the other species
can be found from \baseDir{/db/pipeline/etc/ensembl*.properties}.

You can start the installation once it seems that you have made all your modifications
to it.
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
ant install
\end{Verbatim}
The installation task ends with the initialisation pipeline call, which may take
almost one day to complete. In case this pipeline fails,
\textbf{NO NOT START FROM THE BEGINNING} but go to \baseDir{/pipeline} and retry
with:
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
ant runInit -Dinit.clean=2
\end{Verbatim}
This approach continues from the failing steps and preserves all the previous results.

\subsection{Setting the paths correctly}

It is possible to have multiple instances of Moksiskaan within a single
computer. A special environment variable called |$MOKSISKAAN_HOME| is used to
define the instance of interest. This variable should always point to
\baseDir{/db}.

Moksiskaan installation provides a command line interface for its database.
This interface called Piispanhiippa is further discussed in Section~\ref{sec:piispanhiippa}
but here are instructions on how to make it accessible for the users.

Piispanhiippa itself is a Java application but \baseDir{/db/piispanhiippa} is a bash
script that can be used to launch it. You may launch the script explicitely from
your shell but it is convenient to have it in your |$PATH|. A simple solution is to
link this script to |/usr/bin|:
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
ln -s $MOKSISKAAN_HOME/piispanhiippa /usr/bin/piispanhiippa
\end{Verbatim}

\subsection{Initialisation pipeline}
\label{sec:InitAnd}

Moksiskaan installation provides an initialisation pipeline for Anduril. This
pipeline can be used:
\begin{enumerate}
 \item to test the new installation,
 \item to populate the database base with some real studies, cell lines, and enzyme annotations,
 \item to calculate basic statistics of the database content,
 \item to fetch the Gene Ontology structure, and
 \item to prepare the backgroung probabilities and the regulators of the Gene Ontology terms.
\end{enumerate}
Before executing this pipeline you may want to read Section~\ref{sec:AndurilEnv} about
the Anduril environment.

Initialisation pipeline (|Moksiskaan.and|) resides in \baseDir{/pipeline} together
with some other Anduril scripts. The other scripts are intended to demonstrate the use
of the framework. You can start the initialisation with the Ant script (|build.xml|):
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
ant runInit
\end{Verbatim}

The default configuration will populate the database with some public data but
you may also include your own dataset into this procedure. PrepareInput.and is
an Anduril script fragment that is used to import all studies into the database.
The fragment calls other project specific fragments that are by convention kept
under \baseDir{/pipeline/studies}. New studies can be incorporated by
creating an import script for them and then by including a call for that
script into |PrepareInput.and|. Unique identifiers of the studies are
listed in |Studies.and|.

\subsection{Upgrades}

The current version of Moksiskaan installation can be determined either by starting
Piispanhiippa (|piispanhiippa -help|) or by executing
\baseDir{/db/pipeline/lib/java/moksiskaan.jar} (|java -jar moksiskaan.jar|).
The later option is especially useful when comparing contents of various
installation folders. Moksiskaan.jar will always print its own version and the
path you use for the file will determine the installation.

New upgrades can be installed by replacing the content of \baseDir{} with
a new distribution archive and by following the installation steps from
Section~\ref{sec:PostgreSetup}. It may be wise to
\textcolor{red}{keep the previous version}
as a parallel copy so that you can copy your modified configurations easily.

If the old database schema is incompatible with the new one then you can first
drop the old database. Start your PostgreSQL client (for example |psql|) and
say:
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
DROP   DATABASE moksiskaan;
CREATE DATABASE moksiskaan WITH OWNER=moksiskaan encoding='UTF-8'
       LC_CTYPE = 'en_US.utf8' LC_COLLATE = 'en_US.utf8' TEMPLATE template0;

\end{Verbatim}

Bash listing~\ref{lst:update} shows an example where the current installation
is simply overriden with a new one. The script is executed on the \baseDir{}.
Make sure that you have set |ANDURIL_HOME|, |MOKSISKAAN_HOME| and 
|HIBERNATE_DIR| before calling it

{% Update example...
\lstset{
 language   =bash,
 numbers    =left,
 numberstyle=\footnotesize,
 frame      =shadowbox,
 caption    =An example update that replaces the current installation with the latest release,
 label      =lst:update}
\begin{lstlisting}
rm moksiskaan.zip
wget http://csbi.ltdk.helsinki.fi/moksiskaan/moksiskaan.zip
cp db/build.properties .
rm -R db pipeline userguide.pdf
unzip moksiskaan.zip
if [ ! $? = 0 ]; then
   echo "Could not open the new installation!"
   exit 1
fi
mv build.properties db/
cd db
ant install
if [ ! $? = 0 ]; then
   echo "Installation process failed!"
   exit 1
fi
cd ..
\end{lstlisting}
} % End of source code

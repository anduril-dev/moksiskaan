\part{Working with Anduril}

Moksiskaan has been developed especially for Anduril~\cite{Ovaska2010} users and many analysis tools are
available only for them.
This section explains how to utilise these tools and how to integrate Moksiskaan with your Anduril scripts.
Good example scripts can be found from \baseDir{/pipeline}. You can have a look on |ProcessStudy.and| and on
the related Ant startup script (|build.xml|) to see how to use Moksiskaan components in a working context.
Type |ant runProcessStudy| to launch this example and you are supposed to get your results to
|execProcessStudy| folder.

\section{Execution environment}
\label{sec:AndurilEnv}

A typical execution script for a Moksiskaan pipeline is something like:
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
#!/bin/bash
export PIPELINE=MyScript.and
export EXEC_DIR=exec
source $MOKSISKAAN_HOME/pipeline/execInit.sh
if [ ${1:-run} == clean ]; then
   anduril clean $PIPELINE -d $EXEC_DIR -b $MOKSISKAAN_HOME/pipeline
else
   anduril run   $PIPELINE -d $EXEC_DIR -b $MOKSISKAAN_HOME/pipeline "$@"
fi
\end{Verbatim}
This would start the analysis defined in |MyScript.and|. The script assumes that |$MOKSISKAAN_HOME|
points to \baseDir{/db} and |$HIBERNATE_DIR| has been set to point to your Hibernate installation
directory.

It is also possible to use Apache Ant to launch Anduril pipelines. You may find this option useful
if you are dealing with complex class paths or the Anduril analysis is part of a wider project
that is governed with Ant. Listing~\ref{lst:antbuild} shows how to run the already mentioned
|MyScript.and| in Ant.
{\newpage{}
 \lstset{language=XML,numbers=left,basicstyle=\scriptsize,numberstyle=\footnotesize,frame=shadowbox,
         label=lst:antbuild,
         caption=This Apache Ant script that can be used to launch a Moksiskaan pipeline.}
 \lstinputlisting{AntExample.xml}
}

\section{Anduril script configurations}
\label{sec:AndurilCfg}

This section describes how to invoke Moksiskaan resources from Anduril scripts. Direct database access
can be used for some components like PiispanhiippaAnnotator (see Section~\ref{sec:piispanhiippaAnnot})
and SQLSelect. These components can be used independently of the Hibernate framework but even they benefit from
having the environment set for the persistence layer.

The standard way of defining the database environment in Anduril scripts is to call MoksiskaanInit
function at the beginning of the Moksiskaan related part of the pipeline. MoksiskaanInit declares a
whole bunch of constants that can be used to refer to the database resources. The complete list of these
constants can be found from the component API. MoksiskaanInit converts its Hibernate configurations
into JDBC properties that can be used by the components using the database directly. It is recommended
to use these dynamic JDBC properties instead of hard-coded values as it provides a single point of maintenance
for the database configurations.

Database constants of the studies (Section~\ref{sec:Study}) are defined in \baseDir{/pipeline/Studies.and},
which can be included to the scripts. Before the |include| statement, you can define a Boolean called
|moksiskaanAcademic|, which determines whether you are interested in all academic studies (true) or
if you are a commercial user then you can exclude the forbidden ones (false).

Component descriptions for Moksiskaan components can be found from
\baseDir{\D{}db\D{}pipeline\D{}components\D{}report-descriptions}. Most BibTeX references are found from
Microarray bundle but |moksiskaan.bib| contains some project specific links.
You can refer to the references using the initialised environment:

\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
/** Moksiskaan related references */
bibtexMoksiskaan = INPUT(path=moksiskaanBundle+"/components/report-BibTeX/moksiskaan.bib")
\end{Verbatim}

Genome-wide a priori probabilities~\cite{Ovaska2008} for Gene Ontology~\cite{Ashburner2000} (GO)
terms have been calculated by the initialisation pipeline.
You can access these backgroung probabilities in your GO enrichment analysis
using the following declaration:
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
/** Moksiskaan specific a priori probabilities for Gene Ontology terms~\cite{Ovaska2008}. */
enrichmentTable = INPUT(path=moksiskaanInitOut+"/GOEnrichment_"+organismId+".csv")

/** Moksiskaan specific background similarities of Gene Ontology terms~\cite{Ovaska2008}. */
similarityTable = INPUT(path=moksiskaanInitOut+"/GOSimilarity_"+organismId+".csv")
\end{Verbatim}
The actual ontology of the GO terms (in OBO format) is also stored to the output folder.
The file is used by Kotelosienet to fetch the names of the GO term identifiers.
The same file can be used in AndurilScripts by defining the following INPUT:
\begin{Verbatim}[fontsize=\small,frame=single,baselinestretch=0.7,samepage=true]
/** Gene Ontology~\cite{Ashburner2000} */
go = INPUT(path=moksiskaanInitOut+"/goDB-in.obo")
\end{Verbatim}

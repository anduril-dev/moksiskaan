\part{Kotelosienet}
\glsadd{glos:kotelosienet}

Kotelosienet provides a graphical user interface that can be used to browse bioentities and their annotations.

\section{Startup}
\label{sec:kotelosienetStartup}

Kotelosienet is a Java application that can be used to make queries against Moksiskaan and
Ensembl~\cite{Flicek2011} databases. The application works in two modes. You may launch it directly from the
Moksiskaan web site or you may install it locally as a standalone application. The online version can be
found from the download tab of the web site once. Make sure that your browser supports Java Web Start and the
application can connect to the Ensembl MySQL.

The standalone version of Kotelosienet is especially useful when you have your own Moksiskaan database.
Moksiskaan installation provides a startup script
(\baseDir{/db/kotelosienet}), which can be called to start the application. Type |./kotelosienet -help|
to see all arguments of the program. By default, Gene Ontologies are loaded from the output folder of
the initialization pipeline and Korvasieni is configured for \textit{Homo sapiens} and \textit{Mus musculus}.
The available options for the species depends on what is available in the Moksiskaan database.
You may enter names of Korvasieni databases as arguments to Kotelosienet and they will appear selected
when the program starts.

\section{Browsing}
\label{sec:kotelosienetBrowsing}

\begin{figure}[htb]
    \begin{center}\includegraphics[width=0.95\linewidth]{Kotelosienet.png}\end{center}
    \caption{Kotelosienet is a visual browser for the bioentities.
             This figure shows how the tool describes the \textit{ITPA} gene.
             The orange bars on the bottom field represent the other bioentities
             near by on the same chromosome.}
    \label{fig:kotelosienet}
\end{figure}

The main view of Kotelosienet is shown in Figure~\ref{fig:kotelosienet}.
The view consists of three major elements that have been shown side by side.
Left panel provides a 'basket' where the user may collect shortcuts for his or her favourite bioentities.
The currently selected bioentity is described in the middle pane
whereas the related entities are listed on the right side.
New bioentities can be opened using the search tool (Find by) that is provided on top of the panels.

Typically user starts by opening a bioentity using the search tool.
This can be done by entering its identifier to the 'Find by' field and by pressing enter.
Before doing this one has to confirm that the type of the search key is correctly chosen.
The available types are dependent on the selected Korvasieni annotations, which are
explained later in this section.

The selected bioentities are shown in the middle panel, which is also used for other messages and for the help texts.
A list of bioentity links is shown if the selection consists of more than one bioentity.
Otherwise, the bioentity is described together with the selected annotations and the
related upstream and downstream neighbors are listed on the corresponding tables.
Name, bioentity type, and the link type are shown for each neighbor.
Corresponding link weights for these relationships can be seen as tool tips of the link column.

An interesting neighbor can be opened for further exploration simply by double-clicking on in.
This will replace the current bioentity with the selected one and one may proceed similarly with it.
The previous bioentity can be recovered using the back button ($<$), which restores the
previous view without loading any data. This is quick but for example the genome viewer does not
change its content. Actual data can be reloaded by clicking the identifier of the bioentity. This
link can be found from the first line of each description.

Annotations of each neighbor link can be shown by clicking the second mouse button on the
corresponding link. The pop-up dialog shows the kind of the link and the associated weight score.
The optional annotations are listed in |name = value| format. You may use this information to
track the original source of evidence supporting the link.

The list of favourite bioentities provides an alternative to the history based navigation.
Each opened bioentity is added to this list if 'File' $\rightarrow$ 'Pick selected' has been
set on. In addition, currently active bioentity can be added by clicking the basket icon
on the first row of the bioentity description. The other two buttons are used to remove list items
(the selected one or all). Bioentities of the list are readily accessible and they can be
opened simply by double-clicking the item of interest. An alternative way of opening files is
to define them during the startup. See |./kotelosienet -help| and the Moksiskaan web site (download
tab) for further instructions.

The list of favourites can be populated using file imports, which enables convenient browsing
of long sets of bioentities. The accepted file format is a tab delimited text file (Anduril CSV)
that provides a column of suitable identifiers. The exact type of the identifiers can be
provided on the file selection dialog ('File' $\rightarrow$ 'Import bioentities'), which
has a selector similar to the bioentity search. The set of supported types depends on the
selected Korvasieni databases that are explained later. Column selection is confirmed during the
actual importing if the file contains more than one columns.

\begin{figure}[hbt]
    \begin{center}\includegraphics[width=0.5\linewidth]{KsKorvaSelect.png}\end{center}
    \caption{The active set of annotations can be changed from the dialog that provides a list of
             database types available for Korvasieni.}
    \label{fig:ksKorvaSelect}
\end{figure}

Ensembl provides a rich source of external identifiers that can be used to find and
annotate bioentities. Figure~\ref{fig:ksKorvaSelect} shows the dialog that is found from
'View' $\rightarrow$ 'Korvasieni databases' menu. The selected annotations are fetched for
each gene and the results are shown as found. All selected annotations appear on the
type selector of the search tool and they may be used to query bioentities.
'ID' check box on the right side of the selector is used to tell whether the given search
keys refer to the actual identifiers (selected) or to their human readable counterparts (unselected).

The chromosomal neighbors of the bioentities can be found by selecting 'View' $\rightarrow$
'Show regions'. This selection opens up a simple genome browser below the main view
(see Figure~\ref{fig:kotelosienet}). Each bioentity is shown as a yellow bar and the overlapping
ones are put below each another. A red border highlights the current selection, which can
be moved by pressing the buttons on left (forward strand upstream) and right (downstream).
A click of a bioentity opens it for further inspection.

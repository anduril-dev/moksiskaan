\set organism 10090
--\set organism 4932

DELETE FROM "LinkEvidence" WHERE "studyId" IN
(SELECT "studyId" FROM "Study" WHERE "organismId" = :organism)
;

DELETE FROM "Hit" WHERE "studyId" IN
(SELECT "studyId" FROM "Study" WHERE "organismId" = :organism)
;

DELETE FROM "Study" WHERE "organismId" = :organism
;

DELETE FROM "DNARegion" WHERE "bioentityId" IN
(SELECT "bioentityId" FROM "Bioentity" WHERE "organismId" = :organism)
;

DELETE FROM "Xref" WHERE "bioentityId" IN
(SELECT "bioentityId" FROM "Bioentity" WHERE "organismId" = :organism)
;

DELETE FROM "LinkAnnotation" WHERE "linkId" IN
(SELECT "linkId" FROM "Link" WHERE "source" IN
 (SELECT "bioentityId" FROM "Bioentity" WHERE "organismId" = :organism)
)
;

DELETE FROM "LinkAnnotation" WHERE "linkId" IN
(SELECT "linkId" FROM "Link" WHERE "target" IN
 (SELECT "bioentityId" FROM "Bioentity" WHERE "organismId" = :organism)
)
;

DELETE FROM "Link" WHERE "source" IN
(SELECT "bioentityId" FROM "Bioentity" WHERE "organismId" = :organism)
;

DELETE FROM "Link" WHERE "target" IN
(SELECT "bioentityId" FROM "Bioentity" WHERE "organismId" = :organism)
;

DELETE FROM "Bioentity" WHERE "organismId" = :organism
;
